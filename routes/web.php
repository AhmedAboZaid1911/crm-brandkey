<?php
date_default_timezone_set('Africa/Cairo');

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

  $sitetitle = DB::table('settings')->select('value')->where('key','title_ar')->get();
  view()->share('titlepage',$sitetitle[0]->value);


  $sitelogo = DB::table('settings')->select('value')->where('key','logo')->get();
  view()->share('sitelogo',$sitelogo[0]->value);




  $lang_arr = array( "en" => "اللغة الانجليزية" );
  view()->share('lang_arr',$lang_arr);





Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/', 'HomeController@index');

// admin routes

define('ASSETS',url('resources/views/admin/assets'));//admin assets path
define('THEME_ASSETS',url('resources/assets'));//site template assets folder path
//Route::get('users/create','mycontroller@creat');
//Route::post('users/store','mycontroller@store');


//Admin
Route::get('admin','Admin@login');
Route::get('admin/login','Admin@login');
Route::post('admin/login','Admin@postlogin');
Route::group(['middleware'=>'admin','prefix'=>'admin'],function(){

    Route::get('/chat', 'admin\ChatsController@index');

    Route::get('/messages', 'admin\ChatsController@fetchMessages');
    Route::post('/messages', 'admin\ChatsController@sendMessage');

    Route::get('/index','Admin@index');
    Route::get('editprofile','admin\ProfileController@index');
    Route::post('updateprofile','admin\ProfileController@update');

    Route::get('members/index','admin\MembersController@index');
    Route::get('members/add','admin\MembersController@add');
    Route::post('members/store','admin\MembersController@store');
    Route::get('members/edit/{mid}','admin\MembersController@edit');
    Route::post('members/update','admin\MembersController@update');
    Route::get('members/del/{mid}','admin\MembersController@del');

    Route::get('Clients/index','admin\ClientsController@cats');
    Route::post('Clients/index','admin\ClientsController@cats');
    Route::get('Clients/search','admin\ClientsController@search');
    Route::get('Clients/searchuser','admin\ClientsController@searchuser');

    Route::get('Clients/searchbyuser','admin\ClientsController@searchbyuser');

    Route::post('Clients/Clients_tasks','admin\ClientsController@tasks');
    Route::get('Clients/Clients_add','admin\ClientsController@catadd');
    Route::post('Clients/Clients_store','admin\ClientsController@catstore');
    Route::get('Clients/Clients_edit/{catid}','admin\ClientsController@catedit');
    Route::get('Clients/Clients_view/{catid}','admin\ClientsController@view');
    Route::post('Clients/Clients_update','admin\ClientsController@catupdate');
    Route::post('Clients/Clients_comment','admin\ClientsController@comment');
    Route::post('Clients/Clients_Needs','admin\ClientsController@Needs');
    Route::get('Clients/Clients_Needs_delete/{id}/{cid}','admin\ClientsController@deleteNeeds');
    Route::get('Clients/Clients_del/{catid}','admin\ClientsController@catdel');

    Route::post('ClientsVsServices/Clients_comment','admin\ClientvsServicesController@comment');
    Route::post('ClientsVsServices/Clients_tasks','admin\ClientvsServicesController@tasks');
    Route::post('ClientsVsServices/Clients_tasks_update','admin\ClientvsServicesController@taskupdate');
    Route::get('ClientsVsServices/index/{catid}','admin\ClientvsServicesController@catadd');
    Route::get('ClientsVsServices/service/{serid}','admin\ClientvsServicesController@services');
    Route::get('ClientsVsServices/Clients_add','admin\ClientvsServicesController@catadd');
    Route::post('ClientsVsServices/Clients_store','admin\ClientvsServicesController@catstore');
    Route::get('ClientsVsServices/Clients_edit/{catid}','admin\ClientvsServicesController@catedit');
    Route::post('ClientsVsServices/Clients_update','admin\ClientvsServicesController@catupdate');

    Route::get('ClientsVsServices/Clients_del/{serid}/{cid}','admin\ClientvsServicesController@catdel');

    Route::get('Services/index','admin\ServicesController@cats');
    Route::post('Services/search','admin\ServicesController@search');
    Route::get('Services/Services_add','admin\ServicesController@catadd');
    Route::post('Services/Services_store','admin\ServicesController@catstore');
    Route::get('Services/Services_edit/{catid}','admin\ServicesController@catedit');
    Route::post('Services/Services_update','admin\ServicesController@catupdate');
    Route::get('Services/Services_del/{catid}','admin\ServicesController@catdel');
    Route::get('Services/view/{sc}','admin\ServicesController@serviceswithclients');


    Route::get('Services_name/index','admin\ServicesController@name');
    Route::get('Services/Services_name_add','admin\ServicesController@nameadd');
    Route::post('Services/Services_name_store','admin\ServicesController@namestore');
    Route::get('Services/Services_name_edit/{catid}','admin\ServicesController@nameedit');
    Route::post('Services/Services_name_update','admin\ServicesController@nameupdate');
    Route::get('Services/Services_name_del/{catid}','admin\ServicesController@namedel');

    Route::get('Services_time/index','admin\ServicesController@time');
    Route::get('Services/Services_time_add','admin\ServicesController@timeadd');
    Route::post('Services/Services_time_store','admin\ServicesController@timestore');
    Route::get('Services/Services_time_edit/{catid}','admin\ServicesController@timeedit');
    Route::post('Services/Services_time_update','admin\ServicesController@timeupdate');
    Route::get('Services/Services_time_del/{catid}','admin\ServicesController@timedel');

    Route::get('Services_period/index','admin\ServicesController@period');
    Route::get('Services/Services_period_add','admin\ServicesController@periodadd');
    Route::post('Services/Services_period_store','admin\ServicesController@periodstore');
    Route::get('Services/Services_period_edit/{catid}','admin\ServicesController@periodedit');
    Route::post('Services/Services_period_update','admin\ServicesController@periodupdate');
    Route::get('Services/Services_period_del/{catid}','admin\ServicesController@perioddel');

    Route::get('Services_days/index','admin\ServicesController@days');
    Route::get('Services/Services_days_add','admin\ServicesController@daysadd');
    Route::post('Services/Services_days_store','admin\ServicesController@daysstore');
    Route::get('Services/Services_days_edit/{catid}','admin\ServicesController@daysedit');
    Route::post('Services/Services_days_update','admin\ServicesController@daysupdate');
    Route::get('Services/Services_days_del/{catid}','admin\ServicesController@daysdel');

    Route::get('Services_place/index','admin\ServicesController@place');
    Route::get('Services/Services_place_add','admin\ServicesController@placeadd');
    Route::post('Services/Services_place_store','admin\ServicesController@placestore');
    Route::get('Services/Services_place_edit/{catid}','admin\ServicesController@placeedit');
    Route::post('Services/Services_place_update','admin\ServicesController@placeupdate');
    Route::get('Services/Services_place_del/{catid}','admin\ServicesController@placedel');

    Route::get('Services_currency/index','admin\ServicesController@currency');
    Route::get('Services/Services_currency_add','admin\ServicesController@currencyadd');
    Route::post('Services/Services_currency_store','admin\ServicesController@currencystore');
    Route::get('Services/Services_currency_edit/{catid}','admin\ServicesController@currencyedit');
    Route::post('Services/Services_currency_update','admin\ServicesController@currencyupdate');
    Route::get('Services/Services_currency_del/{catid}','admin\ServicesController@currencydel');

    Route::get('nationality/index','admin\NationalityController@index');
    Route::get('nationality/add','admin\NationalityController@add');
    Route::post('nationality/store','admin\NationalityController@store');
    Route::get('nationality/edit/{pageid}','admin\NationalityController@edit');
    Route::post('nationality/update','admin\NationalityController@update');
    Route::get('nationality/del/{pageid}','admin\NationalityController@del');

    Route::get('leads/index','admin\leadsController@index');
    Route::get('leads/leads_add','admin\leadsController@leadadd');
    Route::post('leads/leads_store','admin\leadsController@leadstore');
    Route::get('leads/edit/{catid}','admin\leadsController@edit');
    Route::post('leads/update','admin\leadsController@update');
    Route::get('leads/leads_del/{catid}','admin\leadsController@del');

    Route::get('Needs/index','admin\NeedsController@index');
    Route::get('Needs/Needs_add','admin\NeedsController@Needadd');
    Route::post('Needs/Needs_store','admin\NeedsController@Needstore');
    Route::get('Needs/edit/{catid}','admin\NeedsController@edit');
    Route::post('Needs/update','admin\NeedsController@update');
    Route::get('Needs/Needs_del/{catid}','admin\NeedsController@del');

    Route::get('interest/index','admin\interestController@index');
    Route::get('interest/add','admin\interestController@add');
    Route::post('interest/store','admin\interestController@store');
    Route::get('interest/edit/{catid}','admin\interestController@edit');
    Route::post('interest/update','admin\interestController@update');
    Route::get('interest/del/{catid}','admin\interestController@del');

    Route::get('gender/index','admin\genderController@index');
    Route::get('gender/add','admin\genderController@add');
    Route::post('gender/store','admin\genderController@store');
    Route::get('gender/edit/{pageid}','admin\genderController@edit');
    Route::post('gender/update','admin\genderController@update');
    Route::get('gender/del/{catid}','admin\genderController@del');

    Route::get('service_type/index'                 ,'admin\ServiceTypeController@index');
    Route::get('service_type/add'                   ,'admin\ServiceTypeController@add');
    Route::post('service_type/store'                ,'admin\ServiceTypeController@store');
    Route::get('service_type/edit/{pageid}'         ,'admin\ServiceTypeController@edit');
    Route::post('service_type/update'               ,'admin\ServiceTypeController@update');
    Route::get('service_type/del/{catid}'           ,'admin\ServiceTypeController@del');

    Route::get('wants/index'                        ,'admin\WantsController@index');
    Route::get('wants/add'                          ,'admin\WantsController@add');
    Route::post('wants/store'                       ,'admin\WantsController@store');
    Route::get('wants/edit/{pageid}'                ,'admin\WantsController@edit');
    Route::post('wants/update'                      ,'admin\WantsController@update');
    Route::get('wants/del/{catid}'                  ,'admin\WantsController@del');

    Route::get('interested_type/index'              ,'admin\InterestedTypeController@index');
    Route::get('interested_type/add'                ,'admin\InterestedTypeController@add');
    Route::post('interested_type/store'             ,'admin\InterestedTypeController@store');
    Route::get('interested_type/edit/{pageid}'      ,'admin\InterestedTypeController@edit');
    Route::post('interested_type/update'            ,'admin\InterestedTypeController@update');
    Route::get('interested_type/del/{catid}'        ,'admin\InterestedTypeController@del');

    Route::get('not_interested/index'               ,'admin\NotInterestedController@index');
    Route::get('not_interested/add'                 ,'admin\NotInterestedController@add');
    Route::post('not_interested/store'              ,'admin\NotInterestedController@store');
    Route::get('not_interested/edit/{pageid}'       ,'admin\NotInterestedController@edit');
    Route::post('not_interested/update'             ,'admin\NotInterestedController@update');
    Route::get('not_interested/del/{catid}'         ,'admin\NotInterestedController@del');

    Route::get('call_status/index'                  ,'admin\CallStatusController@index');
    Route::get('call_status/add'                    ,'admin\CallStatusController@add');
    Route::post('call_status/store'                 ,'admin\CallStatusController@store');
    Route::get('call_status/edit/{pageid}'          ,'admin\CallStatusController@edit');
    Route::post('call_status/update'                ,'admin\CallStatusController@update');
    Route::get('call_status/del/{catid}'            ,'admin\CallStatusController@del');

    Route::get('tags/index'                         ,'admin\TagsController@index');
    Route::get('tags/add'                           ,'admin\TagsController@add');
    Route::post('tags/store'                        ,'admin\TagsController@store');
    Route::get('tags/edit/{pageid}'                 ,'admin\TagsController@edit');
    Route::post('tags/update'                       ,'admin\TagsController@update');
    Route::get('tags/del/{catid}'                   ,'admin\TagsController@del');

    Route::get('tasks/today','admin\TasksController@today');
    Route::get('tasks/late','admin\TasksController@late');
    Route::get('tasks/done','admin\TasksController@done');
    Route::get('tasks/all','admin\TasksController@all');
    Route::get('tasks/view/{pageid}','admin\TasksController@edit');

    Route::post('tasks/search','admin\TasksController@search');
    Route::get('tasks/add','admin\TasksController@add');
    Route::post('tasks/store','admin\TasksController@store');
    Route::get('tasks/edit/{pageid}','admin\TasksController@edit');
    Route::post('tasks/update','admin\TasksController@update');
    Route::get('tasks/del/{catid}','admin\TasksController@del');


    Route::get('reports/view/{pageid}','admin\ReportsController@view');

    Route::get('reports','admin\ReportsController@reports');
    Route::get('reports/service','admin\ReportsController@service_index');

    Route::get('reports/service/service','admin\ReportsController@service_report');
    Route::post('reports/service/service/search','admin\ReportsController@service_report_search');
    Route::post('reports/service/service/excel','admin\ReportsController@service_report_excel')->name('reports_service_search.excel');

    Route::get('reports/service/clientvsservice','admin\ReportsController@clientvsservice_report');
    Route::post('reports/service/clientvsservice/search','admin\ReportsController@clientvsservice_report_search');
    Route::post('reports/service/clientvsservice/excel','admin\ReportsController@clientvsservice_report_excel')->name('reports_clientvsservice_search.excel');

    Route::get('reports/service/cancelledservices','admin\ReportsController@cancelledservice_report');
    Route::post('reports/service/cancelledservices/search','admin\ReportsController@cancelledservice_report_search');
    Route::post('reports/service/cancelledservices/excel','admin\ReportsController@cancelledservice_report_excel')->name('reports_cancelledservices_search.excel');

    Route::get('reports/service/servicevsservices','admin\ReportsController@servicevsservices_report');
    Route::post('reports/service/servicevsservices/search','admin\ReportsController@servicevsservices_report_search');
    Route::post('reports/service/servicevsservices/excel','admin\ReportsController@servicevsservices_report_excel')->name('reports_servicevsservices_search.excel');

    Route::get('reports/client','admin\ReportsController@clients_report');
    Route::post('reports/client/search','admin\ReportsController@clients_report_search');
    Route::post('reports/client/excel','admin\ReportsController@clients_report_excel')->name('reports_client_search.excel');
    // Route::post('reports/client/import','admin\ReportsController@clients_report_import');

    Route::get('reports/clientneeds','admin\ReportsController@clientneeds_report');
    Route::post('reports/clientneeds/search','admin\ReportsController@clientneeds_report_search');
    Route::post('reports/clientneeds/excel','admin\ReportsController@clientneeds_report_excel')->name('reports_clientneeds_search.excel');

    Route::get('reports/task','admin\ReportsController@tasks_report');
    Route::post('reports/task/search','admin\ReportsController@tasks_report_search');
    Route::post('reports/task/excel','admin\ReportsController@tasks_report_excel')->name('reports_tasks_search.excel');

    Route::get('reports/users','admin\ReportsController@users_report');
    Route::post('reports/users/search','admin\ReportsController@users_report_search');
    Route::post('reports/users/excel','admin\ReportsController@users_report_excel')->name('reports_users_search.excel');


    Route::post('reports/search','admin\ReportsController@service');

    Route::get('members/index','admin\MembersController@index');
    Route::get('members/add','admin\MembersController@add');
    Route::post('members/store','admin\MembersController@store');
    Route::get('members/edit/{mid}','admin\MembersController@edit');
    Route::post('members/update','admin\MembersController@update');
    Route::post('members/roles','admin\MembersController@rolesupdate');
    Route::get('members/del/{mid}','admin\MembersController@del');

    Route::get('convert/clients','admin\ConverterController@clients');
    Route::get('convert/clients/1','admin\ConverterController@clients1');
    Route::get('convert/oldclientvsservices','admin\ConverterController@oldclientvsservices');
    Route::get('convert/clientvsservices','admin\ConverterController@clientvsservices');
    Route::get('convert/clientcomments','admin\ConverterController@clientcomments');
    Route::get('convert/tasks','admin\ConverterController@tasks');
    Route::get('convert/oldclientsno','admin\ConverterController@oldclientsno');
    Route::get('convert/period','admin\ConverterController@servicetime');
    Route::get('convert/clientvsneeds','admin\ConverterController@clientvsneeds');
    Route::get('convert/servicesdate','admin\ConverterController@dateservices');

    Route::get('trips/posts','admin\TripsController@posts');
    Route::get('trips/post_add','admin\TripsController@postadd');
    Route::post('trips/post_store','admin\TripsController@poststore');
    Route::get('trips/post_edit/{postid}','admin\TripsController@postedit');
    Route::post('trips/post_update','admin\TripsController@postupdate');
    Route::get('trips/post_del/{postid}','admin\TripsController@postdel');

    Route::get('video/cats','admin\VideoController@cats');
    Route::get('video/cat_add','admin\VideoController@catadd');
    Route::post('video/cat_store','admin\VideoController@catstore');
    Route::get('video/cat_edit/{catid}','admin\VideoController@catedit');
    Route::post('video/cat_update','admin\VideoController@catupdate');
    Route::get('video/cat_del/{catid}','admin\VideoController@catdel');

    Route::get('video/posts','admin\VideoController@posts');
    Route::get('video/post_add','admin\VideoController@postadd');
    Route::post('video/post_store','admin\VideoController@poststore');
    Route::get('video/post_edit/{postid}','admin\VideoController@postedit');
    Route::post('video/post_update','admin\VideoController@postupdate');
    Route::get('video/post_del/{postid}','admin\VideoController@postdel');

    Route::get('album/cats','admin\AlbumController@cats');
    Route::get('album/cat_add','admin\AlbumController@catadd');
    Route::post('album/cat_store','admin\AlbumController@catstore');
    Route::get('album/cat_edit/{catid}','admin\AlbumController@catedit');
    Route::post('album/cat_update','admin\AlbumController@catupdate');
    Route::get('album/cat_del/{catid}','admin\AlbumController@catdel');

    Route::get('album/posts','admin\AlbumController@posts');
    Route::get('album/post_add','admin\AlbumController@postadd');
    Route::post('album/post_store','admin\AlbumController@poststore');
    Route::get('album/post_edit/{postid}','admin\AlbumController@postedit');
    Route::post('album/post_update','admin\AlbumController@postupdate');
    Route::get('album/post_del/{postid}','admin\AlbumController@postdel');

    Route::get('multialbum/cats','admin\MultiAlbumController@cats');
    Route::get('multialbum/cat_add','admin\MultiAlbumController@catadd');
    Route::post('multialbum/cat_store','admin\MultiAlbumController@catstore');
    Route::get('multialbum/cat_edit/{catid}','admin\MultiAlbumController@catedit');
    Route::post('multialbum/cat_update','admin\MultiAlbumController@catupdate');
    Route::get('multialbum/cat_del/{catid}','admin\MultiAlbumController@catdel');

    Route::get('multialbum/posts','admin\MultiAlbumController@posts');
    Route::get('multialbum/post_add','admin\MultiAlbumController@postadd');
    Route::post('multialbum/post_store','admin\MultiAlbumController@poststore');
    Route::get('multialbum/post_edit/{postid}','admin\MultiAlbumController@postedit');
    Route::post('multialbum/post_update','admin\MultiAlbumController@postupdate');
    Route::get('multialbum/post_del/{postid}','admin\MultiAlbumController@postdel');

    Route::get('catAjax','admin\BlogController@getsubcat');
    Route::get('subcat','admin\BlogController@subcatajax');

    Route::get('slider/index','admin\SliderController@index');
    Route::get('slider/add','admin\SliderController@add');
    Route::post('slider/store','admin\SliderController@store');
    Route::get('slider/edit/{postid}','admin\SliderController@edit');
    Route::post('slider/update','admin\SliderController@update');
    Route::get('slider/del/{postid}','admin\SliderController@del');

    Route::get('social/index','admin\SocialController@index');
    Route::get('social/add','admin\SocialController@add');
    Route::post('social/store','admin\SocialController@store');
    Route::get('social/edit/{postid}','admin\SocialController@edit');
    Route::post('social/update','admin\SocialController@update');
    Route::get('social/del/{postid}','admin\SocialController@del');

    Route::get('page/index','admin\PageController@index');
    Route::get('page/add','admin\PageController@add');
    Route::post('page/store','admin\PageController@store');
    Route::get('page/edit/{pageid}','admin\PageController@edit');
    Route::post('page/update','admin\PageController@update');
    Route::get('page/del/{pageid}','admin\PageController@del');

    Route::get('trips/forms/index','admin\FormsController@index');
    Route::get('trips/forms/view/{pageid}','admin\FormsController@view');
    Route::get('trips/forms/del/{pageid}','admin\FormsController@del');

    Route::get('customize/index','admin\CustomizeFormsController@index');
    Route::get('customize/view/{pageid}','admin\CustomizeFormsController@view');
    Route::get('customize/del/{pageid}','admin\CustomizeFormsController@del');

    Route::get('contact/index','admin\ContactFormsController@index');
    Route::get('contact/view/{pageid}','admin\ContactFormsController@view');
    Route::get('contact/del/{pageid}','admin\ContactFormsController@del');

    Route::get('reviews/index','admin\ReviewController@index');
    Route::get('reviews/view/{pageid}','admin\ReviewController@view');
    Route::post('reviews/post_update','admin\ReviewController@reviewupdate');
    Route::get('reviews/del/{pageid}','admin\ReviewController@del');

    Route::get('destination/index','admin\DestinationController@index');
    Route::get('destination/add','admin\DestinationController@add');
    Route::post('destination/store','admin\DestinationController@store');
    Route::get('destination/edit/{pageid}','admin\DestinationController@edit');
    Route::post('destination/update','admin\DestinationController@update');
    Route::get('destination/del/{pageid}','admin\DestinationController@del');



    Route::get('allpagesinfo','admin\PagesInfoController@showallpagesinfo');
    Route::get('editpageinfo/{id}','admin\PagesInfoController@editpageinfo');
    Route::post('updatepageinfo','admin\PagesInfoController@updatepageinfo');

    Route::get('newsletter/index','admin\NewsletterController@index');

    Route::get('ad/index','admin\AdController@index');
    Route::get('ad/add','admin\AdController@add');
    Route::post('ad/store','admin\AdController@store');
    Route::get('ad/edit/{id}','admin\AdController@edit');
    Route::post('ad/update','admin\AdController@update');
    Route::get('ad/del/{id}','admin\AdController@del');

    Route::get('editsettings','admin\SettingsController@editsettings');
    Route::post('updatesettings','admin\SettingsController@updatesettings');


});

Route::get('admin/logout','Admin@logout');
Route::get('admin/settings','Settings@index');
Route::patch('admin/settings','Settings@update');



//User

Route::get('userpanel/login','Userpanel@login');
Route::post('userpanel/login','Userpanel@postlogin');
Route::group(['middleware'=>'userpanel'],function(){

Route::get('userpanel','Userpanel@index');
Route::get('userpanel/index','Userpanel@index');
Route::get('userpanel/editprofile','userpanel\ProfileController@index');
Route::post('userpanel/updateprofile','userpanel\ProfileController@update');

Route::get('userpanel/product/index','userpanel\ProductController@index');
Route::get('userpanel/product/add','userpanel\ProductController@add');
Route::post('userpanel/product/store','userpanel\ProductController@store');
Route::get('userpanel/product/edit/{postid}','userpanel\ProductController@edit');
Route::post('userpanel/product/update','userpanel\ProductController@update');
Route::get('userpanel/product/del/{postid}','userpanel\ProductController@del');

});

Route::get('userpanel/logout','Userpanel@logout');


//Site
Route::get('/','Admin@login');

