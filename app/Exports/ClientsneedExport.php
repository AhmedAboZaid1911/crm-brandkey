<?php

namespace App\Exports;

use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;


class ClientsneedExport implements FromView
{
    public $data;

    public function __construct($data){
        $this->data = $data;
    }

    public function view(): View
    {

        return view('admin.exports.Clientsneed', [
            'clientneeds' => $this->data
        ]);
    }
}
