<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Mail;

class Contact_usController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        if($_POST)
        {
            
            $data = array('name' => $request->name , 'email' => $request->email, 'msg' => $request->msg);
            $validator = Validator::make($data, [
            'name' => 'required',
            'email' => 'required',
            'msg' => 'required'
            ]);


            if ($validator->fails()) {
            
                return redirect(url('contact_us'))
                        ->withErrors($validator)
                        ->withInput();
            }
            
            
            
            $siteemail = DB::table('settings')->select('value')->where('key','email')->get();
  
            // $mail_to = $siteemail[0]->value;
            // $subject = 'Message from '. $request->name;
            // $body_message = 'From: '. $request->name."\n";
            // $body_message .= 'E-mail: ' . $request->email . "\n";
            // $body_message .= 'Message: ' . $request->msg; 
// 
            // $headers = 'From: '.$request->email."\r\n";
            // $headers .= 'Reply-To: '.$request->email."\r\n";
// 
            // mail($mail_to, $subject, $body_message, $headers);
            
            Mail::send('email',
       array(
           'name' => $request->name,
           'email' => $request->email,
           'msg' => $request->msg
       ), function($message)
   {
       $message->from($mail_to);
       $message->to($request->email, $request->name)->subject('Contact Us');
   });
   
       return back()->with('success', 'Thanks for contacting us!');
       
        }
            return view('contact_us');
    }
      public function page($id)
    {
        
          $page = DB::table('pages')->where('url_ar',$id)->get();
          return view('page',compact('page'));
    }
    
    
      public function cat($cat)
    {
        
          $cat = DB::table('cat')->where('url_ar',$cat)->get();
          
          $posts = DB::table('post')->where([['cid',$cat[0]->id],['active',1]])->paginate(6);
          return view('cat',compact('cat','posts'));
    }
    
    
      public function post($cat,$post)
    {
        
          $post = DB::table('post')->where([['url_ar',$post],['active',1]])->get();
          $cat = DB::table('cat')->where([['id',$post[0]->cid]])->get();
          $other_posts = DB::table('post')->where([['id','<>',$post[0]->id],['active',1],['cid',$cat[0]->id]])->orderBy('time', 'desc')->limit(3)->get();
          return view('post',compact('post','cat','other_posts'));
    }
    
}
