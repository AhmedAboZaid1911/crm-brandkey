<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class VideoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
	
    
    
    public function postlist(Request $request){
        $data = array('email' => $request->email);
            $validator = Validator::make($data, [
            'email' => 'unique:emails'
            ]);

            if ($validator->fails()) {
            
                return redirect(url('/index#list'))
                        ->withErrors($validator)
                        ->withInput(); 
            }
        DB::table('emails')->insert(['email' => $request->email]);
        return redirect('/thankyou');
    }   
    
    
	
    public function index($lang)
    {
        $pages = HomeController::getPage(); 
        $social = DB::table('social')->get();
        $settings = HomeController::getSettings();
        $about = HomeController::getAboutPage(); 
        $pagesinfo = HomeController::getPages();
        
        $video = DB::table('post')->where('type','video')->get();
        //dd($album);
        $trips = DB::table('cat')->where([['type','trips'],['cid','0']])->get();
                foreach($trips as $k=>$v)
                {
                    $tours = DB::table('cat')->where('cid',$v->id)->get();
                    $trips[$k]->tours = $tours;

                }
        return view('frontend.video.index',compact('pages','trips','lang','social','settings','about','pagesinfo','video','images','menu','meals'));
    }
    
    public function video($lang,$video)
    {
        $pages = HomeController::getPage(); 
        $social = DB::table('social')->get();
        $settings = HomeController::getSettings();
        $about = HomeController::getAboutPage(); 
        $pagesinfo = HomeController::getPages();
        
        $video = DB::table('post')->where([['type','video'],['url_en',$video]])->get();
        $trips = DB::table('cat')->where([['type','trips'],['cid','0']])->get();
                foreach($trips as $k=>$v)
                {
                    $tours = DB::table('cat')->where('cid',$v->id)->get();
                    $trips[$k]->tours = $tours;

                }
        return view('frontend.video.video',compact('pages','trips','lang','social','settings','about','pagesinfo','images','video','menu','meals'));
    }
   
}