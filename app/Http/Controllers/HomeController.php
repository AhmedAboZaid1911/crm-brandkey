<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
	public static function goadv($adid)
    {
        $visits = DB::table('googleads')->select('visits','adurl')->where('id',$adid)->get();
        DB::table('googleads')->where('id',$adid)->update(['visits' => $visits[0]->visits + 1]);
        return redirect($visits[0]->adurl);
    }
    
    public static function getAd($advid)
    {
        $views = DB::table('googleads')->select('views')->where('id',$advid)->get();
        
        DB::table('googleads')->where('id',$advid)->update(['views' => $views[0]->views + 1]);
        $ad = DB::table('googleads')->where('id',$advid)->get();
        $adhtml = "";
        if($ad[0]->adimg != null){
            $adhtml = "<a href='" . url('go/' . $advid) . "' target='_blank'><img class='img-responsive' src='" . url($ad[0]->adimg) . "' alt='" . $ad[0]->imgalt . "' /></a>";
        }
        if($ad[0]->adcode != null){
            $adhtml = $ad[0]->adcode;
        }
        return $adhtml;
    }
    
    public static function getSettings()
    {
        $settings = DB::table('settings')->get();
        $settingsarr = array();
            for ($i=0; $i < count($settings); $i++) { 
                $settingsarr[$settings[$i]->key] = $settings[$i]->value;
            }
        return $settingsarr;
    }
    public static function getAllSocial()
    {
        $allsocial = DB::table('social')->get();
        return $allsocial;
    }
    public function getHomeSlider()
    {
        $sliderarr = DB::table('homeslider')->get();
        return $sliderarr;
    }
    
    public static function getAboutPage()
    {
        $about = DB::table('pages')->where('type','=','general')->get();
        return $about;
    }
    
    public static function getPage()
    {
        $pages = DB::table('pages')->where([['type','general'],['id','!=','5']])->get();
        return $pages;
    }
    
    public static function getPages()
    {
        $page = DB::table('pages_info')->get();
        return $page;
    }

    public function postlist(Request $request){
        $data = array('email' => $request->email);
            $validator = Validator::make($data, [
            'email' => 'unique:emails'
            ]);

            if ($validator->fails()) {
            
                return redirect(url('/index#list'))
                        ->withErrors($validator)
                        ->withInput(); 
            }
        DB::table('emails')->insert(['email' => $request->email]);
        return redirect('/thankyou');
    } 
	
    public function index($lang="en")
    {
        
            $pages = HomeController::getPage(); 
            $slider = DB::table('slider')->get();
            $settings = HomeController::getSettings(); 
            $about = HomeController::getAboutPage(); 
            $social = HomeController::getAllSocial();
            $tripadvisor = DB::table('social')->select('socialurl')->where('id','10')->get();
            $pagesinfo = HomeController::getPages();
            $trips = DB::table('cat')->where([['type','trips'],['cid','0']])->get();
                foreach($trips as $k=>$v)
                {
                    $tours = DB::table('cat')->where('cid',$v->id)->get();
                    $trips[$k]->tours = $tours;

                }
                
            $related =  DB::table('post')->where('type','trips')->inRandomOrder()->limit(4)->get();
            foreach($related as $k=>$v)
            {
                $tours = DB::table('cat')->select('url_en','cid')->where([['type','trips'],['cid','!=','0'],['id',$v->cid]])->get();
                $related[$k]->tours = $tours[0]->url_en;    
                
                $maincattours = DB::table('cat')->select('url_en')->where([['type','trips'],['cid','0'],['id',$tours[0]->cid]])->get();
                $related[$k]->maincattours = $maincattours[0]->url_en;    
                    
                
            }
            $client = DB::table('images')->where([['type','multialbum'],['itemid','57']])->get();
            $clientname = DB::table('post')->where([['type','multialbum'],['id','57']])->get();
            $destinations = DB::table('pages')->where('type','destination')->get();

            
        return view('home',compact('tour','tripadvisor','pages','destinations','clientname','client','related','slider','pagesinfo','social','settings','about','lang','trips','tour'));
    }
      
    public function about($lang)
    {
        
        $pages = HomeController::getPage(); 
        $about = HomeController::getAboutPage();
        $settings = HomeController::getSettings();
        $social = HomeController::getAllSocial();
        $pagesinfo = HomeController::getPages();
        $trips = DB::table('cat')->where([['type','trips'],['cid','0']])->get();
                foreach($trips as $k=>$v)
                {
                    $tours = DB::table('cat')->where('cid',$v->id)->get();
                    $trips[$k]->tours = $tours;

                }
        
        return view('frontend.pages.about',compact('pages','trips','pagesinfo','social','settings','about','lang'));
    }
    
    public function pages($lang,$pg)
    {
        
        $pages = HomeController::getPage(); 
        $about = HomeController::getAboutPage();
        $settings = HomeController::getSettings();
        $social = HomeController::getAllSocial();
        $pagesinfo = HomeController::getPages();
        $trips = DB::table('cat')->where([['type','trips'],['cid','0']])->get();
                foreach($trips as $k=>$v)
                {
                    $tours = DB::table('cat')->where('cid',$v->id)->get();
                    $trips[$k]->tours = $tours;

                }
        $onepage = DB::table('pages')->where([['type','general'],['id','!=','5'],['url_en',$pg]])->get();
        
        return view('frontend.pages.pages',compact('onepage','pages','trips','pagesinfo','social','settings','about','lang'));
    }
    
    
    public function contactus($lang)
    {
        
        
        $pages = HomeController::getPage();
        $about = HomeController::getAboutPage(); 
        $settings = HomeController::getSettings();
        $social = HomeController::getAllSocial();
        $pagesinfo = HomeController::getPages();
        $trips = DB::table('cat')->where([['type','trips'],['cid','0']])->get();
                foreach($trips as $k=>$v)
                {
                    $tours = DB::table('cat')->where('cid',$v->id)->get();
                    $trips[$k]->tours = $tours;

                }

        
        return view('frontend.pages.contact_us',compact('pages','trips','pagesinfo','social','settings','about','lang'));
    }
    
    public function contactusform(Request $request)
    {
        $settings = HomeController::getSettings();    
        $insertname = $request->name;
        $insertemail = $request->email;
        $insertmobile = $request->mobile;
        
        $inserttripname = $request->tripname;
        
        $insertmsg = $request->msg;

        $inserttime = time();
        
        $inserttype = 'contact';

        DB::table('forms')->insert([
        'name' => $insertname,
        'email'=> $insertemail, 
        'mobile' => $insertmobile , 
        'tripname' => $inserttripname, 
        'msg' => $insertmsg, 
        'time' => $inserttime, 
        'type' => $inserttype ]);
        
        $mail_to = $settings['email'];
            
            $subject = 'Message from Website: '. $inserttripname;
            
            $body_message = 'From: '. $insertname."\n";
            $body_message .= 'E-mail: ' . $insertemail . "\n";
            $body_message .= 'Mobile: ' . $insertmobile . "\n";
            $body_message .= 'Message: ' . $insertmsg; 

            $headers = 'From: '.$insertemail."\r\n";
            $headers .= 'Reply-To: '.$insertemail."\r\n";

          $succ = mail($mail_to, $subject, $body_message, $headers);

        return redirect(url($request->lang . '/thankyou'));

    }


    public function customize($lang)
    {
        
        $pages = HomeController::getPage();
        $about = HomeController::getAboutPage(); 
        $settings = HomeController::getSettings();
        $social = HomeController::getAllSocial();
        $pagesinfo = HomeController::getPages();
        $nationality = DB::table('pages')->where('type','nationality')->get();
        $trips = DB::table('cat')->where([['type','trips'],['cid','0']])->get();
                foreach($trips as $k=>$v)
                {
                    $tours = DB::table('cat')->where('cid',$v->id)->get();
                    $trips[$k]->tours = $tours;

                }

        
        return view('frontend.pages.customize',compact('pages','nationality','trips','pagesinfo','social','settings','about','lang'));
    }
    
    
    public function customizeform(Request $request)
    {        
        $settings = HomeController::getSettings();    
        $insertname = $request->name;
        $insertemail = $request->email;
        $insertmobile = $request->mobile;
        $insertwhatsapp = $request->whatsapp;
        $insertnationality = $request->nationality;
        $insertfrom = $request->from;
        $insertto = $request->to;
        $insertadults = $request->adults;
        $insertchild = $request->child;
        $insertchildage = $request->childage;
        
        $inserttripname = $request->tripname;
        $insertmsg = $request->msg;

        $inserttime = time();
        
        $inserttype = 'customize';

        DB::table('forms')->insert([
        'name' => $insertname,
        'email'=> $insertemail, 
        'mobile' => $insertmobile , 
        'whatsapp' => $insertwhatsapp,
        'nationality' => $insertnationality,
        'from' => $insertfrom, 
        'to' => $insertto, 
        'adults' => $insertadults, 
        'child' => $insertchild, 
        'childage' => $insertchildage, 
        'tripname' => $inserttripname, 
        'msg' => $insertmsg, 
        'time' => $inserttime, 
        'type' => $inserttype ]);

        $mail_to = $settings['email'];
            
        $subject = 'Message from Website: '. $inserttripname;
        
        $body_message = 'From: '. $insertname."\n";
        $body_message .= 'E-mail: ' . $insertemail . "\n";
        $body_message .= 'Mobile: ' . $insertmobile . "\n";
        $body_message .= 'Whats app: ' . $insertwhatsapp. "\n"; 
        $body_message .= 'Nationality: ' . $insertnationality. "\n"; 
        $body_message .= 'From: ' . $insertfrom. "\n";
        $body_message .= 'To: ' . $insertto. "\n";
        $body_message .= 'No. of Adults: ' . $insertadults. "\n"; 
        $body_message .= 'No. of child: ' . $insertchild. "\n";
        $body_message .= 'Child Age: ' . $insertchildage. "\n";
        $body_message .= 'Message: ' . $insertmsg. "\n";

        $headers = 'From: '.$insertemail."\r\n";
        $headers .= 'Reply-To: '.$insertemail."\r\n";

        $succ = mail($mail_to, $subject, $body_message, $headers);

        return redirect(url($request->lang . '/thankyou'));

    }
    
    public function thankyou($lang)
    {
        
        $pages = HomeController::getPage();
        $about = HomeController::getAboutPage(); 
        $settings = HomeController::getSettings();
        $social = HomeController::getAllSocial();
        $pagesinfo = HomeController::getPages();
        $trips = DB::table('cat')->where([['type','trips'],['cid','0']])->get();
                foreach($trips as $k=>$v)
                {
                    $tours = DB::table('cat')->where('cid',$v->id)->get();
                    $trips[$k]->tours = $tours;

                }

        
        return view('frontend.pages.thankyou',compact('pages','trips','pagesinfo','social','settings','about','lang'));
    }
    
    
    public function search( $lang,Request $request )
    {
        
        $pages = HomeController::getPage();
        $about = HomeController::getAboutPage(); 
        $settings = HomeController::getSettings();
        $social = HomeController::getAllSocial();
        $pagesinfo = HomeController::getPages();

        $trips = DB::table('cat')->where([['type','trips'],['cid','0']])->get();
        foreach($trips as $k=>$v)
        {
            $tours = DB::table('cat')->where('cid',$v->id)->get();
            $trips[$k]->tours = $tours;

        }
        
        $searchday =  $request->day;
        $searchprice =  $request->price;
        $searchplace =  $request->place;
        
        $where="WHERE type = 'trips'";
        if($searchprice != ''){
        $where .="AND price_en <= '".$searchprice ."'" ;
        }
        
        if($searchday != ''){
        $where .="AND duration_en <= '".$searchday ."'";
        }
        
        if($searchplace != 'all'){
        $where .="AND destinations like '% ".$searchplace ." %'" ;
        }

        $select ="select * from post " . $where ;

        //if($searchplace =  $request->place == 'all' ) {            
        //$searchrsult = DB::table('post')->where([['type','trips'],['price_'.$lang,'<=',$searchprice],['duration_'.$lang,'<=',$searchday]])->get();
        //} else {
        $searchrsult = DB::select($select);
        //}


        for($i=0;$i<count($searchrsult);$i++){
            $catss = DB::table('cat')->where([['type','trips'],['id',$searchrsult[$i]->cid]])->get();
            $searchrsult[$i]->caturl = $catss[0]->{'url_'.$lang};
             $maincattours = DB::table('cat')->select('url_en')->where([['type','trips'],['cid','0'],['id',$catss[0]->cid]])->get();
             $searchrsult[$i]->maincaturl = $maincattours[0]->url_en;
                
        }
       
       
        return view('frontend.pages.search',compact('pages','searchrsult','trips','pagesinfo','social','settings','about','lang'));
    }
    
    public function sitemapxml($lang)
    {
        
        $pages = HomeController::getPage();
        $about = HomeController::getAboutPage(); 
        $settings = HomeController::getSettings();
        $social = HomeController::getAllSocial();
        $pagesinfo = HomeController::getPages();

        $trips = DB::table('cat')->where([['type','trips'],['cid','0']])->get();
        foreach($trips as $k=>$v)
        {
            $tours = DB::table('cat')->where('cid',$v->id)->get();
            $trips[$k]->tours = $tours;

        }

        
        return view('frontend.pages.sitemapxml',compact('pages','trips','pagesinfo','social','settings','about','lang'));
    }
    
}
