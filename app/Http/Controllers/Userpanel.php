<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use \Illuminate\Support\Facades\Session;


class Userpanel extends Controller
{
    
     public function __construct()
    {
        $logo = DB::table('settings')->select('value')->where('key','logo')->get();
        $title = DB::table('settings')->select('value')->where('key','title_ar')->get();
        Session::put('logo', $logo[0]->value);
        Session::put('title', $title[0]->value);
        
            view()->share('titlepage','الرئيسية');
    }
    
 public function index()
    {

        
        $active = 'home';

        return view('userpanel.home',compact('active'));
    }
    /**
     * Display a login page.
     *
     * @return view
     */
    public function login(){
        /*if (Session::get('useremail')) {
            return redirect(url('admin/index'));
        }*/
        
        return view('userpanel.login');
    }
    /**
     * check login d
     *
     * @return view
     */
    public function postlogin(Request $request){
        $rules = [
                'email'=>'required|email',
                'password'=>'required|min:5'
        ];
        $validate = Validator::make($request->all(),$rules);

        /*$validate->setAttributeNames([
                                    'email'=>'البريد الالكترونى',
                                    'password'=>'كلمة المرور'
                                ]);*/
        if($validate->fails()){
            return redirect()->back()->withInput()->withErrors($validate);
        }else{
            
            if($request->input('remember')){
                $remember = true;
            }else{
                $remember = false;
            }
            if(auth()->attempt(['email'=>$request->input('email'),'password'=>$request->input('password')],$remember)){
                Session::put('useremail', $request->input('email'));
                Session::put('userpass', $request->input('password'));
                $username = DB::table('users')->select('name')->where('email',$request->input('email'))->get();
                $logo = DB::table('settings')->select('value')->where('key','logo')->get();
                Session::put('username', $username[0]->name);
                Session::put('logo', $logo[0]->value);
                return redirect('userpanel/index');
            }else{
                session()->flash('error','خطأ فى تسجيل الدخول تأكد من البيانات');
                return redirect()->back();
            }
        }
    }

    public function logout()

    {

        auth()->logout();

        session()->put('success','تم تسجيل خروجك بنجاح');

        return redirect('userpanel/login');

    }
}
