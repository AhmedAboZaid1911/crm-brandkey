<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class TripController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    
     
    
    public function maincat($lang,$maincat)
    {
        $pages = HomeController::getPage(); 
        $social = DB::table('social')->get();
        $settings = HomeController::getSettings();
        $about = HomeController::getAboutPage(); 
        $pagesinfo = HomeController::getPages();
        $cats = DB::table('cat')->where('type','trips')->get();
        $maincatss = DB::table('cat')->where([['type','trips'],['url_en',$maincat]])->get();
        $cat = DB::table('cat')->where([['type','trips'],['cid',$maincatss[0]->id]])->get();
        
        $items = DB::table('post')->where('type','trips')->get();
        $images = DB::table('images')->where([['type','trips'],['itemid',$items[0]->id ]])->get();
        
        $trips = DB::table('cat')->where([['type','trips'],['cid','0']])->get();
                foreach($trips as $k=>$v)
                {
                    $tours = DB::table('cat')->where('cid',$v->id)->get();
                    $trips[$k]->tours = $tours;

                }
        
        return view('frontend.trips.maincat',compact('pages','lang','images','related','social','settings','about','pagesinfo','cats','cat','maincatss','maincat','catitem','items','items','trips'));
    }
   
    public function cat($lang,$maincat,$cat)
    {
        $pages = HomeController::getPage(); 
        $social = DB::table('social')->get();
        $settings = HomeController::getSettings();
        $about = HomeController::getAboutPage(); 
        $pagesinfo = HomeController::getPages();
        $cats = DB::table('cat')->where('type','trips')->get();
        $maincatss = DB::table('cat')->where([['type','trips'],['url_en',$maincat]])->get();
        $catss = DB::table('cat')->where([['type','trips'],['url_en',$cat]])->get();
        $catitem = DB::table('post')->where([['type','trips'],['cid',$catss[0]->id]])->get();
        
        $items = DB::table('post')->where('type','trips')->get();
        $images = DB::table('images')->where([['type','trips'],['itemid',$items[0]->id ]])->get();
        
        $trips = DB::table('cat')->where([['type','trips'],['cid','0']])->get();
                foreach($trips as $k=>$v)
                {
                    $tours = DB::table('cat')->where('cid',$v->id)->get();
                    $trips[$k]->tours = $tours;

                }
        
        
        return view('frontend.trips.cat',compact('maincat','maincatss','pages','lang','images','related','social','settings','about','pagesinfo','cats','catss','cat','catitem','items','items','trips'));
    }
   
    public function item($lang,$maincat,$cat,$items)
    {
        
        $pages = HomeController::getPage(); 
        $social = DB::table('social')->get();
        $settings = HomeController::getSettings();
        $about = HomeController::getAboutPage(); 
        $pagesinfo = HomeController::getPages();
        $cats = DB::table('cat')->where('type','trips')->get();
        $maincatss = DB::table('cat')->where([['type','trips'],['url_en',$maincat]])->get();
        $catss = DB::table('cat')->where([['type','trips'],['url_en',$cat]])->get();
        $catitem = DB::table('post')->where([['type','trips'],['url_en',$cat]])->get();
        
        $item = DB::table('post')->where([['type','trips'],['url_'. $lang ,$items]])->get();
        $images = DB::table('images')->where([['type','trips'],['itemid',$item[0]->id ]])->get();
        $related =  DB::table('post')->where([['type','trips'],['cid',$catss[0]->id],['id','!=',$item[0]->id]])->inRandomOrder()->get();
        
        $Policies = DB::table('pages')->select('text_en')->where([['type','general'],['id','118']])->get();                

        $loct = DB::table('pages')->select('title_en')->where('type','destination')->whereIn('id',json_decode($item[0]->destinations))->get();
        //dd($loct);
         //dd($related);
        $nationality = DB::table('pages')->where('type','nationality')->get();
        
        $rate = DB::table('review')->where([['active','1'],['itemid',$item[0]->id ]])->get();
        
        $trips = DB::table('cat')->where([['type','trips'],['cid','0']])->get();
        foreach($trips as $k=>$v)
        {
            $tours = DB::table('cat')->where('cid',$v->id)->get();
            $trips[$k]->tours = $tours;

        }
        
        $vote = DB::table("rating")->where([['rateditem_id','=',$item[0]->id],['type','=','trips']])->sum('vote');

        $vcount = DB::table("rating")->where([['rateditem_id','=',$item[0]->id],['type','=','trips']])->count('vote');
                //dd($images);
        return view('frontend.trips.item',compact('Policies','pages','loct','vote','vcount','rate','nationality','lang','images','related','social','settings','about','pagesinfo','cats','maincatss','catss','cat','catitem','items','item','trips'));
    }

    
    public function ratingajax(Request $request)

{
        
    $cid = $request->comId;
        
        
    $newvote = $request->vote;
        
        
    $type = $request->type;
        
    $exist = DB::table('rating')->where([['rateditem_id','=',$cid],['ip_address','=',$request->ip()],['type',$type]])->get();
        
    if(count($exist) == 0){
            
       DB::table('rating')->insert(
            
       ['ip_address' => $request->ip(), 'rateditem_id' => $cid, 'vote' => $newvote, 'type' => $type]
            
       );
        
    }
        
    
}
    public function tripform(Request $request)
    {        
        $settings = HomeController::getSettings();
        $insertname = $request->name;
        $insertemail = $request->email;
        $insertmobile = $request->mobile;
        $insertwhatsapp = $request->whatsapp;
        $insertnationality = $request->nationality;
        $insertfrom = $request->from;
        $insertto = $request->to;
        $insertadults = $request->adults;
        $insertchild = $request->child;
        $insertchildage = $request->childage;
        
        $inserttripname = $request->tripname;
        $insertmsg = $request->msg;

        $inserttime = time();
        
        $inserttype = 'trip';

        DB::table('forms')->insert([
        'name' => $insertname,
        'email'=> $insertemail, 
        'mobile' => $insertmobile , 
        'whatsapp' => $insertwhatsapp,
        'nationality' => $insertnationality,
        'from' => $insertfrom, 
        'to' => $insertto, 
        'adults' => $insertadults, 
        'child' => $insertchild, 
        'childage' => $insertchildage, 
        'tripname' => $inserttripname, 
        'msg' => $insertmsg, 
        'time' => $inserttime, 
        'type' => $inserttype ]);

        $mail_to = $settings['email'];
            
        $subject = 'Message from Website Tour Name:'. $inserttripname;
        
        $body_message = 'From: '. $insertname."\n";
        $body_message .= 'E-mail: ' . $insertemail . "\n";
        $body_message .= 'Mobile: ' . $insertmobile . "\n";
        $body_message .= 'Whats app: ' . $insertwhatsapp. "\n"; 
        $body_message .= 'Nationality: ' . $insertnationality. "\n"; 
        $body_message .= 'From: ' . $insertfrom. "\n"; 
        $body_message .= 'To: ' . $insertto. "\n"; 
        $body_message .= 'No. of Adults: ' . $insertadults. "\n"; 
        $body_message .= 'No. of child: ' . $insertchild. "\n"; 
        $body_message .= 'Child Age: ' . $insertchildage. "\n"; 
        $body_message .= 'Message: ' . $insertmsg. "\n"; 

        $headers = 'From: '.$insertemail."\r\n";
        $headers .= 'Reply-To: '.$insertemail."\r\n";

        $succ = mail($mail_to, $subject, $body_message, $headers);

        return redirect(url($request->lang . '/thankyou'));

    }

    public function review(Request $request)
    {        
        $insertname = $request->name;
        $insertemail = $request->email;                
        $inserttripid = $request->itemid;
        $insertcomments = $request->comments;
        $inserttripname = $request->tripname;
        $inserttime = time();
        $insertstars = '5';
        $insertactive = '0';
        

        DB::table('review')->insert([
        'name' => $insertname,
        'email'=> $insertemail, 
        'itemid' => $inserttripid,
        'tripname' => $inserttripname, 
        'stars' => $insertstars, 
        'comments' => $insertcomments,
        'time' => $inserttime,
        'active' => $insertactive]);
        return redirect(url($request->lang . '/thankyou'));

    }


}