<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class AlbumController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
	
    
    
    public function postlist(Request $request){
        $data = array('email' => $request->email);
            $validator = Validator::make($data, [
            'email' => 'unique:emails'
            ]);

            if ($validator->fails()) {
            
                return redirect(url('/index#list'))
                        ->withErrors($validator)
                        ->withInput(); 
            }
        DB::table('emails')->insert(['email' => $request->email]);
        return redirect('/thankyou');
    }   
    
    
	
    public function index($lang)
    {
        $pages = HomeController::getPage(); 
        $settings = HomeController::getSettings(); 
        $about = HomeController::getAboutPage(); 
        $social = HomeController::getAllSocial();
        $pagesinfo = HomeController::getPages();
        $trips = DB::table('cat')->where([['type','trips'],['cid','0']])->get();
                foreach($trips as $k=>$v)
                {
                    $tours = DB::table('cat')->where('cid',$v->id)->get();
                    $trips[$k]->tours = $tours;

                }
        $album = DB::table('post')->where([['type','multialbum'],['id','!=','57']])->get();
        //dd($album);
        for ($i=0;$i<count($album);$i++){
        $images = DB::table('images')->where([['type','multialbum'],['itemid',$album[$i]->id ],['itemid','!=','57']])->get();
        //dd($images);
        $album[$i]->img = $images[0]->pic;
        }

        
        return view('frontend.album.index',compact('pages','lang','social','settings','about','pagesinfo','album','images','trips','tours'));
    }
    
    public function album($lang,$album)
    {
        $pages = HomeController::getPage(); 
        $settings = HomeController::getSettings(); 
        $about = HomeController::getAboutPage(); 
        $social = HomeController::getAllSocial();
        $pagesinfo = HomeController::getPages();
        $trips = DB::table('cat')->where([['type','trips'],['cid','0']])->get();
        foreach($trips as $k=>$v)
        {
            $tours = DB::table('cat')->where('cid',$v->id)->get();
            $trips[$k]->tours = $tours;

        }
        $album = DB::table('post')->where([['type','multialbum'],['url_en',$album]])->get();
       // dd($album);
        $images = DB::table('images')->where([['type','multialbum'],['itemid',$album[0]->id ],['itemid','!=','57']])->get();
        //dd($images);
        
        return view('frontend.album.album',compact('pages','lang','social','settings','about','pagesinfo','images','album','trips','tours'));
    }
   
}