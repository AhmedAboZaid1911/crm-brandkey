<?php



namespace App\Http\Controllers\admin;



use Validator;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;


use Excel;


class ReportsController extends Controller

{

    public function __construct()

       {

           // parent::__construct();

            $this->type = 'reports';

            $this->module = 'reports';

            $this->picsnum = 5;

            $this->lang_arr = array("en" => "اللغة الانجليزية" );

            view()->share('module', $this->module );

            view()->share('active', 'Reports');

            view()->share('titlepage','Reports');

            view()->share('picsnum', $this->picsnum);

       }



    public function reports()

    {


        return view('admin.reports.index');
    }
    public function service_report()

    {

        $Services = DB::table('services')->get();
        foreach($Services as $k=>$v)
        {
            $time = DB::table('services_time')->select('name')->where('id',$v->service_time)->get();
            $Services[$k]->time = $time[0]->name;

            $periods = DB::table('services_period')->where('id',$v->service_period)->get();
            $Services[$k]->from = $periods[0]->from;
            $Services[$k]->to = $periods[0]->to;

            $places = DB::table('services_place')->select('name')->where('id',$v->service_place)->get();
            $Services[$k]->place = $places[0]->name;

            $products = DB::table('products')->select('name')->where('id',$v->service_name)->get();
            $Services[$k]->name = $products[0]->name;

            $currency = DB::table('services_currency')->select('currency')->where('id',$v->service_currency)->get();
            $Services[$k]->currency = $currency[0]->currency;

            $days = DB::table('services_days')->where('id',$v->service_days)->get();
            $Services[$k]->day1 = $days[0]->day1;
            $Services[$k]->day2 = $days[0]->day2;
            $Services[$k]->day3 = $days[0]->day3;
            $Services[$k]->day4 = $days[0]->day4;
            $Services[$k]->day5 = $days[0]->day5;
            $Services[$k]->day6 = $days[0]->day6;
            $Services[$k]->day7 = $days[0]->day7;
        }

        $clientvsservices = DB::table('clientsvsservices')->where('service','0')->get();
        foreach($clientvsservices as $k=>$v)
        {

            $clients = DB::table('clients')->where('id',$v->client)->get();
            $clientvsservices[$k]->name = $clients[0]->name_en;
            $clientvsservices[$k]->number = $clients[0]->number;
            $clientvsservices[$k]->email = $clients[0]->email;
            $user = DB::table('users')->select('name')->where('id',$clients[0]->assigned_to)->get();
            $clientvsservices[$k]->assigned_to = $user[0]->name;

        }

        $message = '';


        return view('admin.reports.servicereport',compact('message','Services','users','clients','clientvsservices','user'));
    }

    public function service_report_search(Request $request)

    {
        $Services = DB::table('services')->get();
        foreach($Services as $k=>$v)
        {
            $time = DB::table('services_time')->select('name')->where('id',$v->service_time)->get();
            $Services[$k]->time = $time[0]->name;

            $periods = DB::table('services_period')->where('id',$v->service_period)->get();
            $Services[$k]->from = $periods[0]->from;
            $Services[$k]->to = $periods[0]->to;

            $places = DB::table('services_place')->select('name')->where('id',$v->service_place)->get();
            $Services[$k]->place = $places[0]->name;

            $products = DB::table('products')->select('name')->where('id',$v->service_name)->get();
            $Services[$k]->name = $products[0]->name;

            $currency = DB::table('services_currency')->select('currency')->where('id',$v->service_currency)->get();
            $Services[$k]->currency = $currency[0]->currency;

            $days = DB::table('services_days')->where('id',$v->service_days)->get();
            $Services[$k]->day1 = $days[0]->day1;
            $Services[$k]->day2 = $days[0]->day2;
            $Services[$k]->day3 = $days[0]->day3;
            $Services[$k]->day4 = $days[0]->day4;
            $Services[$k]->day5 = $days[0]->day5;
            $Services[$k]->day6 = $days[0]->day6;
            $Services[$k]->day7 = $days[0]->day7;
        }

        $service_name = $request->service_name;

        $clientvsservices = DB::table('clientsvsservices')->where('service',$service_name)->get();
        foreach($clientvsservices as $k=>$v)
        {

            $clients = DB::table('clients')->where('id',$v->client)->get();
            $clientvsservices[$k]->name = $clients[0]->name_en;
            $clientvsservices[$k]->number = $clients[0]->number;
            $clientvsservices[$k]->email = $clients[0]->email;
            $user = DB::table('users')->select('name')->where('id',$clients[0]->assigned_to)->get();
            $clientvsservices[$k]->assigned_to = $user[0]->name;

        }


        $message = "<span style='padding-right:45%;font-weight:bold;'> تم البحث بنجاح : </span> <br><br>";

        if($service_name != null){
            $Servicemsg = DB::table('services')->get();
            foreach($Servicemsg as $k=>$v)
            {
                $time = DB::table('services_time')->select('name')->where('id',$v->service_time)->get();
                $Servicemsg[$k]->time = $time[0]->name;

                $periods = DB::table('services_period')->where('id',$v->service_period)->get();
                $Servicemsg[$k]->from = $periods[0]->from;
                $Servicemsg[$k]->to = $periods[0]->to;

                $places = DB::table('services_place')->select('name')->where('id',$v->service_place)->get();
                $Servicemsg[$k]->place = $places[0]->name;

                $products = DB::table('products')->select('name')->where('id',$v->service_name)->get();
                $Servicemsg[$k]->name = $products[0]->name;

                $currency = DB::table('services_currency')->select('currency')->where('id',$v->service_currency)->get();
                $Servicemsg[$k]->currency = $currency[0]->currency;

                $days = DB::table('services_days')->where('id',$v->service_days)->get();
                $Servicemsg[$k]->day1 = $days[0]->day1;
                $Servicemsg[$k]->day2 = $days[0]->day2;
                $Servicemsg[$k]->day3 = $days[0]->day3;
                $Servicemsg[$k]->day4 = $days[0]->day4;
                $Servicemsg[$k]->day5 = $days[0]->day5;
                $Servicemsg[$k]->day6 = $days[0]->day6;
                $Servicemsg[$k]->day7 = $days[0]->day7;
            }
            $message .=" <span style='padding-left:5%;color:#364150'> في الخدمة :     "  . $Servicemsg[0]->name .' ----'. $Servicemsg[0]->time .' ----'. $Servicemsg[0]->place .' ----'. $Servicemsg[0]->from .' ----'. $Servicemsg[0]->to  .'</span><br>';
        }


        return view('admin.reports.servicereport',compact('message','users','clients','Services','clientvsservices','user'));
    }

    public function clients_report()

    {


        $clients = DB::table('clients')->where('status','2')->get();
        foreach($clients as $k=>$v)
        {
            $user = DB::table('users')->select('name')->where('id',$v->assigned_to)->get();
            $clients[$k]->assigned_to = $user[0]->name;

            $lead = DB::table('lead')->select('lead')->where('id',$v->lead)->get();
            $clients[$k]->lead = $lead[0]->lead;

            $gender = DB::table('gender')->select('type')->where('id',$v->gender)->get();
            $clients[$k]->gender = $gender[0]->type;

            $Interest = DB::table('interest')->select('type')->where('id',$v->Interest)->get();
            $clients[$k]->Interest = $Interest[0]->type;

            $nationality = DB::table('pages')->select('title_en')->where([['id',$v->nationality],['type','nationality']])->get();
            $clients[$k]->nationality = $nationality[0]->title_en;
        }

        $nationality = DB::table('pages')->where('type','nationality')->get();
        $Interest = DB::table('interest')->get();
        $gender = DB::table('gender')->get();
        $lead = DB::table('lead')->get();
        $users = DB::table('users')->get();

        $message = '';

        return view('admin.reports.clientsreport',compact('message','clients','nationality','Interest','gender','lead','users'));
    }

    public function clients_report_search(Request $request)

    {



        $from =  $request->from;
        $to =  $request->to;
        $genders =  $request->gender;
        $nationalitys =  $request->nationality;
        $Interests =  $request->Interest;
        $leads =  $request->lead;
        $assigned_to =  $request->user;


        $where="WHERE status = '1' ";

         if($from != '' && $to != '' ){
            $from = $from." 00:00:00";
            $to = $to." 23:59:59";

            $where .="AND created_at BETWEEN '". $from ."' AND '". $to ."' " ;
        }

        if($from != null && $to == null){
            $from = $from." 00:00:00";
            $to = $to." 23:59:59";
            $where .=" AND created_at >= '". $from ."' " ;
        }

        if($from == null && $to != null){

            $from = $from." 00:00:00";
            $to = $to." 23:59:59";
            $where .=" AND created_at <= '". $to ."'";
        }

        if($genders != 'all'){
        $where .="AND gender = '".$genders ."'" ;
        }
        if($nationalitys != 'all'){
        $where .="AND nationality = '".$nationalitys ."'" ;
        }
        if($Interests != 'all'){
        $where .="AND Interest = '".$Interests ."'" ;
        }
        if($leads != 'all'){
        $where .="AND lead = '".$leads ."'" ;
        }
        if($assigned_to != 'all'){
        $where .="AND assigned_to = '".$assigned_to ."'" ;
        }


        $select =" select * from clients " . $where ;
        //dd($select);
        $clients = DB::select($select);
        foreach($clients as $k=>$v)
        {
            $user = DB::table('users')->select('name')->where('id',$v->assigned_to)->get();
            $clients[$k]->assigned_to = $user[0]->name;

            $lead = DB::table('lead')->select('lead')->where('id',$v->lead)->get();
            $clients[$k]->lead = $lead[0]->lead;

            $gender = DB::table('gender')->select('type')->where('id',$v->gender)->get();
            $clients[$k]->gender = $gender[0]->type;

            $Interest = DB::table('interest')->select('type')->where('id',$v->Interest)->get();
            $clients[$k]->Interest = $Interest[0]->type;

            $nationality = DB::table('pages')->select('title_en')->where([['id',$v->nationality],['type','nationality']])->get();
            $clients[$k]->nationality = $nationality[0]->title_en;
        }


        $nationality = DB::table('pages')->where('type','nationality')->get();
        $Interest = DB::table('interest')->get();
        $gender = DB::table('gender')->get();
        $lead = DB::table('lead')->get();
        $users = DB::table('users')->get();


        $message = "<span style='padding-right:45%;font-weight:bold;'> تم البحث بنجاح : </span> <br><br>";

        if($from != null && $to == null){
            $from = date('d-m-Y');
            $message .=" <span style='padding-left:5%;color:#364150'> التاريخ من :     "  . $from .'</span><br>';
        }

        if($from == null && $to != null){
            $to = date('d-m-Y');
            $message .="<span style='padding-left:5%;color:#364150'> التاريخ الي :"  . $to .'</span><br>';
        }

        if($from != null && $to != null){
        $message .="<span style='padding-left:5%;color:#364150'> التاريخ من  : "  . $from . "    ----      الي :   " . $to .'</span><br>';
        }

        if($genders != 'all'){
            $gendermsg = DB::table('gender')->select('type')->where('id',$genders)->get();
            $message .=" <span style='padding-left:5%;color:#364150'> النوع :     " . $gendermsg[0]->type .'</span>';
        }

        if($nationalitys != 'all'){
            $natmsg = DB::table('pages')->select('title_en')->where([['type','nationality'],['id',$nationalitys]])->get();
            $message .="<span style='padding-left:5%;color:#364150'> الجنسية :     " . $natmsg[0]->title_en .'</span> ' ;

        }

        if($Interests != 'all'){
            $intermsg = DB::table('interest')->select('type')->where('id',$Interests)->get();
            $message .=" <span style='padding-left:5%;color:#364150'> درجة الاهتمام :     " . $intermsg[0]->type .'</span>';
        }

        if($leads != 'all'){
            $leadmsg = DB::table('lead')->select('lead')->where('id',$assigned_to)->get();
            $message .=" <span style='padding-left:5%;color:#364150'> مصدر من :     " . $leadmsg[0]->lead .'</span>';
        }

        if($assigned_to != 'all'){
            $usermsg = DB::table('users')->select('name')->where('id',$assigned_to)->get();
            $message .=" <span style='padding-left:5%;color:#364150'> تابع ل :     " . $usermsg[0]->name.'</span>' ;
        }


        return view('admin.reports.clientsreport',compact('message','clients','nationality','users','Interest','gender','lead'));
    }

    public function clients_report_excel(Request $request)

    {



        $from =  $request->from;
        $to =  $request->to;
        $genders =  $request->gender;
        $nationalitys =  $request->nationality;
        $Interests =  $request->Interest;
        $leads =  $request->lead;
        $assigned_to =  $request->user;


        $where="WHERE status = '1' ";

         if($from != '' && $to != '' ){
            $from = $from." 00:00:00";
            $to = $to." 23:59:59";

            $where .="AND created_at BETWEEN '". $from ."' AND '". $to ."' " ;
        }

        if($from != null && $to == null){
            $from = $from." 00:00:00";
            $to = $to." 23:59:59";
            $where .=" AND created_at >= '". $from ."' " ;
        }

        if($from == null && $to != null){

            $from = $from." 00:00:00";
            $to = $to." 23:59:59";
            $where .=" AND created_at <= '". $to ."'";
        }

        if($genders != 'all'){
        $where .="AND gender = '".$genders ."'" ;
        }
        if($nationalitys != 'all'){
        $where .="AND nationality = '".$nationalitys ."'" ;
        }
        if($Interests != 'all'){
        $where .="AND Interest = '".$Interests ."'" ;
        }
        if($leads != 'all'){
        $where .="AND lead = '".$leads ."'" ;
        }
        if($assigned_to != 'all'){
        $where .="AND assigned_to = '".$assigned_to ."'" ;
        }


        $select =" select * from clients " . $where ;
        //dd($select);
        $clients = DB::table('clients')->where('status','1')->get()->toArray();
        $clients_array[] =  array('اسم العميل','رقم العميل','الايمال','النوع','المصدر','الجنسية','الاهتمام','تابع ل','تاريخ الاضافة');
        foreach($clients as $client_excel)
        {
            $clients_array[] = array(
                'اسم العميل'       => $client_excel->name_en,
                'رقم العميل'       => $client_excel->number,
                'الايمال'           => $client_excel->email,
                'النوع'            => $client_excel->gender,
                'المصدر'           => $client_excel->lead,
                'الجنسية'          => $client_excel->nationality,
                'الاهتمام'          => $client_excel->Interest,
                'تابع ل'           =>  $client_excel->assigned_to,
                'تاريخ الاضافة'    =>  $client_excel->created_at
            );
        }
        Excel::download('العملاء.xlsx', function ($clients_report_excel)
        use ($clients_array){
            $excel->setTitle('العملاء');
            $excel->sheet('العملاء',function($sheet)
            use($clients_array){
                $sheet->fromArray($clients_array, null, 'A1',false,false);
            });
        });

        foreach($clients as $k=>$v)
        {
            $user = DB::table('users')->select('name')->where('id',$v->assigned_to)->get();
            $clients[$k]->assigned_to = $user[0]->name;

            $lead = DB::table('lead')->select('lead')->where('id',$v->lead)->get();
            $clients[$k]->lead = $lead[0]->lead;

            $gender = DB::table('gender')->select('type')->where('id',$v->gender)->get();
            $clients[$k]->gender = $gender[0]->type;

            $Interest = DB::table('interest')->select('type')->where('id',$v->Interest)->get();
            $clients[$k]->Interest = $Interest[0]->type;

            $nationality = DB::table('pages')->select('title_en')->where([['id',$v->nationality],['type','nationality']])->get();
            $clients[$k]->nationality = $nationality[0]->title_en;
        }


    }



    public function tasks_report()

    {
        $tasks = DB::table('tasks')->where('status','=','10')->get();
        foreach($tasks as $k=>$v)
        {
            $user = DB::table('users')->select('name')->where('id',$v->assigned_to)->get();
            $tasks[$k]->assigned_to = $user[0]->name;
            if($v->type == 'client'){
                $clients = DB::table('clients')->where('id',$v->itemid)->get();
                $tasks[$k]->client = $clients[0]->name_en;
                $tasks[$k]->clientnum = $clients[0]->number;
            }
            elseif($v->type == 'clientvsservices'){
            $clientvsservices = DB::table('clientsvsservices')->where('id',$v->itemid)->get();
            $clients = DB::table('clients')->where('id',$clientvsservices[0]->client)->get();
            $tasks[$k]->client = $clients[0]->name_en;
            $tasks[$k]->clientnum = $clients[0]->number;
            $services = DB::table('services')->where('id',$clientvsservices[0]->service)->get();
            $service_name = DB::table('products')->where('id',$services[0]->service_name)->get();
            $tasks[$k]->service = $service_name[0]->name;
            }
        }

        $users = DB::table('users')->get();

        $message = '';

        return view('admin.reports.tasksreport',compact('tasks','message','users'));
    }

    public function tasks_report_search(Request $request)

    {

        $from = $request->from;
        $to = $request->to;
        $assigned_to = $request->user;
        $case = $request->case;

        $where=" where '1' ";

        if($assigned_to != 'all'){
            $where .=" AND assigned_to = '".$assigned_to ."'" ;

        }

        if($case == '1'){
            $where .=" AND status != '2' " ;
        }

        if($case == '2'){
            $date = date('d-m-Y');
            $where .=" AND assigned_at >= '".$date."' And status = '1'" ;
        }

        if($case == '3'){
            $date = date('d-m-Y');
            $where .=" AND assigned_at < '".$date."' And status = '1' " ;
        }

        if($case == '4'){
            $where .=" AND status = '3'" ;
        }

        if($from != null && $to == null){
            $from = date('d-m-Y');
            $where .=" AND assigned_at >= '". $from ."' " ;
        }

        if($from == null && $to != null){
            $to = date('d-m-Y');
            $where .=" AND assigned_at <= '". $to ."'";
        }

        if($from != null && $to != null){
        $where .=" AND assigned_at BETWEEN '". $from ."' AND '". $to ."' " ;
        }
        $select =" select * from tasks  " . $where ;
        //dd($select);
        $tasks = DB::select($select);

        foreach($tasks as $k=>$v)
        {
            $user = DB::table('users')->select('name')->where('id',$v->assigned_to)->get();
            $tasks[$k]->assigned_to = $user[0]->name;
            if($v->type == 'client'){
                $clients = DB::table('clients')->where('id',$v->itemid)->get();
                $tasks[$k]->client = $clients[0]->name_en;
                $tasks[$k]->clientnum = $clients[0]->number;
            }
            elseif($v->type == 'clientvsservices'){
            $clientvsservices = DB::table('clientsvsservices')->where('id',$v->itemid)->get();
            $clients = DB::table('clients')->where('id',$clientvsservices[0]->client)->get();
            $tasks[$k]->client = $clients[0]->name_en;
            $tasks[$k]->clientnum = $clients[0]->number;
            $services = DB::table('services')->where('id',$clientvsservices[0]->service)->get();
            $service_name = DB::table('products')->where('id',$services[0]->service_name)->get();
            $tasks[$k]->service = $service_name[0]->name;
            }
        }
        //dd(\Request::segment(5));

        $message = 'تم البحث بنجاح : <br>';

        if($from != null && $to == null){
            $from = date('d-m-Y');
            $message .=" التاريخ من :     "  . $from .'<br>';
        }

        if($from == null && $to != null){
            $to = date('d-m-Y');
            $message .=" التاريخ الي :"  . $to .'<br>';
        }

        if($from != null && $to != null){
        $message .=" التاريخ من  : "  . $from . "    ----      الي :   " . $to .'<br>';
        }
        if($case == '1'){
            $message .=" الحالة : الكل". "<br>";
        }

        if($case == '2'){
            $date = date('d-m-Y');
            $message .=" الحالة : جاري" . "<br>";
        }

        if($case == '3'){
            $date = date('d-m-Y');
            $message .=" الحالة : متاخرة" . "<br>";
        }

        if($case == '4'){
            $message .=" الحالة : تمت" . "<br>" ;
        }


        if($assigned_to != 'all'){
            $usermsg = DB::table('users')->select('name')->where('id',$assigned_to)->get();
            $message .="  تابع ل :     " . $usermsg[0]->name ;

        }



        $users = DB::table('users')->get();
        return view('admin.reports.tasksreport',compact('message','tasks','users'));
    }


    public function users_report()

    {
        $tasks = DB::table('tasks')->where('status','=','10')->get();
        foreach($tasks as $k=>$v)
        {
            $user = DB::table('users')->select('name')->where('id',$v->assigned_to)->get();
            $tasks[$k]->assigned_to = $user[0]->name;
            if($v->type == 'client'){
                $clients = DB::table('clients')->where('id',$v->itemid)->get();
                $tasks[$k]->client = $clients[0]->name_en;
                $tasks[$k]->clientnum = $clients[0]->number;
            }
            elseif($v->type == 'clientvsservices'){
            $clientvsservices = DB::table('clientsvsservices')->where('id',$v->itemid)->get();
            $clients = DB::table('clients')->where('id',$clientvsservices[0]->client)->get();
            $tasks[$k]->client = $clients[0]->name_en;
            $tasks[$k]->clientnum = $clients[0]->number;
            $services = DB::table('services')->where('id',$clientvsservices[0]->service)->get();
            $service_name = DB::table('products')->where('id',$services[0]->service_name)->get();
            $tasks[$k]->service = $service_name[0]->name;
            }
        }

        $clientvsservices = DB::table('clientsvsservices')->where('service','0')->get();
        foreach($clientvsservices as $k=>$v)
        {

            $clients = DB::table('clients')->where('id',$v->client)->get();
            $clientvsservices[$k]->name = $clients[0]->name_en;
            $clientvsservices[$k]->number = $clients[0]->number;
            $clientvsservices[$k]->email = $clients[0]->email;
            $user = DB::table('users')->select('name')->where('id',$clients[0]->assigned_to)->get();
            $clientvsservices[$k]->assigned_to = $user[0]->name;

        }

        $clients = DB::table('clients')->where('status','2')->get();
        foreach($clients as $k=>$v)
        {
            $user = DB::table('users')->select('name')->where('id',$v->assigned_to)->get();
            $clients[$k]->assigned_to = $user[0]->name;

            $lead = DB::table('lead')->select('lead')->where('id',$v->lead)->get();
            $clients[$k]->lead = $lead[0]->lead;

            $gender = DB::table('gender')->select('type')->where('id',$v->gender)->get();
            $clients[$k]->gender = $gender[0]->type;

            $Interest = DB::table('interest')->select('type')->where('id',$v->Interest)->get();
            $clients[$k]->Interest = $Interest[0]->type;

            $nationality = DB::table('pages')->select('title_en')->where([['id',$v->nationality],['type','nationality']])->get();
            $clients[$k]->nationality = $nationality[0]->title_en;
        }
        $users = DB::table('users')->get();

        $message = '';

        return view('admin.reports.usersreport',compact('tasks','message','users'));
    }

    public function users_report_search(Request $request)

    {

        $from = $request->from;
        $to = $request->to;
        $assigned_to = $request->user;
        $case = $request->case;

        $wheretasks=" where '1' ";

        if($assigned_to != 'all'){
            $wheretasks .=" AND created_by = '".$assigned_to ."' OR assigned_to  = '".$assigned_to ."' " ;
        }

        if($from != null && $to == null){
            $fromtasks = $from." 00:00:00";
            $wheretasks .=" AND created_at >= '". $fromtasks ."' " ;
        }

        if($from == null && $to != null){
            $totasks = $to." 23:59:59";
            $wheretasks .=" AND created_at <= '". $totasks ."'";
        }

        if($from != null && $to != null){
            $fromtasks = $from." 00:00:00";
            $totasks = $to." 23:59:59";
            $wheretasks .=" AND created_at BETWEEN '". $fromtasks ."' AND '". $totasks ."' " ;
        }
        $selecttasks =" select * from tasks  " . $wheretasks ;
        $tasks = DB::select($selecttasks);
        foreach($tasks as $k=>$v)
        {
            $user = DB::table('users')->select('name')->where('id',$v->assigned_to)->get();
            $tasks[$k]->assigned_to = $user[0]->name;
            $user = DB::table('users')->select('name')->where('id',$v->created_by)->get();
            $tasks[$k]->created_by = $user[0]->name;

            if($v->type == 'client'){
                $clientstasks = DB::table('clients')->where('id',$v->itemid)->get();
                $tasks[$k]->client = $clientstasks[0]->name_en;
                $tasks[$k]->clientnum = $clientstasks[0]->number;
            }
            elseif($v->type == 'clientvsservices'){
            $clientvsservices = DB::table('clientsvsservices')->where('id',$v->itemid)->get();
            $clienttasks = DB::table('clients')->where('id',$clientvsservices[0]->client)->get();
            $tasks[$k]->client = $clienttasks[0]->name_en;
            $tasks[$k]->clientnum = $clienttasks[0]->number;
            $services = DB::table('services')->where('id',$clientvsservices[0]->service)->get();
            $service_name = DB::table('products')->where('id',$services[0]->service_name)->get();
            $tasks[$k]->service = $service_name[0]->name;
            }
        }

        //dd($tasks);

        $whereclients=" WHERE status = '1' ";

        if($assigned_to != 'all'){
            $whereclients .=" AND (created_by = '".$assigned_to ."' OR assigned_to  = '".$assigned_to ."' )" ;
        }

        if($from != null && $to == null){
            $fromclients = $from." 00:00:00";
            $whereclients .=" AND created_at >= '". $fromclients ."' " ;
        }

        if($from == null && $to != null){

            $toclients = $to." 23:59:59";
            $whereclients .=" AND created_at <= '". $toclients ."'";
        }

        if($from != null && $to != null){
            $fromclients = $from." 00:00:00";
            $toclients = $to." 23:59:59";
            $whereclients .=" AND created_at BETWEEN '". $fromclients ."' AND '". $toclients ."' " ;
        }
        //dd($whereclients);
        $selectclients =" select * from clients " . $whereclients ;
        $clients = DB::select($selectclients);
        foreach($clients as $k=>$v)
        {
            $user = DB::table('users')->select('name')->where('id',$v->created_by)->get();
            $clients[$k]->created_by = $user[0]->name;

            $user2 = DB::table('users')->select('name')->where('id',$v->assigned_to)->get();
            $clients[$k]->assigned_to = $user2[0]->name;

            $lead = DB::table('lead')->select('lead')->where('id',$v->lead)->get();
            $clients[$k]->lead = $lead[0]->lead;

            $gender = DB::table('gender')->select('type')->where('id',$v->gender)->get();
            $clients[$k]->gender = $gender[0]->type;

            $Interest = DB::table('interest')->select('type')->where('id',$v->Interest)->get();
            $clients[$k]->Interest = $Interest[0]->type;

            $nationality = DB::table('pages')->select('title_en')->where([['id',$v->nationality],['type','nationality']])->get();
            $clients[$k]->nationality = $nationality[0]->title_en;
        }


        $clientvsservices = DB::table('clientsvsservices')->where('created_by',$assigned_to)->get();
        foreach($clientvsservices as $k=>$v)
        {

            $clientservice = DB::table('clients')->where('id',$v->client)->get();
            $clientvsservices[$k]->name = $clientservice[0]->name_en;
            $clientvsservices[$k]->number = $clientservice[0]->number;
            $clientvsservices[$k]->email = $clientservice[0]->email;
            $user = DB::table('users')->select('name')->where('id',$clientservice[0]->created_by)->get();
            $clientvsservices[$k]->created_by = $user[0]->name;

        }

        $message = 'تم البحث بنجاح : <br>';

        if($from != null && $to == null){
            $from = date('d-m-Y');
            $message .=" التاريخ من :     "  . $from .'<br>';
        }

        if($from == null && $to != null){
            $to = date('d-m-Y');
            $message .=" التاريخ الي :"  . $to .'<br>';
        }

        if($from != null && $to != null){
        $message .=" التاريخ من  : "  . $from . "    ----      الي :   " . $to .'<br>';
        }

        if($assigned_to != 'all'){
            $usermsg = DB::table('users')->select('name')->where('id',$assigned_to)->get();
            $message .="  تابع ل :     " . $usermsg[0]->name ;

        }


        $users = DB::table('users')->get();
        return view('admin.reports.usersreport',compact('clientvsservices','message','tasks','clients','users'));
    }



}
