<?php



namespace App\Http\Controllers\admin;



use Validator;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;



class ClientsController extends Controller

{



    public function __construct()

       {

           // parent::__construct();

            $this->type = 'Clients';

            $this->module = 'Clients';

            $this->picsnum = 5;

            $this->day = 15;

            $this->lang_arr = array("en" => "اللغة الانجليزية" );

            view()->share('module', $this->module);

            view()->share('active', 'Clients');



            view()->share('cattitlepage','العملاء');

            view()->share('posttitlepage','العميل ');

            view()->share('picsnum', $this->picsnum);

            view()->share('day', $this->day);

            $id = Session::get('uid');
            $roles = DB::table('usersvsroles')->where("user_id",$id)->get();
            $rolesarr = array();
            for($i=0;$i<count($roles);$i++){
                $rolesarr[$i] = $roles[$i]->role_id;
            }
            view()->share('getroles', $rolesarr);


    }



    public function cats()
    {
        RolesController::checkroles('2');
            // if($_POST){
            //     $countallclients = DB::table('clients')->where([['status',1],['assigned_to',$_POST['user']]])->get();
            //     $clients = DB::table('clients')->where([['status',1],['assigned_to',$_POST['user']]])->orderBy('id','desc')->paginate(50);
            //     foreach($clients as $k=>$v)
            //     {
            //         $user = DB::table('users')->select('name')->where('id',$v->assigned_to)->get();
            //         $clients[$k]->assigned_to = $user[0]->name;
            //     }
            //     $count = floor(count($clients)/4);
            //     $remain = count($clients) - (4 * $count);

            // }else{
                $countallclients = DB::table('clients')->where('status',1)->get();
                $clients = DB::table('clients')->where([['status',1],['assigned_to',Auth::user()->id]])->orderBy('id','desc')->paginate(50);
                foreach($clients as $k=>$v)
                {
                    $user = DB::table('users')->select('name')->where('id',$v->assigned_to)->get();
                    $clients[$k]->assigned_to = $user[0]->name;
                }
                $users = DB::table('users')->where('id',Auth::user()->id)->get();
                $count = floor(count($clients)/4);
                $remain = count($clients) - (4 * $count);
            // }
        return view('admin.'.$this->module.'.cats',compact('clients','users','countallclients'));
    }



    public function catadd()

    {
        RolesController::checkroles('3');


        $user           = DB::table('users')->get();
        $gender         = DB::table('gender')->get();
        $Interest       = DB::table('interest')->get();
        $products       = DB::table('products')->get();
        $leads          = DB::table('lead')->get();
        $nationalitys = DB::table('pages')->where('type','nationality')->get();
        /* 10/3/2020 */
        $wants              = DB::table('wants')->get();
        $tags               = DB::table('tags')->get();
        $service_types      = DB::table('service_type')->get();
        $call_status        = DB::table('call_status')->get();
        $not_interested     = DB::table('not_interested')->get();
        $interested_types   = DB::table('interested_type')->get();
        
        return view('admin.'.$this->module.'.catadd',compact('cats','user','gender','Interest','products','leads','nationalitys','wants','tags','service_types','call_status','not_interested','interested_types'));
    }



    public function catstore(Request $request)

    {
        RolesController::checkroles('3');
        if( $request->number != '' && $request->number2 == ''){
            $client = DB::table('clients')->select('name','id')->where('number','like','%'.$request->number.'%')
            ->Orwhere('number2','like','%'.$request->number.'%')
           ->get();
        }elseif( $request->number2 != '' && $request->number == ''){
            $client = DB::table('clients')->select('name','id')->where('number','like','%'.$request->number2.'%')
            ->Orwhere('number2','like','%'.$request->number2.'%')->get();   
        }elseif( $request->number != '' && $request->number2 != '' ){
            $client = DB::table('clients')->select('name','id')->where('number','like','%'.$request->number.'%')
            ->Orwhere('number2','like','%'.$request->number.'%')
            ->Orwhere('number','like','%'.$request->number2.'%')
            ->Orwhere('number2','like','%'.$request->number2.'%')->get();
        }
        if(count($client)>0){
            $clientid = $client[0]->id;
            $clientname = $client[0]->name;
            return redirect(url('admin/Clients/Clients_add/'))->with(compact('clientid','clientname'))
            ->with('message', 'يوجد عميل بنفس رقم التليفون');

        }
        else{
            unset($_POST['_token']);
            unset($_POST['comment']);

            $_POST['created_at'] = date('Y-m-d H:i:s');
            $_POST['created_by'] = Session::get('uid');
            $newclientid = DB::table('clients')->insertGetId($_POST);
            DB::table('comments')->insert(['created_by' => Session::get('uid'),'created_at' => date('Y-m-d H:i:s'), 'itemid' => $newclientid,'type' => $this->type,'text' => $request->comment ]);
            return redirect(url('admin/Clients/Clients_view/'. $newclientid))->with('message', 'تم بنجاح');
        }


    }



    public function catedit($id)

    {
        RolesController::checkroles('4');
        $clients = DB::table('clients')->where('id',$id)->get();
        $user = DB::table('users')->get();
        $gender = DB::table('gender')->get();
        $Interest = DB::table('interest')->get();
        $products = DB::table('products')->get();
        $leads = DB::table('lead')->get();
        $nationalitys = DB::table('pages')->where('type','nationality')->get();



        return view('admin.'.$this->module.'.catedit',compact('tasks','comment','clients','user','gender','Interest','products','leads','nationalitys'));

    }
    public function view($id)

    {
        RolesController::checkroles('2');
        $gender = DB::table('gender')->get();
        $Interest = DB::table('interest')->get();
        $products = DB::table('products')->get();
        $leads = DB::table('lead')->get();
        $nationalitys = DB::table('pages')->where('type','nationality')->get();
        /* 10/3/2020 */
        $wants                  = DB::table('wants')->get();
        $service_types          = DB::table('service_type')->get();
        $call_status            = DB::table('call_status')->get();
        $not_interested         = DB::table('not_interested')->get();
        $interested_types       = DB::table('interested_type')->get();
        

        $clients = DB::table('clients')->where('id',$id)->get();

        $user = DB::table('users')->get();

        $comment = DB::table('comments')->where([['itemid',$id],['type',$this->type]])->orderBy('id','desc')->get();
        for ($i=0;$i<count($comment);$i++)
        {
            $username = DB::table('users')->where('id',$comment[$i]->created_by)->get();
            $comment[$i]->username = $username[0]->name;
        }

        $Needs = DB::table('Need')->orderBy('id','desc')->get();
        $ClientNeeds = DB::table('clientsvsneeds')->where([['status','1'],['client',$id]])->orderBy('id','desc')->get();
        for ($i=0;$i<count($ClientNeeds);$i++)
        {
            $need = DB::table('Need')->where('id',$ClientNeeds[$i]->need)->get();
            $ClientNeeds[$i]->clientneed = $need[0]->Need;
            // dd($ClientNeeds);
        }
        $tags   = DB::table('tags')->orderBy('id','desc')->get();
        $ClientTags = DB::table('clientsvstags')->where([['status','1'],['client',$id]])->orderBy('id','desc')->get();
        for ($i=0;$i<count($ClientTags);$i++)
        {
            $tag = DB::table('tags')->where('id',$ClientTags[$i]->tag)->get();
            $ClientTags[$i]->clienttag = $tag[0]->type;
            // dd($ClientNeeds);
        }

        $tasks = DB::table('tasks')->where([['itemid',$id],['type','client']])->orderBy('id','desc')->get();
            for ($i=0;$i<count($tasks);$i++)
            {
                $username = DB::table('users')->where('id',$tasks[$i]->created_by)->get();
                $tasks[$i]->username = $username[0]->name;
                $tasks[$i]->logo = $username[0]->logo;
                $assigned_to = DB::table('users')->where('id',$tasks[$i]->assigned_to)->get();
                $tasks[$i]->assigned_to = $assigned_to[0]->name;
            }

        return view('admin.'.$this->module.'.catview',compact('tasks','comment','clients','user','gender','Interest','Needs','leads','ClientTags','tags','ClientNeeds','gender','Interest','products','leads','nationalitys','wants','tags','service_types','call_status','not_interested','interested_types'));

    }



    public function catupdate(Request $request)

    {
        RolesController::checkroles('4');
        unset($_POST['_token']);
        unset($_POST['comment']);

        DB::table('clients')->where('id', $request->id)->update($_POST);

        return redirect(url('admin/Clients/Clients_view/'. $request->id))->with('message', 'تم بنجاح');

    }

    public function comment(Request $request)

    {
        RolesController::checkroles('3');
        unset($_POST['_token']);

        DB::table('clients')->where('id', $request->id)->get();
        $id = Auth::user()->id ;
        //dd($id);
        DB::table('comments')->insert(['created_by' => $id, 'created_at' => date('Y-m-d H:i:s'), 'itemid' => $request->id,'type' => $this->type,'text' => $request->comment ]);

        return redirect(url('admin/Clients/Clients_view/'. $request->id))->with('message', 'تم بنجاح');


    }
    public function Needs (Request $request)

    {
        RolesController::checkroles('3');
        unset($_POST['_token']);

        $_POST['created_at'] = date('Y-m-d H:i:s');
        $_POST['created_by'] = Session::get('uid');

        DB::table('clientsvsneeds')->insert($_POST);
        return redirect(url('admin/Clients/Clients_view/'. $request->client))->with('message', 'تم بنجاح');


    }
    public function deleteNeeds (Request $request)

    {
        RolesController::checkroles('3');
        unset($_POST['_token']);


        DB::table('clientsvsneeds')->where('id',$request->id)->update(['status'=>0,'deleted_by' => Session::get('id'), 'deleted_at' => date('Y-m-d H:i:s') ]);
        return redirect(url('admin/Clients/Clients_view/'. $request->cid))->with('message', 'تم بنجاح');


    }
    public function Tags (Request $request)

    {
        RolesController::checkroles('3');
        unset($_POST['_token']);

        $_POST['created_at'] = date('Y-m-d H:i:s');
        $_POST['created_by'] = Session::get('uid');

        DB::table('clientsvstags')->insert($_POST);
        return redirect(url('admin/Clients/Clients_view/'. $request->client))->with('message', 'تم بنجاح');


    }
    public function deleteTags (Request $request)

    {
        RolesController::checkroles('3');
        unset($_POST['_token']);


        DB::table('clientsvstags')->where('id',$request->id)->update(['status'=>0,'deleted_by' => Session::get('id'), 'deleted_at' => date('Y-m-d H:i:s') ]);
        return redirect(url('admin/Clients/Clients_view/'. $request->cid))->with('message', 'تم بنجاح');


    }

    public function tasks(Request $request)

    {
        RolesController::checkroles('3');
        unset($_POST['_token']);

        DB::table('clients')->where('id', $request->id)->get();
        $id = Auth::user()->id ;
        $logopath = "";
            if ($request->file('pic') != null){
                $imgname = $request->file('pic')->getClientoriginalName();
                $imgpath = $request->pic;

                $target = 'uploads/' . $imgname;
                $logopath = $target;
                move_uploaded_file($imgpath,$target);
            }

        DB::table('tasks')->insert(
            ['created_by' => $id,'created_at' => date('Y-m-d H:i:s'), 'itemid' => $request->id,'title' => $request->title,'pic' => $logopath,'type' => 'client','assigned_to' => $request->assigned_to,'assigned_at' => $request->assigned_at ,'text' => $request->text ]);

        return redirect(url('admin/Clients/Clients_view/'. $request->id))->with('message', 'تم بنجاح');

    }

    public function search(Request $request)

    {
        RolesController::checkroles('2');
        $searchkey = $request->searchkey;
        $search = DB::table('clients')->where([['name','like','%'.$searchkey .'%'],['status',1]])
        ->orwhere([['name_en','like','%'.$searchkey .'%'],['status',1]])
        ->orwhere([['number','like','%'.$searchkey.'%'],['status',1]])
        ->orwhere([['id','like','%'.$searchkey.'%'],['status',1]])
        ->orwhere([['number2','like','%'.$searchkey.'%'],['status',1]])
        ->orwhere([['email','like','%'.$searchkey.'%'],['status',1]])->limit(100)->get();
        foreach($search as $k=>$v)
        {
            $user = DB::table('users')->select('name')->where('id',$v->assigned_to)->get();
            $search[$k]->assigned_to = $user[0]->name;
        }
        echo json_encode($search);
    }
    public function searchuser(Request $request)

    {
        RolesController::checkroles('2');
        $uid = $request->uid;
        $searchkey = $request->searchkey;
        $search = DB::table('clients')->where([['name','like','%'.$searchkey .'%'],['status',1],['assigned_to',$uid]])
        ->orwhere([['name_en','like','%'.$searchkey .'%'],['status',1],['assigned_to',$uid]])
        ->orwhere([['number','like','%'.$searchkey.'%'],['status',1],['assigned_to',$uid]])
        ->orwhere([['id','like','%'.$searchkey.'%'],['status',1],['assigned_to',$uid]])
        ->orwhere([['number2','like','%'.$searchkey.'%'],['status',1],['assigned_to',$uid]])
        ->orwhere([['email','like','%'.$searchkey.'%'],['status',1],['assigned_to',$uid]])->limit(50)->get();
        $user = DB::table('users')->select('name')->where('id',$uid)->get();
        $search->assigned_to = $user[0]->name;
// dd($search);
        echo json_encode($search);
    }

    public function searchbyuser(Request $request)

    {
        $userid = $request->user;
        $from = $request->from;
        $to = $request->to;

        if($from == ''){
        $from = '0000-00-00 00:00:00';
        }

        if($to == ''){
        $to = date('Y-m-d H:i:s');
        }
        // dd($to);

        $countallclients = DB::table('clients')->where([['status',1],['assigned_to',$userid]])->get();

        $clientsbyuser = DB::table('clients')->wherebetween('created_at',[$from,$to])->where([['status',1],['assigned_to',$userid]])->orderBy('id','desc')->paginate(50);
        $searchuser = DB::table('users')->select('name')->where('id',$userid)->get();
        $clientsbyuser->assigned_to = $searchuser[0]->name;
        $clientsbyuser->assigned_to_id = $userid;
        $clientsbyuser->from = $from;
        $clientsbyuser->to = $to;

        $users = DB::table('users')->get();
        $count = floor(count($clientsbyuser)/4);
        $remain = count($clientsbyuser) - (4 * $count);

        return view('admin.'.$this->module.'.clientsearch',compact('clientsbyuser','users','s'));

    }



    public function catdel($catid)

    {
        RolesController::checkroles('5');

        $cat = DB::table('clients')->where('id',$catid)->update(['status'=>0,'deleted_by' => Session::get('username'), 'deleted_at' => date('Y-m-d H:i:s') ]);

		return redirect(url('/admin/'.$this->module.'/index'))->with('message', 'تم بنجاح');

    }




}
