<?php

namespace App\Http\Controllers\admin;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ConverterController extends Controller
{

    public function __construct()
       {


       }

       public function servicetime()
        {
            $oldperiods = DB::table('services_period')->get();
            foreach ($oldperiods as $oldperiod){

                $id = $oldperiod->id;

                $start =  $oldperiod->from ;
                $start = explode(' ',$start);
                $start = explode('-',$start[0]);
                $start = $start[2].'-'.$start[1].'-'.$start[0];

                $end =  $oldperiod->to ;
                $end = explode(' ',$end);
                $end = explode('-',$end[0]);
                $end = $end[2].'-'.$end[1].'-'.$end[0];

// DD($end);
            print $id .'||'.$start .'||'.$end.'<br>';
                DB::table('services_period')->where('id',$id)->update(['start' => $start,'end' => $end]);

            }
            //dd('Done PERIOD');

       }
       public function clients()
       {
           $oldclient = DB::table('old_clients')->get();
           //dd($oldclient);
           foreach ($oldclient as $client){
                   $oldid = $client->id;
                   $name =  $client->FirstName ." ".$client->LastName ;
                   $phone =  $client->PrimaryPhone ;
                   $email =  $client->PrimaryEmail;
                   $Description =  $client->Description ;
                   $oldnotes =  $client->OldNotes ;
                   $created_at =  $client->created_at ;
                   $created_at = explode(' ',$created_at);
                   $created_at = explode('-',$created_at[0]);
                   $created_at = $created_at[2].'-'.$created_at[1].'-'.$created_at[0].' 00:00:00';


                   if ($client->Gender  == 'Female'){
                       $gender = 2;
                   }
                   elseif  ($client->Gender  == 'Male'){
                       $gender = 1;
                   }

                   if ($client->Interest  == 'Interested'){
                       $Interest = 3;
                   }
                   elseif  ($client->Interest  == 'Very Interested'){
                       $Interest = 1;
                   }

                   elseif  ($client->Interest  == '0'){
                       $Interest = 2;
                   }
                   elseif  ($client->Interest  == 'Not Interested'){
                       $Interest = 4;
                   }

                   if ($client->LeadSource  == 'Other'){
                       $leads = 6;
                   }
                   elseif  ($client->LeadSource  == 'Youtube'){
                       $leads = 7;
                   }
                   elseif  ($client->LeadSource  == 'Instagram'){
                       $leads = 2;
                   }
                   elseif  ($client->LeadSource  == 'Twitter'){
                       $leads = 3;
                   }
                   elseif  ($client->LeadSource  == 'Tel Sales'){
                       $leads = 8;
                   }
                   elseif  ($client->LeadSource  == 'Ka3det Mosweken'){
                       $leads = 9;
                   }
                   elseif  ($client->LeadSource  == 'Attendance In Academy'){
                       $leads = 10;
                   }
                   elseif  ($client->LeadSource  == 'Recommendation Customer'){
                       $leads = 11;
                   }
                   elseif  ($client->LeadSource  == 'Relationships'){
                       $leads = 12;
                   }
                   elseif  ($client->LeadSource  == 'Incoming Calls'){
                       $leads = 13;
                   }
                   elseif  ($client->LeadSource  == 'Website'){
                       $leads = 14;
                   }

                   if ($client->Nationalety  == 'مصر'){
                       $Nationalety = 1;
                   }
                   elseif  ($client->Nationalety  == 'الأردن'){
                       $Nationalety = 2;
                   }
                   elseif  ($client->Nationalety  == '0'){
                       $Nationalety = 3;
                   }
                   elseif  ($client->Nationalety  == 'الإمارات'){
                       $Nationalety = 4;
                   }
                   elseif  ($client->Nationalety  == 'السعودية'){
                       $Nationalety = 5;
                   }
                   elseif  ($client->Nationalety  == 'الامارات'){
                       $Nationalety = 4;
                   }
                   elseif  ($client->Nationalety  == 'الكويت'){
                       $Nationalety = 6;
                   }
                   elseif  ($client->Nationalety  == 'قطر'){
                       $Nationalety = 7;
                   }
                   elseif  ($client->Nationalety  == 'اليمن'){
                       $Nationalety = 8;
                   }
                   elseif  ($client->Nationalety  == 'الاردن'){
                       $Nationalety = 2;
                   }
                   elseif  ($client->Nationalety  == 'السودان'){
                       $Nationalety = 9;
                   }
                   elseif  ($client->Nationalety  == 'المغرب'){
                       $Nationalety = 10;
                   }
                   elseif  ($client->Nationalety  == 'عمان'){
                       $Nationalety = 11;
                   }
                   elseif  ($client->Nationalety  == 'البحرين'){
                       $Nationalety = 12;
                   }
                   elseif  ($client->Nationalety  == 'الجزائر'){
                       $Nationalety = 13;
                   }
                   elseif  ($client->Nationalety  == 'لبنان'){
                       $Nationalety = 14;
                   }
                   elseif  ($client->Nationalety  == 'سوريا'){
                       $Nationalety = 15;
                   }
                   elseif  ($client->Nationalety  == 'ليبيا'){
                       $Nationalety = 16;
                   }
                   elseif  ($client->Nationalety  == 'تونس'){
                       $Nationalety = 17;
                   }
                   elseif  ($client->Nationalety  == 'العراق'){
                       $Nationalety = 18;
                   }
                   elseif  ($client->Nationalety  == 'فلسطين'){
                       $Nationalety = 19;
                   }
                   elseif  ($client->Nationalety  == 'غير ذلك'){
                       $Nationalety = 20;
                   }



                   $address = $client->Street ." ".$client->City ." ".$client->Area ." ".$client->Country ;

                    if ($client->AssignedTo  == 'Mohamed Hamada'){
                       $AssignedTo = 4;
                    }elseif  ($client->AssignedTo  == 'Mohamed Abd ElKawy'){
                       $AssignedTo = 6;
                   }

                   else {

                       $AssignedTo = 3;
                   }



                   //dd($client->Nationalety);

                DB::table('clients')->insert(['oldnotes' => $oldnotes,'Description' => $Description,'created_at' => $created_at,'name_en' => $name, 'oldid' => $oldid,'email' => $email,'number' => $phone,'assigned_to' => $AssignedTo,'gender' => $gender, 'Interest' => $Interest, 'lead' => $leads, 'nationality' => $Nationalety, 'address' => $address, 'created_by' => '1' ]);
                // print $id .'||'.$start .'||'.$end.'<br>';
                dd('done');
            }

       }

       public function clients1()
       {
           $oldclient = DB::table('clients2')->get();
           //dd($oldclient);
           foreach ($oldclient as $client){
                   $oldid = $client->id;
                   $name =  $client->name;
                   $phone =  $client->phone ;
                   $email =  $client->email;
                   $Description =  $client->Description ;

                   $created_at = '2020-06-10 00:00:00';


                   if ($client->gender  == 'Female'){
                       $gender = 2;
                   }
                   elseif  ($client->gender  == 'Male'){
                       $gender = 1;
                   }else{
                       $gender = 1;
                   }

                   if ($client->Interest  == 'interested'){
                       $Interest = 3;
                   }
                   elseif  ($client->Interest  == 'very interested'){
                       $Interest = 1;
                   }

                   elseif  ($client->Interest  == '0'){
                       $Interest = 2;
                   }
                   elseif  ($client->Interest  == 'not interested'){
                       $Interest = 4;
                   }else{
                    $Interest = 2;
                   }


                   if ($client->lead  == 'Other'){
                       $leads = 6;
                   }
                   elseif  ($client->lead  == 'Youtube'){
                       $leads = 7;
                   }
                   elseif  ($client->lead  == 'Instagram'){
                       $leads = 2;
                   }
                   elseif  ($client->lead  == 'Twitter'){
                       $leads = 3;
                   }
                   elseif  ($client->lead  == 'Tel Sales'){
                       $leads = 8;
                   }
                   elseif  ($client->lead  == 'Ka3det Mosweken'){
                       $leads = 9;
                   }
                   elseif  ($client->lead  == 'Attendance In Academy'){
                       $leads = 10;
                   }
                   elseif  ($client->lead  == 'Recommendation Customer'){
                       $leads = 11;
                   }
                   elseif  ($client->lead  == 'Relationships'){
                       $leads = 12;
                   }
                   elseif  ($client->lead  == 'Incoming Calls'){
                       $leads = 13;
                   }
                   elseif  ($client->lead  == 'Web'){
                       $leads = 14;
                   }else{
                    $leads = 14;
                   }

                   if ($client->nationality  == 'مصر'){
                       $nationality = 1;
                   }
                   elseif  ($client->nationality  == 'الأردن'){
                       $nationality = 2;
                   }
                   elseif  ($client->nationality  == '0'){
                       $nationality = 3;
                   }
                   elseif  ($client->nationality  == 'الإمارات'){
                       $nationality = 4;
                   }
                   elseif  ($client->nationality  == 'السعودية'){
                       $nationality = 5;
                   }
                   elseif  ($client->nationality  == 'الامارات'){
                       $nationality = 4;
                   }
                   elseif  ($client->nationality  == 'الكويت'){
                       $nationality = 6;
                   }
                   elseif  ($client->nationality  == 'قطر'){
                       $nationality = 7;
                   }
                   elseif  ($client->nationality  == 'اليمن'){
                       $nationality = 8;
                   }
                   elseif  ($client->nationality  == 'الاردن'){
                       $nationality = 2;
                   }
                   elseif  ($client->nationality  == 'السودان'){
                       $nationality = 9;
                   }
                   elseif  ($client->nationality  == 'المغرب'){
                       $nationality = 10;
                   }
                   elseif  ($client->nationality  == 'عمان'){
                       $nationality = 11;
                   }
                   elseif  ($client->nationality  == 'البحرين'){
                       $nationality = 12;
                   }
                   elseif  ($client->nationality  == 'الجزائر'){
                       $nationality = 13;
                   }
                   elseif  ($client->nationality  == 'لبنان'){
                       $nationality = 14;
                   }
                   elseif  ($client->nationality  == 'سوريا'){
                       $nationality = 15;
                   }
                   elseif  ($client->nationality  == 'ليبيا'){
                       $nationality = 16;
                   }
                   elseif  ($client->nationality  == 'تونس'){
                       $nationality = 17;
                   }
                   elseif  ($client->nationality  == 'العراق'){
                       $nationality = 18;
                   }
                   elseif  ($client->nationality  == 'فلسطين'){
                       $nationality = 19;
                   }
                   elseif  ($client->nationality  == 'غير ذلك'){
                       $nationality = 20;
                   }



                   $address ='null' ;

                    if($client->assigned_to == 'Mohamed Abd Elkawy'){
                       $AssignedTo = 6;
                    }elseif  ($client->assigned_to == 'lamees mohamed'){
                       $AssignedTo = 8;
                    }else {
                       $AssignedTo = 3;
                    }



                   //dd($client->nationality);

                DB::table('clients')->insert(['Description' => $Description,'created_at' => $created_at,'name' => $name, 'oldid' => $oldid,'email' => $email,'number' => $phone,'assigned_to' => $AssignedTo,'gender' => $gender, 'Interest' => $Interest, 'lead' => $leads, 'nationality' => $nationality, 'address' => $address, 'created_by' => '3' ]);

                // print $id .'||'.$start .'||'.$end.'<br>';
            }
            dd('done');

       }
       public function oldclientsno()
       {


           $oldnumber = DB::table('old_clients_no')->get();


           foreach ($oldnumber as $oldnumbers){
                $id = $oldnumbers->id;
                $number =  $oldnumbers->number ;

                DB::table('clients')
                ->where('oldid', $id)
                ->update(['number2' => $number]);


            }
            dd('Done Numbers');

       }
       public function oldclientvsservices()
       {


           $oldpro = DB::table('cvss')->where([['PM','!=','-'],['PID','=','']])->get();


           foreach ($oldpro as $pro){
                $id = $pro->id;
                $pm =  $pro->PM ;
                $pronum = DB::table('allpro')->select('ProductNumber')->where('ProductName',$pm)->get();
                // dd($pronum[0]->ProductNumber);
                if(!empty($pronum)){

                DB::table('cvss')
                ->where('id', $id)
                ->update(['PID' => $pronum[0]->ProductNumber]);
                }

            }
            dd('Done OLD CVSS');

       }

       public function clientvsservices()
       {

           $oldpro = DB::table('cvss')->where('PID','!=',' ')->get();
           foreach ($oldpro as $pro){
                $cid = $pro->CID;
                $pid =  $pro->PID ;
                $client = DB::table('clients')->select('id')->where('oldid',$cid)->get();

                $services = DB::table('services')->select('id')->where('oldid',$pid)->get();

                    DB::table('clientsvsservices')->insert(['client' => $client[0]->id,'service' => $services[0]->id]);

            }

            dd('Done Client vs Services!');
       }
       public function clientvsneeds()
       {
           $oldneeds = DB::table('old_needs')->get();

           foreach ($oldneeds as $needs){
                $cid = $needs->cid;
                $need =  $needs->need ;

                $client = DB::table('clients')->select('id')->where('oldid',$cid)->get();
                $needss = DB::table('need')->select('id')->where('Need',$need)->get();

                    DB::table('clientsvsneeds')->insert(['client' => $client[0]->id,'need' => $needss[0]->id]);

            }

            dd('Done Client vs Needs!');
       }

       public function clientcomments()
       {

        $oldcomments = DB::table('oldcomments')->orderby('id','asc')->get();
           foreach ($oldcomments as $oldcomment){
                $id = $oldcomment->id;
                $text = $oldcomment->Comment;
                if ($oldcomment->Creator  == 'Mohamed Hamada'){
                    $Creator = 4;
                }
                elseif  ($oldcomment->Creator  == 'Walid Basha'){
                    $Creator = 5;
                }
                elseif  ($oldcomment->Creator  == 'Mohamed Abd ElKawy'){
                    $Creator = 6;
                }
                elseif  ($oldcomment->Creator  == 'Rana Mostafa'){
                    $Creator = 7;
                }
                elseif  ($oldcomment->Creator  == 'Manar Habashy'){
                    $Creator = 8;
                }
                elseif  ($oldcomment->Creator  == 'ibrahem El-Bahrawy'){
                    $Creator = 9;
                }
                elseif  ($oldcomment->Creator  == 'aya aya'){
                    $Creator = 10;
                }
                elseif  ($oldcomment->Creator  == 'Sara Muhammad'){
                    $Creator = 11;
                }
                elseif  ($oldcomment->Creator  == 'Muhammed Muhammed'){
                    $Creator = 12;
                }
                elseif  ($oldcomment->Creator  == 'Ahmed Nasar'){
                    $Creator = 13;
                }
                elseif  ($oldcomment->Creator  == 'Mohamed Sobhy'){
                    $Creator = 14;
                }
                elseif  ($oldcomment->Creator  == 'mohamed hassan'){
                    $Creator = 15;
                }
                elseif  ($oldcomment->Creator  == 'Aya Medhat'){
                    $Creator = 16;
                }
                elseif  ($oldcomment->Creator  == 'mahmoud elgohary'){
                    $Creator = 17;
                }
                elseif  ($oldcomment->Creator  == 'Hossam Hossam'){
                    $Creator = 18;
                }
                elseif  ($oldcomment->Creator  == 'mohsen mohsen'){
                    $Creator = 19;
                }
                elseif  ($oldcomment->Creator  == 'yossef yossef'){
                    $Creator = 20;
                }
                elseif  ($oldcomment->Creator  == 'Amr Shaker'){
                    $Creator = 21;
                }
                elseif  ($oldcomment->Creator  == 'Mohamed Abd Elsalam'){
                    $Creator = 22;
                }
                elseif  ($oldcomment->Creator  == 'admin test'){
                    $Creator = 23;
                }
                elseif  ($oldcomment->Creator  == 'SubAdmin SubAdmin'){
                    $Creator = 24;
                }
                elseif  ($oldcomment->Creator  == 'Ehab Mohamed'){
                    $Creator = 25;
                }
                else {

                    $Creator = 1;
                }


                $RelatedTo =  $oldcomment->RelatedTo ;
                $CreatedTime =  $oldcomment->CreatedTime ;
                $CreatedTime = explode(' ',$CreatedTime);
                $CreatedTime = explode('-',$CreatedTime[0]);
                $CreatedTime = $CreatedTime[2].'-'.$CreatedTime[1].'-'.$CreatedTime[0].' 00:00:00';

                //DD($CreatedTime);

                if ($oldcomment->AssignedTo  == 'Mohamed Hamada'){
                    $AssignedTo = 4;
                }
                elseif  ($oldcomment->AssignedTo  == 'Walid Basha'){
                    $AssignedTo = 5;
                }
                elseif  ($oldcomment->AssignedTo  == 'Mohamed Abd ElKawy'){
                    $AssignedTo = 6;
                }
                elseif  ($oldcomment->AssignedTo  == 'Rana Mostafa'){
                    $AssignedTo = 7;
                }
                elseif  ($oldcomment->AssignedTo  == 'Manar Habashy'){
                    $AssignedTo = 8;
                }
                elseif  ($oldcomment->AssignedTo  == 'ibrahem El-Bahrawy'){
                    $AssignedTo = 9;
                }
                elseif  ($oldcomment->AssignedTo  == 'aya aya'){
                    $AssignedTo = 10;
                }
                elseif  ($oldcomment->AssignedTo  == 'Samar Muhammad'){
                    $AssignedTo = 11;
                }
                elseif  ($oldcomment->AssignedTo  == 'Muhammed Muhammed'){
                    $AssignedTo = 12;
                }
                elseif  ($oldcomment->AssignedTo  == 'Ahmed Nasar'){
                    $AssignedTo = 13;
                }
                elseif  ($oldcomment->AssignedTo  == 'Mohamed Sobhy'){
                    $AssignedTo = 14;
                }
                elseif  ($oldcomment->AssignedTo  == 'mohamed hassan'){
                    $AssignedTo = 15;
                }
                elseif  ($oldcomment->AssignedTo  == 'Aya Medhat'){
                    $AssignedTo = 16;
                }
                elseif  ($oldcomment->AssignedTo  == 'mahmoud elgohary'){
                    $AssignedTo = 17;
                }
                elseif  ($oldcomment->AssignedTo  == 'Hossam Hossam'){
                    $AssignedTo = 18;
                }
                elseif  ($oldcomment->AssignedTo  == 'mohsen mohsen'){
                    $AssignedTo = 19;
                }
                elseif  ($oldcomment->AssignedTo  == 'yossef yossef'){
                    $AssignedTo = 20;
                }
                elseif  ($oldcomment->AssignedTo  == 'Amr Shaker'){
                    $AssignedTo = 21;
                }
                elseif  ($oldcomment->AssignedTo  == 'Mohamed Abd Elsalam'){
                    $AssignedTo = 22;
                }
                elseif  ($oldcomment->AssignedTo  == 'admin test'){
                    $AssignedTo = 23;
                }
                elseif  ($oldcomment->AssignedTo  == 'SubAdmin SubAdmin'){
                    $AssignedTo = 24;
                }
                elseif  ($oldcomment->AssignedTo  == 'Ehab Mohamed'){
                    $AssignedTo = 25;
                }
                else {

                    $AssignedTo = 1;
                }

                $clientid = DB::table('clients')->where('name_en',$RelatedTo)->get();
                //dd($clientid);
                 if(!empty($clientid[0])){

                    DB::table('comments')->insert(['text' => $text,'itemid' => $clientid[0]->id,'created_by' => $Creator, 'oldid' => $id,'created_at' => $CreatedTime, 'type' => 'Clients']);
                }
            }
            DD('Done !!');
       }

       public function tasks()
       {

        $oldtasks = DB::table('oldtasks')->orderby('id','asc')->get();
           foreach ($oldtasks as $oldtask){
                $id = $oldtask->id;
                $title = $oldtask->CalendarSubject;
                $text = $oldtask->CalendarDescription;

                if ($oldtask->CalendarCreatedBy  == 'Mohamed Hamada'){
                    $Creator = 4;
                }
                elseif  ($oldtask->CalendarCreatedBy  == 'Walid Basha'){
                    $Creator = 5;
                }
                elseif  ($oldtask->CalendarCreatedBy  == 'Mohamed Abd ElKawy'){
                    $Creator = 6;
                }
                elseif  ($oldtask->CalendarCreatedBy  == 'Rana Mostafa'){
                    $Creator = 7;
                }
                elseif  ($oldtask->CalendarCreatedBy  == 'Manar Habashy'){
                    $Creator = 8;
                }
                elseif  ($oldtask->CalendarCreatedBy  == 'ibrahem El-Bahrawy'){
                    $Creator = 9;
                }
                elseif  ($oldtask->CalendarCreatedBy  == 'aya aya'){
                    $Creator = 10;
                }
                elseif  ($oldtask->CalendarCreatedBy  == 'Sara Muhammad'){
                    $Creator = 11;
                }
                elseif  ($oldtask->CalendarCreatedBy  == 'Muhammed Muhammed'){
                    $Creator = 12;
                }
                elseif  ($oldtask->CalendarCreatedBy  == 'Ahmed Nasar'){
                    $Creator = 13;
                }
                elseif  ($oldtask->CalendarCreatedBy  == 'Mohamed Sobhy'){
                    $Creator = 14;
                }
                elseif  ($oldtask->CalendarCreatedBy  == 'mohamed hassan'){
                    $Creator = 15;
                }
                elseif  ($oldtask->CalendarCreatedBy  == 'Aya Medhat'){
                    $Creator = 16;
                }
                elseif  ($oldtask->CalendarCreatedBy  == 'mahmoud elgohary'){
                    $Creator = 17;
                }
                elseif  ($oldtask->CalendarCreatedBy  == 'Hossam Hossam'){
                    $Creator = 18;
                }
                elseif  ($oldtask->CalendarCreatedBy  == 'mohsen mohsen'){
                    $Creator = 19;
                }
                elseif  ($oldtask->CalendarCreatedBy  == 'yossef yossef'){
                    $Creator = 20;
                }
                elseif  ($oldtask->CalendarCreatedBy  == 'Amr Shaker'){
                    $Creator = 21;
                }
                elseif  ($oldtask->CalendarCreatedBy  == 'Mohamed Abd Elsalam'){
                    $Creator = 22;
                }
                elseif  ($oldtask->CalendarCreatedBy  == 'admin test'){
                    $Creator = 23;
                }
                elseif  ($oldtask->CalendarCreatedBy  == 'SubAdmin SubAdmin'){
                    $Creator = 24;
                }
                elseif  ($oldtask->CalendarCreatedBy  == 'Ehab Mohamed'){
                    $Creator = 25;
                }
                else {

                    $Creator = 1;
                }


                $RelatedTo =  $oldtask->ContactsContactId;

                $assigned_at =  $oldtask->CalendarEndDate;
                $assigned_at = explode(' ',$assigned_at);
                $assigned_at = explode('-',$assigned_at[0]);
                $assigned_at = $assigned_at[2].'-'.$assigned_at[1].'-'.$assigned_at[0];

                $CreatedTime =  $oldtask->CalendarCreatedTime;
                $CreatedTime = explode(' ',$CreatedTime);
                $CreatedTime = explode('-',$CreatedTime[0]);
                $CreatedTime = $CreatedTime[2].'-'.$CreatedTime[1].'-'.$CreatedTime[0].' 00:00:00';

                //DD($CreatedTime);

                if ($oldtask->CalendarAssignedTo  == 'Mohamed Hamada'){
                    $AssignedTo = 4;
                }
                elseif  ($oldtask->CalendarAssignedTo  == 'Walid Basha'){
                    $AssignedTo = 5;
                }
                elseif  ($oldtask->CalendarAssignedTo  == 'Mohamed Abd ElKawy'){
                    $AssignedTo = 6;
                }
                elseif  ($oldtask->CalendarAssignedTo  == 'Rana Mostafa'){
                    $AssignedTo = 7;
                }
                elseif  ($oldtask->CalendarAssignedTo  == 'Manar Habashy'){
                    $AssignedTo = 8;
                }
                elseif  ($oldtask->CalendarAssignedTo  == 'ibrahem El-Bahrawy'){
                    $AssignedTo = 9;
                }
                elseif  ($oldtask->CalendarAssignedTo  == 'aya aya'){
                    $AssignedTo = 10;
                }
                elseif  ($oldtask->CalendarAssignedTo  == 'Samar Muhammad'){
                    $AssignedTo = 11;
                }
                elseif  ($oldtask->CalendarAssignedTo  == 'Muhammed Muhammed'){
                    $AssignedTo = 12;
                }
                elseif  ($oldtask->CalendarAssignedTo  == 'Ahmed Nasar'){
                    $AssignedTo = 13;
                }
                elseif  ($oldtask->CalendarAssignedTo  == 'Mohamed Sobhy'){
                    $AssignedTo = 14;
                }
                elseif  ($oldtask->CalendarAssignedTo  == 'mohamed hassan'){
                    $AssignedTo = 15;
                }
                elseif  ($oldtask->CalendarAssignedTo  == 'Aya Medhat'){
                    $AssignedTo = 16;
                }
                elseif  ($oldtask->CalendarAssignedTo  == 'mahmoud elgohary'){
                    $AssignedTo = 17;
                }
                elseif  ($oldtask->CalendarAssignedTo  == 'Hossam Hossam'){
                    $AssignedTo = 18;
                }
                elseif  ($oldtask->CalendarAssignedTo  == 'mohsen mohsen'){
                    $AssignedTo = 19;
                }
                elseif  ($oldtask->CalendarAssignedTo  == 'yossef yossef'){
                    $AssignedTo = 20;
                }
                elseif  ($oldtask->CalendarAssignedTo  == 'Amr Shaker'){
                    $AssignedTo = 21;
                }
                elseif  ($oldtask->CalendarAssignedTo  == 'Mohamed Abd Elsalam'){
                    $AssignedTo = 22;
                }
                elseif  ($oldtask->CalendarAssignedTo  == 'admin test'){
                    $AssignedTo = 23;
                }
                elseif  ($oldtask->CalendarAssignedTo  == 'SubAdmin SubAdmin'){
                    $AssignedTo = 24;
                }
                elseif  ($oldtask->CalendarAssignedTo  == 'Ehab Mohamed'){
                    $AssignedTo = 25;
                }
                else {

                    $AssignedTo = 1;
                }

                if  ($oldtask->ContactsContactId  == '-'){
                    $type = 'general';
                    $itemid= 0;
                }
                else {

                    $clientid = DB::table('clients')->where('oldid',$RelatedTo)->get();
                    $type = 'client';
                    $itemid= $clientid[0]->id;
                }

                if  ($oldtask->CalendarStatus  == 'Done'){
                    $status = 3;
                }
                else {

                    $status = 1;
                }
                //dd($clientid);
                 if(!empty($clientid[0])){

                    DB::table('tasks')->insert(
                    ['title' => $title,
                    'text' => $text,
                    'itemid' => $itemid,
                    'assigned_at' => $assigned_at,
                    'assigned_to' => $AssignedTo,
                    'created_by' => $Creator,
                    'oldid' => $id,
                    'created_at' => $CreatedTime,
                    'type' => $type,
                    'status' => $status]);
                }
            }
            DD('Done !!');
       }
       public function dateservices()
       {

        $services = DB::table('services')->get();

           foreach ($services as $service){
                $serviceid = $service->id;
                $servicedate =  $service->service_period;

                // $client = DB::table('services')->select('service_period')->where('id',$serviceid)->get();
                $date = DB::table('services_period')->select('from')->where('id',$servicedate)->get();

                    DB::table('services')->where('id',$serviceid)->update(['servicesdate' => $date[0]->from]);

            }

            dd('Done services');
       }
}
