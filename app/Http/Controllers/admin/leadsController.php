<?php



namespace App\Http\Controllers\admin;



use Validator;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;



class leadsController extends Controller

{

    public function __construct()

       {

           // parent::__construct();

            $this->type = 'leads';

            $this->module = 'leads';

            $this->picsnum = 5;

            $this->lang_arr = array("en" => "اللغة الانجليزية" );

            view()->share('module', $this->module );

            view()->share('active', 'settings');

            view()->share('titlepage','Leads');

            view()->share('picsnum', $this->picsnum);

            $id = Session::get('uid');
            $roles = DB::table('usersvsroles')->where("user_id",$id)->get();
            $rolesarr = array();
            for($i=0;$i<count($roles);$i++){
                $rolesarr[$i] = $roles[$i]->role_id;
            }
            view()->share('getroles', $rolesarr);

       }



    public function index()

    {
       RolesController::checkroles('53');
       $leads = DB::table('lead')->paginate(10);
        return view('admin.settings.leads',compact('leads'));
    }



    public function leadadd()

    {
        RolesController::checkroles('54');
        return view('admin.settings.leadsadd');
    }



    public function leadstore(Request $request)

    {
        RolesController::checkroles('54');
        unset($_POST['_token']);
        DB::table('lead')->insert($_POST);
        return redirect(url('admin/leads/index'))->with('message', 'تم بنجاح');

    }

    public function edit ($id)
    {
        RolesController::checkroles('55');
        $places = DB::table('lead')->where('id',$id)->get();
        return view('admin.settings.leadsedit',compact('cats','time','gender','places','products','leads','nationalitys'));
    }

    public function update(Request $request)
    {
        RolesController::checkroles('55');
        unset($_POST['_token']);
        DB::table('lead')->where('id', $request->id)->update($_POST);
        return redirect(url('admin/leads/index'))->with('message', 'تم بنجاح');
    }


    public function del($id)

    {
        RolesController::checkroles('56');
        $cat = DB::table('lead')->where('id',$id)->delete();
		return redirect(url('/admin/'.$this->module.'/index'))->with('message', 'تم بنجاح');
    }

}
