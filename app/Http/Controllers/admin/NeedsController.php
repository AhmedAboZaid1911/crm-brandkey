<?php



namespace App\Http\Controllers\admin;



use Validator;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;



class NeedsController extends Controller

{

    public function __construct()

       {

           // parent::__construct();

            $this->type = 'Needs';

            $this->module = 'Needs';

            $this->picsnum = 5;

            $this->lang_arr = array("en" => "اللغة الانجليزية" );

            view()->share('module', $this->module );

            view()->share('active', 'settings');

            view()->share('titlepage','Needs');

            view()->share('picsnum', $this->picsnum);



       }



    public function index()

    {
        RolesController::checkroles('53');
        $Needs = DB::table('Need')->paginate(10);
        return view('admin.settings.Needs',compact('Needs'));
    }



    public function Needadd()

    {
        RolesController::checkroles('54');
        return view('admin.settings.Needsadd');
    }



    public function Needstore(Request $request)

    {
        RolesController::checkroles('54');
        unset($_POST['_token']);
        DB::table('Need')->insert($_POST);
        return redirect(url('admin/Needs/index'))->with('message', 'تم بنجاح');

    }

    public function edit ($id)
    {
        RolesController::checkroles('55');
        $places = DB::table('Need')->where('id',$id)->get();
        return view('admin.settings.Needsedit',compact('cats','time','gender','places','products','Needs','nationalitys'));
    }

    public function update(Request $request)
    {
        RolesController::checkroles('55');
        unset($_POST['_token']);
        DB::table('Need')->where('id', $request->id)->update($_POST);
        return redirect(url('admin/Needs/index'))->with('message', 'تم بنجاح');
    }


    public function del($id)

    {
        RolesController::checkroles('56');
        $cat = DB::table('Need')->where('id',$id)->delete();
		return redirect(url('/admin/'.$this->module.'/index'))->with('message', 'تم بنجاح');
    }

}
