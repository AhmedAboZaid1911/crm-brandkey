<?php



namespace App\Http\Controllers\admin;



use Validator;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;



class NationalityController extends Controller

{

    public function __construct()

       {

           // parent::__construct();

            $this->type = 'nationality';

            $this->module = 'nationality';

            $this->picsnum = 5;

            $this->lang_arr = array("en" => "اللغة الانجليزية" );

            view()->share('module', $this->module );

            view()->share('active', 'settings');

            view()->share('titlepage','الجنسيات');

            view()->share('picsnum', $this->picsnum);


            $id = Session::get('uid');
            $roles = DB::table('usersvsroles')->where("user_id",$id)->get();
            $rolesarr = array();
            for($i=0;$i<count($roles);$i++){
                $rolesarr[$i] = $roles[$i]->role_id;
            }
            view()->share('getroles', $rolesarr);


       }



    public function index()

    {
        RolesController::checkroles('49');

        // dd(date('Y-m-d H:i:s')) ;
        $pages = DB::table('pages')->where('type',$this->type)->paginate(10);
        return view('admin.'.$this->module.'.index',compact('pages'));
    }



    public function add()

    {
        RolesController::checkroles('82');
        return view('admin.'.$this->module.'.add');
    }



    public function store(Request $request)

    {
        RolesController::checkroles('82');
        foreach($this->lang_arr as $k => $v)
        {
            $data['title_' . $k] = $request->{'title_' . $k};
            $rules['title_'.$k] = 'required|unique:pages';
        }
            $validator = Validator::make($data, $rules);
			if ($validator->fails()) {
				return redirect(url('/admin/'.$this->module.'/add'))->withErrors($validator)->withInput();
			}
            unset($_POST['_token']);
            $_POST['type'] = $this->type;
            $insertedid = DB::table('pages')->insertGetId($_POST);
        return redirect(url('/admin/'.$this->module.'/index'))->with('message', 'تم بنجاح');

    }



    public function edit($id)

    {
        RolesController::checkroles('50');
        $page = DB::table('pages')->where('id',$id)->get();
        $images = DB::table('images')->where([['type',$this->type],['itemid',$id]])->get();
        for ($i=0; $i < count($images); $i++) {
            $j=$i+1;
            $page[0]->{'pic'.$j} = $images[$i]->pic;
            foreach($this->lang_arr as $k => $v){
            $page[0]->{'pic' . $j . 'alt_' . $k} = $images[$i]->{'picalt_' . $k};
            }
            $page[0]->{'pic' . $j . 'id'} = $images[$i]->id;
        }
        return view('admin.'.$this->module.'.edit',compact('page'));

    }



    public function update(Request $request)

    {

        RolesController::checkroles('50');




        foreach($this->lang_arr as $k => $v)
        {
            $data['title_' . $k] = $request->{'title_' . $k};
            $rules['title_'.$k] = 'required';
        }
            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
                return redirect(url('/admin/'.$this->module.'/edit/' . $request->id))->withErrors($validator)->withInput();
            }
        unset($_POST['_token']);
        DB::table('pages')->where('id', $request->id)->update($_POST);
        return redirect(url('/admin/'.$this->module.'/index'))->with('message', 'تم بنجاح');

    }



    public function del($id)

    {
        RolesController::checkroles('51');
            DB::table('images')->where([['itemid',$id],['type',$this->type]])->delete();

            $cat = DB::table('pages')->where('id',$id)->delete();



			return redirect(url('/admin/'.$this->module.'/index'))->with('message', 'تم بنجاح');



    }

}
