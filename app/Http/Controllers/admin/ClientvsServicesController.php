<?php



namespace App\Http\Controllers\admin;



use Validator;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Auth;
use \Illuminate\Support\Facades\Session;


class ClientvsServicesController extends Controller

{



    public function __construct()

       {

           // parent::__construct();

            $this->type = 'Clients';

            $this->module = 'Clients';

            $this->picsnum = 5;

            $this->day = 15;

            $this->lang_arr = array("en" => "اللغة الانجليزية" );

            view()->share('module', $this->module);

            view()->share('active', 'Clients');



            view()->share('cattitlepage','العملاء');

            view()->share('posttitlepage','خدمات العميل ');

            view()->share('picsnum', $this->picsnum);

            view()->share('day', $this->day);

            $id = Session::get('uid');
            $roles = DB::table('usersvsroles')->where("user_id",$id)->get();
            $rolesarr = array();
            for($i=0;$i<count($roles);$i++){
                $rolesarr[$i] = $roles[$i]->role_id;
            }
            view()->share('getroles', $rolesarr);

       }



    public function cats()

    {

            $clients = DB::table('Clients')->paginate(10);

            foreach($clients as $k=>$v)
            {
                $user = DB::table('users')->select('name')->where('id',$v->assigned_to)->get();

                $clients[$k]->assigned_to = $user[0]->name;
            }
            return view('admin.'.$this->module.'.cats',compact('clients','user'));



    }



    public function catadd($id)

    {

        $clients = DB::table('clients')->where('id',$id)->get();

        $user = DB::table('users')->get();

        $clientvsservices = DB::table('clientsvsservices')->where([['client',$id],['status','!=','0']])->orderBy('id','desc')->get();
        foreach($clientvsservices as $k=>$v){
            $sid = $v->service;
            $serviceinfo = DB::table('services')->where('id',$sid)->get();

            $time = DB::table('services_time')->select('name')->where('id',$serviceinfo[0]->service_time)->get();
            $clientvsservices[$k]->time = $time[0]->name;

            // $periods = DB::table('services_period')->where('id',$serviceinfo[0]->service_period)->get();
            // $clientvsservices[$k]->from = $periods[0]->from;
            // $clientvsservices[$k]->to = $periods[0]->to;

            $places = DB::table('services_place')->select('name')->where('id',$serviceinfo[0]->service_place)->get();
            $clientvsservices[$k]->place = $places[0]->name;

            $products = DB::table('products')->select('name')->where('id',$serviceinfo[0]->service_name)->get();
            $clientvsservices[$k]->name = $products[0]->name;

            $price = DB::table('services')->where('id',$serviceinfo[0]->id)->get();
            $clientvsservices[$k]->cost = $price[0]->service_price;

            $servicesdate = DB::table('services')->where('id',$serviceinfo[0]->id)->get();
            $clientvsservices[$k]->servicesdate = $servicesdate[0]->servicesdate;

            $currency = DB::table('services_currency')->select('currency')->where('id',$serviceinfo[0]->service_currency)->get();
            $clientvsservices[$k]->currency = $currency[0]->currency;

            $days = DB::table('services_days')->where('id',$serviceinfo[0]->service_days)->get();
            $clientvsservices[$k]->day1 = $days[0]->day1;
            $clientvsservices[$k]->day2 = $days[0]->day2;
            $clientvsservices[$k]->day3 = $days[0]->day3;
            $clientvsservices[$k]->day4 = $days[0]->day4;
            $clientvsservices[$k]->day5 = $days[0]->day5;
            $clientvsservices[$k]->day6 = $days[0]->day6;
            $clientvsservices[$k]->day7 = $days[0]->day7;
        }

        // dd($clientvsservices);

        $Services = DB::table('services')->orderBy('id','desc')->get();

        foreach($Services as $k=>$v)
        {
            $time = DB::table('services_time')->select('name')->where('id',$v->service_time)->get();
            $Services[$k]->time = $time[0]->name;

            // $periods = DB::table('services_period')->where('id',$v->service_period)->get();
            // // dd($v);
            // $Services[$k]->from = $periods[0]->from;
            // $Services[$k]->to = $periods[0]->to;

            $servicesdate = DB::table('services')->where('id',$v->id)->get();
            $Services[$k]->servicesdate = $servicesdate[0]->servicesdate;

            $places = DB::table('services_place')->select('name')->where('id',$v->service_place)->get();
            $Services[$k]->place = $places[0]->name;

            $products = DB::table('products')->select('name')->where('id',$v->service_name)->get();
            $Services[$k]->name = $products[0]->name;

            $currency = DB::table('services_currency')->select('currency')->where('id',$v->service_currency)->get();
            $Services[$k]->currency = $currency[0]->currency;

            $days = DB::table('services_days')->where('id',$v->service_days)->get();
            $Services[$k]->day1 = $days[0]->day1;
            $Services[$k]->day2 = $days[0]->day2;
            $Services[$k]->day3 = $days[0]->day3;
            $Services[$k]->day4 = $days[0]->day4;
            $Services[$k]->day5 = $days[0]->day5;
            $Services[$k]->day6 = $days[0]->day6;
            $Services[$k]->day7 = $days[0]->day7;
        }



        $currency = DB::table('services_currency')->get();

        return view('admin.client-services.catadd',compact('currency','clientvsservices','Services','clients','cats','user','gender','Interest','products','leads','nationalitys'));

    }



    public function catstore(Request $request)

    {

        unset($_POST['_token']);
        $_POST['created_at'] = date('Y-m-d H:i:s');
        $_POST['created_by'] = Auth::user()->id ;
        DB::table('clientsvsservices')->insert($_POST);

        return redirect(url('admin/Clients/Clients_view/'. $request->client))->with('message', 'تم بنجاح');

    }


    public function comment(Request $request)

    {
        unset($_POST['_token']);

        DB::table('clientsvsservices')->where('id', $request->id)->get();
        $id = Auth::user()->id ;
        //dd($id);
        DB::table('comments')->insert(['created_by' => $id, 'created_at' => date('Y-m-d H:i:s'),'itemid' => $request->id,'type' => $this->type,'text' => $request->comment ]);

        return redirect(url('admin/ClientsVsServices/service/'. $request->id))->with('message', 'تم بنجاح');

    }
    public function tasks(Request $request)

    {
        unset($_POST['_token']);

        DB::table('clientsvsservices')->where('id', $request->id)->get();
        $id = Auth::user()->id ;
        $logopath = "";
            if ($request->file('pic') != null){
                $imgname = $request->file('pic')->getClientoriginalName();
                $imgpath = $request->pic;

                $target = 'uploads/' . $imgname;
                $logopath = $target;
                move_uploaded_file($imgpath,$target);
            }

        DB::table('tasks')->insert(
            ['created_by' => $id, 'created_at' => date('Y-m-d H:i:s'), 'itemid' => $request->id,'title' => $request->title,'pic' => $logopath,'type' => 'clientvsservices','assigned_to' => $request->assigned_to,'assigned_at' => $request->assigned_at ,'text' => $request->text ]);

        return redirect(url('admin/ClientsVsServices/service/'. $request->id))->with('message', 'تم بنجاح');

    }
    public function taskupdate(Request $request)

    {
        unset($_POST['_token']);
        DB::table('tasks')->where('id',$request->id)->update(['status'=>$request->case,'updated_by' => Session::get('username'),'updated_at' => date('Y-m-d H:i:s') ]);

        return redirect(url('admin/tasks/view/'. $request->id))->with('message', 'تم بنجاح');

    }


    public function services($id)

    {
        $comment = DB::table('comments')->where([['itemid',$id],['type',$this->type]])->orderBy('id','desc')->get();
        for ($i=0;$i<count($comment);$i++) {
            $username = DB::table('users')->where('id',$comment[$i]->created_by)->get();
            $comment[$i]->username = $username[0]->name;
            $comment[$i]->logo = $username[0]->logo;
        }

        $tasks = DB::table('tasks')->where([['itemid',$id],['type','clientvsservices']])->orderBy('id','desc')->get();
        for ($i=0;$i<count($tasks);$i++) {
            $username = DB::table('users')->where('id',$tasks[$i]->created_by)->get();
            $tasks[$i]->username = $username[0]->name;
            $tasks[$i]->logo = $username[0]->logo;
            $assigned_to = DB::table('users')->where('id',$tasks[$i]->assigned_to)->get();
            $tasks[$i]->assigned_to = $assigned_to[0]->name;
        }


        $user = DB::table('users')->get();

        $clientvsservices = DB::table('clientsvsservices')->where([['id',$id],['status','1']])->get();
        foreach($clientvsservices as $k=>$v){
            $sid = $v->service;
            $serviceinfo = DB::table('services')->where('id',$sid)->get();

            $price = DB::table('services')->where('id',$sid)->get();
            $clientvsservices[$k]->price1 = $price[0]->service_price;

            $time = DB::table('services_time')->select('name')->where('id',$serviceinfo[0]->service_time)->get();
            $clientvsservices[$k]->time = $time[0]->name;

            // $periods = DB::table('services_period')->where('id',$serviceinfo[0]->service_period)->get();
            // $clientvsservices[$k]->from = $periods[0]->from;
            // $clientvsservices[$k]->to = $periods[0]->to;
            
            $servicesdate = DB::table('services')->where('id',$serviceinfo[0]->id)->get();
            $clientvsservices[$k]->servicesdate = $servicesdate[0]->servicesdate;

            $places = DB::table('services_place')->select('name')->where('id',$serviceinfo[0]->service_place)->get();
            $clientvsservices[$k]->place = $places[0]->name;

            $products = DB::table('products')->select('name')->where('id',$serviceinfo[0]->service_name)->get();
            $clientvsservices[$k]->name = $products[0]->name;

            $currency1 = DB::table('services_currency')->select('currency')->where('id',$serviceinfo[0]->service_currency)->get();
            $clientvsservices[$k]->currency1 = $currency1[0]->currency;

            $currency = DB::table('services_currency')->select('currency')->where('id',$clientvsservices[0]->currency)->get();
            $clientvsservices[$k]->currency = $currency[0]->currency;

            $days = DB::table('services_days')->where('id',$serviceinfo[0]->service_days)->get();
            $clientvsservices[$k]->day1 = $days[0]->day1;
            $clientvsservices[$k]->day2 = $days[0]->day2;
            $clientvsservices[$k]->day3 = $days[0]->day3;
            $clientvsservices[$k]->day4 = $days[0]->day4;
            $clientvsservices[$k]->day5 = $days[0]->day5;
            $clientvsservices[$k]->day6 = $days[0]->day6;
            $clientvsservices[$k]->day7 = $days[0]->day7;
        }

        $clients = DB::table('clients')->where('id',$clientvsservices[0]->client)->get();

        $Services = DB::table('services')->orderBy('id','desc')->get();
        foreach($Services as $k=>$v)
        {
            $time = DB::table('services_time')->select('name')->where('id',$v->service_time)->get();
            $Services[$k]->time = $time[0]->name;

            // $periods = DB::table('services_period')->where('id',$v->service_period)->get();
            // $Services[$k]->from = $periods[0]->from;
            // $Services[$k]->to = $periods[0]->to;
            
            $servicesdate = DB::table('services')->where('id',$v->id)->get();
            $Services[$k]->servicesdate = $servicesdate[0]->servicesdate;

            $places = DB::table('services_place')->select('name')->where('id',$v->service_place)->get();
            $Services[$k]->place = $places[0]->name;

            $products = DB::table('products')->select('name')->where('id',$v->service_name)->get();
            $Services[$k]->name = $products[0]->name;

            $currency = DB::table('services_currency')->select('currency')->where('id',$v->service_currency)->get();
            $Services[$k]->currency = $currency[0]->currency;

            $days = DB::table('services_days')->where('id',$v->service_days)->get();
            $Services[$k]->day1 = $days[0]->day1;
            $Services[$k]->day2 = $days[0]->day2;
            $Services[$k]->day3 = $days[0]->day3;
            $Services[$k]->day4 = $days[0]->day4;
            $Services[$k]->day5 = $days[0]->day5;
            $Services[$k]->day6 = $days[0]->day6;
            $Services[$k]->day7 = $days[0]->day7;
        }

        $currency = DB::table('services_currency')->get();


        return view('admin.client-services.services',compact('tasks','comment','currency','clientvsservices','Services','clients','cats','user','gender','Interest','products','leads','nationalitys'));

    }




    public function catedit($id)

    {

            $clients = DB::table('clients')->where('id',$id)->get();

            $user = DB::table('user')->get();

            $gender = DB::table('gender')->get();

            $Interest = DB::table('Interest')->get();

            $products = DB::table('products')->get();

            $leads = DB::table('lead')->get();

            $nationalitys = DB::table('pages')->where('type','nationality')->get();

            return view('admin.'.$this->module.'.catedit',compact('clients','user','gender','Interest','products','leads','nationalitys'));

    }



    public function catupdate(Request $request)

    {
        //dd("hhh");
        unset($_POST['_token']);

        DB::table('clientsvsservices')->where('id', $request->id)->update(['service' => $request->service,'price' => $request->price,'currency' => $request->currency,'paid' => $request->paid,'remainderValue' => $request->remainderValue,'servicesstatus' => $request->servicesstatus]);
        //DB::table('clientsvsservices')->where('id', $request->id)->update($_POST);

        return redirect(url('admin/ClientsVsServices/service/'. $request->id))->with('message', 'تم بنجاح');
    }





    public function catdel(Request $request)

    {

        // dd($request->cid);


        DB::table('clientsvsservices')->where('id',$request->serid)->update(['status'=>0,'deleted_by' => Session::get('username'), 'deleted_at' => date('Y-m-d H:i:s') ]);

		return redirect(url('admin/ClientsVsServices/index/'. $request->cid))->with('message', 'تم بنجاح');

    }





}
