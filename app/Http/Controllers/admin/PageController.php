<?php

namespace App\Http\Controllers\admin;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class PageController extends Controller
{
    public function __construct()
       {
           // parent::__construct();
            $this->type = 'general';
            $this->module = 'page';
            $this->picsnum = 0;
            $this->lang_arr = array("en" => "اللغة الانجليزية" );
            view()->share('module', $this->module );
            view()->share('active', 'page');
            view()->share('titlepage','الصفحات');
            view()->share('picsnum', $this->picsnum);
       }

    public function index()
    {
            $pages = DB::table('pages')->where('type',$this->type)->paginate(10);
            return view('admin.'.$this->module.'.index',compact('pages'));
        
    }

    public function add()
    {
            return view('admin.'.$this->module.'.add');
        
    }

    public function store(Request $request)
    {
        foreach($this->lang_arr as $k => $v){

            if($request->{'url_' . $k} != ""){
                $_POST['url_' . $k] = str_replace(' ', '-', trim($request->{'url_' . $k}));
            }
            else{
                $_POST['url_' . $k] = str_replace(' ', '-', trim($request->{'title_' . $k}));
            }

            if($request->{'desc_' . $k} == ""){
                $_POST['desc_' . $k] = str_limit(strip_tags($request->{'text_' . $k}),'160','');
            }
            
            if($request->{'alt_' . $k} == ""){
                $_POST['alt_' . $k] = $request->{'title_' . $k};
            }

            $data['title_' . $k] = $request->{'title_' . $k};
            $data['text_' . $k] = $request->{'text_' . $k};
            $rules['title_'.$k] = 'required|unique:pages';
            $rules['text_'.$k] = 'required';
        }

		
            $validator = Validator::make($data, $rules);


			if ($validator->fails()) {

				return redirect(url('/admin/'.$this->module.'/add'))
                        ->withErrors($validator)
                        ->withInput();
			}

        
        
            unset($_POST['_token']);
            for ($i=1; $i <= $this->picsnum; $i++) {
                unset($_POST['pic' . $i]);
                foreach($this->lang_arr as $k => $v){
                    unset($_POST['pic' . $i . 'alt_' . $k]);
                }
            }
            $_POST['type'] = $this->type;

            $insertedid = DB::table('pages')->insertGetId($_POST);

        
        for ($i=1; $i <= $this->picsnum; $i++) {
            foreach($this->lang_arr as $k => $v){
                if($request->{'pic' . $i . 'alt_' . $k} == ""){
                    $request->{'pic' . $i . 'alt_' . $k} = $request->{'title_' . $k};
                } 
                
                $imgarr['picalt_' . $k] = $request->{'pic' . $i . 'alt_' . $k};
            }
            $imgarr['pic'] = $request->{'pic' . $i};
            $imgarr['itemid'] = $insertedid;
            $imgarr['type'] = $this->type;
            DB::table('images')->insert($imgarr);
        }

        return redirect(url('/admin/'.$this->module.'/index'))->with('message', 'تم بنجاح');
    }

    public function edit($id)
    {
            $page = DB::table('pages')->where('id',$id)->get();
            
            $images = DB::table('images')->where([['type',$this->type],['itemid',$id]])->get();
            for ($i=0; $i < count($images); $i++) { 
                    $j=$i+1;
                $page[0]->{'pic'.$j} = $images[$i]->pic;
                foreach($this->lang_arr as $k => $v){
                $page[0]->{'pic' . $j . 'alt_' . $k} = $images[$i]->{'picalt_' . $k};
                }
                $page[0]->{'pic' . $j . 'id'} = $images[$i]->id;
            }
                       // dd($page);
            return view('admin.'.$this->module.'.edit',compact('page'));
       
    }

    public function update(Request $request)
    {
        
        for ($i=1; $i <= $this->picsnum; $i++) { 
            foreach($this->lang_arr as $k => $v){
                if($request->{'pic' . $i . 'alt_'  . $k} == ""){
                    $request->{'pic' . $i . 'alt_'  . $k} = $request->{'title_'  . $k};
                } 
                $imgarr['picalt_' . $k] = $request->{'pic' . $i . 'alt_' . $k};
            }
            DB::table('images')
                ->where([['id', $request->{'pic' . $i . 'id'}],['type',$this->type],['itemid',$request->id]])
                ->update($imgarr);

            if($request->{'pic' . $i} != null){
                if($request->{'pic' . $i} == "remove"){
                    $pic = "";
                }
                else{
                    $pic = $request->{'pic' . $i};
                }
                
                DB::table('images')
                ->where([['id', $request->{'pic' . $i . 'id'}],['type',$this->type],['itemid',$request->id]])
                ->update(['pic' => $pic]);
            
            }
        }

        foreach($this->lang_arr as $k => $v){

            if($request->{'url_' . $k} != ""){
                $_POST['url_' . $k] = str_replace(' ', '-', trim($request->{'url_' . $k}));
            }
            else{
                $_POST['url_' . $k] = str_replace(' ', '-', trim($request->{'title_' . $k}));
            }

            if($request->{'desc_' . $k} == ""){
                $_POST['desc_' . $k] = str_limit(strip_tags($request->{'text_' . $k}),'160','');
            }
            
            if($request->{'alt_' . $k} == ""){
                $_POST['alt_' . $k] = $request->{'title_' . $k};
            }

            $data['title_' . $k] = $request->{'title_' . $k};
            $data['text_' . $k] = $request->{'text_' . $k};
            $rules['title_'.$k] = 'required';
            $rules['text_'.$k] = 'required';
        }

		
            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
            
                return redirect(url('/admin/'.$this->module.'/edit/' . $request->id))
                        ->withErrors($validator)
                        ->withInput();
            }

        

        unset($_POST['_token']);
            for ($i=1; $i <= $this->picsnum; $i++) {
                unset($_POST['pic' . $i]);
                foreach($this->lang_arr as $k => $v){
                    unset($_POST['pic' . $i . 'alt_' . $k]);
                }
                unset($_POST['pic' . $i . 'id']);
            }
            //dd($_POST);

        DB::table('pages')->where('id', $request->id)->update($_POST);

        
        return redirect(url('/admin/'.$this->module.'/index'))->with('message', 'تم بنجاح');
    }

    public function del($id)
    {
            DB::table('images')->where([['itemid',$id],['type',$this->type]])->delete();
            $cat = DB::table('pages')->where('id',$id)->delete();

			return redirect(url('/admin/'.$this->module.'/index'))->with('message', 'تم بنجاح');
        
    }
}
