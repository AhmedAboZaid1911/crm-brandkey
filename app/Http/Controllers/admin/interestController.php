<?php



namespace App\Http\Controllers\admin;



use Validator;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;




class interestController extends Controller

{

    public function __construct()

       {

           // parent::__construct();

            $this->type = 'interest';

            $this->module = 'interest';

            $this->picsnum = 5;

            $this->lang_arr = array("en" => "اللغة الانجليزية" );

            view()->share('module', $this->module );

            view()->share('active', 'settings');

            view()->share('titlepage','interest');

            view()->share('picsnum', $this->picsnum);

            $id = Session::get('uid');
            $roles = DB::table('usersvsroles')->where("user_id",$id)->get();
            $rolesarr = array();
            for($i=0;$i<count($roles);$i++){
                $rolesarr[$i] = $roles[$i]->role_id;
            }
            view()->share('getroles', $rolesarr);

       }



    public function index()

    {
        RolesController::checkroles('58');
        $leads = DB::table('interest')->paginate(10);
        return view('admin.settings.interest',compact('leads'));
    }



    public function add()

    {
        RolesController::checkroles('59');
        return view('admin.settings.interestadd');
    }



    public function store(Request $request)

    {
        RolesController::checkroles('59');
        unset($_POST['_token']);
        DB::table('interest')->insert($_POST);
        return redirect(url('admin/interest/index'))->with('message', 'تم بنجاح');
    }

    public function edit ($id)
    {
        RolesController::checkroles('60 ');
        $places = DB::table('interest')->where('id',$id)->get();
        return view('admin.settings.interestedit',compact('cats','time','gender','places','products','leads','nationalitys'));
    }

    public function update(Request $request)
    {
        RolesController::checkroles('60');
        unset($_POST['_token']);
        DB::table('interest')->where('id', $request->id)->update($_POST);
        return redirect(url('admin/interest/index'))->with('message', 'تم بنجاح');
    }



    public function del($id)

    {
        RolesController::checkroles('61');
        $cat = DB::table('interest')->where('id',$id)->delete();
		return redirect(url('/admin/interest/index'))->with('message', 'تم بنجاح');
    }

}
