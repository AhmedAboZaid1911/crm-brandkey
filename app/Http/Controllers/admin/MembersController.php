<?php

namespace App\Http\Controllers\admin;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class MembersController extends Controller
{

    public function __construct()
       {
           // parent::__construct();
            $this->type = 'member';
            $this->module = 'members';

            view()->share('module', $this->module );
            view()->share('titlepage','Members');

        //    $id = Auth::user()->id;
            $id = Session::get('uid');
            $roles = DB::table('usersvsroles')->where("user_id",$id)->get();
            $rolesarr = array();
            for($i=0;$i<count($roles);$i++){
                $rolesarr[$i] = $roles[$i]->role_id;
            }
            view()->share('rolesarr', $rolesarr);
       }

    public function index()
    {

        RolesController::checkroles('68');
        $active = 'settings';
        $users = DB::table('users')->where('id','!=','3')->paginate(10);
        return view('admin.'.$this->module.'.index',compact('users','active','getroles'));
    }

    public function add()
    {
        RolesController::checkroles('69');
        //$roles = DB::table('roles')->get();
        $roles = DB::table('group_roles')->get();
          
        $active = 'settings';
        return view('admin.'.$this->module.'.add',compact('active','roles'));

    }

    public function store(Request $request)
    {
        // dd($_POST);
        RolesController::checkroles('69');
		$data = array('name' => $request->name , 'email' => $request->email, 'password' => $request->password);
            $validator = Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6'
            ]);

			if ($validator->fails()) {

				return redirect(url('/admin/'.$this->module.'/add'))
                        ->withErrors($validator)
                        ->withInput();
			}

        $createdate = date('Y-m-d H:i:s');

        $logopath = "";
            if ($request->file('logo') != null){
                $imgname = $request->file('logo')->getClientoriginalName();
                $imgpath = $request->logo;

                $target = 'uploads/' . $imgname;
                $logopath = $target;
                move_uploaded_file($imgpath,$target);
            }

            $newuser = DB::table('users')->insertGetId(
            ['name' => $request->name, 'email' => $request->email, 'password' => bcrypt($request->password),
             'created_at' => $createdate, 'logo' => $logopath, 'phone' => $request->phone]);

            foreach($_POST['role'] as $r){

                DB::table('usersvsroles')->insert(['user_id' => $newuser, 'role_id' => $r]);

            }

        return redirect(url('/admin/'.$this->module.'/index'));
    }

    public function edit($mid)
    {
        RolesController::checkroles('70');
        $roles = DB::table('group_roles')->get();
            foreach($roles as $k=>$v)
            {
                $role = DB::table('roles')->where('cid',$v->id)->get();
                $roles[$k]->role = $role;
            }
        $active = 'settings';
        $exist = array();
        $existroles = DB::table('usersvsroles')->select('role_id')->where('user_id',$mid)->get();
        for($i=0;$i<count($existroles);$i++){
            $exist[$i] = $existroles[$i]->role_id;
        }
        $user = DB::table('users')->where('id',$mid)->get();
        return view('admin.'.$this->module.'.edit',compact('user','active','roles','existroles','exist'));


    }

    public function update(Request $request)
    {
		   // print_r($request);
           // exit();
           RolesController::checkroles('70');
            if ($request->file('logo') != null){
                $imgname = $request->file('logo')->getClientoriginalName();
                $imgpath = $request->logo;

                $target = 'uploads/' . $imgname;
                $logopath = $target;
                move_uploaded_file($imgpath,$target);
                DB::table('users')
                ->where('id', $request->mid)
                ->update(['logo' => $logopath]);
            }

            if ($request->password != ""){
                DB::table('users')
                ->where('id', $request->mid)
                ->update(['password' => bcrypt($request->password)]);
            }

            $data = array('name' => $request->name , 'email' => $request->email);
            $validator = Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255'
            ]);

            if ($validator->fails()) {

                return redirect(url('/admin/'.$this->module.'/edit/' . $request->mid))
                        ->withErrors($validator)
                        ->withInput();
            }
            $updatedate = date('Y-m-d H:i:s');

        DB::table('users')
            ->where('id', $request->mid)
            ->update(['name' => $request->name, 'email' => $request->email, 'updated_at' => $updatedate, 'phone' => $request->phone]);

        return redirect(url('/admin/'.$this->module.'/index'));
    }

    public function rolesstore(Request $request)
    {


            foreach($_POST['role'] as $r){

                DB::table('usersvsroles')->insert(['user_id' => $user_id, 'role_id' => $r]);

            }

            return redirect(url('/admin/'.$this->module.'/index'));
    }

    public function rolesupdate(Request $request)
    {

        $user_id = $request->uid;

         DB::table('usersvsroles')->where('user_id',$user_id)->delete();
            foreach($_POST['role'] as $r){
                DB::table('usersvsroles')->insert(['user_id' => $user_id, 'role_id' => $r]);
            }
            return redirect(url('/admin/'.$this->module.'/index'));
    }

    public function del($mid)
    {
        RolesController::checkroles('71');
        $user = DB::table('users')->where('id',$mid)->delete();
		return redirect(url('/admin/'.$this->module.'/index'));

    }


}
