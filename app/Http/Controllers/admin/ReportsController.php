<?php



namespace App\Http\Controllers\admin;



use Validator;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Exports\ClientsExport;
use App\Exports\UsersExport;
use App\Exports\ServicesExport;
use App\Exports\CancelledServiceExport;
use App\Exports\ClientVsServiceExport;
use App\Exports\ClientsneedExport;
use App\Exports\TasksExport;
use App\Imports\UsersImport;
use Excel;



class ReportsController extends Controller

{

    public function __construct()

       {

           // parent::__construct();

            $this->type = 'reports';

            $this->module = 'reports';

            $this->picsnum = 5;

            $this->lang_arr = array("en" => "اللغة الانجليزية" );

            view()->share('module', $this->module );

            view()->share('active', 'Reports');

            view()->share('titlepage','Reports');

            view()->share('picsnum', $this->picsnum);

            $id = Session::get('uid');
            $roles = DB::table('usersvsroles')->where("user_id",$id)->get();
            $rolesarr = array();
            for($i=0;$i<count($roles);$i++){
                $rolesarr[$i] = $roles[$i]->role_id;
            }
            view()->share('getroles', $rolesarr);

       }



    public function reports()

    {
        RolesController::checkroles('73');

        return view('admin.reports.index');

    }
    public function service_index()

    {
        RolesController::checkroles('73');

        return view('admin.reports.servicesindex');

    }

    public function service_report()

    {
        RolesController::checkroles('80');
            $Services = DB::table('products')->orderBy('id','desc')->get();

            $nationality = DB::table('pages')->where('type','nationality')->get();
            $users = DB::table('users')->get();
            $gender = DB::table('gender')->get();
            $message = '';


            return view('admin.reports.servicereport',compact('finalclients','users','nationality','gender','message','Services','users','clients','clientvsservices','user'));

    }

    public function service_report_search(Request $request)

    {
            RolesController::checkroles('80');

            $Services = DB::table('products')->orderBy('id','desc')->get();
            $nationality = DB::table('pages')->where('type','nationality')->get();
            $users = DB::table('users')->get();
            $gender = DB::table('gender')->get();

            $service_name = $request->service_name;
            $nationalitys = $request->nationality;
            $genders = $request->gender;
            $user = $request->user;
            $from = $request->from;
            $to = $request->to;

            if($from == ''){
            $from = '0000-00-00 00:00:00';
            }

            if($to == ''){
            $to = date('Y-m-d H:i:s');
            }

            $servicesname = DB::table('services')->select('id')->where('service_name',$service_name)->get();
            $servicename = DB::table('products')->select('name')->where('id',$service_name)->get();

            $ids = array();
                foreach($servicesname as $k=>$v){

            $ids[]= $v->id;
            }

            $clientvsservice = DB::table('clientsvsservices')->whereIn('service',$ids);
            $clientvsservice->where('status','!=','0');
            $clientvsservice->wherebetween('created_at',[$from,$to]);

            if ($request->filled('servicesstatus')) {
                $clientvsservice->where('servicesstatus', $request->servicesstatus);
            }

            $clientvsservices = $clientvsservice->orderBy('clientsvsservices.id','desc')->get();
            $finalclients = array();
            if($clientvsservices->count() > 0){
                foreach($clientvsservices as $k=>$v)
                {

                    $clients = DB::table('clients')->where('id',$v->client);

                    if ($request->filled('nationality')) {
                        $clients->where('nationality', $request->nationality);
                    }

                    if ($request->filled('gender')) {
                        $clients->where('gender', $request->gender);
                    }

                    if ($request->filled('user')) {
                        $clients->where('assigned_to', $request->user);
                    }


                    $theclient = $clients->get();
                    //$theclient = $clients->toSql();
                            //dd($theclient);

                    //print $theclient->count().'<br>';
                    if($theclient->count() > 0){

                    //print $theclient[0]->number.'<br>';
                    $clientvsservices[$k]->name = $theclient[0]->name;
                    $clientvsservices[$k]->name_en = $theclient[0]->name_en;
                    $clientvsservices[$k]->number = $theclient[0]->number;
                    $clientvsservices[$k]->email = $theclient[0]->email;

                    $user = DB::table('users')->select('name')->where('id',$theclient[0]->assigned_to)->get();
                    $clientvsservices[$k]->assigned_to = $user[0]->name;
                    $nat = DB::table('pages')->where('id',$theclient[0]->nationality)->get();
                    $clientvsservices[$k]->nationality = $nat[0]->title_en;
                    $gen = DB::table('gender')->where('id',$theclient[0]->gender)->get();
                    $clientvsservices[$k]->gender = $gen[0]->type;
                    $finalclients[$k] = $clientvsservices[$k];
                }


            }
        }
        $message = 'تم البحث بنجاح (عدد النتائج '.count($finalclients).'): <br><br>';

        if ($request->filled('user')) {
            $user_name = DB::table('users')->select('name')->where('id',$request->user)->get();
           $user_name =  $user_name[0]->name;
        }else{
            $user_name = 'الكل';
        }

        if ($request->filled('gender')) {
            //$user_gender= DB::table('gender')->select('type')->where('id',$request->user)->get();
            $user_gender= DB::table('gender')->select('type')->where('id',$request->gender)->get();
           $user_gender =  $user_gender[0]->type;
        }else{
            $user_gender = 'الكل';
        }

        if ($request->filled('pages')) {
            $user_nationality = DB::table('pages')->select('title_en')->where('id',$request->user)->get();
           $user_nationality =  $user_nationality[0]->title_en;
        }else{
            $user_nationality = 'الكل';
        }


        $message .=" <span style='padding-left:5%;color:#364150'> في الخدمة :     "
        .' '. $servicename[0]->name  .' '.'</span>';
        $message .=" <span style='padding-left:5%;color:#364150'> من :     "
        .' '. $from  .' '.'</span>';
        $message .=" <span style='padding-left:5%;color:#364150'> الى :     "
        .' '. $to  .' '.'</span>';
        $message .=" <span style='padding-left:5%;color:#364150'> تابع ل :     "
        .' '. $user_name  .' '.'</span>';
        $message .=" <span style='padding-left:5%;color:#364150'> النوع :     "
        .' '. $user_gender  .' '.'</span>';
        $message .=" <span style='padding-left:5%;color:#364150'> الجنسية :     "
        .' '. $user_nationality  .' '.'</span>';
        return view('admin.reports.servicereport',compact('from','to','finalclients','users','nationality','genders','gender','message','users','clients','Services','clientvsservices','user','service_name'));

    }

    public function service_report_excel(Request $request)

    {
        RolesController::checkroles('81');

            $service_name1 = $request->service_name;
            $nationalitys1 = $request->nationality;
            $genders1 = $request->gender;
            $user1 = $request->user;
            $from1 = $request->from;
            $to1 = $request->to;
            if($from1 == ''){
            $from1 = '0000-00-00 00:00:00';
            }

            if($to1 == ''){
            $to1 = date('Y-m-d H:i:s');
            }

            $servicesname = DB::table('services')->select('id')->where('service_name',$service_name1)->get();
            $servicename = DB::table('products')->select('name')->where('id',$service_name1)->get();

            $ids = array();
                foreach($servicesname as $k=>$v){

            $ids[]= $v->id;
            }

            $clientvsservice = DB::table('clientsvsservices')->whereIn('service',$ids);
            $clientvsservice->where('status','!=','0');
            $clientvsservice->wherebetween('created_at',[$from1,$to1]);

            if ($request->filled('servicesstatus')) {
                $clientvsservice->where('servicesstatus', $request->servicesstatus1);
            }

            $clientvsservices = $clientvsservice->orderBy('clientsvsservices.id','desc')->get();
            $finalclients = array();
            if($clientvsservices->count() > 0){
                foreach($clientvsservices as $k=>$v)
                {
                    $clients = DB::table('clients')->where('id',$v->client);

                    if ($request->filled('nationality')) {
                        $clients->where('nationality', $request->nationality);
                    }

                    if ($request->filled('gender')) {
                        $clients->where('gender', $request->gender);
                    }

                    if ($request->filled('user')) {
                        $clients->where('assigned_to', $request->user);
                    }


                    $theclient = $clients->get();

                    if($theclient->count() > 0){

                    $clientvsservices[$k]->name = $theclient[0]->name;
                    $clientvsservices[$k]->name_en = $theclient[0]->name_en;
                    $clientvsservices[$k]->number = $theclient[0]->number;
                    $clientvsservices[$k]->email = $theclient[0]->email;

                    $user = DB::table('users')->select('name')->where('id',$theclient[0]->assigned_to)->get();
                    $clientvsservices[$k]->assigned_to = $user[0]->name;
                    $nat = DB::table('pages')->where('id',$theclient[0]->nationality)->get();
                    $clientvsservices[$k]->nationality = $nat[0]->title_en;
                    $gen = DB::table('gender')->where('id',$theclient[0]->gender)->get();
                    $clientvsservices[$k]->gender = $gen[0]->type;
                    $finalclients[$k] = $clientvsservices[$k];
                }


            }
        }
        // dd( $clientvsservices);
        return Excel::download(new ServicesExport($finalclients), 'Clients_report-'.date("d-m-Y-h:i:s-A").'.xlsx');

    }
    public function servicevsservices_report()

    {
        RolesController::checkroles('80');
            $Services = DB::table('products')->orderBy('id','desc')->get();




            return view('admin.reports.servicevsservicesreport',compact('Services'));

    }

    public function servicevsservices_report_search(Request $request)

    {
            RolesController::checkroles('80');

            $Services = DB::table('products')->orderBy('id','desc')->get();


            $service_name = $request->service_name;
            $service_name2 = $request->service_name2;



            $servicesname = DB::table('services')->select('id')->where('service_name',$service_name)->get();
            $servicename = DB::table('products')->select('name')->where('id',$service_name)->get();

            $ids = array();
                foreach($servicesname as $k=>$v){

            $ids[]= $v->id;
            }

            $servicesname2 = DB::table('services')->select('id')->where('service_name',$service_name2)->get();
            $servicename2 = DB::table('products')->select('name')->where('id',$service_name2)->get();

            $ids2 = array();
            foreach($servicesname2 as $k=>$v){

                $ids2[]= $v->id;
            }

            $clientvsservice1 = DB::table('clientsvsservices')->whereIn('service',$ids);
            $clientvsservice1->where('status','!=','0');
            $clientvsservices1 = $clientvsservice1->orderBy('clientsvsservices.id','desc')->get();
            $cid1 = array();
            foreach($clientvsservices1 as $k=>$v){

                $cid1[]= $v->client;
            }

            $clientvsservice2 = DB::table('clientsvsservices')->whereIn('service',$ids2);
            $clientvsservice2->where('status','!=','0');
            // dd($clientvsservice2);
            $clientvsservices2 = $clientvsservice2->orderBy('clientsvsservices.id','desc')->get();
            $cid2 = array();
            foreach($clientvsservices2 as $k=>$v){

                $cid2[]= $v->client;
            }
            $clientvsservices = array_diff($cid1, $cid2);
            //var_dump(array_diff($cid1, $cid2));

            // dd($diff);
            $finalclients = array();


            if(count($clientvsservices) > 0){
                foreach($clientvsservices as $k=>$v)
                {
                    $clients = DB::table('clients')->where('id',$v);



                    $theclient = $clients->get();

                    if($theclient->count() > 0){


                    $finalclients[$k]['id'] = $theclient[0]->id;
                    $finalclients[$k]['name'] = $theclient[0]->name;
                    // dd($finalclients);
                    $finalclients[$k]['name_en']= $theclient[0]->name_en;
                    $finalclients[$k]['number']= $theclient[0]->number;
                    $finalclients[$k]['email']= $theclient[0]->email;

                    $user = DB::table('users')->select('name')->where('id',$theclient[0]->assigned_to)->get();
                    $finalclients[$k]['assigned_to'] = $user[0]->name;
                    $nat = DB::table('pages')->where('id',$theclient[0]->nationality)->get();
                    $finalclients[$k]['nationality'] = $nat[0]->title_en;
                    $gen = DB::table('gender')->where('id',$theclient[0]->gender)->get();
                    $finalclients[$k]['gender']= $gen[0]->type;
                    // $finalclients[$k] = $clientvsservices[$k];
                }


            }
        }
        // dd($finalclients);
        $message = 'تم البحث بنجاح (عدد النتائج '.count($finalclients).'): <br><br>';


        $message .=" <span style='padding-left:5%;color:#364150'> حصل على  :     "
        .' '. $servicename[0]->name  .' '.'</span>';
        $message .=" <span style='padding-left:5%;color:#364150'> لم يحصل على  :     "
        .' '. $servicename2[0]->name  .' '.'</span>';

        return view('admin.reports.servicevsservicesreport',compact('from','to','finalclients','users','nationality','genders','gender','message','users','clients','Services','clientvsservices','user','service_name'));

    }

    public function servicevsservices_report_excel(Request $request)

    {
        RolesController::checkroles('81');

            $service_name1 = $request->service_name;
            $nationalitys1 = $request->nationality;
            $genders1 = $request->gender;
            $user1 = $request->user;
            $from1 = $request->from;
            $to1 = $request->to;
            if($from1 == ''){
            $from1 = '0000-00-00 00:00:00';
            }

            if($to1 == ''){
            $to1 = date('Y-m-d H:i:s');
            }

            $servicesname = DB::table('services')->select('id')->where('service_name',$service_name1)->get();
            $servicename = DB::table('products')->select('name')->where('id',$service_name1)->get();

            $ids = array();
                foreach($servicesname as $k=>$v){

            $ids[]= $v->id;
            }

            $clientvsservice = DB::table('clientsvsservices')->whereIn('service',$ids);
            $clientvsservice->where('status','!=','0');
            $clientvsservice->wherebetween('created_at',[$from1,$to1]);

            if ($request->filled('servicesstatus')) {
                $clientvsservice->where('servicesstatus', $request->servicesstatus1);
            }

            $clientvsservices = $clientvsservice->orderBy('clientsvsservices.id','desc')->get();
            $finalclients = array();
            if($clientvsservices->count() > 0){
                foreach($clientvsservices as $k=>$v)
                {
                    $clients = DB::table('clients')->where('id',$v->client);

                    if ($request->filled('nationality')) {
                        $clients->where('nationality', $request->nationality);
                    }

                    if ($request->filled('gender')) {
                        $clients->where('gender', $request->gender);
                    }

                    if ($request->filled('user')) {
                        $clients->where('assigned_to', $request->user);
                    }


                    $theclient = $clients->get();

                    if($theclient->count() > 0){

                    $clientvsservices[$k]->name = $theclient[0]->name;
                    $clientvsservices[$k]->name_en = $theclient[0]->name_en;
                    $clientvsservices[$k]->number = $theclient[0]->number;
                    $clientvsservices[$k]->email = $theclient[0]->email;

                    $user = DB::table('users')->select('name')->where('id',$theclient[0]->assigned_to)->get();
                    $clientvsservices[$k]->assigned_to = $user[0]->name;
                    $nat = DB::table('pages')->where('id',$theclient[0]->nationality)->get();
                    $clientvsservices[$k]->nationality = $nat[0]->title_en;
                    $gen = DB::table('gender')->where('id',$theclient[0]->gender)->get();
                    $clientvsservices[$k]->gender = $gen[0]->type;
                    $finalclients[$k] = $clientvsservices[$k];
                }


            }
        }
        // dd( $clientvsservices);
        return Excel::download(new ServicesExport($finalclients), 'Clients_report-'.date("d-m-Y-h:i:s-A").'.xlsx');

    }

    public function clientvsservice_report()

    {
        RolesController::checkroles('80');
            $Services = DB::table('services')->orderBy('id','desc')->get();
            foreach($Services as $k=>$v)
            {

                $time = DB::table('services_time')->select('name')->where('id',$v->service_time)->get();
                $Services[$k]->time = $time[0]->name;

                // $periods = DB::table('services_period')->where('id',$v->service_period)->get();
                // $Services[$k]->from = $periods[0]->from;
                // $Services[$k]->to = $periods[0]->to;

                $places = DB::table('services_place')->select('name')->where('id',$v->service_place)->get();
                $Services[$k]->place = $places[0]->name;

                $products = DB::table('products')->select('name')->where('id',$v->service_name)->get();
                $Services[$k]->name = $products[0]->name;

                $currency = DB::table('services_currency')->select('currency')->where('id',$v->service_currency)->get();
                $Services[$k]->currency = $currency[0]->currency;

                $days = DB::table('services_days')->where('id',$v->service_days)->get();
                $Services[$k]->day1 = $days[0]->day1;
                $Services[$k]->day2 = $days[0]->day2;
                $Services[$k]->day3 = $days[0]->day3;
                $Services[$k]->day4 = $days[0]->day4;
                $Services[$k]->day5 = $days[0]->day5;
                $Services[$k]->day6 = $days[0]->day6;
                $Services[$k]->day7 = $days[0]->day7;
            }

            $clientvsservices = DB::table('clientsvsservices')->where('service','0')->orderBy('id','desc')->get();
            foreach($clientvsservices as $k=>$v)
            {

                $clients = DB::table('clients')->where('id',$v->client)->get();
                $clientvsservices[$k]->name = $clients[0]->name;
                $clientvsservices[$k]->name_en = $clients[0]->name_en;
                $clientvsservices[$k]->number = $clients[0]->number;
                $clientvsservices[$k]->email = $clients[0]->email;
                $user = DB::table('users')->select('name')->where('id',$clients[0]->assigned_to)->get();
                $clientvsservices[$k]->assigned_to = $user[0]->name;

            }
            $nationality = DB::table('pages')->where('type','nationality')->get();
            $users = DB::table('users')->get();
            $gender = DB::table('gender')->get();
            $message = '';


            return view('admin.reports.clientvsservicereport',compact('users','nationality','gender','message','Services','users','clients','clientvsservices','user'));

    }

    public function clientvsservice_report_search(Request $request)

    {
        RolesController::checkroles('80');
            $Services = DB::table('services')->orderBy('id','desc')->get();
            foreach($Services as $k=>$v)
            {
                $time = DB::table('services_time')->select('name')->where('id',$v->service_time)->get();
                $Services[$k]->time = $time[0]->name;

                // $periods = DB::table('services_period')->where('id',$v->service_period)->get();
                // $Services[$k]->from = $periods[0]->from;
                // $Services[$k]->to = $periods[0]->to;

                $places = DB::table('services_place')->select('name')->where('id',$v->service_place)->get();
                $Services[$k]->place = $places[0]->name;

                $products = DB::table('products')->select('name')->where('id',$v->service_name)->get();
                $Services[$k]->name = $products[0]->name;

                $currency = DB::table('services_currency')->select('currency')->where('id',$v->service_currency)->get();
                $Services[$k]->currency = $currency[0]->currency;

                $days = DB::table('services_days')->where('id',$v->service_days)->get();
                $Services[$k]->day1 = $days[0]->day1;
                $Services[$k]->day2 = $days[0]->day2;
                $Services[$k]->day3 = $days[0]->day3;
                $Services[$k]->day4 = $days[0]->day4;
                $Services[$k]->day5 = $days[0]->day5;
                $Services[$k]->day6 = $days[0]->day6;
                $Services[$k]->day7 = $days[0]->day7;
            }

            $service_name = $request->service_name;

            $clientvsservices = DB::table('clientsvsservices')->where('service',$service_name)->orderBy('id','desc')->get();
            foreach($clientvsservices as $k=>$v)
            {

                $clients = DB::table('clients')->where('id',$v->client)->get();
                $clientvsservices[$k]->name = $clients[0]->name;
                $clientvsservices[$k]->name_en = $clients[0]->name_en;
                $clientvsservices[$k]->number = $clients[0]->number;
                $clientvsservices[$k]->email = $clients[0]->email;
                $user = DB::table('users')->select('name')->where('id',$clients[0]->assigned_to)->get();
                $clientvsservices[$k]->assigned_to = $user[0]->name;

            }


            // $message = "<span style='padding-right:45%;font-weight:bold;'> تم البحث بنجاح  (عدد النتائج '.count($clientvsservices).'): </span> <br><br>";
            $message = 'تم البحث بنجاح (عدد النتائج '.count($clientvsservices).'): <br>';

            if($service_name != null){
                $Servicemsg = DB::table('services')->where('id',$service_name)->get();

                foreach($Servicemsg as $k=>$v)
                {
                    $time = DB::table('services_time')->select('name')->where('id',$v->service_time)->get();
                    $Servicemsg[$k]->time = $time[0]->name;

                    // $periods = DB::table('services_period')->where('id',$v->service_period)->get();
                    // $Servicemsg[$k]->from = $periods[0]->from;
                    // $Servicemsg[$k]->to = $periods[0]->to;

                    $places = DB::table('services_place')->select('name')->where('id',$v->service_place)->get();
                    $Servicemsg[$k]->place = $places[0]->name;

                    $products = DB::table('products')->select('name')->where('id',$v->service_name)->get();
                    $Servicemsg[$k]->name = $products[0]->name;

                    $currency = DB::table('services_currency')->select('currency')->where('id',$v->service_currency)->get();
                    $Servicemsg[$k]->currency = $currency[0]->currency;

                    $days = DB::table('services_days')->where('id',$v->service_days)->get();
                    $Servicemsg[$k]->day1 = $days[0]->day1;
                    $Servicemsg[$k]->day2 = $days[0]->day2;
                    $Servicemsg[$k]->day3 = $days[0]->day3;
                    $Servicemsg[$k]->day4 = $days[0]->day4;
                    $Servicemsg[$k]->day5 = $days[0]->day5;
                    $Servicemsg[$k]->day6 = $days[0]->day6;
                    $Servicemsg[$k]->day7 = $days[0]->day7;
                }
                $message .=" <span style='padding-left:5%;color:#364150'> في الخدمة :     "
                  .' '. $Servicemsg[0]->name  .' '. $Servicemsg[0]->time .' '. $Servicemsg[0]->place .' '. $Servicemsg[0]->servicesdate  .
                  '</span><br>';

            }


            return view('admin.reports.clientvsservicereport',compact('message','users','clients','Services','clientvsservices','user','service_name'));

    }

    public function clientvsservice_report_excel(Request $request)

    {
        RolesController::checkroles('81');
        $service_name = $request->servicename;

        $service_title = DB::table('services')->where('id',$service_name)->get();

        foreach($service_title as $k=>$v)
        {
            $time = DB::table('services_time')->select('name')->where('id',$v->service_time)->get();
            $service_title[$k]->time = $time[0]->name;

            $places = DB::table('services_place')->select('name')->where('id',$v->service_place)->get();
            $service_title[$k]->place = $places[0]->name;

            $products = DB::table('products')->select('name')->where('id',$v->service_name)->get();
            $service_title[$k]->name = $products[0]->name;


        }
        $title = $service_title[0]->name  .' '. $service_title[0]->time .''. $service_title[0]->servicesdate ;

            $clientvsservices = DB::table('clientsvsservices')->where('service',$service_name)->orderBy('id','desc')->get();
            foreach($clientvsservices as $k=>$v)
            {

                $clients = DB::table('clients')->where('id',$v->client)->get();
                $clientvsservices[$k]->name = $clients[0]->name;
                $clientvsservices[$k]->name_en = $clients[0]->name_en;
                $clientvsservices[$k]->number = $clients[0]->number;
                $clientvsservices[$k]->email = $clients[0]->email;
                $user = DB::table('users')->select('name')->where('id',$clients[0]->assigned_to)->get();
                $clientvsservices[$k]->assigned_to = $user[0]->name;

            }

        return Excel::download(new ClientVsServiceExport($clientvsservices,$title), 'ClientVsServices-_report-'.date("d-m-Y-h:i:s-A").'.xlsx');

    }

    public function cancelledservice_report()

    {
        RolesController::checkroles('80');
            $Services = DB::table('services')->orderBy('id','desc')->get();
            foreach($Services as $k=>$v)
            {

                $time = DB::table('services_time')->select('name')->where('id',$v->service_time)->get();
                $Services[$k]->time = $time[0]->name;

                // $periods = DB::table('services_period')->where('id',$v->service_period)->get();
                // $Services[$k]->from = $periods[0]->from;
                // $Services[$k]->to = $periods[0]->to;

                $places = DB::table('services_place')->select('name')->where('id',$v->service_place)->get();
                $Services[$k]->place = $places[0]->name;

                $products = DB::table('products')->select('name')->where('id',$v->service_name)->get();
                $Services[$k]->name = $products[0]->name;

                $currency = DB::table('services_currency')->select('currency')->where('id',$v->service_currency)->get();
                $Services[$k]->currency = $currency[0]->currency;

                $days = DB::table('services_days')->where('id',$v->service_days)->get();
                $Services[$k]->day1 = $days[0]->day1;
                $Services[$k]->day2 = $days[0]->day2;
                $Services[$k]->day3 = $days[0]->day3;
                $Services[$k]->day4 = $days[0]->day4;
                $Services[$k]->day5 = $days[0]->day5;
                $Services[$k]->day6 = $days[0]->day6;
                $Services[$k]->day7 = $days[0]->day7;
            }

            $clientvsservices = DB::table('clientsvsservices')->where('service','0')->orderBy('id','desc')->get();
            foreach($clientvsservices as $k=>$v)
            {

                $clients = DB::table('clients')->where('id',$v->client)->get();
                $clientvsservices[$k]->name = $clients[0]->name;
                $clientvsservices[$k]->name_en = $clients[0]->name_en;
                $clientvsservices[$k]->number = $clients[0]->number;
                $clientvsservices[$k]->email = $clients[0]->email;
                $user = DB::table('users')->select('name')->where('id',$clients[0]->assigned_to)->get();
                $clientvsservices[$k]->assigned_to = $user[0]->name;

            }
            $nationality = DB::table('pages')->where('type','nationality')->get();
            $users = DB::table('users')->get();
            $gender = DB::table('gender')->get();
            $message = '';


            return view('admin.reports.cancelledservicereport',compact('users','nationality','gender','message','Services','users','clients','clientvsservices','user'));

    }

    public function cancelledservice_report_search(Request $request)

    {
        RolesController::checkroles('80');
            $Services = DB::table('services')->orderBy('id','desc')->get();
            foreach($Services as $k=>$v)
            {
                $time = DB::table('services_time')->select('name')->where('id',$v->service_time)->get();
                $Services[$k]->time = $time[0]->name;

                // $periods = DB::table('services_period')->where('id',$v->service_period)->get();
                // $Services[$k]->from = $periods[0]->from;
                // $Services[$k]->to = $periods[0]->to;

                $places = DB::table('services_place')->select('name')->where('id',$v->service_place)->get();
                $Services[$k]->place = $places[0]->name;

                $products = DB::table('products')->select('name')->where('id',$v->service_name)->get();
                $Services[$k]->name = $products[0]->name;

                $currency = DB::table('services_currency')->select('currency')->where('id',$v->service_currency)->get();
                $Services[$k]->currency = $currency[0]->currency;

                $days = DB::table('services_days')->where('id',$v->service_days)->get();
                $Services[$k]->day1 = $days[0]->day1;
                $Services[$k]->day2 = $days[0]->day2;
                $Services[$k]->day3 = $days[0]->day3;
                $Services[$k]->day4 = $days[0]->day4;
                $Services[$k]->day5 = $days[0]->day5;
                $Services[$k]->day6 = $days[0]->day6;
                $Services[$k]->day7 = $days[0]->day7;
            }

            $service_name = $request->service_name;

            $clientvsservices = DB::table('clientsvsservices')->where([['service',$service_name],['status','0']])->orderBy('id','desc')->get();
            foreach($clientvsservices as $k=>$v)
            {

                $clients = DB::table('clients')->where('id',$v->client)->get();
                $clientvsservices[$k]->name = $clients[0]->name;
                $clientvsservices[$k]->name_en = $clients[0]->name_en;
                $clientvsservices[$k]->number = $clients[0]->number;
                $clientvsservices[$k]->email = $clients[0]->email;
                $user = DB::table('users')->select('name')->where('id',$clients[0]->assigned_to)->get();
                $clientvsservices[$k]->assigned_to = $user[0]->name;

            }


            // $message = "<span style='padding-right:45%;font-weight:bold;'> تم البحث بنجاح  (عدد النتائج '.count($clientvsservices).'): </span> <br><br>";
            $message = 'تم البحث بنجاح (عدد النتائج '.count($clientvsservices).'): <br>';

            if($service_name != null){
                $Servicemsg = DB::table('services')->where('id',$service_name)->get();

                foreach($Servicemsg as $k=>$v)
                {
                    $time = DB::table('services_time')->select('name')->where('id',$v->service_time)->get();
                    $Servicemsg[$k]->time = $time[0]->name;

                    // $periods = DB::table('services_period')->where('id',$v->service_period)->get();
                    // $Servicemsg[$k]->from = $periods[0]->from;
                    // $Servicemsg[$k]->to = $periods[0]->to;

                    $places = DB::table('services_place')->select('name')->where('id',$v->service_place)->get();
                    $Servicemsg[$k]->place = $places[0]->name;

                    $products = DB::table('products')->select('name')->where('id',$v->service_name)->get();
                    $Servicemsg[$k]->name = $products[0]->name;

                    $currency = DB::table('services_currency')->select('currency')->where('id',$v->service_currency)->get();
                    $Servicemsg[$k]->currency = $currency[0]->currency;

                    $days = DB::table('services_days')->where('id',$v->service_days)->get();
                    $Servicemsg[$k]->day1 = $days[0]->day1;
                    $Servicemsg[$k]->day2 = $days[0]->day2;
                    $Servicemsg[$k]->day3 = $days[0]->day3;
                    $Servicemsg[$k]->day4 = $days[0]->day4;
                    $Servicemsg[$k]->day5 = $days[0]->day5;
                    $Servicemsg[$k]->day6 = $days[0]->day6;
                    $Servicemsg[$k]->day7 = $days[0]->day7;
                }
                $message .=" <span style='padding-left:5%;color:#364150'> في الخدمة :     "
                  .' '. $Servicemsg[0]->name  .' '. $Servicemsg[0]->time .' '. $Servicemsg[0]->place .' '. $Servicemsg[0]->servicesdate  .
                  '</span><br>';

            }


            return view('admin.reports.cancelledservicereport',compact('message','users','clients','Services','clientvsservices','user','service_name'));

    }

    public function cancelledservice_report_excel(Request $request)

    {
        RolesController::checkroles('81');
        $service_name = $request->servicename;

        $service_title = DB::table('services')->where('id',$service_name)->get();

        foreach($service_title as $k=>$v)
        {
            $time = DB::table('services_time')->select('name')->where('id',$v->service_time)->get();
            $service_title[$k]->time = $time[0]->name;

            $places = DB::table('services_place')->select('name')->where('id',$v->service_place)->get();
            $service_title[$k]->place = $places[0]->name;

            $products = DB::table('products')->select('name')->where('id',$v->service_name)->get();
            $service_title[$k]->name = $products[0]->name;


        }
        $title = $service_title[0]->name  .' '. $service_title[0]->time .''. $service_title[0]->servicesdate ;

            $clientvsservices = DB::table('clientsvsservices')->where([['service',$service_name],['status','0']])->orderBy('id','desc')->get();
            foreach($clientvsservices as $k=>$v)
            {

                $clients = DB::table('clients')->where('id',$v->client)->get();
                $clientvsservices[$k]->name = $clients[0]->name;
                $clientvsservices[$k]->name_en = $clients[0]->name_en;
                $clientvsservices[$k]->number = $clients[0]->number;
                $clientvsservices[$k]->email = $clients[0]->email;
                $user = DB::table('users')->select('name')->where('id',$clients[0]->assigned_to)->get();
                $clientvsservices[$k]->assigned_to = $user[0]->name;

            }

        return Excel::download(new CancelledServiceExport($clientvsservices,$title), 'CancelledServices-_report-'.date("d-m-Y-h:i:s-A").'.xlsx');

    }

    public function clientneeds_report()

    {
        RolesController::checkroles('80');

            $needs = DB::table('Need')->get();
            // $need_id = $request->need_id;
            $clientneeds = DB::table('clientsvsneeds')->where('status','2')->orderBy('id','desc')->get();
            foreach($clientneeds as $k=>$v)
                {
                    $clientsvsneed = DB::table('clients')->where('id',$v->client)->get();
                    $clientneeds[$k]->name = $clientsvsneed[0]->name;
                    $clientneeds[$k]->name_en = $clientsvsneed[0]->name_en;
                    $clientneeds[$k]->number = $clientsvsneed[0]->number;
                    $clientneeds[$k]->number2 = $clientsvsneed[0]->number2;
                    $clientneeds[$k]->email = $clientsvsneed[0]->email;
                    $user = DB::table('users')->select('name')->where('id',$clientsvsneed[0]->assigned_to)->get();
                    $clientneeds[$k]->assigned_to = $user[0]->name;

                    // DD($needs);
                    $need = DB::table('Need')->select('Need')->where('id',$v->need)->get();
                    $clientneeds[$k]->needs = $need[0]->Need;

                }
            $message = '';


            return view('admin.reports.clientsneedreport',compact('message','need','needs','clientneeds','clients','clientvsservices','user'));

    }

    public function clientneeds_report_search(Request $request)

    {
        RolesController::checkroles('80');

        $needs = DB::table('Need')->get();
        $need_id = $request->need_id;
        $clientneeds = DB::table('clientsvsneeds')->where('need',$need_id)->orderBy('id','desc')->get();
        foreach($clientneeds as $k=>$v)
            {
                $clientsvsneed = DB::table('clients')->where('id',$v->client)->get();
                $clientneeds[$k]->name = $clientsvsneed[0]->name;
                $clientneeds[$k]->name_en = $clientsvsneed[0]->name_en;
                $clientneeds[$k]->number = $clientsvsneed[0]->number;
                $clientneeds[$k]->number2 = $clientsvsneed[0]->number2;
                $clientneeds[$k]->email = $clientsvsneed[0]->email;
                $user = DB::table('users')->select('name')->where('id',$clientsvsneed[0]->assigned_to)->get();
                $clientneeds[$k]->assigned_to = $user[0]->name;
                $need = DB::table('Need')->select('Need')->where('id',$v->need)->get();
                $clientneeds[$k]->needs = $need[0]->Need;

            }
        // $message = "<span style='padding-right:45%;font-weight:bold;'> تم البحث بنجاح  (عدد النتائج '.count($clientvsservices).'): </span> <br><br>";
        $message = 'تم البحث بنجاح (عدد النتائج '.count($clientneeds).'): <br>';

        return view('admin.reports.clientsneedreport',compact('message','need_id','users','clientneeds','needs','clientvsservices','user','service_name'));

    }

    public function clientneeds_report_excel(Request $request)

    {
        RolesController::checkroles('81');
        $need_id = $request->need_id;

        $clientneeds = DB::table('clientsvsneeds')->where('need',$need_id)->orderBy('id','desc')->get();
        foreach($clientneeds as $k=>$v)
            {
                $clientsvsneed = DB::table('clients')->where('id',$v->client)->get();
                $clientneeds[$k]->name = $clientsvsneed[0]->name;
                $clientneeds[$k]->name_en = $clientsvsneed[0]->name_en;
                $clientneeds[$k]->number = $clientsvsneed[0]->number;
                $clientneeds[$k]->number2 = $clientsvsneed[0]->number2;
                $clientneeds[$k]->email = $clientsvsneed[0]->email;
                $user = DB::table('users')->select('name')->where('id',$clientsvsneed[0]->assigned_to)->get();
                $clientneeds[$k]->assigned_to = $user[0]->name;
                $need = DB::table('Need')->select('need')->where('id',$need_id)->get();
                $clientneeds[$k]->needs = $need[0]->need;
            }

        return Excel::download(new ClientsneedExport($clientneeds,$need_id), 'Clients-Need-'.date("d-m-Y-h:i:s-A").'.xlsx');

    }

    public function clients_report()

    {
        RolesController::checkroles('74');
            $clients = DB::table('clients')->where('status','2')->orderBy('id','desc')->get();
            foreach($clients as $k=>$v)
            {
                $user = DB::table('users')->select('name')->where('id',$v->assigned_to)->get();
                $clients[$k]->assigned_to = $user[0]->name;

                $lead = DB::table('lead')->select('lead')->where('id',$v->lead)->get();
                $clients[$k]->lead = $lead[0]->lead;

                $gender = DB::table('gender')->select('type')->where('id',$v->gender)->get();
                $clients[$k]->gender = $gender[0]->type;

                $Interest = DB::table('interest')->select('type')->where('id',$v->Interest)->get();
                $clients[$k]->Interest = $Interest[0]->type;

                $nationality = DB::table('pages')->select('title_en')->where([['id',$v->nationality],['type','nationality']])->get();
                $clients[$k]->nationality = $nationality[0]->title_en;
            }

            $nationality = DB::table('pages')->where('type','nationality')->get();
            $Interest = DB::table('interest')->get();
            $gender = DB::table('gender')->get();
            $lead = DB::table('lead')->get();
            $users = DB::table('users')->get();

            $message = '';

            return view('admin.reports.clientsreport',compact('message','clients','nationality','Interest','gender','lead','users','from','to','genders','nationalitys','Interests','leads','assigned_to'));

    }

    public function clients_report_search(Request $request)

    {

        RolesController::checkroles('74');
            $from =  $request->from;
            $to =  $request->to;
            $genders =  $request->gender;
            $nationalitys =  $request->nationality;
            $Interests =  $request->Interest;
            $leads =  $request->lead;
            $assigned_to =  $request->user;


            $where="WHERE status = '1' ";

             if($from != '' && $to != '' ){
                $from = $from." 00:00:00";
                $to = $to." 23:59:59";

                $where .="AND created_at BETWEEN '". $from ."' AND '". $to ."' " ;
            }

            if($from != null && $to == null){
                $from = $from." 00:00:00";
                $to = $to." 23:59:59";
                $where .=" AND created_at >= '". $from ."' " ;
            }

            if($from == null && $to != null){

                $from = $from." 00:00:00";
                $to = $to." 23:59:59";
                $where .=" AND created_at <= '". $to ."'";
            }

            if($genders != 'all'){
            $where .="AND gender = '".$genders ."'" ;
            }
            if($nationalitys != 'all'){
            $where .="AND nationality = '".$nationalitys ."'" ;
            }
            if($Interests != 'all'){
            $where .="AND Interest = '".$Interests ."'" ;
            }
            if($leads != 'all'){
            $where .="AND lead = '".$leads ."'" ;
            }
            if($assigned_to != 'all'){
            $where .="AND assigned_to = '".$assigned_to ."'" ;
            }


            $select =" select * from clients " . $where ." ORDER BY id DESC";
            //dd($select);
            $clients = DB::select($select);
            foreach($clients as $k=>$v)
            {
                $user = DB::table('users')->select('name')->where('id',$v->assigned_to)->get();
                $clients[$k]->assigned_to = $user[0]->name;

                $lead = DB::table('lead')->select('lead')->where('id',$v->lead)->get();
                $clients[$k]->lead = $lead[0]->lead;

                $gender = DB::table('gender')->select('type')->where('id',$v->gender)->get();
                $clients[$k]->gender = $gender[0]->type;

                $Interest = DB::table('interest')->select('type')->where('id',$v->Interest)->get();
                $clients[$k]->Interest = $Interest[0]->type;

                $nationality = DB::table('pages')->select('title_en')->where([['id',$v->nationality],['type','nationality']])->get();
                $clients[$k]->nationality = $nationality[0]->title_en;
            }


            $nationality = DB::table('pages')->where('type','nationality')->get();
            $Interest = DB::table('interest')->get();
            $gender = DB::table('gender')->get();
            $lead = DB::table('lead')->get();
            $users = DB::table('users')->get();


            $message = "<span style='padding-right:45%;font-weight:bold;'> تم البحث بنجاح : </span> <br><br>";

            $message = 'تم البحث بنجاح (عدد النتائج '.count($clients).'): <br>';

            if($from != null && $to == null){
                $from = date('d-m-Y');
                $message .=" <span style='padding-left:5%;color:#364150'> التاريخ من :     "  . $from .'</span><br>';
            }

            if($from == null && $to != null){
                $to = date('d-m-Y');
                $message .="<span style='padding-left:5%;color:#364150'> التاريخ الي :"  . $to .'</span><br>';
            }

            if($from != null && $to != null){
            $message .="<span style='padding-left:5%;color:#364150'> التاريخ من  : "  . $from . "    ----      الي :   " . $to .'</span><br>';
            }

            if($genders != 'all'){
                $gendermsg = DB::table('gender')->select('type')->where('id',$genders)->get();
                $message .=" <span style='padding-left:5%;color:#364150'> النوع :     " . $gendermsg[0]->type .'</span>';
            }

            if($nationalitys != 'all'){
                $natmsg = DB::table('pages')->select('title_en')->where([['type','nationality'],['id',$nationalitys]])->get();
                $message .="<span style='padding-left:5%;color:#364150'> الجنسية :     " . $natmsg[0]->title_en .'</span> ' ;

            }

            if($Interests != 'all'){
                $intermsg = DB::table('interest')->select('type')->where('id',$Interests)->get();
                $message .=" <span style='padding-left:5%;color:#364150'> درجة الاهتمام :     " . $intermsg[0]->type .'</span>';
            }

            if($leads != 'all'){
                $leadmsg = DB::table('lead')->select('lead')->where('id',$assigned_to)->get();
                $message .=" <span style='padding-left:5%;color:#364150'> مصدر من :     " . $leadmsg[0]->lead .'</span>';
            }

            if($assigned_to != 'all'){
                $usermsg = DB::table('users')->select('name')->where('id',$assigned_to)->get();
                $message .=" <span style='padding-left:5%;color:#364150'> تابع ل :     " . $usermsg[0]->name.'</span>' ;
            }


            return view('admin.reports.clientsreport',compact('message','clients','nationality','users','Interest','gender','lead','from','to','genders','nationalitys','Interests','leads','assigned_to'));

    }

    public function clients_report_excel(Request $request)

    {
        RolesController::checkroles('75');

            $from =  $request->from;
            $to =  $request->to;
            $genders =  $request->gender;
            $nationalitys =  $request->nationality;
            $Interests =  $request->Interest;
            $leads =  $request->lead;
            $assigned_to =  $request->assigned_to;


            $where="WHERE status = '1' ";

             if($from != '' && $to != '' ){
                $from = $from." 00:00:00";
                $to = $to." 23:59:59";

                $where .="AND created_at BETWEEN '". $from ."' AND '". $to ."' " ;
            }

            if($from != null && $to == null){
                $from = $from." 00:00:00";
                $to = $to." 23:59:59";
                $where .=" AND created_at >= '". $from ."' " ;
            }

            if($from == null && $to != null){

                $from = $from." 00:00:00";
                $to = $to." 23:59:59";
                $where .=" AND created_at <= '". $to ."'";
            }

            if($genders != 'all'){
            $where .="AND gender = '".$genders ."'" ;
            }
            if($nationalitys != 'all'){
            $where .="AND nationality = '".$nationalitys ."'" ;
            }
            if($Interests != 'all'){
            $where .="AND Interest = '".$Interests ."'" ;
            }
            if($leads != 'all'){
            $where .="AND lead = '".$leads ."'" ;
            }
            if($assigned_to != 'all'){
            $where .="AND assigned_to = '".$assigned_to ."'" ;
            }


            $select =" select * from clients " . $where ." ORDER BY id DESC" ;
            $clients = DB::select($select);
            foreach($clients as $k=>$v)
            {
                $user = DB::table('users')->select('name')->where('id',$v->assigned_to)->get();
                $clients[$k]->assigned_to = $user[0]->name;

                $lead = DB::table('lead')->select('lead')->where('id',$v->lead)->get();
                $clients[$k]->lead = $lead[0]->lead;

                $gender = DB::table('gender')->select('type')->where('id',$v->gender)->get();
                $clients[$k]->gender = $gender[0]->type;

                $Interest = DB::table('interest')->select('type')->where('id',$v->Interest)->get();
                $clients[$k]->Interest = $Interest[0]->type;

                $nationality = DB::table('pages')->select('title_en')->where([['id',$v->nationality],['type','nationality']])->get();
                $clients[$k]->nationality = $nationality[0]->title_en;
            }

            return Excel::download(new ClientsExport($clients), 'Clients_report-'.date("d-m-Y-h:i:s-A").'.xlsx');

    }
    public function clients_report_import(Request $request)

    {


        Excel::import(new UsersImport, 'clients.xls');

        return redirect('/')->with('success', 'All good!');
    }



    public function tasks_report()

    {
        RolesController::checkroles('78');
            $tasks = DB::table('tasks')->where('status','=','10')->get();
            foreach($tasks as $k=>$v)
            {
                $user = DB::table('users')->select('name')->where('id',$v->assigned_to)->get();
                $tasks[$k]->assigned_to = $user[0]->name;
                if($v->type == 'client'){
                    $clients = DB::table('clients')->where('id',$v->itemid)->get();
                    $tasks[$k]->client = $clients[0]->name;
                    $tasks[$k]->client_en = $clients[0]->name_en;
                    $tasks[$k]->clientnum = $clients[0]->number;
                }
                elseif($v->type == 'clientvsservices'){
                $clientvsservices = DB::table('clientsvsservices')->where('id',$v->itemid)->get();
                $clients = DB::table('clients')->where('id',$clientvsservices[0]->client)->get();
                $tasks[$k]->client = $clients[0]->name;
                $tasks[$k]->client_en = $clients[0]->name_en;
                $tasks[$k]->clientnum = $clients[0]->number;
                $services = DB::table('services')->where('id',$clientvsservices[0]->service)->get();
                $service_name = DB::table('products')->where('id',$services[0]->service_name)->get();
                $tasks[$k]->service = $service_name[0]->name;
                }
            }

            $users = DB::table('users')->get();

            $message = '';

            return view('admin.reports.tasksreport',compact('tasks','message','users'));
    }

    public function tasks_report_search(Request $request)

    {
        RolesController::checkroles('78');
            $from = $request->from;
            $to = $request->to;
            $assigned_to = $request->user;
            $case = $request->case;

            $where=" where '1' ";

            if($assigned_to != 'all'){
                $where .=" AND assigned_to = '".$assigned_to ."'" ;

            }

            if($case == '1'){
                $where .=" AND status != '2' " ;
            }

            if($case == '2'){
                $date = date('Y-m-d');
                $where .=" AND assigned_at >= '".$date."' And status = '1'" ;
            }

            if($case == '3'){
                $date = date('Y-m-d');
                $where .=" AND assigned_at < '".$date."' And status = '1' " ;
            }

            if($case == '4'){
                $where .=" AND status = '3'" ;
            }

            if($from != null && $to == null){
                $from = date('d-m-Y');
                $where .=" AND assigned_at >= '". $from ."' " ;
            }

            if($from == null && $to != null){
                $to = date('d-m-Y');
                $where .=" AND assigned_at <= '". $to ."'";
            }

            if($from != null && $to != null){
            $where .=" AND assigned_at BETWEEN '". $from ."' AND '". $to ."' " ;
            }
            $select =" select * from tasks  " . $where ." ORDER BY id DESC ";
            // dd($select);
            $tasks = DB::select($select);

            foreach($tasks as $k=>$v)
            {
                $user = DB::table('users')->select('name')->where('id',$v->assigned_to)->get();
                $tasks[$k]->assigned_to = $user[0]->name;
                if($v->type == 'client'){
                    $clients = DB::table('clients')->where('id',$v->itemid)->get();
                    $tasks[$k]->client = $clients[0]->name;
                    $tasks[$k]->client_en = $clients[0]->name_en;
                    $tasks[$k]->clientnum = $clients[0]->number;
                }
                elseif($v->type == 'clientvsservices'){
                $clientvsservices = DB::table('clientsvsservices')->where('id',$v->itemid)->get();
                $clients = DB::table('clients')->where('id',$clientvsservices[0]->client)->get();
                $tasks[$k]->client = $clients[0]->name;
                $tasks[$k]->client_en = $clients[0]->name_en;
                $tasks[$k]->clientnum = $clients[0]->number;
                $services = DB::table('services')->where('id',$clientvsservices[0]->service)->get();
                $service_name = DB::table('products')->where('id',$services[0]->service_name)->get();
                $tasks[$k]->service = $service_name[0]->name;
                }
            }
            //dd(\Request::segment(5));

            $message = 'تم البحث بنجاح (عدد النتائج '.count($tasks).'): <br>';

            if($from != null && $to == null){
                $from = date('d-m-Y');
                $message .=" التاريخ من :     "  . $from .'<br>';
            }

            if($from == null && $to != null){
                $to = date('d-m-Y');
                $message .=" التاريخ الي :"  . $to .'<br>';
            }

            if($from != null && $to != null){
            $message .=" التاريخ من  : "  . $from . "    ----      الي :   " . $to .'<br>';
            }
            if($case == '1'){
                $message .=" الحالة : الكل". "<br>";
            }

            if($case == '2'){
                $date = date('d-m-Y');
                $message .=" الحالة : جاري" . "<br>";
            }

            if($case == '3'){
                $date = date('d-m-Y');
                $message .=" الحالة : متاخرة" . "<br>";
            }

            if($case == '4'){
                $message .=" الحالة : تمت" . "<br>" ;
            }


            if($assigned_to != 'all'){
                $usermsg = DB::table('users')->select('name')->where('id',$assigned_to)->get();
                $message .="  تابع ل :     " . $usermsg[0]->name ;

            }

        //dd(count($tasks));

            $users = DB::table('users')->get();
            return view('admin.reports.tasksreport',compact('message','tasks','users','from','to','case','assigned_to'));
    }


    public function tasks_report_excel(Request $request)

    {
        RolesController::checkroles('79');
        $from = $request->from;
        $to = $request->to;
        $assigned_to = $request->assigned_to;
        $case = $request->case;

        $where=" where '1' ";

        if($assigned_to != 'all'){
            $where .=" AND assigned_to = '".$assigned_to ."'" ;

        }

        if($case == '1'){
            $where .=" AND status != '2' " ;
        }

        if($case == '2'){
            $date = date('d-m-Y');
            $where .=" AND assigned_at >= '".$date."' And status = '1'" ;
        }

        if($case == '3'){
            $date = date('d-m-Y');
            $where .=" AND assigned_at < '".$date."' And status = '1' " ;
        }

        if($case == '4'){
            $where .=" AND status = '3'" ;
        }

        if($from != null && $to == null){
            $from = date('d-m-Y');
            $where .=" AND assigned_at >= '". $from ."' " ;
        }

        if($from == null && $to != null){
            $to = date('d-m-Y');
            $where .=" AND assigned_at <= '". $to ."'";
        }

        if($from != null && $to != null){
        $where .=" AND assigned_at BETWEEN '". $from ."' AND '". $to ."' " ;
        }
        $select =" select * from tasks  " . $where ;
        //dd($select);
        $tasks = DB::select($select);

        foreach($tasks as $k=>$v)
        {
            $user = DB::table('users')->select('name')->where('id',$v->assigned_to)->get();
            $tasks[$k]->assigned_to = $user[0]->name;
            if($v->type == 'client'){
                $clients = DB::table('clients')->where('id',$v->itemid)->get();
                $tasks[$k]->client = $clients[0]->name;
                $tasks[$k]->client_en = $clients[0]->name_en;
                $tasks[$k]->clientnum = $clients[0]->number;
            }
            elseif($v->type == 'clientvsservices'){
            $clientvsservices = DB::table('clientsvsservices')->where('id',$v->itemid)->get();
            $clients = DB::table('clients')->where('id',$clientvsservices[0]->client)->get();
            $tasks[$k]->client = $clients[0]->name;
            $tasks[$k]->client_en = $clients[0]->name_en;
            $tasks[$k]->clientnum = $clients[0]->number;
            $services = DB::table('services')->where('id',$clientvsservices[0]->service)->get();
            $service_name = DB::table('products')->where('id',$services[0]->service_name)->get();
            $tasks[$k]->service = $service_name[0]->name;
            }
        }
        //dd(\Request::segment(5));

        $message = 'تم البحث بنجاح : <br>';

        if($from != null && $to == null){
            $from = date('d-m-Y');
            $message .=" التاريخ من :     "  . $from .'<br>';
        }

        if($from == null && $to != null){
            $to = date('d-m-Y');
            $message .=" التاريخ الي :"  . $to .'<br>';
        }

        if($from != null && $to != null){
        $message .=" التاريخ من  : "  . $from . "    ----      الي :   " . $to .'<br>';
        }
        if($case == '1'){
            $message .=" الحالة : الكل". "<br>";
        }

        if($case == '2'){
            $date = date('d-m-Y');
            $message .=" الحالة : جاري" . "<br>";
        }

        if($case == '3'){
            $date = date('d-m-Y');
            $message .=" الحالة : متاخرة" . "<br>";
        }

        if($case == '4'){
            $message .=" الحالة : تمت" . "<br>" ;
        }


        if($assigned_to != 'all'){
            $usermsg = DB::table('users')->select('name')->where('id',$assigned_to)->get();
            $message .="  تابع ل :     " . $usermsg[0]->name ;

        }




        return Excel::download(new TasksExport($tasks), 'Tasks_report-'.date("d-m-Y-h:i:s-A").'.xlsx');

    }

    public function users_report()

    {
        RolesController::checkroles('76');
             $tasks = DB::table('tasks')->where('status','=','10')->get();
            foreach($tasks as $k=>$v)
            {
                $user = DB::table('users')->select('name')->where('id',$v->assigned_to)->get();
                $tasks[$k]->assigned_to = $user[0]->name;
                if($v->type == 'client'){
                    $clients = DB::table('clients')->where('id',$v->itemid)->get();
                    $tasks[$k]->client = $clients[0]->name;
                    $tasks[$k]->client_en = $clients[0]->name_en;
                    $tasks[$k]->clientnum = $clients[0]->number;
                }
                elseif($v->type == 'clientvsservices'){
                $clientvsservices = DB::table('clientsvsservices')->where('id',$v->itemid)->get();
                $clients = DB::table('clients')->where('id',$clientvsservices[0]->client)->get();
                $tasks[$k]->client = $clients[0]->name;
                $tasks[$k]->client_en = $clients[0]->name_en;
                $tasks[$k]->clientnum = $clients[0]->number;
                $services = DB::table('services')->where('id',$clientvsservices[0]->service)->get();
                $service_name = DB::table('products')->where('id',$services[0]->service_name)->get();
                $tasks[$k]->service = $service_name[0]->name;
                }
            }

            $clientvsservices = DB::table('clientsvsservices')->where('service','0')->get();
            foreach($clientvsservices as $k=>$v)
            {

                $clients = DB::table('clients')->where('id',$v->client)->get();
                $clientvsservices[$k]->name = $clients[0]->name;
                $clientvsservices[$k]->name_en = $clients[0]->name_en;
                $clientvsservices[$k]->number = $clients[0]->number;
                $clientvsservices[$k]->email = $clients[0]->email;
                $user = DB::table('users')->select('name')->where('id',$clients[0]->assigned_to)->get();
                $clientvsservices[$k]->assigned_to = $user[0]->name;

            }

            $clients = DB::table('clients')->where('status','2')->get();
            foreach($clients as $k=>$v)
            {
                $user = DB::table('users')->select('name')->where('id',$v->assigned_to)->get();
                $clients[$k]->assigned_to = $user[0]->name;

                $lead = DB::table('lead')->select('lead')->where('id',$v->lead)->get();
                $clients[$k]->lead = $lead[0]->lead;

                $gender = DB::table('gender')->select('type')->where('id',$v->gender)->get();
                $clients[$k]->gender = $gender[0]->type;

                $Interest = DB::table('interest')->select('type')->where('id',$v->Interest)->get();
                $clients[$k]->Interest = $Interest[0]->type;

                $nationality = DB::table('pages')->select('title_en')->where([['id',$v->nationality],['type','nationality']])->get();
                $clients[$k]->nationality = $nationality[0]->title_en;
            }
            $users = DB::table('users')->get();

            $message = '';

            return view('admin.reports.usersreport',compact('tasks','message','users','clients','clientvsservices'));
    }

    public function users_report_search(Request $request)

    {
        RolesController::checkroles('76');

            $from = $request->from;
            $to = $request->to;
            $assigned_to = $request->user;
            $case = $request->case;

            $wheretasks=" where '1' ";

            if($assigned_to != 'all'){
                $wheretasks .=" AND created_by = '".$assigned_to ."' OR assigned_to  = '".$assigned_to ."' " ;
            }

            if($from != null && $to == null){
                $fromtasks = $from." 00:00:00";
                $wheretasks .=" AND created_at >= '". $fromtasks ."' " ;
            }

            if($from == null && $to != null){
                $totasks = $to." 23:59:59";
                $wheretasks .=" AND created_at <= '". $totasks ."'";
            }

            if($from != null && $to != null){
                $fromtasks = $from." 00:00:00";
                $totasks = $to." 23:59:59";
                $wheretasks .=" AND created_at BETWEEN '". $fromtasks ."' AND '". $totasks ."' " ;
            }
            $selecttasks =" select * from tasks  " . $wheretasks ." ORDER BY id DESC " ;
            $tasks = DB::select($selecttasks);
            foreach($tasks as $k=>$v)
            {
                $user = DB::table('users')->select('name')->where('id',$v->assigned_to)->get();
                $tasks[$k]->assigned_to = $user[0]->name;
                $user = DB::table('users')->select('name')->where('id',$v->created_by)->get();
                $tasks[$k]->created_by = $user[0]->name;

                if($v->type == 'client'){
                    $clientstasks = DB::table('clients')->where('id',$v->itemid)->get();
                    $tasks[$k]->client = $clientstasks[0]->name;
                    $tasks[$k]->client_en = $clientstasks[0]->name_en;
                    $tasks[$k]->clientnum = $clientstasks[0]->number;
                }
                elseif($v->type == 'clientvsservices'){
                $clientvsservices = DB::table('clientsvsservices')->where('id',$v->itemid)->get();
                $clienttasks = DB::table('clients')->where('id',$clientvsservices[0]->client)->get();
                $tasks[$k]->client = $clienttasks[0]->name;
                $tasks[$k]->client_en = $clienttasks[0]->name_en;
                $tasks[$k]->clientnum = $clienttasks[0]->number;
                $services = DB::table('services')->where('id',$clientvsservices[0]->service)->get();
                $service_name = DB::table('products')->where('id',$services[0]->service_name)->get();
                $tasks[$k]->service = $service_name[0]->name;
                }
            }

            //dd($tasks);

            $whereclients=" WHERE status = '1' ";

            if($assigned_to != 'all'){
                $whereclients .=" AND (created_by = '".$assigned_to ."' OR assigned_to  = '".$assigned_to ."' )" ;
            }

            if($from != null && $to == null){
                $fromclients = $from." 00:00:00";
                $whereclients .=" AND created_at >= '". $fromclients ."' " ;
            }

            if($from == null && $to != null){

                $toclients = $to." 23:59:59";
                $whereclients .=" AND created_at <= '". $toclients ."'";
            }

            if($from != null && $to != null){
                $fromclients = $from." 00:00:00";
                $toclients = $to." 23:59:59";
                $whereclients .=" AND created_at BETWEEN '". $fromclients ."' AND '". $toclients ."' " ;
            }
            //dd($whereclients);
            $selectclients =" select * from clients " . $whereclients ." ORDER BY id DESC " ;
            $clients = DB::select($selectclients);
            //dd($clients);
            foreach($clients as $k=>$v)
            {
                $user = DB::table('users')->select('name')->where('id',$v->created_by)->get();
                $clients[$k]->created_by = $user[0]->name;

                $user2 = DB::table('users')->select('name')->where('id',$v->assigned_to)->get();
                $clients[$k]->assigned_to = $user2[0]->name;

                $lead = DB::table('lead')->select('lead')->where('id',$v->lead)->get();
                $clients[$k]->lead = $lead[0]->lead;

                $gender = DB::table('gender')->select('type')->where('id',$v->gender)->get();
                $clients[$k]->gender = $gender[0]->type;

                $Interest = DB::table('interest')->select('type')->where('id',$v->Interest)->get();
                $clients[$k]->Interest = $Interest[0]->type;

                $nationality = DB::table('pages')->select('title_en')->where([['id',$v->nationality],['type','nationality']])->get();
                $clients[$k]->nationality = $nationality[0]->title_en;
                //dd($clients);
            }
            //dd($clients);
            $clientvsservices = DB::table('clientsvsservices')->where('created_by',$assigned_to)->orderBy('id', 'desc')->get();
            foreach($clientvsservices as $k=>$v)
            {

                $clientservice = DB::table('clients')->where('id',$v->client)->get();
                $clientvsservices[$k]->name = $clientservice[0]->name;
                $clientvsservices[$k]->name_en = $clientservice[0]->name_en;
                $clientvsservices[$k]->number = $clientservice[0]->number;
                $clientvsservices[$k]->email = $clientservice[0]->email;
                $user = DB::table('users')->select('name')->where('id',$clientservice[0]->created_by)->get();
                $clientvsservices[$k]->created_by = $user[0]->name;

            }
            //dd($clientvsservices);

            $message = 'تم البحث بنجاح : <br>';

            if($from != null && $to == null){
                $from = date('d-m-Y');
                $message .=" التاريخ من :     "  . $from .'<br>';
            }

            if($from == null && $to != null){
                $to = date('d-m-Y');
                $message .=" التاريخ الي :"  . $to .'<br>';
            }

            if($from != null && $to != null){
            $message .=" التاريخ من  : "  . $from . "    ----      الي :   " . $to .'<br>';
            }

            if($assigned_to != 'all'){
                $usermsg = DB::table('users')->select('name')->where('id',$assigned_to)->get();
                $message .="  تابع ل :     " . $usermsg[0]->name ;

            }


            $users = DB::table('users')->get();
            return view('admin.reports.usersreport',compact('clientvsservices','message','tasks','clients','users','from','to','assigned_to','case'));

    }

    public function users_report_excel(Request $request)

    {
        RolesController::checkroles('76');

            $from = $request->from;
            $to = $request->to;
            $assigned_to = $request->assigned_to;
            $case = $request->case;

            $wheretasks=" where '1' ";

            if($assigned_to != 'all'){
                $wheretasks .=" AND created_by = '".$assigned_to ."' OR assigned_to  = '".$assigned_to ."' " ;
            }

            if($from != null && $to == null){
                $fromtasks = $from." 00:00:00";
                $wheretasks .=" AND created_at >= '". $fromtasks ."' " ;
            }

            if($from == null && $to != null){
                $totasks = $to." 23:59:59";
                $wheretasks .=" AND created_at <= '". $totasks ."'";
            }

            if($from != null && $to != null){
                $fromtasks = $from." 00:00:00";
                $totasks = $to." 23:59:59";
                $wheretasks .=" AND created_at BETWEEN '". $fromtasks ."' AND '". $totasks ."' " ;
            }
            $selecttasks =" select * from tasks  " . $wheretasks ;
            $tasks = DB::select($selecttasks);
            foreach($tasks as $k=>$v)
            {
                $user = DB::table('users')->select('name')->where('id',$v->assigned_to)->get();
                $tasks[$k]->assigned_to = $user[0]->name;
                $user = DB::table('users')->select('name')->where('id',$v->created_by)->get();
                $tasks[$k]->created_by = $user[0]->name;

                if($v->type == 'client'){
                    $clientstasks = DB::table('clients')->where('id',$v->itemid)->get();
                    $tasks[$k]->client = $clientstasks[0]->name;
                    $tasks[$k]->client_en = $clientstasks[0]->name_en;
                    $tasks[$k]->clientnum = $clientstasks[0]->number;
                }
                elseif($v->type == 'clientvsservices'){
                $clientvsservices = DB::table('clientsvsservices')->where('id',$v->itemid)->get();
                $clienttasks = DB::table('clients')->where('id',$clientvsservices[0]->client)->get();
                $tasks[$k]->client = $clienttasks[0]->name;
                $tasks[$k]->client_en = $clienttasks[0]->name_en;
                $tasks[$k]->clientnum = $clienttasks[0]->number;
                $services = DB::table('services')->where('id',$clientvsservices[0]->service)->get();
                $service_name = DB::table('products')->where('id',$services[0]->service_name)->get();
                $tasks[$k]->service = $service_name[0]->name;
                }
            }

            //dd($tasks);

            $whereclients=" WHERE status = '1' ";

            if($assigned_to != 'all'){
                $whereclients .=" AND (created_by = '".$assigned_to ."' OR assigned_to  = '".$assigned_to ."' )" ;
            }

            if($from != null && $to == null){
                $fromclients = $from." 00:00:00";
                $whereclients .=" AND created_at >= '". $fromclients ."' " ;
            }

            if($from == null && $to != null){

                $toclients = $to." 23:59:59";
                $whereclients .=" AND created_at <= '". $toclients ."'";
            }

            if($from != null && $to != null){
                $fromclients = $from." 00:00:00";
                $toclients = $to." 23:59:59";
                $whereclients .=" AND created_at BETWEEN '". $fromclients ."' AND '". $toclients ."' " ;
            }
            //dd($whereclients);
            $selectclients =" select * from clients " . $whereclients ;
            $clients = DB::select($selectclients);
            //dd($clients);
            foreach($clients as $k=>$v)
            {
                $user = DB::table('users')->select('name')->where('id',$v->created_by)->get();
                $clients[$k]->created_by = $user[0]->name;

                $user2 = DB::table('users')->select('name')->where('id',$v->assigned_to)->get();
                $clients[$k]->assigned_to = $user2[0]->name;

                $lead = DB::table('lead')->select('lead')->where('id',$v->lead)->get();
                $clients[$k]->lead = $lead[0]->lead;

                $gender = DB::table('gender')->select('type')->where('id',$v->gender)->get();
                $clients[$k]->gender = $gender[0]->type;

                $Interest = DB::table('interest')->select('type')->where('id',$v->Interest)->get();
                $clients[$k]->Interest = $Interest[0]->type;

                $nationality = DB::table('pages')->select('title_en')->where([['id',$v->nationality],['type','nationality']])->get();
                $clients[$k]->nationality = $nationality[0]->title_en;
                //dd($clients);
            }
            //dd($clients);
            $clientvsservices = DB::table('clientsvsservices')->where('created_by',$assigned_to)->get();
            foreach($clientvsservices as $k=>$v)
            {

                $clientservice = DB::table('clients')->where('id',$v->client)->get();
                $clientvsservices[$k]->name = $clientservice[0]->name;
                $clientvsservices[$k]->name = $clientservice[0]->name;
                $clientvsservices[$k]->name_en = $clientservice[0]->name_en;
                $clientvsservices[$k]->number = $clientservice[0]->number;
                $clientvsservices[$k]->email = $clientservice[0]->email;
                $user = DB::table('users')->select('name')->where('id',$clientservice[0]->created_by)->get();
                $clientvsservices[$k]->created_by = $user[0]->name;

            }
            //dd($clientvsservices);

            $message = 'تم البحث بنجاح : <br>';

            if($from != null && $to == null){
                $from = date('d-m-Y');
                $message .=" التاريخ من :     "  . $from .'<br>';
            }

            if($from == null && $to != null){
                $to = date('d-m-Y');
                $message .=" التاريخ الي :"  . $to .'<br>';
            }

            if($from != null && $to != null){
            $message .=" التاريخ من  : "  . $from . "    ----      الي :   " . $to .'<br>';
            }

            if($assigned_to != 'all'){
                $usermsg = DB::table('users')->select('name')->where('id',$assigned_to)->get();
                $message .="  تابع ل :     " . $usermsg[0]->name ;

            }


            $users = DB::table('users')->get();


            return Excel::download(new UsersExport($clients,$clientvsservices,$tasks), 'Users_report-'.date("d-m-Y-h:i:s-A").'.xlsx');

    }

}
