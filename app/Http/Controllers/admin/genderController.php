<?php



namespace App\Http\Controllers\admin;



use Validator;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;




class genderController extends Controller

{

    public function __construct()

       {

           // parent::__construct();

            $this->type = 'gender';

            $this->module = 'gender';

            $this->picsnum = 5;

            $this->lang_arr = array("en" => "اللغة الانجليزية" );

            view()->share('module', $this->module );

            view()->share('active', 'settings');

            view()->share('titlepage','Gender');

            view()->share('picsnum', $this->picsnum);

            $id = Session::get('uid');
            $roles = DB::table('usersvsroles')->where("user_id",$id)->get();
            $rolesarr = array();
            for($i=0;$i<count($roles);$i++){
                $rolesarr[$i] = $roles[$i]->role_id;
            }
            view()->share('getroles', $rolesarr);

       }



    public function index()

    {
        RolesController::checkroles('63');

        $leads = DB::table('gender')->paginate(10);
        return view('admin.settings.gender',compact('leads'));
    }



    public function add()

    {
        RolesController::checkroles('64');

        return view('admin.settings.genderadd');
    }



    public function store(Request $request)

    {
        RolesController::checkroles('64');

        unset($_POST['_token']);
        DB::table('gender')->insert($_POST);
        return redirect(url('admin/gender/index'))->with('message', 'تم بنجاح');

    }


    public function edit ($id)
    {
       RolesController::checkroles('65');

       $places = DB::table('gender')->where('id',$id)->get();
       return view('admin.settings.genderedit',compact('cats','time','gender','places','products','leads','nationalitys'));
    }

    public function update(Request $request)
    {
        RolesController::checkroles('65');

        unset($_POST['_token']);
        DB::table('gender')->where('id', $request->id)->update($_POST);
        return redirect(url('admin/gender/index'))->with('message', 'تم بنجاح');
    }



    public function del($id)

    {
        RolesController::checkroles('66');

        $cat = DB::table('gender')->where('id',$id)->delete();
		return redirect(url('/admin/gender/index'))->with('message', 'تم بنجاح');
    }

}
