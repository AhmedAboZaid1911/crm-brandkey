<?php



namespace App\Http\Controllers\admin;



use Validator;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;



class TasksController extends Controller

{

    public function __construct()

       {

           // parent::__construct();

            $this->type = 'tasks';

            $this->module = 'tasks';

            $this->picsnum = 5;

            $this->lang_arr = array("en" => "اللغة الانجليزية" );

            view()->share('module', $this->module );

            view()->share('active', 'Tasks');

            view()->share('titlepage','Tasks');

            view()->share('picsnum', $this->picsnum);


            $id = Session::get('uid');
            $roles = DB::table('usersvsroles')->where("user_id",$id)->get();
            $rolesarr = array();
            for($i=0;$i<count($roles);$i++){
                $rolesarr[$i] = $roles[$i]->role_id;
            }
            view()->share('getroles', $rolesarr);

       }



    public function all()

    {
        RolesController::checkroles('7');
        $id = Session::get('uid');
        $tasks = DB::table('tasks')->where([['status','!=','2'],['assigned_to',$id]])->orderBy('assigned_at', 'asc')->paginate(10);
        foreach($tasks as $k=>$v)
        {
            $user = DB::table('users')->select('name')->where('id',$v->assigned_to)->get();
            $tasks[$k]->assigned_to = $user[0]->name;
            if($v->type == 'client'){
                $clients = DB::table('clients')->where('id',$v->itemid)->get();
                $tasks[$k]->client = $clients[0]->name;
                $tasks[$k]->client_en = $clients[0]->name_en;
                $tasks[$k]->clientnum = $clients[0]->number;
            }
            elseif($v->type == 'clientvsservices'){
            $clientvsservices = DB::table('clientsvsservices')->where('id',$v->itemid)->get();
            $clients = DB::table('clients')->where('id',$clientvsservices[0]->client)->get();
            $tasks[$k]->client = $clients[0]->name;
            $tasks[$k]->client_en = $clients[0]->name_en;
            $tasks[$k]->clientnum = $clients[0]->number;
            $services = DB::table('services')->where('id',$clientvsservices[0]->service)->get();
            $service_name = DB::table('products')->where('id',$services[0]->service_name)->get();
            $tasks[$k]->service = $service_name[0]->name;
            }
        }
        return view('admin.tasks.tasks',compact('tasks','id'));
    }

    public function done()

    {
        RolesController::checkroles('7');
        $id = Session::get('uid');
        $tasks = DB::table('tasks')->where([['status','3'],['assigned_to',$id]])->orderBy('assigned_at', 'desc')->paginate(10);
        foreach($tasks as $k=>$v)
        {
            $user = DB::table('users')->select('name')->where('id',$v->assigned_to)->get();
            $tasks[$k]->assigned_to = $user[0]->name;
            if($v->type == 'client'){
                $clients = DB::table('clients')->where('id',$v->itemid)->get();
                $tasks[$k]->client = $clients[0]->name;
                $tasks[$k]->client_en = $clients[0]->name_en;
                $tasks[$k]->clientnum = $clients[0]->number;
            }
            elseif($v->type == 'clientvsservices'){
            $clientvsservices = DB::table('clientsvsservices')->where('id',$v->itemid)->get();
            $clients = DB::table('clients')->where('id',$clientvsservices[0]->client)->get();
            $tasks[$k]->client = $clients[0]->name;
            $tasks[$k]->client_en = $clients[0]->name_en;
            $tasks[$k]->clientnum = $clients[0]->number;
            $services = DB::table('services')->where('id',$clientvsservices[0]->service)->get();
            $service_name = DB::table('products')->where('id',$services[0]->service_name)->get();
            $tasks[$k]->service = $service_name[0]->name;
            }
        }


            return view('admin.tasks.tasks',compact('tasks','id' ));

    }
    public function late()
    {
        RolesController::checkroles('7');
        $id = Session::get('uid');
        $tasks = DB::table('tasks')->where([['assigned_at','<',date('Y-m-d')],['status','=','1'],['assigned_to',$id]])->orderBy('assigned_at', 'asc')->paginate(10);

        foreach($tasks as $k=>$v)
        {
            $user = DB::table('users')->select('name')->where('id',$v->assigned_to)->get();
            $tasks[$k]->assigned_to = $user[0]->name;
            if($v->type == 'client'){
                $clients = DB::table('clients')->where('id',$v->itemid)->get();
                $tasks[$k]->client = $clients[0]->name;
                $tasks[$k]->client_en = $clients[0]->name_en;
                $tasks[$k]->clientnum = $clients[0]->number;
            }
            elseif($v->type == 'clientvsservices'){
            $clientvsservices = DB::table('clientsvsservices')->where('id',$v->itemid)->get();
            $clients = DB::table('clients')->where('id',$clientvsservices[0]->client)->get();
            $tasks[$k]->client = $clients[0]->name;
            $tasks[$k]->client_en = $clients[0]->name_en;
            $tasks[$k]->clientnum = $clients[0]->number;
            $services = DB::table('services')->where('id',$clientvsservices[0]->service)->get();
            $service_name = DB::table('products')->where('id',$services[0]->service_name)->get();
            $tasks[$k]->service = $service_name[0]->name;
            }
        }

        return view('admin.tasks.tasks',compact('tasks','id'));

    }
    public function today()

    {
        RolesController::checkroles('7');
        $id = Session::get('uid');
        $tasks = DB::table('tasks')->where([['assigned_at','=',date('Y-m-d')],['status','=','1'],['assigned_to',$id]])->orderBy('assigned_at', 'desc')->paginate(10);
        foreach($tasks as $k=>$v)
        {
            $user = DB::table('users')->select('name')->where('id',$v->assigned_to)->get();
            $tasks[$k]->assigned_to = $user[0]->name;
            if($v->type == 'client'){
                $clients = DB::table('clients')->where('id',$v->itemid)->get();
                $tasks[$k]->client = $clients[0]->name;
                $tasks[$k]->client_en = $clients[0]->name_en;
                $tasks[$k]->clientnum = $clients[0]->number;
            }
            elseif($v->type == 'clientvsservices'){
            $clientvsservices = DB::table('clientsvsservices')->where('id',$v->itemid)->get();
            $clients = DB::table('clients')->where('id',$clientvsservices[0]->client)->get();
            $tasks[$k]->client = $clients[0]->name;
            $tasks[$k]->client_en = $clients[0]->name_en;
            $tasks[$k]->clientnum = $clients[0]->number;
            $services = DB::table('services')->where('id',$clientvsservices[0]->service)->get();
            $service_name = DB::table('products')->where('id',$services[0]->service_name)->get();
            $tasks[$k]->service = $service_name[0]->name;

            }

        }


        return view('admin.tasks.tasks',compact('tasks','id'));

    }
    public function search(Request $request)

    {
        RolesController::checkroles('7');
        $id = Session::get('uid');
        $from = $request->from;
        $to = $request->to;

        if($to == ''){
        $to = date('Y-m-d');
        }
        if( $request->case == '1'){
        $tasks = DB::table('tasks')->wherebetween('assigned_at',[$from,$to])->where([['status','!=','2'],['assigned_to',$id]])->orderBy('assigned_at', 'desc')->paginate(100);
        }
        elseif($request->case == '2'){
        $tasks = DB::table('tasks')->wherebetween('assigned_at',[$from,$to])->where([['status','!=','2'],['status','!=','3'],['assigned_to',$id]])->orderBy('assigned_at', 'desc')->paginate(100);
        }
        elseif($request->case =='3'){
        $tasks = DB::table('tasks')->wherebetween('assigned_at',[$from,$to])->where([['status','=','3'],['assigned_to',$id]])->orderBy('assigned_at', 'desc')->paginate(100);
        }

        foreach($tasks as $k=>$v)
        {
            $user = DB::table('users')->select('name')->where('id',$v->assigned_to)->get();
            $tasks[$k]->assigned_to = $user[0]->name;
            if($v->type == 'client'){
                $clients = DB::table('clients')->where('id',$v->itemid)->get();
                $tasks[$k]->client = $clients[0]->name;
                $tasks[$k]->client_en = $clients[0]->name_en;
                $tasks[$k]->clientnum = $clients[0]->number;
            }
            elseif($v->type == 'clientvsservices'){
            $clientvsservices = DB::table('clientsvsservices')->where('id',$v->itemid)->get();
            $clients = DB::table('clients')->where('id',$clientvsservices[0]->client)->get();
            $tasks[$k]->client = $clients[0]->name;
            $tasks[$k]->client_en = $clients[0]->name_en;
            $tasks[$k]->clientnum = $clients[0]->number;
            $services = DB::table('services')->where('id',$clientvsservices[0]->service)->get();
            $service_name = DB::table('products')->where('id',$services[0]->service_name)->get();
            $tasks[$k]->service = $service_name[0]->name;
            }
        }

        return view('admin.tasks.tasks',compact('tasks','id'));

    }



    public function view($id)

    {
        RolesController::checkroles('7');
        $tasks = DB::table('tasks')->where('id',$id)->get();
        for ($i=0;$i<count($tasks);$i++) {
            $username = DB::table('users')->where('id',$tasks[$i]->created_by)->get();
            $tasks[$i]->username = $username[0]->name;
            $tasks[$i]->logo = $username[0]->logo;
            $assigned_to = DB::table('users')->where('id',$tasks[$i]->assigned_to)->get();
            $tasks[$i]->assigned_to = $assigned_to[0]->name;
        }



        $clientvsservices = DB::table('clientsvsservices')->where('id',$tasks[0]->itemid)->get();
        foreach($clientvsservices as $k=>$v){
            $sid = $v->service;
            $serviceinfo = DB::table('services')->where('id',$sid)->get();

            $price = DB::table('services')->get();
            $clientvsservices[$k]->price1 = $price[0]->service_price;

            $time = DB::table('services_time')->select('name')->where('id',$serviceinfo[0]->service_time)->get();
            $clientvsservices[$k]->time = $time[0]->name;

            // $periods = DB::table('services_period')->where('id',$serviceinfo[0]->service_period)->get();
            // $clientvsservices[$k]->from = $periods[0]->from;
            // $clientvsservices[$k]->to = $periods[0]->to;

            $places = DB::table('services_place')->select('name')->where('id',$serviceinfo[0]->service_place)->get();
            $clientvsservices[$k]->place = $places[0]->name;

            $products = DB::table('products')->select('name')->where('id',$serviceinfo[0]->service_name)->get();
            $clientvsservices[$k]->name = $products[0]->name;

            $currency = DB::table('services_currency')->select('currency')->where('id',$serviceinfo[0]->service_currency)->get();
            $clientvsservices[$k]->currency1 = $currency[0]->currency;

            $days = DB::table('services_days')->where('id',$serviceinfo[0]->service_days)->get();
            $clientvsservices[$k]->day1 = $days[0]->day1;
            $clientvsservices[$k]->day2 = $days[0]->day2;
            $clientvsservices[$k]->day3 = $days[0]->day3;
            $clientvsservices[$k]->day4 = $days[0]->day4;
            $clientvsservices[$k]->day5 = $days[0]->day5;
            $clientvsservices[$k]->day6 = $days[0]->day6;
            $clientvsservices[$k]->day7 = $days[0]->day7;
        }

        $clients = DB::table('clients')->where('id',$clientvsservices[0]->client)->get();

        $comment = DB::table('comments')->where([['itemid',$id],['type','tasks']])->get();
        for ($i=0;$i<count($comment);$i++) {
            $username = DB::table('users')->where('id',$comment[$i]->created_by)->get();
            $comment[$i]->username = $username[0]->name;
            $comment[$i]->logo = $username[0]->logo;
        }

        $user = DB::table('users')->get();



        return view('admin.tasks.tasksadd',compact('users','clients','clientvsservices','comment','tasks','user'));

    }


    public function add ()
    {
        RolesController::checkroles('8');
        $user = DB::table('users')->get();

       return view('admin.tasks.tasksadd',compact('user'));

    }
    public function store(Request $request)

    {
        RolesController::checkroles('8');
        unset($_POST['_token']);
        $_POST['created_at'] = date('Y-m-d H:i:s');
        $_POST['created_by'] = Auth::user()->id ;
        $_POST['type'] = 'general' ;
        DB::table('tasks')->insert($_POST);
        return redirect(url('admin/tasks/all'))->with('message', 'تم بنجاح');

    }


    public function edit ($id)
    {
        RolesController::checkroles('9');



        $tasks = DB::table('tasks')->where('id',$id)->get();
        for ($i=0;$i<count($tasks);$i++) {
            $username = DB::table('users')->where('id',$tasks[$i]->created_by)->get();
            $tasks[$i]->username = $username[0]->name;
            $tasks[$i]->logo = $username[0]->logo;
            $assigned_to = DB::table('users')->where('id',$tasks[$i]->assigned_to)->get();
            $tasks[$i]->assigned_to = $assigned_to[0]->name;
        }





        $user = DB::table('users')->get();

        if($tasks[0]->type != 'general'){
            if($tasks[0]->type == 'client'){
                $clients = DB::table('clients')->where('id',$tasks[0]->itemid)->get();
                }else{
                    $clientvsservices = DB::table('clientsvsservices')->where('id',$tasks[0]->itemid)->get();
                    foreach($clientvsservices as $k=>$v){
                        $sid = $v->service;
                        $serviceinfo = DB::table('services')->where('id',$sid)->get();

                        $price = DB::table('services')->get();
                        $clientvsservices[$k]->price1 = $price[0]->service_price;

                        $time = DB::table('services_time')->select('name')->where('id',$serviceinfo[0]->service_time)->get();
                        $clientvsservices[$k]->time = $time[0]->name;

                        // $periods = DB::table('services_period')->where('id',$serviceinfo[0]->service_period)->get();
                        // $clientvsservices[$k]->from = $periods[0]->from;
                        // $clientvsservices[$k]->to = $periods[0]->to;

                        $places = DB::table('services_place')->select('name')->where('id',$serviceinfo[0]->service_place)->get();
                        $clientvsservices[$k]->place = $places[0]->name;

                        $products = DB::table('products')->select('name')->where('id',$serviceinfo[0]->service_name)->get();
                        $clientvsservices[$k]->name = $products[0]->name;

                        $currency = DB::table('services_currency')->select('currency')->where('id',$serviceinfo[0]->service_currency)->get();
                        $clientvsservices[$k]->currency1 = $currency[0]->currency;

                        $days = DB::table('services_days')->where('id',$serviceinfo[0]->service_days)->get();
                        $clientvsservices[$k]->day1 = $days[0]->day1;
                        $clientvsservices[$k]->day2 = $days[0]->day2;
                        $clientvsservices[$k]->day3 = $days[0]->day3;
                        $clientvsservices[$k]->day4 = $days[0]->day4;
                        $clientvsservices[$k]->day5 = $days[0]->day5;
                        $clientvsservices[$k]->day6 = $days[0]->day6;
                        $clientvsservices[$k]->day7 = $days[0]->day7;
                    }
                    $clients = DB::table('clients')->where('id',$clientvsservices[0]->client)->get();
            }
        }

       return view('admin.tasks.tasksview',compact('tasks','comment','currency','clientvsservices','Services','clients','cats','user','gender','Interest','products','leads','nationalitys'));

    }

    public function update(Request $request)
    {
        RolesController::checkroles('9');
        unset($_POST['_token']);
        DB::table('tasks')->where('id', $request->id)->update($_POST);
        return redirect(url('admin/tasks/index'))->with('message', 'تم بنجاح');
    }


    public function del($id)
    {
        RolesController::checkroles('10');
        $cat = DB::table('tasks')->where('id',$id)->update(['status'=> 0,'deleted_by' => Session::get('username'), 'deleted_at' => date('Y-m-d H:i:s') ]);
        return redirect(url('/admin/tasks/index'))->with('message', 'تم بنجاح');
    }

}
