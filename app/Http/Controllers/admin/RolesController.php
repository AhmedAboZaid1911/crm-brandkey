<?php

namespace App\Http\Controllers\admin;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class RolesController extends Controller
{

    public function __construct()
       {
           // parent::__construct();
            $this->type = 'member';
            $this->module = 'members';

            view()->share('module', $this->module );
            view()->share('titlepage','Members');


       }

    public static function getroles()
    {
        $id = Auth::user()->id;

        $roles = DB::table('usersvsroles')->where("user_id",$id)->get();
        $rolesarr = array();
        for($i=0;$i<count($roles);$i++){
            $rolesarr[$i] = $roles[$i]->role_id;
        }

        return $rolesarr;
    }

    /* For Controller  */
    public static function checkroles ($rid)
    {
        $getroles = RolesController::getroles();

        if (!in_array($rid,$getroles) ){
            ?>
                <meta http-equiv="refresh" content="0;URL='<?=url('/admin/index')?>'" />

            <?php
            die;
        }

    }



    /* For View */

    public static function checkroles_menu ($rid)
    {

        $getroles = RolesController::getroles();

        if (in_array($rid,$getroles) ){
           return true;
        }

    }
}
