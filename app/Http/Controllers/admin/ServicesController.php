<?php



namespace App\Http\Controllers\admin;



use Validator;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;



class ServicesController extends Controller

{



    public function __construct()

       {

           // parent::__construct();

            $this->type = 'Services';

            $this->module = 'Services';

            $this->picsnum = 5;

            $this->day = 15;

            $this->lang_arr = array("en" => "اللغة الانجليزية" );

            view()->share('module', $this->module);

            view()->share('active', 'Services');



            view()->share('cattitlepage','الخدمات');

            view()->share('posttitlepage','الخدمة ');

            view()->share('picsnum', $this->picsnum);

            view()->share('day', $this->day);

            $id = Session::get('uid');
            $roles = DB::table('usersvsroles')->where("user_id",$id)->get();
            $rolesarr = array();
            for($i=0;$i<count($roles);$i++){
                $rolesarr[$i] = $roles[$i]->role_id;
            }
            view()->share('getroles', $rolesarr);

       }



    public function cats()

    {
        RolesController::checkroles('12');


        $countallServices = DB::table('services')->get();
        $Services = DB::table('services')->orderBy('servicesdate', 'desc')->Paginate(20);
        foreach($Services as $k=>$v)
        {
        $time = DB::table('services_time')->select('name')->where('id',$v->service_time)->get();
        $Services[$k]->time = $time[0]->name;

        $time = DB::table('services_currency')->select('currency')->where('id',$v->service_currency)->get();
        $Services[$k]->currency = $time[0]->currency;

        // $periods = DB::table('services_period')->where('id',$v->service_period)->get();
        // $Services[$k]->from = $periods[0]->from;
        // $Services[$k]->to = $periods[0]->to;

        $days = DB::table('services_days')->where('id',$v->service_days)->get();
        $Services[$k]->day1 = $days[0]->day1;
        $Services[$k]->day2 = $days[0]->day2;
        $Services[$k]->day3 = $days[0]->day3;
        $Services[$k]->day4 = $days[0]->day4;
        $Services[$k]->day5 = $days[0]->day5;
        $Services[$k]->day6 = $days[0]->day6;
        $Services[$k]->day7 = $days[0]->day7;

        $places = DB::table('services_place')->select('name')->where('id',$v->service_place)->get();
        $Services[$k]->place = $places[0]->name;

        $products = DB::table('products')->select('name')->where('id',$v->service_name)->get();
        $Services[$k]->name = $products[0]->name;

        $num = DB::table('clientsvsservices')->where([['service',$v->id],['status','1']])->get();
        $Services[$k]->num = count($num);
        }
        return view('admin.'.$this->module.'.cats',compact('Services','user','countallServices'));
    }

    public function search(Request $request)

    {
        RolesController::checkroles('12');
        $searchkey = $request->search;

        $name = DB::table('products')->select('id')->where('name','like','%'.$searchkey .'%')->get();

        foreach ($name as $key => $value) {
            $search1[$key] = DB::table('services')->where('service_name',$value->id)->orderBy('servicesdate', 'desc')->get();

        }
        //dd($search1);
        //print count($name);
        foreach($search1 as $k=>$v)
        {
            if(count($v) > 0){
                foreach ($v as $key => $value) {

            $serv = DB::table('services')->select('id','service_price','servicesdate')->where('id',$value->id)->get();
            $search[$k][$key]['id'] = $serv[0]->id;
            $search[$k][$key]['service_price'] = $serv[0]->service_price;
            $search[$k][$key]['servicesdate'] = $serv[0]->servicesdate;

            $time = DB::table('services_time')->select('name')->where('id',$value->service_time)->get();
            $search[$k][$key]['time'] = $time[0]->name;

            $time = DB::table('services_currency')->select('currency')->where('id',$value->service_currency)->get();
            $search[$k][$key]['currency'] = $time[0]->currency;

            // $periods = DB::table('services_period')->where('id',$value->service_period)->get();
            // $search[$k][$key]['from'] = $periods[0]->from;
            // $search[$k][$key]['to'] = $periods[0]->to;

            $days = DB::table('services_days')->where('id',$value->service_days)->get();
            $search[$k][$key]['day1'] = $days[0]->day1;
            $search[$k][$key]['day2'] = $days[0]->day2;
            $search[$k][$key]['day3'] = $days[0]->day3;
            $search[$k][$key]['day4'] = $days[0]->day4;
            $search[$k][$key]['day5'] = $days[0]->day5;
            $search[$k][$key]['day6'] = $days[0]->day6;
            $search[$k][$key]['day7'] = $days[0]->day7;

            $places = DB::table('services_place')->select('name')->where('id',$value->service_place)->get();
            $search[$k][$key]['place'] = $places[0]->name;

            $products = DB::table('products')->select('name')->where('id',$value->service_name)->get();
            $search[$k][$key]['name'] = $products[0]->name;


            $num = DB::table('clientsvsservices')->select('id')->where([['service',$value->id],['status','1']])->get()->count();

            if($num >0){
                $search[$k][$key]['num'] = $num;
            }else{
                $search[$k][$key]['num'] = '0';
            }
        }
    }else{
        $search =  array();
    }
    }
$fsearch = array();
foreach ($search as $key => $value) {
foreach ($value as $k => $v) {
$fsearch[] = $v;
}
}
    $Services = array_map(function($array){
        return (object)$array;
    }, $fsearch);


    return view('admin.'.$this->module.'.cats_search',compact('Services'));
    }

    public function catadd()
    {
        RolesController::checkroles('13');
        $time = DB::table('services_time')->get();
        // $periods = DB::table('services_period')->orderBy('from', 'desc')->get();
        $places = DB::table('services_place')->get();

        $days = DB::table('services_days')->orderby('id','desc')->get();
        $currency = DB::table('services_currency')->get();
        $products = DB::table('products')->get();
        $leads = DB::table('lead')->get();
        $nationalitys = DB::table('pages')->where('type','nationality')->get();
        return view('admin.'.$this->module.'.catadd',compact('currency','days','clients','time','gender','periods','places','products','leads','nationalitys'));

    }



    public function catstore(Request $request)
    {
        RolesController::checkroles('13');
        unset($_POST['_token']);
        $_POST['created_by'] = Session::get('uid');
        DB::table('services')->insert($_POST);
        return redirect(url('admin/Services/index'))->with('message', 'تم بنجاح');
    }

    public function catedit($id)
    {
        RolesController::checkroles('14');
        $service = DB::table('services')->where('id',$id)->get();

        $time = DB::table('services_time')->get();
        // $periods = DB::table('services_period')->orderBy('from', 'desc')->get();
        $places = DB::table('services_place')->get();
        $days = DB::table('services_days')->orderby('id','desc')->get();
        $currency = DB::table('services_currency')->get();
        $products = DB::table('products')->get();
        // // $time = DB::table('services_time')->where('id',$service[0]->service_time)->get();
        // $periods = DB::table('services_period')->where('id',$service[0]->service_period)->get();
        // $places = DB::table('services_place')->where('id',$service[0]->service_place)->get();
        // $days = DB::table('services_days')->where('id',$service[0]->service_days)->get();
        // $currency = DB::table('services_currency')->where('id',$service[0]->service_currency)->get();
        // $products = DB::table('products')->where('id',$service[0]->service_name)->get();

        return view('admin.'.$this->module.'.catedit',compact('service','currency','days','clients','time','Service','periods','places','products','leads','nationalitys'));
    }

    public function catupdate(Request $request)
    {

        RolesController::checkroles('14');
        unset($_POST['_token']);
        DB::table('services')->where('id', $request->id)->update($_POST);
        return redirect(url('admin/Services/index'))->with('message', 'تم بنجاح');
    }

    public function catdel($catid)
    {

        RolesController::checkroles('15');
        $cat = DB::table('services')->where('id',$catid)->delete();
		return redirect(url('/admin/'.$this->module.'/index'))->with('message', 'تم بنجاح');
    }



    public function name()
    {
        RolesController::checkroles('17');
        $Services = DB::table('products')->paginate(10);
        return view('admin.'.$this->module.'.name.name',compact('Services','user'));
    }

    public function nameadd()
    {
        RolesController::checkroles('18');
        $products = DB::table('products')->get();
        return view('admin.'.$this->module.'.name.nameadd',compact('cats','time','gender','periods','products','leads','nationalitys'));
    }



    public function namestore(Request $request)
    {
        RolesController::checkroles('18');
        unset($_POST['_token']);
        DB::table('products')->insert($_POST);
        return redirect(url('admin/Services_name/index'))->with('message', 'تم بنجاح');
    }

    public function nameedit ($id)
    {
       RolesController::checkroles('19');
       $products = DB::table('products')->where('id',$id)->get();
       return view('admin.'.$this->module.'.name.nameedit',compact('cats','time','gender','places','products','leads','nationalitys'));
    }

    public function nameupdate(Request $request)
    {
        RolesController::checkroles('19');
        unset($_POST['_token']);
        DB::table('products')->where('id', $request->id)->update($_POST);
        return redirect(url('admin/Services_name/index'))->with('message', 'تم بنجاح');
    }

    public function namedel($catid)
    {
        RolesController::checkroles('20');
        $cat = DB::table('products')->where('id',$catid)->delete();
		return redirect(url('/admin/Services_name/index'))->with('message', 'تم بنجاح');
    }

    public function time()
    {
        RolesController::checkroles('22');
        $Services = DB::table('services_time')->paginate(10);
        return view('admin.'.$this->module.'.time.time',compact('Services','user'));
    }


    public function timeadd()
    {
        RolesController::checkroles('23');
        $time = DB::table('services_time')->get();
        return view('admin.'.$this->module.'.time.timeadd',compact('cats','time','gender','periods','products','leads','nationalitys'));
    }

    public function timestore(Request $request)
    {
        RolesController::checkroles('23');
        unset($_POST['_token']);
        DB::table('services_time')->insert($_POST);
        return redirect(url('admin/Services_time/index'))->with('message', 'تم بنجاح');
    }

    public function timeedit ($id)
    {
        RolesController::checkroles('24');
        $places = DB::table('services_time')->where('id',$id)->get();
       return view('admin.'.$this->module.'.time.timeedit',compact('cats','time','gender','places','products','leads','nationalitys'));
    }

    public function timeupdate(Request $request)
    {
        RolesController::checkroles('24');
        unset($_POST['_token']);
        DB::table('services_time')->where('id', $request->id)->update($_POST);
        return redirect(url('admin/Services_time/index'))->with('message', 'تم بنجاح');
    }

    public function timedel($catid)
    {
        RolesController::checkroles('25');
        $cat = DB::table('services_time')->where('id',$catid)->delete();
		return redirect(url('/admin/Services_time/index'))->with('message', 'تم بنجاح');
    }

    public function currency()
    {
        RolesController::checkroles('42');
        $Services = DB::table('services_currency')->paginate(10);
        return view('admin.'.$this->module.'.currency.currency',compact('Services','user'));
    }

    public function currencyadd()
    {
        RolesController::checkroles('43');
        $currency = DB::table('services_currency')->get();
        return view('admin.'.$this->module.'.currency.currencyadd',compact('cats','currency','gender','periods','products','leads','nationalitys'));
    }

    public function currencystore(Request $request)
    {
        RolesController::checkroles('43');
        unset($_POST['_token']);
        DB::table('services_currency')->insert($_POST);
        return redirect(url('admin/Services_currency/index'))->with('message', 'تم بنجاح');
    }
    public function currencyedit ($id)
    {
        RolesController::checkroles('44');
        $places = DB::table('services_currency')->where('id',$id)->get();
       return view('admin.'.$this->module.'.currency.currencyedit',compact('cats','time','gender','places','products','leads','nationalitys'));
    }

    public function currencyupdate(Request $request)
    {
        RolesController::checkroles('44');
        unset($_POST['_token']);
        DB::table('services_currency')->where('id', $request->id)->update($_POST);
        return redirect(url('admin/Services_currency/index'))->with('message', 'تم بنجاح');
    }
    public function currencydel($catid)
    {
        RolesController::checkroles('45');
        $cat = DB::table('services_currency')->where('id',$catid)->delete();
		return redirect(url('/admin/Services_currency/index'))->with('message', 'تم بنجاح');
    }

    // public function period()
    // {

    //     RolesController::checkroles('27');
    //     $Services = DB::table('services_period')->orderBy('from', 'desc')->paginate(25);
    //     return view('admin.'.$this->module.'.period.period',compact('Services','user'));
    // }

    // public function periodadd()
    // {
    //     RolesController::checkroles('28');
    //     $periods = DB::table('services_period')->get();
    //     return view('admin.'.$this->module.'.period.periodadd',compact('cats','time','gender','periods','products','leads','nationalitys'));
    // }

    // public function periodstore(Request $request)
    // {
    //     RolesController::checkroles('28');
    //     unset($_POST['_token']);
    //     unset($_POST['to']);
    //     DB::table('services_period')->insert($_POST);
    //     return redirect(url('admin/Services_period/index'))->with('message', 'تم بنجاح');
    // }

    // public function periodedit ($id)
    // {
    //     RolesController::checkroles('29');
    //     $places = DB::table('services_period')->where('id',$id)->get();
    //    return view('admin.'.$this->module.'.period.periodedit',compact('cats','time','gender','places','products','leads','nationalitys'));
    // }

    // public function periodupdate(Request $request)
    // {
    //     RolesController::checkroles('29');
    //     unset($_POST['_token']);
    //     DB::table('services_period')->where('id', $request->id)->update($_POST);
    //     return redirect(url('admin/Services_period/index'))->with('message', 'تم بنجاح');
    // }

    // public function perioddel($catid)
    // {
    //     RolesController::checkroles('30');
    //     $cat = DB::table('services_period')->where('id',$catid)->delete();
	//     return redirect(url('/admin/Services_period/index'))->with('message', 'تم بنجاح');
    // }

    public function days()
    {
        RolesController::checkroles('37');
        $Services = DB::table('services_days')->paginate(10);
        return view('admin.'.$this->module.'.days.days',compact('Services','user'));
    }

    public function daysadd()
    {
        RolesController::checkroles('38');
        $periods = DB::table('services_days')->get();
        return view('admin.'.$this->module.'.days.daysadd',compact('cats','time','gender','periods','products','leads','nationalitys'));
    }



    public function daysstore(Request $request)
    {
        RolesController::checkroles('38');
        unset($_POST['_token']);
        DB::table('services_days')->insert($_POST);
        return redirect(url('admin/Services_days/index'))->with('message', 'تم بنجاح');
    }

    public function daysedit ($id)
    {
        RolesController::checkroles('39');
        $places = DB::table('services_days')->where('id',$id)->get();
       return view('admin.'.$this->module.'.days.daysedit',compact('cats','time','gender','places','products','leads','nationalitys'));
    }

    public function daysupdate(Request $request)
    {
        RolesController::checkroles('39');
        unset($_POST['_token']);
        DB::table('services_days')->where('id', $request->id)->update($_POST);
        return redirect(url('admin/Services_days/index'))->with('message', 'تم بنجاح');
    }

    public function daysdel($catid)
    {
        RolesController::checkroles('40');
        $cat = DB::table('services_days')->where('id',$catid)->delete();
		return redirect(url('/admin/Services_days/index'))->with('message', 'تم بنجاح');
    }

    public function place()
    {
        RolesController::checkroles('32');
        $Services = DB::table('services_place')->paginate(10);
        return view('admin.'.$this->module.'.place.place',compact('Services','user'));
    }


    public function placeadd()
    {

        RolesController::checkroles('33');
        $places = DB::table('services_place')->get();
        return view('admin.'.$this->module.'.place.placeadd',compact('cats','time','gender','places','products','leads','nationalitys'));
    }



    public function placestore(Request $request)
    {
       RolesController::checkroles('33');
       unset($_POST['_token']);
       DB::table('services_place')->insert($_POST);
       return redirect(url('admin/Services_place/index'))->with('message', 'تم بنجاح');
    }

    public function placeedit ($id)

    {
       RolesController::checkroles('34');
       $places = DB::table('services_place')->where('id',$id)->get();
       return view('admin.'.$this->module.'.place.placeedit',compact('cats','time','gender','places','products','leads','nationalitys'));
    }

    public function placeupdate(Request $request)

    {
        RolesController::checkroles('34');
        unset($_POST['_token']);
        DB::table('services_place')->where('id', $request->id)->update($_POST);
        return redirect(url('admin/Services_place/index'))->with('message', 'تم بنجاح');
    }

    public function placedel($catid)

    {
        RolesController::checkroles('35');
        $cat = DB::table('services_place')->where('id',$catid)->delete();
		return redirect(url('/admin/Services_place/index'))->with('message', 'تم بنجاح');

    }

    public function serviceswithclients($sc)

    {
        RolesController::checkroles('12');


        $countallServices = DB::table('services')->get();
        $Services = DB::table('services')->where('id',$sc)->orderBy('servicesdate', 'desc')->Paginate(20);
        foreach($Services as $k=>$v)
        {
        $time = DB::table('services_time')->select('name')->where('id',$v->service_time)->get();
        $Services[$k]->time = $time[0]->name;

        $time = DB::table('services_currency')->select('currency')->where('id',$v->service_currency)->get();
        $Services[$k]->currency = $time[0]->currency;

        // $periods = DB::table('services_period')->where('id',$v->service_period)->get();
        // $Services[$k]->from = $periods[0]->from;
        // $Services[$k]->to = $periods[0]->to;

        $days = DB::table('services_days')->where('id',$v->service_days)->get();
        $Services[$k]->day1 = $days[0]->day1;
        $Services[$k]->day2 = $days[0]->day2;
        $Services[$k]->day3 = $days[0]->day3;
        $Services[$k]->day4 = $days[0]->day4;
        $Services[$k]->day5 = $days[0]->day5;
        $Services[$k]->day6 = $days[0]->day6;
        $Services[$k]->day7 = $days[0]->day7;

        $places = DB::table('services_place')->select('name')->where('id',$v->service_place)->get();
        $Services[$k]->place = $places[0]->name;

        $products = DB::table('products')->select('name')->where('id',$v->service_name)->get();
        $Services[$k]->name = $products[0]->name;

        $num = DB::table('clientsvsservices')->where([['service',$sc],['status','1']])->get();
        $Services[$k]->num = count($num);
        }

        $clientvsservices = DB::table('clientsvsservices')->where([['service',$sc],['status','1']])->orderBy('id','desc')->get();
        foreach($clientvsservices as $k=>$v)
        {

            $clients = DB::table('clients')->where([['id',$v->client],['status','1']])->get();
            $clientvsservices[$k]->name = $clients[0]->name;
            $clientvsservices[$k]->name_en = $clients[0]->name_en;
            $clientvsservices[$k]->number = $clients[0]->number;
            $clientvsservices[$k]->email = $clients[0]->email;
            $user = DB::table('users')->select('name')->where('id',$clients[0]->assigned_to)->get();
            $clientvsservices[$k]->assigned_to = $user[0]->name;

        }
        return view('admin.'.$this->module.'.serviceswithclients',compact('Services','user','countallServices','clientvsservices'));

    }

}
