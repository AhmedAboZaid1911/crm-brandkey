<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Excel;
use App\User;
use App\Contest;
use App\Advertising;
use App\Adv_view;
use App\Video;
use App\Section;
use App\News_list;

class Reports extends Controller
{

	public function index(Request $request){

		if($request->type == 'users'){
			return view(ad.'.reports.users',["report_title"=>"تقارير المشتركين"]);

		}elseif($request->type == 'activites'){

			return view(ad.'.reports.activity',["report_title"=>"تقارير التفاعل"]);

		}elseif($request->type == 'contests'){
			$contests = Contest::all();
			return view(ad.'.reports.contests',["report_title"=>"تقارير المسابقات","contests"=>$contests]);

		}elseif($request->type == 'advertising'){
			$advertisings = Advertising::all();
			return view(ad.'.reports.advertising',["report_title"=>"تقارير الاعلانات","advertisings"=>$advertisings]);
		}else{

			return view(ad.'.reports.users',["report_title"=>"تقارير المشتركين"]);
		}
		
	}

	public function view_reports(Request $request){

		$type = $request->type;


		if(!isset($request->type)){
			$request->session()->flash('alert-error', 'اختر نوع التقرير');
        	return back();
		}else{

			if($type == "users"){

				$country = $request->countrie_id;
				$city = $request->citie_id;
				$speciality = $request->speciality;
				$month = $request->month;

				$where = [
					["level","=","user"]
				];

				if($country != 0){
					array_push($where,["countrie_id","=",$country]);
				}
				if($city != 0){
					array_push($where,["citie_id","=",$city]);
				}
				if($speciality != 0){
					array_push($where,["speciality","=",$city]);
				}
				
				if(!empty($month)){
					$date = explode("/",$month);
					//dd($date);
					$members = User::where($where)->whereMonth('created_at','=',$date[0])->whereYear('created_at','=',$date[1])->get();
					
				}else{
					$members = User::where($where)->get();
				}

				Excel::create('users_report-'.date('d-m-Y-h:i:s-A'), function($excel) use ($members){

				    // Set the title
				    $excel->setTitle('Report About the users ');
				    $excel->sheet('Users List', function($sheet)  use ($members){
				    	 $sheet->setRightToLeft(true);
								$sheet->cells('A1:G1', function ($cells) {
								    $cells->setBackground('#cccccc');
								    $cells->setAlignment('center');
								});
			    		$cellss = array();
				    	foreach ($members as $key=>$member){
				    		array_push($cellss,
				    			array(
				    				'م'=>$key+1,
				    				'الاسلم'=>$member->name,
				    				'البريد الالكتروني'=>$member->email,
				    				'رقم الجوال'=>$member->mobile,
				    				'البلد'=>($member->countrie ? $member->countrie->name : ''),
				    				'المدينه'=>($member->citie ? $member->citie->name : ''),
				    				'التخصص'=>$member->speciality,
				    				'كود التسجيل'=>$member->member_code
				    			)
				    		);
				    	}
				        $sheet->fromArray($cellss);

				    });
				})->download('xlsx');

				
			}elseif($type == "contests"){
				$contest = $request->contest;
				if($contest == 'topscore'){

				$members = User::where("points","!=",0)->orderBy('points','desc')->get();

				Excel::create('top_scorers-'.date('d-m-Y-h:i:s-A'), function($excel) use ($members){

				    // Set the title
				    $excel->setTitle('Report About the top score users ');
				    $excel->sheet('top score Users List', function($sheet)  use ($members){
				    	 $sheet->setRightToLeft(true);
								$sheet->cells('A1:I1', function ($cells) {
								    $cells->setBackground('#cccccc');
								    $cells->setAlignment('center');
								});
			    		$cellss = array();
				    	foreach ($members as $key=>$member){
				    		array_push($cellss,
				    			array(
				    				'م'=>$key+1,
				    				'الاسلم'=>$member->name,
				    				'البريد الالكتروني'=>$member->email,
				    				'رقم الجوال'=>$member->mobile,
				    				'البلد'=>($member->countrie ? $member->countrie->name : ''),
				    				'المدينه'=>($member->citie ? $member->citie->name : ''),
				    				'التخصص'=>$member->speciality,
				    				'كود التسجيل'=>$member->member_code,
				    				'عدد النقاط'=>$member->points
				    			)
				    		);
				    	}
				        $sheet->fromArray($cellss);

				    });
				})->download('xlsx');

				}else{

				if($contest == 0){
					$contests_list = Contest::all();
				}else{
					$contests_list = Contest::where('id',$contest)->get();
					
				}
				

				Excel::create('contests_report-'.date('d-m-Y-h:i:s-A'), function($excel) use ($contests_list){

				    // Set the title
				    $excel->setTitle('Report About the contests ');

				     foreach ($contests_list as $key=>$contest){
				     	if(count($contest->expectation) != 0){
				    $excel->sheet(trans('teams.'.strtolower(str_replace(" ","_",$contest->localteam_name))).'-'.trans('teams.'.strtolower(str_replace(" ","_",$contest->visitorteam_name))), function($sheet)  use ($contest){
				    	 $sheet->setRightToLeft(true);
								$sheet->cells('A1:E1', function ($cells) {
								    $cells->setBackground('#cccccc');
								    $cells->setAlignment('center');
								});
			    		$cellss = array();

			    		

				    	foreach ($contest->expectation as $key=>$expectation){

				    		

				    		array_push($cellss,
				    			array(
				    				'م'=>$key+1,
				    				'بتاريخ'=>$expectation->created_at->diffforhumans(),
				    				'المشترك'=>$expectation->user->name,
				    				' توقع'=>trans('teams.'.strtolower(str_replace(" ","_",$contest->localteam_name))).' '. $contest->localteam_score .' - '. $contest->visitorteam_score .' '. trans('teams.'.strtolower(str_replace(" ","_",$contest->visitorteam_name))),
				    				'حصل على '=>$expectation->points
				    			)
				    		);
				    	}


			    		

				        $sheet->fromArray($cellss);

				    });
				    }
				}
				})->download('xlsx');

			}

			}elseif($type == "advertising"){

				$advertising = $request->advertising;

				if($advertising == 0){
					$advertising_list = Advertising::all();
				}else{
					$advertising_list = Advertising::where('id',$advertising)->get();
					
				}
				

				Excel::create('advertising_report-'.date('d-m-Y-h:i:s-A'), function($excel) use ($advertising_list){

				    // Set the title
				    $excel->setTitle('Report About the advertising ');

				    foreach ($advertising_list as $key=>$advs){

				    $excel->sheet($advs->title, function($sheet)  use ($advs){
				    	$sheet->setRightToLeft(true);
						$sheet->cells('A1:C1', function ($cells) {
						    $cells->setBackground('#cccccc');
						    $cells->setAlignment('center');
						});
			    		$cellss = array();

				    	foreach ($advs->adv_view as $key=>$adv_view){

				    		array_push($cellss,
				    			array(
				    				'م'=>$key+1,
				    				'المشترك'=>$adv_view->user->name,
				    				'مرات العرض'=>$adv_view->views
				    			)
				    		);
				    	}

				        $sheet->fromArray($cellss);

				    	
				    });

					}

				})->download('xlsx');


			
			}elseif($type == "activities"){

				$activities = $request->activities;

				if($activities == 'gallery'){
					$data_list = Section::has('likes')->get();
				}elseif($activities == 'news'){
					$data_list = News_list::has('likes')->get();
				}elseif($activities == 'videos'){
					$data_list = Video::has('likes')->get();
				}
				
				     $types = array(
			            'love'=>'بالحب',
			            'like'=>'بالاعجاب',
			            'angry'=>'بالغضب',
			            'laughing'=>'بالضحك',
			            'sad'=>'بالحزن',
			            'wow'=>'بالدهشه'
			        );

				Excel::create($activities.'_report-'.date('d-m-Y-h:i:s-A'), function($excel) use ($data_list,$types,$activities){

				    // Set the title
				    $excel->setTitle('Report About the advertising ');

				    foreach ($data_list as $key=>$data){

				    /*$sheetname = preg_replace('/[^a-zA-Z0-9\']/', '_', $data->name);
				    str_limit(strip_tags($sheetname),25)*/
				    if($activities == 'news'){
				    	$sheetname = 'news_'.$data->id;
				    }else{
				    	$sheetname = $data->name;
				    }
				    $excel->sheet(str_limit(strip_tags($sheetname),25), function($sheet)  use ($data,$types){
				    	$sheet->setRightToLeft(true);
						$sheet->cells('A1:D1', function ($cells) {
						    $cells->setBackground('#cccccc');
						    $cells->setAlignment('center');
						});
			    		$cellss = array();

				    	foreach ($data->likes as $key=>$likes){

				    		$user = User::find($likes->user_id);
				    		array_push($cellss,
				    			array(
				    				'م'=>$key+1,
				    				'بتاريخ'=>$likes->created_at->diffforhumans(),
				    				'المشترك'=>(isset($user->name) ? $user->name : 'غير معروف' .$likes->user_id),
				    				' تفاعل'=>$types[$likes->like_code]
				    			)
				    		);
				    	}

				        $sheet->fromArray($cellss);

				    	
				    });

					}

				})->download('xlsx');



			}

			}

		
	}
}
