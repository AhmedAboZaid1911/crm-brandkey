<?php



namespace App\Http\Controllers\admin;



use Validator;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;



class SettingsController extends Controller

{



    public function __construct()

       {

           // parent::__construct();

            $this->type = 'general';

            $this->module = 'setings';

            $this->lang_arr = array("en" => "اللغة الانجليزية" );



            view()->share('module', $this->module );

            view()->share('active', 'settings');

            view()->share('titlepage','الاعدادات');

       }




    public function editsettings()

    {
        RolesController::checkroles('47');
        

        $settingsarr = DB::table('settings')->get();
        $settings = array();
        for ($i=0; $i < count($settingsarr); $i++) {
            $settings[$settingsarr[$i]->key] = $settingsarr[$i]->value;
        }

        return view('admin.settings.edit',compact('settings'));



    }



    public function updatesettings(Request $request)

    {
        RolesController::checkroles('47');
    	if($request->logo != null){

    		//echo "test";

    		//exit();

            $imgname = $request->logo;



            DB::table('settings')

            ->where('key', 'logo')

            ->update(['value' => $imgname]);



        }






        $data = array('title_en' => $request->title_en);
            $validator = Validator::make($data, [
            'title_en' => 'required',
            ]);

            if ($validator->fails()) {



                return redirect(url('/admin/editsettings'))

                        ->withErrors($validator)

                        ->withInput();

            }



    	$settingsarr = DB::table('settings')->get();

            $settings = array();

            for ($i=0; $i < count($settingsarr); $i++) {

            	$key = $settingsarr[$i]->key;

        		DB::table('settings')

            		->where([['key' , '=' , $key] , ['key' , '!=' , 'logo']])

            		->update(['value' => $request->$key]);

        	}

        return redirect(url('/admin/editsettings'))->with('message', 'تم بنجاح');

    }



}
