<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;


class UserController extends Controller
{
    public function __construct()
       {
           // parent::__construct();
            $this->module = 'user';

            view()->share('module', $this->module );
       }

    public function editprofile()
     {
         return view('editprofile');
     }


    public function export()
    {
        return Excel::download(new clientsExport, 'users.xlsx');
    }
}


