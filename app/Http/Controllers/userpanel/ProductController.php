<?php

namespace App\Http\Controllers\userpanel;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    public function __construct()
       {
           // parent::__construct();
            $this->type = 'product';
            $this->module = 'product';

            view()->share('module', $this->module );
            view()->share('active', 'product');
            view()->share('titlepage','السلع');
       }

    public function index()
    {
            $posts = DB::table('post')->where([['type',$this->type],['uid',Auth::user()->id]])->paginate(10);
            return view('userpanel.'.$this->module.'.index',compact('posts'));
    }

    public function add()
    {
            $cats = DB::table('cat')->select('id','title_ar')->where([['type',$this->type]])->get();
            return view('userpanel.'.$this->module.'.add',compact('cats'));
    }

    public function store(Request $request)
    {
        $data = array('title_ar' => $request->title_ar , 'text_ar' => $request->text_ar, 'url_ar' => $request->url_ar);
            $validator = Validator::make($data, [
            'title_ar' => 'required|unique:pages',
            'text_ar' => 'required'
            ]);


            if ($validator->fails()) {

                return redirect(url('/userpanel/'.$this->module.'/add'))
                        ->withErrors($validator)
                        ->withInput();
            }

        if($request->url_ar != ""){
            $request->url_ar = str_replace(' ', '-', trim($request->url_ar));
        }
        else{
            $request->url_ar = str_replace(' ', '-', trim($request->title_ar));
        }


        DB::table('post')->insert(
            ['uid'=>Auth::user()->id,'title_ar' => $request->title_ar,'cid' => $request->cid, 'pic' => $request->pic, 'text_ar' => $request->text_ar, 'keyword_ar' => $request->keyword_ar, 'desc_ar' => $request->desc_ar, 'url_ar' => $request->url_ar,'price' => $request->price,'type' => $this->type,'active' => $request->active, 'time' => time(), 'pic1' => $request->pic1, 'pic2' => $request->pic2, 'pic3' => $request->pic3, 'pic4' => $request->pic4, 'pic5' => $request->pic5]);

        return redirect(url('/userpanel/'.$this->module.'/index'))->with('message', 'تم بنجاح');
    }

    public function edit($id)
    {
            $post = DB::table('post')->where([['id',$id],['uid',Auth::user()->id]])->get();
            $cats = DB::table('cat')->select('id','title_ar')->where([['cid',0],['type',$this->type]])->get();
            return view('userpanel.'.$this->module.'.edit',compact('post','cats'));
    }

    public function update(Request $request)
    {
     
        $data = array('title_ar' => $request->title_ar , 'cid' => $request->cid, 'url_ar' => $request->url_ar);
            $validator = Validator::make($data, [
            'title_ar' => 'required',
            'cid' => 'required'
            ]);

            if ($validator->fails()) {
            
                return redirect(url('/userpanel/'.$this->module.'/edit/' . $request->id))
                        ->withErrors($validator)
                        ->withInput();
            }

        if($request->url_ar != ""){
            $request->url_ar = str_replace(' ', '-', trim($request->url_ar));
        }
        else{
            $request->url_ar = str_replace(' ', '-', trim($request->title_ar));
        }

        DB::table('post')
            ->where('id', $request->id)
            ->update(['title_ar' => $request->title_ar,'cid' => $request->cid, 'pic' => $request->pic, 'text_ar' => $request->text_ar, 'keyword_ar' => $request->keyword_ar, 'desc_ar' => $request->desc_ar, 'url_ar' => $request->url_ar,'price' => $request->price, 'type' => $this->type,'active' => $request->active, 'pic1' => $request->pic1, 'pic2' => $request->pic2, 'pic3' => $request->pic3, 'pic4' => $request->pic4, 'pic5' => $request->pic5]);
		
        return redirect(url('/userpanel/'.$this->module.'/index'));
    }

    public function del($id)
    {
            $cat = DB::table('posts')->where([['id',$id],['uid',Auth::user()->id]])->delete();
			return redirect(url('/userpanel/'.$this->module.'/index'));
    }
}
