<?php

namespace App\Http\Controllers\userpanel;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use \Illuminate\Support\Facades\Session;

class ProfileController extends Controller
{
 public function __construct()
       {
           // parent::__construct();
         

            view()->share('active', 'profile');
            view()->share('titlepage','تعديل البروفايل ');
       }

    public function index()
    {
            $uemail = Session::get('useremail');
            $upass = Session::get('userpass');
            
            return view('userpanel.editprofile',compact('uemail','upass'));
       
    }

    public function update(Request $request)
    {
         $rules = [
                'email'=>'required|email',
                'password'=>'required|min:5'
        ];
        $validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
            return redirect()->back()->withInput()->withErrors($validate);
        }

        
        DB::table('users')
            ->where('email', Session::get('useremail'))
            ->update(['email' => $request->email,'password' => bcrypt($request->password)]);
		
        return redirect(url('/userpanel/index'));
    }

}
