<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      
    }

    public function show($id)
    {
        
          $page = DB::table('pages')->where('url_ar',$id)->get();
            return view('page',compact('page'));
    }

  
}
