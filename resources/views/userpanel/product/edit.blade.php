@extends('layouts.userpanellayout')
@section('pagejs')
    <script src="{{url('/assetsAdmin/pages/scripts/form-samples.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/assetsAdmin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
<script>
CKEDITOR.replace( 'text_ar' );
</script>
@endsection
@section('pagecss')
<link href="{{url('/assetsAdmin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="portlet box green">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-gift"></i>تعديل {{$titlepage}} </div>

                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form method="post" action="{{url('userpanel/')}}/{{$module}}/update" class="form-horizontal" enctype="multipart/form-data">

                                                        <div class="form-body">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="id" value="{{$post[0]->id}}">
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">العنوان</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control input-circle" placeholder="العنوان" name="title_ar" value="{{$post[0]->title_ar}}">
                                                                    <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('title_ar') }}</div>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">القسم</label>
                                                                <div class="col-md-4">
                                                                    <select name="cid" class="form-control input-medium">

                                                                    @foreach($cats as $cats)
                                                                        <option value="{{$cats->id}}" <?=($cats->id == $post[0]->cid) ? 'selected' : '' ?>>{{$cats->title_ar}}</option>
                                                                    @endforeach
                                                                    </select>
                                                                    <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('cid') }}</div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group last">
                                                            <label class="control-label col-md-3">الصورة</label>
                                                            <div class="col-md-9">

                                                                <div class="fileinput fileinput-new" data-provides="fileinput" style="margin-bottom: 15px">
                                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                        @if($post[0]->pic == "")
                                                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" id="prev_img" />
                                                                        @else
                                                                        <img src="{{url($post[0]->pic)}}" alt="" id="prev_img" />
                                                                        @endif
                                                                    </div>
                                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                                                    </div>
                                                                    <div>
                                                                        <span class="btn default btn-file">
                                                                        <span class="fileinput-new"> <a data-toggle="modal" data-target="#thepic" class="btn" type="button">Select image </a></span>
                                                                        <span class="fileinput-exists"> Change </span>
                                                                         </span>
                                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                        <a style="margin-right: 20px" onclick="setremove()" class="btn red">Delete image</a>
                                                                    </div>
                                                                    <br>
                                                                    <input id="pic" type="hidden" value="{{url($post[0]->pic)}}" name="pic">

                                                                </div>

                                                            </div>
                                                            </div>

       <div class="input-append">

      <!--<a data-toggle="modal" data-target="#basma" class="btn" type="button">Select</a>-->
    </div>

    <div class="modal fade" id="thepic">
<div class="modal-dialog" >
  <div class="modal-content" style="width: 900px;height: 500px">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4 class="modal-title">اختر صورة</h4>
    </div>
    <div class="modal-body" style="width: 900px;height: 450px">
      <iframe width="100%" height="100%" src="{{url('/filemanager/dialog.php?type=2&field_id=pic&fldr=')}}" frameborder="0"></iframe>
    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">المحتوى</label>
                                                                <div class="col-md-8">
                                                                    <textarea rows="15" placeholder="المحتوى" id="form-field-22" class="form-control" name="text_ar" required><?=$post[0]->text_ar?></textarea>

                                                                    <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('text_ar') }}</div>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">الكلمات الدلالية</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control input-circle" name="keyword_ar"" placeholder="الكلمات الدلالية" value="{{$post[0]->keyword_ar}}">

                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">وصف الصفحة</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control input-circle" name="desc_ar" placeholder="وصف الصفحة" value="{{$post[0]->desc_ar}}">

                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">URL</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control input-circle" name="url_ar" placeholder="URL" value="{{$post[0]->url_ar}}">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">السعر</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control input-circle" name="price" placeholder="السعر" value="{{$post[0]->price}}">
                                                                </div>
                                                            </div>


                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">الحالة</label>
                                                                <div class="col-md-4">
                                                                   <div class="mt-radio-list">
                                                        <label class="mt-radio mt-radio-outline"> يعمل
                                                            <input type="radio" value="1" name="active" <?=($post[0]->active == 1) ? 'checked' : '' ?> />
                                                            <span></span>
                                                        </label>
                                                        
                                                        <label class="mt-radio mt-radio-outline"> لايعمل
                                                            <input type="radio" value="0" name="active" <?=($post[0]->active == 0) ? 'checked' : '' ?>  />
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            
                                                            <!--  -->
                                                            <?php
                                                            $pics = array();
                                                            ?>
                                                          @for($i=1;$i<=5;$i++)
                                                          <? array_push($pics,"pic".$i);?>
                                                      <div class="form-group last">
                                                            <label class="control-label col-md-3">الصورة {{$i}}</label>
                                                            <div class="col-md-9">

                                                                <div class="fileinput fileinput-new" data-provides="fileinput" style="margin-bottom: 15px">
                                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                        @if($post[0]->pic[$i] == "")
                                                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" id="prev_img{{$i}}" />
                                                                        @else
                                                                        <img src="{{url($post[0]->$pics[$i-1])}}" alt="" id="prev_img{{$i}}" />
                                                                        @endif
                                                                    </div>
                                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                                                    </div>
                                                                    <div>
                                                                        <span class="btn default btn-file">
                                                                        <span class="fileinput-new"> <a data-toggle="modal" data-target="#thepic{{$i}}" class="btn" type="button">Select image </a></span>
                                                                        <span class="fileinput-exists"> Change </span>
                                                                         </span>
                                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                        <a style="margin-right: 20px" onclick="setremove()" class="btn red">Delete image</a>
                                                                    </div>
                                                                    <br>
                                                                    <input id="pic{{$i}}" type="text" value="{{url($post[0]->$pics[$i-1])}}" name="pic{{$i}}">

                                                                </div>

                                                            </div>
                                                            </div>

       <div class="input-append">

      <!--<a data-toggle="modal" data-target="#basma" class="btn" type="button">Select</a>-->
    </div>

    <div class="modal fade" id="thepic{{$i}}">
<div class="modal-dialog" >
  <div class="modal-content" style="width: 900px;height: 500px">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4 class="modal-title">اختر صورة</h4>
    </div>
    <div class="modal-body" style="width: 900px;height: 450px">
      <iframe width="100%" height="100%" src="{{url('/filemanager/dialog.php?type=2&field_id=pic')}}{{$i}}&fldr=" frameborder="0"></iframe>
    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div>
                                                                
                                                          
                                                          @endfor
                                                        </div>
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-3 col-md-9">
                                                                    <button type="submit" class="btn btn-circle green">تعديل</button>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>
                                            <script>
                                                function responsive_filemanager_callback(field_id) {
                                                    var url = {!! json_encode(url('')) !!};
                                                    var image = $('#' + field_id).val();
                                                    if(field_id == 'pic'){
                                                        $('#prev_img').attr('src',url+image);
                                                    }
                                                    for (var i=0; i <=5; i++) {

                                                    if(field_id == 'pic'+[i]){
                                                        $('#prev_img'+[i]).attr('src',url+image);
                                                    }

                                                    };
                                                }
                                                function setremove() {
                                                    document.getElementById("pic").value = "remove";
                                                    document.getElementById("prev_img").setAttribute("src", "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image");


                                                    for (var i=0; i <=5; i++) {
                                                    document.getElementById("pic"+[i]).value = "remove";
                                                    document.getElementById("prev_img"+[i]).setAttribute("src", "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image");

                                                    }

                                                }
                                            </script>
@endsection

