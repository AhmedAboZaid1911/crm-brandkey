@extends('layouts.userpanellayout')
@section('pagejs')
    <script src="{{url('/assetsAdmin/pages/scripts/form-samples.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/assetsAdmin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
    <script>
CKEDITOR.replace( 'text_ar' );
</script>
@endsection
@section('pagecss')
<link href="{{url('/assetsAdmin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="portlet box green">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-gift"></i>اضافة {{$titlepage}} </div>
                                                    
                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form method="post" action="{{url('userpanel/')}}/{{$module}}/store" class="form-horizontal" enctype="multipart/form-data">

                                                        <div class="form-body">
                                                        {{ csrf_field() }}
                                                         
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">العنوان</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control input-circle" placeholder="العنوان" name="title_ar">
                                                                    <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('title_ar') }}</div>
                                                                </div>
                                                            </div>
                                                            
                                                             <div class="form-group">
                                                                <label class="col-md-3 control-label">القسم </label>
                                                                <div class="col-md-4">
                                                                    <select name="cid" class="form-control input-medium">
                                                                        <option value="">اختر</option>
                                                                    @foreach($cats as $cat)
                                                                        <option value="{{$cat->id}}">{{$cat->title_ar}}</option>
                                                                    @endforeach
                                                                    </select>
                                                                    <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('cid') }}</div>
                                                                </div>
                                                            </div>
                                                                
<div class="form-group last">
<label class="control-label col-md-3">الصورة</label>
<div class="col-md-9">
    
    <div class="fileinput fileinput-new" data-provides="fileinput" style="margin-bottom: 15px">
        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" id="prev_img" /> 
        </div>
        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> 
        </div>
        <div>
            <span class="btn default btn-file">
            <span class="fileinput-new"> <a data-toggle="modal" data-target="#thepic" class="btn" type="button">Select image </a></span>
            <span class="fileinput-exists"> Change </span>
             </span>
            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
        </div>
        <br>
        <input id="pic" type="hidden" value="" name="pic">
    </div>

</div>
</div>
                                                            
      

<div class="modal fade" id="thepic">
<div class="modal-dialog" >
  <div class="modal-content" style="width: 900px;height: 500px">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4 class="modal-title">اختر صورة</h4>
    </div>
    <div class="modal-body" style="width: 900px;height: 450px">
      <iframe width="100%" height="100%" src="{{url('/filemanager/dialog.php?type=2&field_id=pic&fldr=')}}" frameborder="0"></iframe>
    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div>
                                                            
                                                            
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">المحتوى</label>
                                                                <div class="col-md-8">
                                                                    <textarea rows="15" placeholder="المحتوى" id="form-field-22" class="form-control" name="text_ar" required></textarea>

                                                                    <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('text_ar') }}</div>
                                                                </div>
                                                            </div>
                                                            
                                                            
                                                            
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">الكلمات الدلالية</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control input-circle" name="keyword_ar" placeholder="الكلمات الدلالية">

                                                                    <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('keyword_ar') }}</div>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">الوصف</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control input-circle" name="desc_ar" placeholder="الوصف">

                                                                    <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('desc_ar') }}</div>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">URL</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control input-circle" name="url_ar" placeholder="URL">

                                                                </div>
                                                            </div>


                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">السعر</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control input-circle" name="price" placeholder="السعر">

                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">الحالة</label>
                                                                <div class="col-md-4">
                                                                   <div class="mt-radio-list">
                                                        <label class="mt-radio mt-radio-outline"> يعمل
                                                            <input type="radio" value="1" name="active" checked="" />
                                                            <span></span>
                                                        </label>
                                                        
                                                        <label class="mt-radio mt-radio-outline"> لايعمل
                                                            <input type="radio" value="0" name="active" />
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <!--  -->
                                                          @for($i=1;$i<=5;$i++)
                                                              
<div class="form-group last">
<label class="control-label col-md-3">الصورة {{$i}}</label>
<div class="col-md-9">
    
    <div class="fileinput fileinput-new" data-provides="fileinput" style="margin-bottom: 15px">
        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" id="prev_img{{$i}}" /> 
        </div>
        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> 
        </div>
        <div>
            <span class="btn default btn-file">
            <span class="fileinput-new"> <a data-toggle="modal" data-target="#thepic{{$i}}" class="btn" type="button">Select image </a></span>
            <span class="fileinput-exists"> Change </span>
             </span>
            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
        </div>
        <br>
        <input id="pic{{$i}}" type="hidden" value="" name="pic{{$i}}">
    </div>

</div>
</div>
                                                            
      

<div class="modal fade" id="thepic{{$i}}">
<div class="modal-dialog" >
  <div class="modal-content" style="width: 900px;height: 500px">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4 class="modal-title">اختر صورة</h4>
    </div>
    <div class="modal-body" style="width: 900px;height: 450px">
      <iframe width="100%" height="100%" src="{{url('/filemanager/dialog.php?type=2&field_id=pic')}}{{$i}}&fldr=" frameborder="0"></iframe>
    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div>
                                                          @endfor
                                                            <!--  -->
                                                        </div>
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-3 col-md-9">
                                                                    <button type="submit" class="btn btn-circle green">أضف</button>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>
                                            <script>
                                                function responsive_filemanager_callback(field_id) {
                                                    var url = {!! json_encode(url('')) !!};
                                                    var image = $('#' + field_id).val();
                                                    if(field_id == 'pic'){
                                                        $('#prev_img').attr('src',url+image);
                                                    }
                                                    for (var i=0; i <=5; i++) {

                                                    if(field_id == 'pic'+[i]){
                                                        $('#prev_img'+[i]).attr('src',url+image);
                                                    }

                                                    }
                                                }
                                            </script>
@endsection

