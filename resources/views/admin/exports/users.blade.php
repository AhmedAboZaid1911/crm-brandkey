<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
    <thead>
        <tr>
            <th>م</th>
            <th> اسم العميل </th>
            <th> اسم العميل بالانجليزية</th>
            <th> رقم العميل </th>
            <th> الايمال </th>
            <th> النوع </th>
            <th> المصدر </th>
            <th> الجنسية </th>
            <th> الاهتمام </th>
            <th> انشاء بواسطة </th>
            <th> تابع ل </th>
            <th>تاريخ الاضافة </th>

        </tr>
    </thead>
    <tbody>
    @foreach($clients as $k=>$client)
        <tr>
            <td>{{$k+1}}</td>

            <td> {{  $client->name}}  </td>
            <td> {{  $client->name_en}}  </td>
            <td> {{  $client->number }} </td>
            <td> {{  $client->email }} </td>
            <td> {{  $client->gender }} </td>
            <td> {{  $client->lead }} </td>
            <td> {{  $client->nationality }} </td>
            <td> {{  $client->Interest  }} </td>
            <td> {{  $client->created_by  }} </td>
            <td> {{  $client->assigned_to  }} </td>
            <td> {{  $client->created_at  }} </td>

        </tr>
    @endforeach
    </tbody>
</table>
<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
    <thead>
        <tr>
            <th>م</th>
            <th> اسم العميل </th>
            <th> رقم العميل </th>
            <th> الايمال </th>
            <th> انشاء بواسطة </th>
            <th> تعديل الخدمات </th>
        </tr>
    </thead>
    <tbody>
    @foreach($clientvsservices as $k=>$clientvsservice)
        <tr>
            <td>{{$k+1}}</td>

            <td> {{  $clientvsservice->name}}  </td>
            <td> {{  $clientvsservice->number }} </td>
            <td> {{  $clientvsservice->email }} </td>
            <td> {{  $clientvsservice->created_by }} </td>

        </tr>
    @endforeach
    </tbody>
</table>
<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
    <thead>
        <tr>
            <th>م</th>
            <th>
                اسم العميل / الخدمة
            </th>
            <th>الرقم</th>
            <th>المهمة </th>
            <th>التاريخ </th>
            <th> انشاء بواسطة </th>
            <th>تابع ل </th>

        </tr>
    </thead>
    <tbody>
    @foreach($tasks as $k=>$task)
        @if($task->status == '3')
        <tr class="bg-green-jungle ">
        @elseif($task->assigned_at < '05-11-2018' )
        <tr class="bg-red-pink">
        @else
        <tr class="warning">
        @endif
            <td>{{$k+1}}</td>
            <td>
            @if($task->type =='clientvsservices')
                 {{$task->service}} / {{$task->client}}
            @endif
            @if($task->type =='client')
            {{$task->client}}
            @endif
            </td>
            <td> {{  $task->clientnum  }}  </td>
            <td> {{  $task->title  }}  </td>
            <td> {{  $task->assigned_at  }}  </td>
            <td> {{  $task->created_by }}  </td>
            <td> {{  $task->assigned_to  }}  </td>

        </tr>
    @endforeach
    </tbody>
</table>
<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
    <thead>
        <tr>
            <th>م</th>
            <th>
                اسم العميل / الخدمة
            </th>
            <th>الرقم</th>
            <th>المهمة </th>
            <th>التاريخ </th>
            <th> انشاء بواسطة </th>
            <th>تابع ل </th>

        </tr>
    </thead>
    <tbody>
    @foreach($tasks as $k=>$task)
        @if($task->status == '3')
        <tr class="bg-green-jungle ">
        @elseif($task->assigned_at < '05-11-2018' )
        <tr class="bg-red-pink">
        @else
        <tr class="warning">
        @endif
            <td>{{$k+1}}</td>
            <td>
            @if($task->type =='clientvsservices')
                 {{$task->service}} / {{$task->client}}
            @endif
            @if($task->type =='client')
            {{$task->client}}
            @endif
            </td>
            <td> {{  $task->clientnum  }}  </td>
            <td> {{  $task->title  }}  </td>
            <td> {{  $task->assigned_at  }}  </td>
            <td> {{  $task->created_by }}  </td>
            <td> {{  $task->assigned_to  }}  </td>

        </tr>
    @endforeach
    </tbody>
</table>
