<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
    <thead>
        <tr>
            <th>م</th>
            <th>
            اسم العميل بالانجليزية
            </th>
            <th>
                اسم العميل / الخدمة
            </th>
            <th>الرقم</th>
            <th>المهمة </th>
            <th>التاريخ </th>

            <th>تابع ل </th>
        </tr>
    </thead>
    <tbody>
    @foreach($tasks as $k=>$task)
        @if($task->status == '3')
        <tr class="bg-green-jungle ">
        @elseif($task->assigned_at < date('d-m-Y') )
        <tr class="bg-red-pink">
        @else
        <tr class="warning">
        @endif
            <td>{{$k+1}}</td>
            <td>
            @if($task->type =='clientvsservices')
                 {{$task->service}} / {{$task->client}}
            @endif
            @if($task->type =='client')
            {{$task->client}}
            @endif
            @if($task->type =='general')
                عام
            @endif
            </td>
            <td>
            @if($task->type =='clientvsservices')
                {{$task->client_en}}
            @endif
            @if($task->type =='client')
            {{$task->client_en}}
            @endif
            @if($task->type =='general')
                عام
            @endif
            </td>
            @if($task->type =='general')
            <td> عام  </td>
            @else
            <td> {{  $task->clientnum  }}  </td>
            @endif
            <td> {{  $task->title  }}  </td>
            <td> {{  $task->assigned_at  }}  </td>
            <td> {{  $task->assigned_to  }}  </td>

        </tr>
    @endforeach
    </tbody>
</table>
