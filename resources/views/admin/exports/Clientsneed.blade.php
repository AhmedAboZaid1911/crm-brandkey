<table>
    <thead>
    <tr>
        <th>م</th>
        <th> اسم العميل </th>
        <th> اسم العميل بالانجليزية</th>
        <th>  رقم الموبيل  </th>
        <th>  رقم التليفون	</th>
        <th> الايميل </th>
        <th> تابع ل </th>
        <th>تاريخ الاضافة </th>
        <th>الاحتياج </th>
    </tr>
    </thead>
    <tbody>
    @foreach($clientneeds as $k=>$clientneed)
        <tr>
            <td>{{$k+1}}</td>
            <td> {{  $clientneed->name}}  </td>
            <td> {{  $clientneed->name_en}}  </td>
            <td> {{  $clientneed->number }} </td>
            <td> {{  $clientneed->number2 }} </td>
            <td> {{  $clientneed->email }} </td>
            <td> {{  $clientneed->assigned_to  }} </td>
            <td> {{  $clientneed->created_at  }} </td>
            <td> {{  $clientneed->needs  }} </td>
        </tr>
    @endforeach
    </tbody>
</table>
