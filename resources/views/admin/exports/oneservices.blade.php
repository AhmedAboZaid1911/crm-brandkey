<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
    <thead> $</thead>
    <thead>
        <tr>
            <th>م</th>
            <th> اسم العميل </th>
            <th>  اسم العميل بالانجليزية</th>
            <th> رقم العميل </th>
            <th> الايمال </th>
            <th> تابع ل </th>
        </tr>
    </thead>
    <tbody>
    @foreach($clientvsservices as $k=>$clientvsservice)
        <tr>
            <td>{{$k+1}}</td>
            <td> {{  $clientvsservice->name}}  </td>
            <td> {{  $clientvsservice->name_en}}  </td>
            <td> {{  $clientvsservice->number }} </td>
            <td> {{  $clientvsservice->email }} </td>
            <td> {{  $clientvsservice->assigned_to  }} </td>
        </tr>
    @endforeach
    </tbody>
    </table>
