<table>
        <thead>
        <tr>
            <th>م</th>
            <th> اسم العميل </th>
            <th> اسم العميل بالانجليزية</th>
            <th> رقم التليفون </th>
            <th> الايميل </th>
            <th> النوع </th>
            <th> المصدر </th>
            <th> الجنسية </th>
            <th> الاهتمام </th>
            <th> تابع ل </th>
            <th>تاريخ الاضافة </th>
        </tr>
        </thead>
        <tbody>

        @foreach($clients as $k=>$client)
            <tr>
                <td>{{$k+1}}</td>
                <td>{{ $client->name }}</td>
                <td>{{ $client->name_en }}</td>
                <td> {{  $client->number }} </td>
                <td> {{  $client->email }} </td>
                <td> {{  $client->gender }} </td>
                <td> {{  $client->lead }} </td>
                <td> {{  $client->nationality }} </td>
                <td> {{  $client->Interest  }} </td>
                <td> {{  $client->assigned_to  }} </td>
                <td> {{  $client->created_at  }} </td>

            </tr>
        @endforeach
        </tbody>
    </table>
