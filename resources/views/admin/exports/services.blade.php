<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
    <thead> $</thead>
    <thead>
        <tr>
            <th>م</th>
            <th> اسم العميل </th>
            <th>  اسم العميل بالانجليزية</th>
            <th> رقم العميل </th>
            <th> الايمال </th>
            <th> تابع ل </th>
        </tr>
    </thead>
    <tbody>
    @foreach($finalclients as $k=>$finalclient)
        <tr>
            <td>{{$k+1}}</td>
            <td> {{  $finalclient->name}}  </td>
            <td> {{  $finalclient->name_en}}  </td>
            <td> {{  $finalclient->number }} </td>
            <td> {{  $finalclient->email }} </td>
            <td> {{  $finalclient->assigned_to  }} </td>
        </tr>
    @endforeach
    </tbody>
    </table>
