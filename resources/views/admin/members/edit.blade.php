@extends('layouts.adminlayout')
@section('pagejs')
    <script src="{{url('/assetsAdmin/pages/scripts/form-samples.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/assetsAdmin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script type="text/javascript">
        $("#checkAll").click(function(){
            $('input:checkbox').not(this).prop('checked', this.checked);
        });
    </script>
@endsection
@section('pagecss')
<link href="{{url('/assetsAdmin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i>تعديل  بيانات المستخدم
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form method="post" action="{{url('/admin/members/update')}}" class="form-horizontal" enctype="multipart/form-data">
                <div class="form-body">
                {{ csrf_field() }}
                <input type="hidden" name="mid" value="{{$user[0]->id}}">
                    <div class="form-group">
                        <label class="col-md-2 control-label">الاسم</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control input-circle" placeholder="Name" name="name" value="{{$user[0]->name}}">
                            <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('name') }}</div>
                        </div>
                        <label class="col-md-2 control-label">رقم الموبيل</label>
                        <div class="col-md-4">
                            <input type="number" class="form-control input-circle" placeholder="Phone" name="phone" value="{{$user[0]->phone}}">
                            <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('phone') }}</div>
                        </div>

                    </div>
                    <div class="form-group">
                            <label class="col-md-2 control-label">الايميل</label>
                        <div class="col-md-4">
                            <input type="email" class="form-control input-circle" placeholder="Email" name="email" value="{{$user[0]->email}}">
                            <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('email') }}</div>
                        </div>
                        <label class="col-md-2 control-label">الباسورد</label>
                        <div class="col-md-4">
                            <input type="password" class="form-control input-circle" placeholder="Password" name="password">
                            <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('password') }}</div>
                        </div>


                    </div>
                    <div class="form-group">

                        <label class="control-label col-md-2">الصورة</label>
                        <div class="col-md-4">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                    <img src="@if($user[0]->logo != null) {{url($user[0]->logo)}} @else http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image @endif" alt="" /> </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                <div>
                                    <span class="btn default btn-file">
                                        <span class="fileinput-new"> Select image </span>
                                        <span class="fileinput-exists"> Change </span>
                                        <input type="file" name="logo"> </span>
                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-5 col-md-7">
                            <button type="submit" class="btn btn-circle green">تعديل</button>
                        </div>
                    </div>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i>صلاحيات المستخدم
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form method="post" action="{{url('/admin/members/roles')}}" class="form-horizontal" enctype="multipart/form-data">
                <div class="form-body">
                {{ csrf_field() }}
                    <div class="row">
                        <input type="hidden" name="uid" value="{{$user[0]->id}}">
                        <div class="col-md-offset-5 col-md-7">

                            <input type="checkbox" id="checkAll" name="" value="" <?=( count ($roles) == count($exist)  ) ? 'checked' : '' ?> > الكل
                        </div>
                        <hr>
                        @for ( $i=0 ; $i<count($roles) ; $i++)
                        <div class="col-md-12">
                            <h4 style="color: #0078c7;padding: 20px 0px;border-bottom: 1px solid #32c5d2;">{{$roles[$i]->name}}</h3>
                            @foreach ($roles[$i]->role as $role )
                            <div class="col-md-3">
                            <?php if(is_array($exist)) { ?>
                                <input type="checkbox" name="role[]" value="{{$role->id}}"
                                <?=( in_array($role->id,$exist)  ) ? 'checked' : '' ?>  >
                                <?php } else
                                { ?> <input type="checkbox" name="role[]" value="{{$role->id}}" > <?php } ?>
                                {{$role->name}}
                            </div>
                            @endforeach
                        </div>
                        @endfor
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-5 col-md-4">
                            <button type="submit" class="btn btn-circle green">تعديل</button>
                        </div>
                    </div>
                </div>

            </form>
            <!-- END FORM-->
        </div>
    </div>

@endsection

