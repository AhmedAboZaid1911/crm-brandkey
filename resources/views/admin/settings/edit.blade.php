@extends('layouts.adminlayout')

@section('pagejs')

    <script src="{{url('/assetsAdmin/pages/scripts/form-samples.min.js')}}" type="text/javascript"></script>

    <script src="{{url('/assetsAdmin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>

    <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=AIzaSyCowwpc9kwaNfMVUw7J4Z-4CJ8Y6xsVT3U" type="text/javascript"></script>



@endsection

@section('pagecss')

<link href="{{url('/assetsAdmin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css" />

@endsection



@section('content')

<div class="portlet box green">

                                                <div class="portlet-title">

                                                    <div class="caption">

                                                        <i class="fa fa-gift"></i>{{$titlepage}}</div>

                                                    

                                                </div>

                                                <div class="portlet-body form">



                                          @if(session()->has('message'))

                                        <div class="alert alert-success">

                                        {{ session()->get('message') }}

                                        </div>

                                        @endif

                                                    <!-- BEGIN FORM-->

                                                    <form method="post" action="{{url('/admin/updatesettings')}}" class="form-horizontal" enctype="multipart/form-data">



                                                        <div class="form-body">

                                                        {{ csrf_field() }}

                                                        @foreach($lang_arr as $k => $v)
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">عنوان الموقع</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control input-circle" name="title_{{$k}}" value="{{$settings['title_' . $k]}}">
                                                                    <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('title_' . $k) }}</div>
                                                                </div>
                                                            </div>
                                                            @endforeach

                                                            <hr>


                                                            <div class="form-group last">

                                                            <label class="control-label col-md-3">اللوجو</label>

                                                            <div class="col-md-9">

                                                                

                                                                <div class="fileinput fileinput-new" data-provides="fileinput" style="margin-bottom: 15px">

                                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">

                                                                        @if($settings['logo'] == "")

                                                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" id="prev_img" /> 

                                                                        @else

                                                                        <img src="{{url($settings['logo'])}}" alt="" id="prev_img" />

                                                                        @endif 

                                                                    </div>

                                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> 

                                                                    </div>

                                                                    <div>

                                                                        <span class="btn default btn-file">

                                                                        <span class="fileinput-new"> <a data-toggle="modal" data-target="#basma" class="btn" type="button">Select image </a></span>

                                                                        <span class="fileinput-exists"> Change </span>

                                                                         </span>

                                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>

                                                                    </div>

                                                                    <br>

                                                                    <input id="fieldID4" type="hidden" value="" name="logo">

                                                                    

                                                                </div>

                                                            

                                                            </div>

                                                            </div>

                                                            

       <div class="input-append">

       

      <!--<a data-toggle="modal" data-target="#basma" class="btn" type="button">Select</a>-->

    </div>



    <div class="modal fade" id="basma">

<div class="modal-dialog" >

  <div class="modal-content" style="width: 900px;height: 500px">

    <div class="modal-header">

      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

      <h4 class="modal-title">اختر صورة</h4>

    </div>

    <div class="modal-body" style="width: 900px;height: 450px">

      <iframe width="100%" height="100%" src="{{url('/filemanager/dialog.php?type=2&field_id=fieldID4&fldr=')}}" frameborder="0"></iframe>

    </div>

  </div><!-- /.modal-content -->

</div><!-- /.modal-dialog -->

</div>

                                                            

                                                            
                                                        <div class="form-actions">

                                                            <div class="row">

                                                                <div class="col-md-offset-3 col-md-9">

                                                                    <button type="submit" class="btn btn-circle green">تعديل</button>

                                                                    

                                                                </div>

                                                            </div>

                                                        </div>

                                                    </form>

                                                    <!-- END FORM-->

                                                </div>

                                            </div>

                                            <script>

                                                function responsive_filemanager_callback(field_id) {

                                                    var url = {!! json_encode(url('')) !!};

                                                    var image = $('#' + field_id).val();

                                                    $('#prev_img').attr('src',url + image);

                                                }

                                            </script>

@endsection



