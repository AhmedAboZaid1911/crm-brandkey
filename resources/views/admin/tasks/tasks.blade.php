@extends('layouts.adminlayout')
@section('pagejs')
    <script src="{{url('/assetsAdmin/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{url('/assetsAdmin/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/assetsAdmin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>

    <script src="{{url('assetsAdmin/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/clockface/js/clockface.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>
@endsection
@section('pagecss')

    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/clockface/css/clockface.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{url('/assetsAdmin/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap-rtl.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light portlet-fit bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-settings font-red"></i>
                        <span class="caption-subject font-red sbold uppercase"> المهام </span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-3">
                                    <div class="btn-group">
                                        <a href="{{url('/admin/tasks/add')}}" id="sample_editable_1_new" class="btn blue-ebonyclay " style="color:#fff;"> اضافة
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="btn-group">
                                        <a href="{{url('/admin/tasks/today')}}" id="sample_editable_1_new" class="btn yellow-saffron " style="color:#fff;"> اليوم
                                            <i class="fa fa-calendar-minus-o"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="btn-group">
                                        <a href="{{url('/admin/tasks/all')}}" id="sample_editable_1_new" class="btn blue"> الكل
                                            <i class="fa fa-calendar"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="btn-group">
                                        <a href="{{url('/admin/tasks/late')}}" id="sample_editable_1_new" class="btn red"> متأخرة
                                            <i class="fa fa-calendar-times-o"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="btn-group">
                                        <a href="{{url('/admin/tasks/done')}}" id="sample_editable_1_new" class="btn " style="background-color:#1bbc9b;color:#fff;"> تمت
                                            <i class="fa fa-fa fa-calendar-check-o	"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>
                            <hr>
                            <div class="col-md-12">
                                <form method="post" action="{{url('admin/tasks/search')}}" class="form-horizontal" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="col-md-3">
                                        <div class="input-group input-medium date date-picker" data-date="" data-date-format="yyyy-mm-dd" data-date-viewmode="years">
                                            <input type="text" class="form-control" name="from" value="" required="required" readonly >
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button" required="required">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <!-- /input-group -->
                                        <span class="help-block">  </span>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="input-group input-medium date date-picker" data-date="" data-date-format="yyyy-mm-dd" data-date-viewmode="years">
                                            <input type="text" class="form-control" name="to" value=""  readonly >
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <!-- /input-group -->
                                        <span class="help-block">  </span>
                                    </div>
                                    <div class="col-md-3">
                                        <select name="case" id="case" class="form-control select2 input-medium" required>
                                            <option value="1" > الكل</option>
                                            <option value="2" > متاخرة</option>
                                            <option value="3" > تمت</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <button type="submit" class="btn btn-circle green">بحث</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    @if(session()->has('message'))
                    <div class="alert alert-success">
                    {{ session()->get('message') }}
                    </div>
                    @endif
                    <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                        <thead style="background-color: #364150;color:#fff;">
                            <tr>
                                <th>
                                    اسم العميل / الخدمة
                                </th>
                                <th>
                                    اسم العميل بالانجليزية
                                </th>
                                <th>الرقم</th>
                                <th>المهمة </th>
                                <th>التاريخ </th>
                                <th>تابع ل </th>
                                <th>تاريخ الانتهاء </th>
                                <th> الانتهاء من قبل </th>
                                @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('7'))
                                <th> تفاصيل </th>
                                @endif
                                @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('10'))
                                <th> حذف </th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($tasks as $task)
                            @if($task->status == '3')
                            <tr class="bg-green-jungle ">
                            @elseif($task->assigned_at < date('Y-m-d') )
                            <tr class="bg-red-pink">
                            @else
                            <tr class="warning">
                            @endif
                                <td>
                                @if($task->type =='clientvsservices')
                                     {{$task->service}} / {{$task->client}}
                                @endif
                                @if($task->type =='client')
                                <a  style="color:#000;" href="{{url('admin/Clients/Clients_view')}}/{{$task->itemid}}">
                                {{$task->client}}
                                </a>
                                @endif
                                @if($task->type =='general')
                                    عام
                                @endif
                                </td>
                                <td>
                                @if($task->type =='clientvsservices')
                                    {{$task->client_en}}
                                @endif
                                @if($task->type =='client')
                                <a  style="color:#000;" href="{{url('admin/Clients/Clients_view')}}/{{$task->itemid}}">
                                {{$task->client_en}}
                                </a>
                                @endif
                                @if($task->type =='general')
                                    عام
                                @endif
                                </td>
                                @if($task->type =='general')
                                <td> عام  </td>
                                @else
                                <td> {{  $task->clientnum  }}  </td>
                                @endif
                                <td> {{  $task->title  }}  </td>
                                <td> {{  $task->assigned_at  }}  </td>
                                <td> {{  $task->assigned_to  }}  </td>
                                <td> {{  $task->updated_at  }}  </td>
                                <td> {{  $task->updated_by  }}  </td>
                                @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('7'))
                                <td>
                                    <a class="edit  btn btn-circle blue-soft active" href="{{url('admin/tasks/edit')}}/{{$task->id}}" > تفاصيل <i class="fa fa-edit"></i></a>
                                </td>
                                @endif
                                @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('10'))
                                <td>
                                    <a class="delete btn btn-transparent red btn-outline btn-circle btn-sm active" href="{{url('admin/tasks/del')}}/{{$task->id}}" onclick="return confirm('Are you sure you want to delete this item?');"> حذف <i class="fa fa-trash-o"></i></a>
                                </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $tasks->links() }}
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

@endsection

