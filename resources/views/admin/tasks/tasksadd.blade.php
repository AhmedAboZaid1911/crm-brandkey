@extends('layouts.adminlayout')

@section('pagejs')
    <script src="{{url('/assetsAdmin/pages/scripts/form-samples.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/assetsAdmin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>

    <script src="{{url('assetsAdmin/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/clockface/js/clockface.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/plugins/jquery.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/plugins/js.cookie.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>

    <script src="{{url('assetsAdmin/global/plugins/bootbox/bootbox.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/layouts/layout/scripts/demo.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/layouts/global/scripts/quick-sidebar.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/layouts/global/scripts/quick-nav.min.js')}}" type="text/javascript"></script>



    <script src="{{url('js/multiuploadscript.js')}}"></script>
    <script>
            $( "#paidValue" ).keyup(function() {
                var remainderValue = 0;
                remainderValue =   $('#price').val() - $(this).val();
                $('#remainderValue').val(remainderValue);
                });
    </script>
    <script>
            $(document).ready(function()
            {
                $('#clickmewow').click(function()
                {
                    $('#radio1003').attr('checked', 'checked');
                });
            })
    </script>

@endsection

@section('pagecss')


    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="{{url('css/multiuploadstyle.css')}}">

    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/clockface/css/clockface.css')}}" rel="stylesheet" type="text/css" />


@endsection



@section('content')

    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i> اضافة مهمة
            </div>
        </div>
        <div class="portlet-body form">
           <div class="row">
               <div class="col-md-12">
                <form  class="form-horizontal" method="post" action="{{url('admin/tasks/store')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="control-label-client-edit col-md-2"> تاريخ</label>
                        <div class="col-md-4">
                            <div class="input-group input-larag date date-picker" data-date="" data-date-format="yyyy-mm-dd" data-date-viewmode="years">
                                <input type="text" class="form-control input-larag" name="assigned_at" value="{{ old('assigned_at') }}" required readonly >
                                <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                            <!-- /input-group -->
                            <span class="help-block">  </span>
                        </div>
                            <label class="col-md-2 control-label-client-edit">  العنوان  <span style="color: red"> *</span> </label>
                            <div class="col-md-4">
                                <input type="text" class="form-control input-circle" id="title" name="title" placeholder="العنوان " value="" >
                            </div>
                        </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label-client-edit"> المهمة <span style="color: red"> *</span></label>
                        <div class="col-md-4">
                            <textarea rows="10" placeholder="المحتوى" id="form-field-22" class="form-control" name="text" ></textarea>
                        </div>
                        <label class="control-label-client-edit col-md-2">الصورة</label>
                        <div class="col-md-4">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                <div>
                                    <span class="btn default btn-file">
                                        <span class="fileinput-new"> Select image </span>
                                        <span class="fileinput-exists"> Change </span>
                                        <input type="file" name="pic"> </span>
                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                </div>
                            </div>
                            <div class="clearfix margin-top-10">
                                <span class="label label-danger"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label-client-edit">تابع ل <span style="color: red"> *</span></label>
                        <div class="col-md-4">
                            <select name="assigned_to" id="assigned_to" class="form-control select2 input-larag" required>
                            @foreach($user as $users)
                                <option value="{{$users->id}}" <?=($users->id == old('assigned_to')) ? 'selected' : '' ?>>
                                    {{$users->{'name'} }}
                                </option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-5 col-md-7">
                                <button type="submit" class="btn btn-circle green">اضافة مهمة</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>


        </div>
        </div>
    </div>


@endsection



