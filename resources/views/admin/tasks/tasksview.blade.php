@extends('layouts.adminlayout') @section('pagejs')



<script src="{{url('/assetsAdmin/pages/scripts/form-samples.min.js')}}" type="text/javascript"></script>



<script src="{{url('/assetsAdmin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>



<script src="{{url('assetsAdmin/layouts/layout/scripts/demo.min.js')}}" type="text/javascript"></script>

<script src="{{url('assetsAdmin/layouts/global/scripts/quick-sidebar.min.js')}}" type="text/javascript"></script>

<script src="{{url('assetsAdmin/layouts/global/scripts/quick-nav.min.js')}}" type="text/javascript"></script>



<script src="{{url('assetsAdmin/global/plugins/moment.min.js')}}" type="text/javascript"></script>

<script src="{{url('assetsAdmin/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>

<script src="{{url('assetsAdmin/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>

<script src="{{url('assetsAdmin/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>

<script src="{{url('assetsAdmin/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>

<script src="{{url('assetsAdmin/global/plugins/clockface/js/clockface.js')}}" type="text/javascript"></script>

<script src="{{url('assetsAdmin/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>









<script src="{{url('assetsAdmin/plugins/jquery.min.js')}}" type="text/javascript"></script>

<script src="{{url('assetsAdmin/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>

<script src="{{url('assetsAdmin/plugins/js.cookie.min.js')}}" type="text/javascript"></script>

<script src="{{url('assetsAdmin/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>

<script src="{{url('assetsAdmin/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>

<script src="{{url('assetsAdmin/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>





<script src="{{url('js/multiuploadscript.js')}}"></script>

<script>

    $("#paidValue").keyup(function() {

        var remainderValue = 0;

        remainderValue = $('#price').val() - $(this).val();

        $('#remainderValue').val(remainderValue);

    });



</script>

<script>

    $(document).ready(function() {

        $('#clickmewow').click(function() {

            $('#radio1003').attr('checked', 'checked');

        });

    })



</script>

@endsection @section('pagecss')





<link href="{{url('/assetsAdmin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css" />



<link rel="stylesheet" type="text/css" href="{{url('css/multiuploadstyle.css')}}">



<link href="{{url('/assetsAdmin/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />

<link href="{{url('/assetsAdmin/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />

<link href="{{url('/assetsAdmin/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css" />

<link href="{{url('/assetsAdmin/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />

<link href="{{url('/assetsAdmin/global/plugins/clockface/css/clockface.css')}}" rel="stylesheet" type="text/css" /> @endsection @section('content')



<div class="portlet box green">

    <div class="portlet-title">

        <div class="caption">

            <i class="fa fa-gift"></i> تفاصيل المهمة

        </div>

    </div>

    <div class="portlet-body form">

        @if(session()->has('message'))

        <div class="alert alert-success">

            {{ session()->get('message') }}

        </div>

        @endif

        <!-- BEGIN FORM-->

        @if ($tasks[0]->type != 'general')

        <div class="row">

            <div class="col-md-12">

                <div class="portlet light bordered" id="blockui_sample_1_portlet_body">

                    <div class="portlet-title">

                        <div class="caption">

                            <i class="icon-bubble font-green-sharp"></i>

                            <span class="caption-subject font-green-sharp sbold">بيانات العميل </span>

                        </div>

                    </div>

                    <div class="portlet-body">

                        <div class="row">

                            <div class="col-md-12">

                                <!-- BEGIN SAMPLE TABLE PORTLET-->

                                <div class="portlet-body">

                                    <div class="table-scrollable">

                                        <table class="table table-striped table-bordered table-advance table-hover">

                                            <thead>

                                                <tr>

                                                    <th><i class="fa fa-user"></i> </th>

                                                    <th class="hidden-xs"> <i class="fa fa-edit"></i> </th>

                                                </tr>

                                            </thead>

                                            <tbody>

                                                <tr>

                                                    <td class="highlight">

                                                        <div class="success"></div>

                                                        الاسم بالكامل

                                                    </td>

                                                    <td class="hidden-xs">



                                                        <a href="{{url('admin/Clients/Clients_view/')}}/{{$clients[0]->id}}">

                                                                    {{ $clients[0]->name }}

                                                                </a>

                                                    </td>



                                                </tr>
                                                <tr>

                                                    <td class="highlight">

                                                        <div class="success"></div>

                                                        الاسم بالكامل بالانجليزية

                                                    </td>

                                                    <td class="hidden-xs">



                                                        <a href="{{url('admin/Clients/Clients_view/')}}/{{$clients[0]->id}}">

                                                                    {{ $clients[0]->name_en }}

                                                                </a>

                                                    </td>



                                                </tr>

                                                <tr>

                                                    <td class="highlight">

                                                        <div class="info"> </div>

                                                        E-mail

                                                    </td>

                                                    <td class="hidden-xs"> {{ $clients[0]->email }} </td>

                                                </tr>

                                                <tr>

                                                    <td class="highlight">

                                                        <div class="info"> </div>

                                                        رقم التليفون

                                                    </td>

                                                    <td class="hidden-xs"> {{ $clients[0]->number }} </td>

                                                </tr>

                                                <tr>

                                                    <td class="highlight">

                                                        <div class="info"> </div>

                                                        رقم التليفون الثاني

                                                    </td>

                                                    <td class="hidden-xs"> {{ $clients[0]->number2 }} </td>

                                                </tr>

                                            </tbody>

                                        </table>

                                    </div>

                                </div>

                                <!-- END SAMPLE TABLE PORTLET-->

                            </div>

                        </div>

                    </div>

                </div>

            </div>



            @endif @if ($tasks[0]->type == 'clientvsservices')

            <div class="row">

                <div class="col-md-12">

                    <div class="portlet light bordered" id="blockui_sample_1_portlet_body">

                        <div class="portlet-title">

                            <div class="caption">

                                <i class="icon-bubble font-green-sharp"></i>

                                <span class="caption-subject font-green-sharp sbold">  الخدمة   </span>

                            </div>

                        </div>

                        <div class="portlet-body">

                            <div class="row">

                                <div class="col-md-12">

                                    <!-- BEGIN SAMPLE TABLE PORTLET-->

                                    <div class="portlet-body">

                                        <div class="table-scrollable">

                                            <table class="table table-striped table-hover table-bordered" id="sample_editable_1">

                                                <thead>

                                                    <tr>

                                                        <th> اسم الخدمة </th>

                                                        <th> معاد </th>

                                                        <th> فترة </th>

                                                        <th> مكان </th>

                                                        <th> ايام الخدمة </th>

                                                        <th> تكلفة الخدمة </th>

                                                        <th> السعر </th>

                                                    </tr>

                                                </thead>

                                                <tbody>

                                                    <tr>

                                                        <td> {{ $clientvsservices[0]->name }} </td>

                                                        <td> {{$clientvsservices[0]->time}} </td>

                                                        <td> {{$clientvsservices[0]->servicesdate}}  </td>

                                                        <td> {{$clientvsservices[0]->place}} </td>

                                                        <td> {{ $clientvsservices[0]->day1 }} / {{ $clientvsservices[0]->day2 }} / {{ $clientvsservices[0]->day3 }} / {{ $clientvsservices[0]->day4 }} / {{ $clientvsservices[0]->day5 }} / {{ $clientvsservices[0]->day6 }} / {{ $clientvsservices[0]->day7 }}

                                                        </td>

                                                        <td> {{ $clientvsservices[0]->price }} {{ $clientvsservices[0]->currency }} </td>

                                                        <td> {{ $clientvsservices[0]->price1 }} {{ $clientvsservices[0]->currency1 }} </td>

                                                    </tr>

                                                </tbody>

                                            </table </div>

                                        </div>

                                        <!-- END SAMPLE TABLE PORTLET-->

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>



                    @endif



                    <div class="col-md-12">

                        <div class="col-md-12">

                            <div class="portlet light bordered" id="blockui_sample_1_portlet_body">

                                <div class="portlet-title">

                                    <div class="caption">

                                        <i class="icon-bubble font-green-sharp"></i>

                                        <span class="caption-subject font-green-sharp sbold">  المهمة   </span>

                                    </div>

                                </div>

                                <div class="portlet-body">

                                    <div class="row">

                                        <div class="col-md-12">

                                            <!-- BEGIN SAMPLE TABLE PORTLET-->

                                            <div class="portlet-body">

                                                <div class="table-scrollable">

                                                    <form class="form-horizontal" method="post" action="{{url('admin/ClientsVsServices/Clients_tasks_update')}}" enctype="multipart/form-data">

                                                        <div class="form-group">

                                                            {{ csrf_field() }}

                                                            <input type="hidden" name="id" value="{{$tasks[0]->id}}">

                                                            <div class="col-md-12">

                                                                <div class="portlet-body">

                                                                    <div class="mt-timeline-2">

                                                                        <ul class="mt-container">

                                                                            <li class="mt-item">

                                                                                <div class="mt-timeline-content">

                                                                                    <div class="mt-content-container ">

                                                                                        <div class="mt-author">

                                                                                            <div class="mt-avatar">

                                                                                                <img src="{{url($tasks[0]->logo)}}" />

                                                                                            </div>



                                                                                            <div class="mt-author-name">



                                                                                                <a href="javascript:;" class="font-blue-madison"><span  style="color:#36c6d3"> من :</span> {{$tasks[0]->username }}</a>

                                                                                            </div>

                                                                                            <div class="mt-author-notes font-grey-mint">{{$tasks[0]->created_at}}</div>

                                                                                        </div>

                                                                                        <div class="mt-title">



                                                                                            <h3 class="mt-content-title font-blue-madison"><a href="javascript:;" class="font-blue-madison"><span  style="color:#36c6d3;"> الى :</span> {{ $tasks[0]->assigned_to}}</a></h3>

                                                                                            <div class="mt-author-notes font-grey-mint ">{{$tasks[0]->assigned_at}}</div>

                                                                                        </div>

                                                                                        <div class="mt-content border-grey-salt" style="text-align:center;">





                                                                                            <div class="col-md-9">

                                                                                                <p style="border-bottom: 1px dashed #0078c7;margin: 10px;color: #0078c7;padding: 10px;text-align: right;font-size: 20px;">{{$tasks[0]->title}}</p>

                                                                                                <p style="text-align:right;">

                                                                                                    {{$tasks[0]->text}}

                                                                                                </p>





                                                                                            </div>



                                                                                            <div class="col-md-3">

                                                                                                @if ($tasks[0]->pic != null)

                                                                                                <a href="{{url('')}}/{{$tasks[0]->pic}}" target="_blank" style="padding-down:5px;">

                                                                                                        <img class="timeline-body-img "  style="max-width:100%;Margin:3%;border: 1px solid #32c5d2;" src="{{url($tasks[0]->pic)}}" alt="{{$tasks[0]->title}}">

                                                                                                    </a> @endif

                                                                                            </div>

                                                                                        </div>

                                                                                        @if($tasks[0]->status == '3')

                                                                                        <div class="mt-content border-grey-salt" style="text-align:center;">

                                                                                            <p>

                                                                                                <span aria-hidden="true" class="icon-like"></span>

                                                                                                <span class="glyphicon glyphicon-ok"> </span> تم أغلاق المهمة

                                                                                            </p>

                                                                                        </div>

                                                                                        @endif

                                                                                    </div>

                                                                                </div>

                                                                            </li>

                                                                        </ul>

                                                                    </div>

                                                                </div>

                                                            </div>





                                                            <div class="col-md-8 col-md-offset-3">

                                                                @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('9'))
                                                                     @if($tasks[0]->status == '1')

                                                                    <div class="col-md-6" hidden>

                                                                        <select name="case" id="case" class="form-control select2 input-medium" required>
                                                                            <option value="3"> تمت</option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="col-md-6">

                                                                        <button type="submit" class="btn btn-circle green">تمت</button>

                                                                    </div>

                                                                    @endif
                                                                @endif

                                                            </div>





                                                        </div>

                                                    </form>

                                                </div>

                                            </div>

                                            <!-- END SAMPLE TABLE PORTLET-->

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>





    @endsection

