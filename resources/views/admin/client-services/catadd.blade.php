@extends('layouts.adminlayout')

@section('pagejs')

    <script src="{{url('/assetsAdmin/pages/scripts/form-samples.min.js')}}" type="text/javascript"></script>

    <script src="{{url('/assetsAdmin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>

    <script src="{{url('js/multiuploadscript.js')}}"></script>
    <script>
        $( "#paidValue" ).keyup(function() {
            var remainderValue = 0;
                        remainderValue =   $('#price').val() - $(this).val();


                        $('#remainderValue').val(remainderValue);

                    });
    </script>

    <script src="{{url('/assetsAdmin/pages/scripts/components-select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/assetsAdmin/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>

    <script language="JavaScript">
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
    </script>
@endsection

@section('pagecss')

    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css" />

    <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="{{url('/assetsAdmin/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('/assetsAdmin/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->

    <link rel="stylesheet" type="text/css" href="{{url('css/multiuploadstyle.css')}}">

@endsection



@section('content')

    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i> {{$posttitlepage}}
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light bordered" id="blockui_sample_1_portlet_body">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-bubble font-green-sharp"></i>
                                <span class="caption-subject font-green-sharp sbold">بيانات العميل </span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                                        <div class="portlet-body">
                                            <div class="table-scrollable">
                                                <table class="table table-striped table-bordered table-advance table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th><i class="fa fa-user"></i>  </th>
                                                            <th class="hidden-xs"> <i class="fa fa-edit"></i>  </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td class="highlight">
                                                                <div class="success"></div>
                                                                الاسم بالكامل
                                                            </td>
                                                            <td class="hidden-xs">
                                                                <a href="{{url('admin/')}}/{{$module}}/Clients_view/{{$clients[0]->id}}">
                                                                {{ $clients[0]->name }} </a></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="highlight">
                                                                <div class="success"></div>
                                                                الانجليزيةالاسم بالكامل
                                                            </td>
                                                            <td class="hidden-xs">
                                                                <a href="{{url('admin/')}}/{{$module}}/Clients_view/{{$clients[0]->id}}">
                                                                {{ $clients[0]->name_en }} </a></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="highlight">
                                                                <div class="info"> </div>
                                                                E-mail
                                                            </td>
                                                            <td class="hidden-xs"> {{ $clients[0]->email }} </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="highlight">
                                                                <div class="info"> </div>
                                                                رقم التليفون
                                                            </td>
                                                            <td class="hidden-xs"> {{ $clients[0]->number }} </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="highlight">
                                                                <div class="info"> </div>
                                                                رقم التليفون الثاني
                                                            </td>
                                                            <td class="hidden-xs"> {{ $clients[0]->number2 }} </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    <!-- END SAMPLE TABLE PORTLET-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered" id="blockui_sample_1_portlet_body">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-bubble font-green-sharp"></i>
                                    <span class="caption-subject font-green-sharp sbold"> الخدمات </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- BEGIN SAMPLE TABLE PORTLET-->
                                            <div class="portlet-body">
                                                <div class="table-scrollable">
                                                    <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                                                            <thead>
                                                                    <tr>
                                                                        <th> م</th>
                                                                        <th> اسم الخدمة </th>
                                                                        <th> ميعاد  </th>
                                                                        <th>  فترة من </th>
                                                                        <th> فترة الى </th>
                                                                        <th> مكان </th>
                                                                        <th> ايام الخدمة </th>
                                                                        <th> تكلفة الخدمة </th>
                                                                        <th> السعر </th>
                                                                        <th> الحالة </th>
                                                                        <th> تفاصيل </th>
                                                                        <th> حذف </th>
                                                                    </tr>
                                                            </thead>
                                                            <tbody>
                                                                    @foreach($clientvsservices as $k=>$clientvsservice)
                                                                    <tr>

                                                                        <td>{{$k+=1}}</td>
                                                                        <td> {{ $clientvsservice->name }} </td>
                                                                        <td> {{$clientvsservice->time}} </td>
                                                                        <td> {{$clientvsservice->servicesdate}} </td>
                                                                        <td> {{$clientvsservice->place}} </td>
                                                                        <td>
                                                        @if ( $clientvsservice->day1 != NULL )
                                                        {{  $clientvsservice->day1  }}
                                                        @else
                                                        {{  $clientvsservice->day1  }}
                                                        @endif

                                                        @if ( $clientvsservice->day2 != NULL )
                                                        / {{  $clientvsservice->day2  }}
                                                        @else
                                                        {{  $clientvsservice->day2  }}
                                                        @endif

                                                        @if ( $clientvsservice->day3 != NULL )
                                                        / {{  $clientvsservice->day3  }}
                                                        @else
                                                        {{  $clientvsservice->day3  }}
                                                        @endif

                                                        @if ( $clientvsservice->day4 != NULL )
                                                        / {{  $clientvsservice->day4  }}
                                                        @else
                                                        {{  $clientvsservice->day4  }}
                                                        @endif

                                                        @if ( $clientvsservice->day5 != NULL )
                                                        / {{  $clientvsservice->day5  }}
                                                        @else
                                                        {{  $clientvsservice->day5  }}
                                                        @endif

                                                        @if ( $clientvsservice->day6 != NULL )
                                                        / {{  $clientvsservice->day6  }}
                                                        @else
                                                        {{  $clientvsservice->day6  }}
                                                        @endif

                                                        @if ( $clientvsservice->day7 != NULL )
                                                        / {{  $clientvsservice->day7  }}
                                                        @else
                                                        {{  $clientvsservice->day7  }}
                                                        @endif


                                                         </td>
                                                         <td>  {{ $clientvsservice->cost   }}  {{  $clientvsservice->currency   }} </td>
                                                         <td>  {{ $clientvsservice->price  }}  {{  $clientvsservice->currency   }} </td>
                                                        @if($clientvsservice->servicesstatus == 1)
                                                        <td>  حجز </td>
                                                        @else
                                                        <td>  اتصال  </td>
                                                        @endif
                                                         <td>
                                                                 <a class="edit btn btn-transparent blue btn-outline btn-circle btn-sm active" href="{{url('admin/ClientsVsServices/service/')}}/{{$clientvsservice->id}}"> تفاصيل <i class="fa fa-edit"></i></a>
                                                         </td>
                                                         <td>
                                                             <a class="delete btn btn-transparent red btn-outline btn-circle btn-sm active" href="{{url('admin/ClientsVsServices/Clients_del/')}}/{{$clientvsservice->id}}/{{$clients[0]->id}}" onclick="return confirm('Are you sure you want to delete this item?');"> حذف <i class="fa fa-trash-o"></i></a>
                                                         </td>
                                                         </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        <!-- END SAMPLE TABLE PORTLET-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered" id="blockui_sample_1_portlet_body">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-bubble font-green-sharp"></i>
                                    <span class="caption-subject font-green-sharp sbold">  اضافة خدمة جديدة   </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- BEGIN SAMPLE TABLE PORTLET-->
                                            <div class="portlet-body">
                                                <div class="table-scrollable">
                                                    <form method="post" action="{{url('admin/ClientsVsServices/Clients_store')}}" class="form-horizontal" enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="client" value="{{$clients[0]->id}}">
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">  الخدمة الجديدة    </label>
                                                            <div class="col-md-9">
                                                                <select name="service" id="service" class="form-control select2 input-lager" required>
                                                                    @foreach($Services as $Service)
                                                                        <option value="{{$Service->id}}" <?=($Service->id == old('service')) ? 'selected' : '' ?>>
                                                                            {{$Service->name }} {{ $Service->time }} {{$Service->servicesdate }} {{$Service->place }}

                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group ">
                                                            <label class="col-md-2 control-label">  السعر   </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control input-circle" id="price" name="price" placeholder="السعر " value="{{ old('price') }}">
                                                            </div>
                                                            <label class="col-md-2 control-label">  العملة   </label>
                                                            <div class="col-md-4">
                                                                <select name="currency" id="currency" class="form-control  select2 input-lager" required>
                                                                    @foreach($currency as $currencys)
                                                                        <option value="{{$currencys->id}}" <?=($currencys->id == old('currency')) ? 'selected' : '' ?>>
                                                                            {{ $currencys->currency  }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">  المدفوع   </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control input-circle" id="paidValue" name="paid" placeholder="المدفوع " value="{{ old('paid') }}">
                                                            </div>
                                                            <label class="col-md-2 control-label">  المتبقي   </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control input-circle" id="remainderValue" name="remainderValue" placeholder="المتبقي " value="" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">  الحالة   </label>
                                                            <div class="col-md-4">
                                                                <select name="servicesstatus" id="servicesstatus" class="form-control  select2 input-lager" required>
                                                                    <option value="1" >
                                                                        حجز
                                                                    </option>
                                                                    <option value="2" >
                                                                        اتصال
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                            <!--
                                                        <a href="#" onclick="printDiv('printableArea')" class="btn btn-circle red btn-lg">
                                                                <span class="glyphicon glyphicon-print"></span> Print
                                                        </a> -->
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-9">
                                                                    <button type="submit" class="btn btn-circle green">اضافة</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        <!-- END SAMPLE TABLE PORTLET-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>





@endsection



