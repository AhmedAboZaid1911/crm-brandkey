@extends('layouts.adminlayout')

@section('pagejs')

    <script src="{{url('/assetsAdmin/pages/scripts/form-samples.min.js')}}" type="text/javascript"></script>

    <script src="{{url('/assetsAdmin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>

    <script src="{{url('assetsAdmin/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/clockface/js/clockface.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>

    <script src="{{url('js/multiuploadscript.js')}}"></script>
    <script>
            $( "#paidValue" ).keyup(function() {
                var remainderValue = 0;
                remainderValue =   $('#price').val() - $(this).val();
                $('#remainderValue').val(remainderValue);
                });
    </script>
    <script src="{{url('/assetsAdmin/pages/scripts/components-select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/assetsAdmin/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
@endsection

@section('pagecss')


    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="{{url('css/multiuploadstyle.css')}}">

    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/clockface/css/clockface.css')}}" rel="stylesheet" type="text/css" />

    <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="{{url('/assetsAdmin/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('/assetsAdmin/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->


@endsection



@section('content')

    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i> {{$posttitlepage}}
            </div>
        </div>
        <div class="portlet-body form">

                @if(session()->has('message'))
                <div class="alert alert-success">
                {{ session()->get('message') }}
                </div>
                @endif



        <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered" id="blockui_sample_1_portlet_body">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-bubble font-green-sharp"></i>
                                    <span class="caption-subject font-green-sharp sbold">بيانات العميل </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- BEGIN SAMPLE TABLE PORTLET-->
                                            <div class="portlet-body">
                                                <div class="table-scrollable">
                                                    <table class="table table-striped table-bordered table-advance table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th><i class="fa fa-user"></i>  </th>
                                                                <th class="hidden-xs"> <i class="fa fa-edit"></i>  </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td class="highlight">
                                                                    <div class="success"></div>
                                                                    الاسم بالكامل
                                                                </td>
                                                                <td class="hidden-xs">  <a href="{{url('admin/')}}/{{$module}}/Clients_view/{{$clients[0]->id}}">
                                                                    {{ $clients[0]->name }} </a></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="highlight">
                                                                    <div class="success"></div>
                                                                    الاسم بالكامل باللغة الانجليزية
                                                                </td>
                                                                <td class="hidden-xs">  <a href="{{url('admin/')}}/{{$module}}/Clients_view/{{$clients[0]->id}}">
                                                                    {{ $clients[0]->name_en }} </a></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="highlight">
                                                                    <div class="info"> </div>
                                                                    E-mail
                                                                </td>
                                                                <td class="hidden-xs"> {{ $clients[0]->email }} </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="highlight">
                                                                    <div class="info"> </div>
                                                                    رقم التليفون
                                                                </td>
                                                                <td class="hidden-xs"> {{ $clients[0]->number }} </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="highlight">
                                                                    <div class="info"> </div>
                                                                    رقم التليفون الثاني
                                                                </td>
                                                                <td class="hidden-xs"> {{ $clients[0]->number2 }} </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        <!-- END SAMPLE TABLE PORTLET-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <li style="float:left;padding-left: 15px;">

                        <a class="edit btn btn-transparent green-jungle btn-outline btn-circle btn-sm active" href="{{url('admin/ClientsVsServices/index')}}/{{$clients[0]->id}}"> الخدمات <i class="fa fa-edit"></i></a>

                    </li>

            <div class="row">
                <div class="col-md-12">
                        <div class="portlet light bordered" id="blockui_sample_1_portlet_body">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-bubble font-green-sharp"></i>
                                    <span class="caption-subject font-green-sharp sbold">  الخدمة   </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- BEGIN SAMPLE TABLE PORTLET-->
                                            <div class="portlet-body">
                                                <div class="table-scrollable">
                                                    <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                                                        <thead>
                                                            <tr>
                                                                <th> اسم الخدمة </th>
                                                                <th> معاد  </th>
                                                                <th> فترة </th>
                                                                <th> مكان </th>
                                                                <th> ايام الخدمة </th>
                                                                <th> تكلفة الخدمة </th>
                                                                <th> السعر </th>
                                                                <th> الحالة </th>
                                                                <th> حذف </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td> {{ $clientvsservices[0]->name }} </td>
                                                                <td> {{$clientvsservices[0]->time}} </td>

                                                                <td>  {{$clientvsservices[0]->place}} </td>
                                                                <td>
                                                                        @if ( $clientvsservices[0]->day1 != NULL )
                                                                        {{  $clientvsservices[0]->day1  }}
                                                                        @else
                                                                        {{  $clientvsservices[0]->day1  }}
                                                                        @endif

                                                                        @if ( $clientvsservices[0]->day2 != NULL )
                                                                        / {{  $clientvsservices[0]->day2  }}
                                                                        @else
                                                                        {{  $clientvsservices[0]->day2  }}
                                                                        @endif

                                                                        @if ( $clientvsservices[0]->day3 != NULL )
                                                                        / {{  $clientvsservices[0]->day3  }}
                                                                        @else
                                                                        {{  $clientvsservices[0]->day3  }}
                                                                        @endif

                                                                        @if ( $clientvsservices[0]->day4 != NULL )
                                                                        / {{  $clientvsservices[0]->day4  }}
                                                                        @else
                                                                        {{  $clientvsservices[0]->day4  }}
                                                                        @endif

                                                                        @if ( $clientvsservices[0]->day5 != NULL )
                                                                        / {{  $clientvsservices[0]->day5  }}
                                                                        @else
                                                                        {{  $clientvsservices[0]->day5  }}
                                                                        @endif

                                                                        @if ( $clientvsservices[0]->day6 != NULL )
                                                                        / {{  $clientvsservices[0]->day6  }}
                                                                        @else
                                                                        {{  $clientvsservices[0]->day6  }}
                                                                        @endif

                                                                        @if ( $clientvsservices[0]->day7 != NULL )
                                                                        / {{  $clientvsservices[0]->day7  }}
                                                                        @else
                                                                        {{  $clientvsservices[0]->day7  }}
                                                                        @endif
                                                                </td>
                                                                <td>  {{ $clientvsservices[0]->price1 }}  {{  $clientvsservices[0]->currency1  }} </td>
                                                                <td>  {{ $clientvsservices[0]->price  }}  {{  $clientvsservices[0]->currency   }} </td>
                                                                @if($clientvsservices[0]->servicesstatus == 1)
                                                                <td>  حجز </td>
                                                                @else
                                                                <td>  اتصال  </td>
                                                                @endif
                                                                <td>
                                                                    <a class="delete btn btn-transparent red btn-outline btn-circle btn-sm active" href="{{url('admin/ClientsVsServices/Clients_del/')}}/{{$clientvsservices[0]->id}}/{{$clients[0]->id}}" onclick="return confirm('Are you sure you want to delete this item?');"> حذف <i class="fa fa-trash-o"></i></a>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table
                                                </div>
                                            </div>
                                        <!-- END SAMPLE TABLE PORTLET-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                        <div class="portlet light bordered" id="blockui_sample_1_portlet_body">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-bubble font-green-sharp"></i>
                                    <span class="caption-subject font-green-sharp sbold">  تعديل الخدمة    </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- BEGIN SAMPLE TABLE PORTLET-->
                                            <div class="portlet-body">
                                                <div class="table-scrollable">
                                                        <form method="post" action="{{url('admin/ClientsVsServices/Clients_update')}}" class="form-horizontal" enctype="multipart/form-data">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="id" value="{{$clientvsservices[0]->id}}">
                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">  تعديل الخدمة    </label>
                                                                <div class="col-md-10">
                                                                    <select name="service" id="service" class="form-control select2 input-lager" required>
                                                                        @foreach($Services as $Service)
                                                                            <option value="{{$Service->id}}" <?=($Service->id == $clientvsservices[0]->service) ? 'selected' : '' ?>>
                                                                                {{$Service->name }} {{ $Service->time }}  {{$Service->place }}
                                                                            </option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">  السعر   </label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control input-circle" id="price" name="price" placeholder="السعر " value="{{ $clientvsservices[0]->price }}">
                                                                </div>
                                                                <label class="col-md-2 control-label">  العملة   </label>
                                                                <div class="col-md-4">
                                                                    <select name="currency" id="currency" class="form-control select2 input-larag" required>

                                                                        @foreach($currency as $currencys)
                                                                            <option value="{{$currencys->id}}" <?=($currencys->id == $clientvsservices[0]->currency) ? 'selected' : '' ?>>
                                                                                {{' ( ' . $currencys->currency . ' ) ' }}
                                                                            </option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                    <label class="col-md-2 control-label">  المدفوع   </label>
                                                                    <div class="col-md-4">
                                                                        <input type="text" class="form-control input-circle" id="paidValue" name="paid" placeholder="المدفوع " value="{{ $clientvsservices[0]->paid }}">
                                                                    </div>
                                                                <label class="col-md-2 control-label">  المتبقي   </label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control input-circle" id="remainderValue" name="remainderValue" placeholder="المتبقي " value="{{ $clientvsservices[0]->remainderValue }}" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">  الحالة   </label>
                                                                <div class="col-md-4">
                                                                    <select name="servicesstatus" id="servicesstatus" class="form-control  select2 input-lager" required>
                                                                        <option value="1" @if($clientvsservices[0]->servicesstatus == '1') selected @endif>
                                                                            حجز
                                                                        </option>
                                                                        <option value="2" @if($clientvsservices[0]->servicesstatus == '2') selected @endif >
                                                                            اتصال
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="form-actions">
                                                                <div class="row">
                                                                    <div class="col-md-offset-2 col-md-9">
                                                                        <button type="submit" class="btn btn-circle green">تعديل</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                </div>
                                            </div>
                                        <!-- END SAMPLE TABLE PORTLET-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet light bordered">
                <div class="portlet-body">
                    <ul class="nav nav-pills">
                        <li class="active">
                            <a href="#tab_2_1" data-toggle="tab"> التعليقات </a>
                        </li>
                        <li>
                            <a href="#tab_2_2" data-toggle="tab"> المهام </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="tab_2_1">
                                <!-- BEGIN FORM-->
                                <form method="post" action="{{url('admin/ClientsVsServices/Clients_comment')}}" class="form-horizontal" enctype="multipart/form-data">
                                    <div class="form-group">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="id" value="{{$clientvsservices[0]->id}}">

                                        <div class="col-md-12">
                                            @foreach ($comment as $comments )

                                            <div class="portlet-body">
                                                <div class="mt-timeline-2">
                                                    <ul class="mt-container">
                                                        <li class="mt-item">
                                                            <div class="mt-timeline-content">
                                                                <div class="mt-content-container ">
                                                                    <div class="mt-title">
                                                                        <h3 class="mt-content-title font-blue-madison">{{ $comments->username }}</h3>
                                                                    </div>
                                                                    <div class="mt-author">
                                                                        <div class="mt-author-name">
                                                                            <a href="javascript:;" class="font-blue-madison"></a>
                                                                        </div>
                                                                        <div class="mt-author-notes font-grey-mint">{{$comments->created_at}}</div>
                                                                    </div>
                                                                    <div class="mt-content border-grey-salt">
                                                                        <p>
                                                                            {{$comments->text}}
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="form-group">

                                        <label class="col-md-3 control-label">تعليق</label>

                                        <div class="col-md-9">

                                            <textarea rows="5" placeholder="تعليق" id="form-field-22" class="form-control" name="comment" ></textarea>



                                        </div>

                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn btn-circle green">اضافة تعليق</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <!-- END FORM-->
                        </div>
                        <div class="tab-pane fade" id="tab_2_2">
                                <!-- BEGIN FORM-->
                                <form method="post" action="{{url('admin/ClientsVsServices/Clients_tasks')}}" class="form-horizontal" enctype="multipart/form-data">
                                    <div class="form-group">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="id" value="{{$clientvsservices[0]->id}}">
                                        <div class="col-md-12">
                                                    @foreach ($tasks as $task )

                                                    <div class="portlet-body">
                                                        <div class="mt-timeline-2">
                                                            <ul class="mt-container">
                                                                <li class="mt-item">
                                                                    <div class="mt-timeline-content">
                                                                        <div class="mt-content-container ">
                                                                            <div class="mt-title">
                                                                                الي
                                                                                <h2 class="mt-content-title ">{{ $task->assigned_to}}</h3>
                                                                            </div>
                                                                            <div class="mt-author">
                                                                                <div class="mt-avatar">
                                                                                    <img src="{{url($task->logo)}}" />
                                                                                </div>
                                                                                <div class="mt-author-name">
                                                                                    <a href="javascript:;" class="font-blue-madison"></a>
                                                                                </div>
                                                                                <div class="mt-author-name">
                                                                                    من
                                                                                    <a href="javascript:;" class="font-blue-madison">{{$task->username }}</a>
                                                                                </div>
                                                                                <div class="mt-author-notes font-grey-mint">{{$task->created_at}}</div>

                                                                            </div>
                                                                            <div class="mt-content border-grey-salt">

                                                                                <h3>{{$task->title}}</h3>
                                                                                <div class="col-md-10">
                                                                                        <p>
                                                                                                {{$task->text}}
                                                                                            </p>
                                                                                </div>

                                                                                <div class="col-md-2">
                                                                                        <a href="{{url('admin/tasks/view/')}}/{{$task->id}}" class="btn btn-circle blue">Read More</a>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    @endforeach
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">تاريخ</label>
                                        <div class="col-md-3">
                                            <div class="input-group input-medium date date-picker" data-date="" data-date-format="yyyy-mm-dd" data-date-viewmode="years">
                                                <input type="text" class="form-control" name="assigned_at" value="{{ old('assigned_at') }}" required readonly >
                                                <span class="input-group-btn">
                                                    <button class="btn default" type="button">
                                                        <i class="fa fa-calendar"></i>
                                                    </button>
                                                </span>
                                            </div>
                                            <!-- /input-group -->
                                            <span class="help-block">  </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                            <label class="col-md-3 control-label">  العنوان  <span style="color: red"> *</span> </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control input-circle" id="title" name="title" placeholder="العنوان " value="" required>
                                            </div>
                                        </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"> المهمة <span style="color: red"> *</span></label>
                                        <div class="col-md-8">
                                            <textarea rows="5" placeholder="المحتوى" id="form-field-22" class="form-control" name="text" required></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group last">
                                        <label class="control-label col-md-3">الصورة</label>
                                        <div class="col-md-9">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                <div>
                                                    <span class="btn default btn-file">
                                                        <span class="fileinput-new"> Select image </span>
                                                        <span class="fileinput-exists"> Change </span>
                                                        <input type="file" name="pic"> </span>
                                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                </div>
                                            </div>
                                            <div class="clearfix margin-top-10">
                                                <span class="label label-danger"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">تابع ل <span style="color: red"> *</span></label>
                                        <div class="col-md-4">
                                            <select name="assigned_to" id="assigned_to" class="form-control  select2 input-medium" required>
                                            @foreach($user as $users)
                                                <option value="{{$users->id}}" <?=($users->id == old('assigned_to')) ? 'selected' : '' ?>>
                                                    {{$users->{'name'} }}
                                                </option>
                                            @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn btn-circle green">اضافة مهمة</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection



