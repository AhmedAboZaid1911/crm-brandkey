@extends('layouts.adminlayout')
@section('pagejs')
    <script src="{{url('/assetsAdmin/pages/scripts/form-samples.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/assetsAdmin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>

@endsection
@section('pagecss')
<link href="{{url('/assetsAdmin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="portlet box green">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-gift"></i>تعديل {{$titlepage}} </div>

                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form method="post" action="{{url('admin/')}}/{{$module}}/update" class="form-horizontal" enctype="multipart/form-data">

                                                        <div class="form-body">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="id" value="{{$page[0]->id}}">
                                                        @foreach($lang_arr as $k => $v)
                                                            <h4>{{$v}}</h4>
                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">العنوان</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control input-circle" placeholder="العنوان" name="title_{{$k}}" value="{{$page[0]->{'title_' . $k} }}" required>

                                                                    <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('title_' . $k) }}</div>
                                                                </div>
                                                            </div>






                                                          @endforeach




                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-5 col-md-7">
                                                                    <button type="submit" class="btn btn-circle green">تعديل</button>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>
                                            <script>
                                                function responsive_filemanager_callback(field_id) {

                                                    var url = {!! json_encode(url('')) !!};
                                                    var image = $('#' + field_id).val();
                                                    if(field_id == 'pic'){
                                                        $('#prev_img').attr('src',url+image);
                                                    }
                                                    var picsnum = <?=$picsnum?>;
                                                    for (var i=1; i <= picsnum; i++) {

                                                    if(field_id == 'pic'+[i]){
                                                        $('#prev_img'+[i]).attr('src',url+image);
                                                    }

                                                    }
                                                }
                                                function setremovepic() {
                                                    document.getElementById("pic").value = "remove";
                                                    document.getElementById("prev_img").setAttribute("src", "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image");
                                                }
                                                function setremove(i) {


                                                    document.getElementById("pic"+[i]).value = "remove";
                                                    document.getElementById("prev_img"+[i]).setAttribute("src", "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image");


                                                }
                                            </script>
@endsection

