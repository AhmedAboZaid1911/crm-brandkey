@extends('layouts.adminlayout')
@section('pagejs')
    <script src="{{url('/assetsAdmin/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{url('/assetsAdmin/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/assetsAdmin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>


@endsection
@section('pagecss')
        <link href="{{url('/assetsAdmin/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('/assetsAdmin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap-rtl.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light portlet-fit bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-settings font-red"></i>
                                            <span class="caption-subject font-red sbold uppercase">{{$cattitlepage}}</span>
                                        </div>
                                    </div>
                                    <div class="portlet-body">

                                        @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('13'))
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="btn-group">
                                                        <a href="{{url('/admin/Services/Services_add')}}" id="sample_editable_1_new" class="btn blue-ebonyclay " style="color:#fff;"> اضافة
                                                            <i class="fa fa-plus"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endif

                                        @if(session()->has('message'))
                                        <div class="alert alert-success">
                                        {{ session()->get('message') }}
                                        </div>
                                        @endif
                                        <form action="{{url('admin/Services/search')}}" method="POST">
                                            {{ csrf_field() }}
                                            <div class="form-group">
                                                <label class="col-md-1 control-label">البحث</label>
                                                <div class="col-md-11">
                                                    <input type="text" class="form-control input-circle-left" placeholder="البحث" id="search_" name="search" style="width: 95%;
                                                    float: right;">
                                                    <button type="submit" class="input-group-addon input-circle-right" style="padding: 14px 14px;">
                                                        <i class="fa fa-search"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                        <div class="col-md-12">
                                            <br>
                                        </div>
                                        <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                                                <thead style="background-color: #364150;color:#fff;">
                                                        <tr>
                                                    <th> م </th>
                                                    <th> الكورس </th>
                                                    <th> المكان الكورس </th>
                                                    <th> ايام الكورس </th>
                                                    <th> تكلفة الخدمة </th>
                                                    <th> العدد </th>

                                                    @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('14'))
                                                    <th> تعديل </th>
                                                    @endif
                                                    @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('15'))
                                                    <th> حذف </th>
                                                    @endif

                                                </tr>
                                            </thead>
                                            <tbody id="olddata">
                                            @foreach($Services as $k=>$Service)
                                                <tr>
                                                    <td> {{  $k+1  }} </td>
                                                    <td>
                                                        <a href="{{url('admin/Services/view/')}}/{{$Service->id}}">
                                                            {{  $Service->name  }}
                                                            {{  $Service->time  }}
                                                            {{  $Service->servicesdate  }}
                                                        </a>
                                                     </td>
                                                    <td>  {{  $Service->place  }} </td>
                                                    <td>
                                                        @if ( $Service->day1 != NULL )
                                                        {{  $Service->day1  }}
                                                        @else
                                                        {{  $Service->day1  }}
                                                        @endif

                                                        @if ( $Service->day2 != NULL )
                                                        / {{  $Service->day2  }}
                                                        @else
                                                        {{  $Service->day2  }}
                                                        @endif

                                                        @if ( $Service->day3 != NULL )
                                                        / {{  $Service->day3  }}
                                                        @else
                                                        {{  $Service->day3  }}
                                                        @endif

                                                        @if ( $Service->day4 != NULL )
                                                        / {{  $Service->day4  }}
                                                        @else
                                                        {{  $Service->day4  }}
                                                        @endif

                                                        @if ( $Service->day5 != NULL )
                                                        / {{  $Service->day5  }}
                                                        @else
                                                        {{  $Service->day5  }}
                                                        @endif

                                                        @if ( $Service->day6 != NULL )
                                                        / {{  $Service->day6  }}
                                                        @else
                                                        {{  $Service->day6  }}
                                                        @endif

                                                        @if ( $Service->day7 != NULL )
                                                        / {{  $Service->day7  }}
                                                        @else
                                                        {{  $Service->day7  }}
                                                        @endif




                                                    </td>
                                                    <td>  {{  $Service->service_price  }} -- {{  $Service->currency }} </td>
                                                    <td>  {{  $Service->num  }}</td>
                                                    @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('14'))
                                                    <td>
                                                        <a class="edit btn btn-transparent blue btn-outline btn-circle btn-sm active" href="{{url('admin/')}}/{{$module}}/Services_edit/{{$Service->id}}"> تعديل <i class="fa fa-edit"></i></a>
                                                    </td>
                                                    @endif
                                                    @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('15'))
                                                    <td>
                                                        <a class="delete btn btn-transparent red btn-outline btn-circle btn-sm active" href="{{url('admin/')}}/{{$module}}/Services_del/{{$Service->id}}" onclick="return confirm('Are you sure you want to delete this item?');"> حذف <i class="fa fa-trash-o"></i></a>
                                                    </td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-12 result" id="olddatap">
                                  <div class="col-md-12 result2">
                                            <div class="col-md-4">
                                                <div class="col-md-4">
                                                    من
                                                </div>
                                                <div class="col-md-4">
                                                    {{ ((($Services->currentPage()-1)*$Services->perPage())+1) }}
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="col-md-4">
                                                    الى
                                                </div>
                                                <div class="col-md-4">
                                                    {{ $Services->currentPage()*$Services->perPage() }}
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="col-md-4">
                                                    من اجمالى
                                                </div>
                                                <div class="col-md-4">
                                                    {{ count($countallServices) }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            {{ $Services->links() }}
                                        </div>
                                    </div>

                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>

@endsection

