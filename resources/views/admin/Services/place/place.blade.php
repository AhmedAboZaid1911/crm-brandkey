@extends('layouts.adminlayout')
@section('pagejs')
    <script src="{{url('/assetsAdmin/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{url('/assetsAdmin/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/assetsAdmin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
@endsection
@section('pagecss')
        <link href="{{url('/assetsAdmin/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('/assetsAdmin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap-rtl.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light portlet-fit bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-settings font-red"></i>
                                            <span class="caption-subject font-red sbold uppercase"> الاماكن </span>
                                        </div>

                                    </div>
                                    <div class="portlet-body">

                                        @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('33'))
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="btn-group">
                                                        <a href="{{url('/admin/Services/Services_place_add')}}" id="sample_editable_1_new" class="btn blue-ebonyclay " style="color:#fff;"> اضافة
                                                            <i class="fa fa-plus"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                        @if(session()->has('message'))
                                        <div class="alert alert-success">
                                        {{ session()->get('message') }}
                                        </div>
                                        @endif
                                        <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                                                <thead style="background-color: #364150;color:#fff;">
                                                        <tr>
                                                    <th> المكان </th>
                                                    @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('34'))
                                                    <th> تعديل </th>
                                                    @endif
                                                    @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('35'))
                                                    <th> حذف </th>
                                                    @endif

                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($Services as $Service)
                                                <tr>
                                                    <td> {{  $Service->name  }} </td>
                                                    @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('34'))
                                                    <td>
                                                        <a class="edit btn btn-transparent blue btn-outline btn-circle btn-sm active" href="{{url('admin/')}}/{{$module}}/Services_place_edit/{{$Service->id}}" >تعديل <i class="fa fa-edit"></i></a>
                                                    </td>
                                                    @endif
                                                    @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('35'))
                                                    <td>
                                                        <a class="delete btn btn-transparent red btn-outline btn-circle btn-sm active" href="{{url('admin/')}}/{{$module}}/Services_place_del/{{$Service->id}}" onclick="return confirm('Are you sure you want to delete this item?');"> حذف <i class="fa fa-trash-o"></i></a>
                                                    </td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    {{ $Services->links() }}
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>

@endsection

