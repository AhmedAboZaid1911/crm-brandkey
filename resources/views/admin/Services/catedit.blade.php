@extends('layouts.adminlayout')

@section('pagejs')

    <script src="{{url('/assetsAdmin/pages/scripts/form-samples.min.js')}}" type="text/javascript"></script>

    <script src="{{url('/assetsAdmin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>


    <script src="{{url('assetsAdmin/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/clockface/js/clockface.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/plugins/jquery.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/plugins/js.cookie.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>

    <script src="{{url('assetsAdmin/global/plugins/bootbox/bootbox.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/layouts/layout/scripts/demo.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/layouts/global/scripts/quick-sidebar.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/layouts/global/scripts/quick-nav.min.js')}}" type="text/javascript"></script>



    <script src="{{url('js/multiuploadscript.js')}}"></script>
    <script>
        $( "#paidValue" ).keyup(function() {
            var remainderValue = 0;
            remainderValue =   $('#price').val() - $(this).val();
            $('#remainderValue').val(remainderValue);
            });
    </script>
    <script>
        $(document).ready(function()
        {
            $('#clickmewow').click(function()
            {
                $('#radio1003').attr('checked', 'checked');
            });
        })
    </script>




@endsection

@section('pagecss')

<link href="{{url('/assetsAdmin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css" />

<link rel="stylesheet" type="text/css" href="{{url('css/multiuploadstyle.css')}}">

<link href="{{url('/assetsAdmin/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/assetsAdmin/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/assetsAdmin/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/assetsAdmin/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/assetsAdmin/global/plugins/clockface/css/clockface.css')}}" rel="stylesheet" type="text/css" />
@endsection



@section('content')



<div class="portlet box green">

                                                <div class="portlet-title">

                                                    <div class="caption">

                                                        <i class="fa fa-gift"></i>اضافة {{$posttitlepage}} </div>



                                                </div>

                                                <div class="portlet-body form">

                                                    <!-- BEGIN FORM-->

                                                    <form method="post" action="{{url('admin/Services/Services_update')}}" class="form-horizontal" enctype="multipart/form-data">



                                                        <div class="form-body">

                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="id" value="{{$service[0]->id}}" />


                                                            <div class="form-group">

                                                                <label class="col-md-2 control-label"> الكورس <span style="color: red"> *</span></label>

                                                                <div class="col-md-4">

                                                                    <select name="service_name" id="service_name" class="form-control select2 input-larag" required>
                                                                    @foreach($products as $product)
                                                                        <option value="{{$product->id}}" <?=($product->id == $service[0]->service_name ) ? 'selected' : '' ?>>
                                                                            {{ $product->{'name'} }}
                                                                        </option>
                                                                    @endforeach
                                                                    </select>

                                                                    <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('cid') }}</div>

                                                                </div>



                                                                <label class="col-md-2 control-label">معاد الكورس <span style="color: red"> *</span></label>

                                                                <div class="col-md-4">

                                                                    <select name="service_time" id="service_time" class="form-control select2 input-larag" required>



                                                                    @foreach($time as $times)

                                                                        <option value="{{$times->id}}" <?=($times->id == $service[0]->service_time ) ? 'selected' : '' ?>>



                                                                            {{ $times->{'name'} }}



                                                                        </option>

                                                                    @endforeach

                                                                    </select>

                                                                    <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('cid') }}</div>

                                                                </div>

                                                            </div>


                                                            <div class="form-group">

                                                                <label class="col-md-2 control-label"> فترة الكورس <span style="color: red"> *</span></label>

                                                                <div class="col-md-4">

                                                                    <div class="input-group input-larag date date-picker" data-date="" data-date-format="yyyy-mm-dd" data-date-viewmode="years">
                                                                        <input type="text" class="form-control input-larag" name="servicesdate" value="{{ $service[0]->servicesdate}}" required readonly >
                                                                        <span class="input-group-btn">
                                                                            <button class="btn default" type="button">
                                                                                <i class="fa fa-calendar"></i>
                                                                            </button>
                                                                        </span>
                                                                    </div>
                                                                    <!-- /input-group -->
                                                                    <span class="help-block">  </span>

                                                                </div>


                                                                <label class="col-md-2 control-label"> ايام الكورس <span style="color: red"> *</span></label>
                                                                <div class="col-md-4">
                                                                    <select name="service_days" id="service_days" class="form-control select2 input-larag" required>
                                                                    @foreach($days as $day)
                                                                        <option value="{{$day->id}}" <?=($day->id == $service[0]->service_days) ? 'selected' : '' ?>>

                                                                            @if ( $day->day1 != NULL )
                                                                            {{  $day->day1  }}
                                                                            @else
                                                                            {{  $day->day1  }}
                                                                            @endif
                                                                            @if ( $day->day2 != NULL )
                                                                            / {{  $day->day2  }}
                                                                            @else
                                                                            {{  $day->day2  }}
                                                                            @endif
                                                                            @if ( $day->day3 != NULL )
                                                                            / {{  $day->day3  }}
                                                                            @else
                                                                            {{  $day->day3  }}
                                                                            @endif
                                                                            @if ( $day->day4 != NULL )
                                                                            / {{  $day->day4  }}
                                                                            @else
                                                                            {{  $day->day4  }}
                                                                            @endif
                                                                            @if ( $day->day5 != NULL )
                                                                            / {{  $day->day5  }}
                                                                            @else
                                                                            {{  $day->day5  }}
                                                                            @endif
                                                                            @if ( $day->day6 != NULL )
                                                                            / {{  $day->day6  }}
                                                                            @else
                                                                            {{  $day->day6  }}
                                                                            @endif
                                                                            @if ( $day->day7 != NULL )
                                                                            / {{  $day->day7  }}
                                                                            @else
                                                                            {{  $day->day7  }}
                                                                            @endif

                                                                        </option>

                                                                    @endforeach

                                                                    </select>

                                                                    <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('cid') }}</div>

                                                                </div>

                                                            </div>

                                                            <div class="form-group">

                                                                <label class="col-md-2 control-label"> مكان الكورس <span style="color: red"> *</span></label>

                                                                <div class="col-md-4">

                                                                    <select name="service_place" id="service_place" class="form-control select2 input-larag" required>



                                                                    @foreach($places as $place)

                                                                        <option value="{{$place->id}}" <?=($place->id == $service[0]->service_place) ? 'selected' : '' ?>>



                                                                            {{ $place->{'name'} }}



                                                                        </option>

                                                                    @endforeach

                                                                    </select>

                                                                    <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('cid') }}</div>

                                                                </div>

                                                                    <label class="col-md-2 control-label"> تكلفة الخدمة <span style="color: red"> *</span></label>
                                                                    <div class="col-md-2">
                                                                        <input type="text" class="form-control input-circle" name="service_price" placeholder="السعر " value="{{ $service[0]->service_price }}" required>
                                                                    </div>
                                                                    <div class="col-md-2">

                                                                        <select name="service_currency" id="service_currency" class="form-control select2 input-larag" required>

                                                                        @foreach($currency as $currencys)

                                                                            <option value="{{$currencys->id}}" <?=($currencys->id == $service[0]->service_currency) ? 'selected' : '' ?>>



                                                                                {{ $currencys->{'currency'} }}



                                                                            </option>

                                                                        @endforeach

                                                                        </select>

                                                                        <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('cid') }}</div>

                                                                    </div>

                                                                </div>


                                                        <div class="form-actions">

                                                            <div class="row">

                                                                <div class="col-md-offset-6 col-md-6">

                                                                    <button type="submit" class="btn btn-circle green">تعديل</button>



                                                                </div>

                                                            </div>

                                                        </div>

                                                    </form>

                                                    <!-- END FORM-->

                                                </div>

                                            </div>


@endsection



