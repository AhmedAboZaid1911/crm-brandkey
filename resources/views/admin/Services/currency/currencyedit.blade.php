@extends('layouts.adminlayout')

@section('pagejs')

    <script src="{{url('/assetsAdmin/pages/scripts/form-samples.min.js')}}" type="text/javascript"></script>

    <script src="{{url('/assetsAdmin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>



@endsection

@section('pagecss')

<link href="{{url('/assetsAdmin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css" />

@endsection



@section('content')



<div class="portlet box green">

                                                <div class="portlet-title">

                                                    <div class="caption">
                                                        <i class="fa fa-gift"></i>تعديل العملة </div>
                                                    </div>

                                                <div class="portlet-body form">

                                                    <!-- BEGIN FORM-->

                                                    <form method="post" action="{{url('admin/Services/Services_currency_update')}}" class="form-horizontal" enctype="multipart/form-data">

                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="id" value="{{$places[0]->id}}">
                                                        <div class="form-body">

                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label"> العملة <span style="color: red"> *</span></label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control input-circle" placeholder="" name="currency" value="{{$places[0]->{'currency'} }}" required>
                                                                <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('currency') }}</div>
                                                            </div>
                                                        </div>

                                                        @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('44'))
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-5 col-md-7">
                                                                    <button type="submit" class="btn btn-circle green">تعديل</button>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        @endif

                                                    </form>

                                                    <!-- END FORM-->

                                                </div>

                                            </div>


@endsection



