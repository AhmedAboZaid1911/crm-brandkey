@extends('layouts.adminlayout')

@section('pagejs')

    <script src="{{url('/assetsAdmin/pages/scripts/form-samples.min.js')}}" type="text/javascript"></script>

    <script src="{{url('/assetsAdmin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>


    <script src="{{url('assetsAdmin/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/clockface/js/clockface.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>

@endsection

@section('pagecss')
<link href="{{url('/assetsAdmin/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/assetsAdmin/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/assetsAdmin/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/assetsAdmin/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/assetsAdmin/global/plugins/clockface/css/clockface.css')}}" rel="stylesheet" type="text/css" />

<link href="{{url('/assetsAdmin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css" />

@endsection



@section('content')



<div class="portlet box green">

                                                <div class="portlet-title">

                                                    <div class="caption">

                                                        <i class="fa fa-gift"></i> اضافة  فترة {{$posttitlepage}} </div>



                                                </div>

                                                <div class="portlet-body form">

                                                    <!-- BEGIN FORM-->

                                                    <form method="post" action="{{url('admin/Services/Services_period_store')}}" class="form-horizontal" enctype="multipart/form-data">



                                                        <div class="form-body">

                                                        {{ csrf_field() }}


                                                        <div class="form-group">
                                                            <label class="control-label col-md-2" style="text-align: center;">من</label>
                                                            <div class="col-md-3">
                                                                <div class="input-group input-medium date date-picker" data-date="" data-date-format="yyyy-mm-dd" data-date-viewmode="years">
                                                                    <input type="text" class="form-control" name="from" value="{{ old('from') }}" required readonly >
                                                                    <span class="input-group-btn">
                                                                        <button class="btn default" type="button">
                                                                            <i class="fa fa-calendar"></i>
                                                                        </button>
                                                                    </span>
                                                                </div>
                                                                <!-- /input-group -->
                                                                <span class="help-block">  </span>
                                                            </div>

                                                            {{--  <label class="control-label col-md-2" style="text-align: center;">الي</label>
                                                            <div class="col-md-3">
                                                                <div class="input-group input-medium date date-picker" data-date="" data-date-format="yyyy-mm-dd" data-date-viewmode="years">
                                                                    <input type="text" class="form-control" name="to" value="{{ old('to') }}" required readonly >
                                                                    <span class="input-group-btn">
                                                                        <button class="btn default" type="button">
                                                                            <i class="fa fa-calendar"></i>
                                                                        </button>
                                                                    </span>
                                                                </div>
                                                                <!-- /input-group -->
                                                                <span class="help-block">  </span>
                                                            </div>  --}}
                                                        </div>
                                                        <div class="form-actions">

                                                            <div class="row">

                                                                <div class="col-md-offset-5 col-md-7">

                                                                    <button type="submit" class="btn btn-circle green">أضف</button>



                                                                </div>

                                                            </div>

                                                        </div>

                                                    </form>

                                                    <!-- END FORM-->

                                                </div>

                                            </div>


@endsection



