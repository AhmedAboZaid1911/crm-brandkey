@extends('layouts.adminlayout')

@section('pagejs')

    <script src="{{url('/assetsAdmin/pages/scripts/form-samples.min.js')}}" type="text/javascript"></script>

    <script src="{{url('/assetsAdmin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>


    <script src="{{url('assetsAdmin/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/clockface/js/clockface.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>

@endsection

@section('pagecss')
<link href="{{url('/assetsAdmin/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/assetsAdmin/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/assetsAdmin/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/assetsAdmin/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/assetsAdmin/global/plugins/clockface/css/clockface.css')}}" rel="stylesheet" type="text/css" />

<link href="{{url('/assetsAdmin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css" />

@endsection



@section('content')



<div class="portlet box green">

                                                <div class="portlet-title">

                                                    <div class="caption">

                                                        <i class="fa fa-gift"></i> اضافة  يوم {{$posttitlepage}} </div>



                                                </div>

                                                <div class="portlet-body form">

                                                    <!-- BEGIN FORM-->

                                                    <form method="post" action="{{url('admin/Services/Services_days_store')}}" class="form-horizontal" enctype="multipart/form-data">



                                                        <div class="form-body">

                                                        {{ csrf_field() }}
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label"> يوم  <span style="color: red"> *</span></label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control input-circle" placeholder="" name="day1" value="{{ old('day1') }}" required>
                                                            </div>

                                                            <label class="col-md-2 control-label"> يوم  <span style="color: red"> *</span></label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control input-circle" placeholder="" name="day2" value="{{ old('day2') }}" >
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label"> يوم  <span style="color: red"> *</span></label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control input-circle" placeholder="" name="day3" value="{{ old('day3') }}" >
                                                            </div>

                                                            <label class="col-md-2 control-label"> يوم  <span style="color: red"> *</span></label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control input-circle" placeholder="" name="day4" value="{{ old('day4') }}" >
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label"> يوم  <span style="color: red"> *</span></label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control input-circle" placeholder="" name="day5" value="{{ old('day5') }}" >
                                                            </div>
                                                            <label class="col-md-2 control-label"> يوم  <span style="color: red"> *</span></label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control input-circle" placeholder="" name="day6" value="{{ old('day6') }}" >
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label"> يوم  <span style="color: red"> *</span></label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control input-circle" placeholder="" name="day7" value="{{ old('day7') }}" >
                                                            </div>
                                                        </div>
                                                        @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('38'))
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-5 col-md-7">
                                                                    <button type="submit" class="btn btn-circle green">أضف</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endif
                                                    </form>

                                                    <!-- END FORM-->

                                                </div>

                                            </div>


@endsection



