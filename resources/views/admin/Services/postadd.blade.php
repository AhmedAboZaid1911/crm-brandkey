@extends('layouts.adminlayout')

@section('pagejs')

    <script src="{{url('/assetsAdmin/pages/scripts/form-samples.min.js')}}" type="text/javascript"></script>

    <script src="{{url('/assetsAdmin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>

    

    <script>

        $(document).ready(function()

        {

            $subc = "<?=old('subcid')?>";

        $.getJSON("{{url('admin/subcat')}}",{subcat: $subc}, function(j)

        {

           var options = '';

            for (var i = 0; i < j.length; i++)

            {

                options += '<option value="' + j[i].id + '">' + <?php foreach($lang_arr as $k => $v){ ?> ' ( ' + j[i].title_{{$k}} + ')' + <?php } ?>   '</option>';

            }

            

            

            $("#subcid").append(options);

        }); 





                

            $("select#cid").change(function(){

                

        $.getJSON("{{url('admin/catAjax')}}",{maincat: $(this).val()}, function(j)

        {

           var options = '';

            for (var i = 0; i < j.length; i++)

            {

                options += '<option value="' + j[i].id + '">' + <?php foreach($lang_arr as $k => $v){ ?> ' ( ' + j[i].title_{{$k}} + ')' + <?php } ?>   '</option>';

            }

            

            $("#subcid").html('<option value="0">اختر القسم الفرعي</option>  ');

            $("#subcid").append(options);

        });  

    });





        });

    </script>

@endsection

@section('pagecss')

<link href="{{url('/assetsAdmin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css" />

@endsection



@section('content')

<div class="portlet box green">

                                                <div class="portlet-title">

                                                    <div class="caption">

                                                        <i class="fa fa-gift"></i>اضافة {{$posttitlepage}} </div>

                                                    

                                                </div>

                                                <div class="portlet-body form">

                                                    <!-- BEGIN FORM-->

                                                    <form method="post" action="{{url('admin/')}}/{{$module}}/post_store" class="form-horizontal" enctype="multipart/form-data">



                                                        <div class="form-body">

                                                        {{ csrf_field() }}

                                                         @foreach($lang_arr as $k => $v)

                                                            <h3>{{$v}}</h3>

                                                            <div class="form-group">

                                                                <label class="col-md-3 control-label">العنوان <span style="color: red"> *</span></label>

                                                                <div class="col-md-4">

                                                                    <input type="text" class="form-control input-circle" placeholder="العنوان" name="title_{{$k}}" value="{{ old('title_' . $k) }}" required>

                                                                    <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('title_' . $k) }}</div>

                                                                </div>

                                                            </div>

                                                            

                                                             

                                                            

                                                            

                                                            

                                                            <div class="form-group">

                                                                <label class="col-md-3 control-label">المحتوى <span style="color: red"> *</span></label>

                                                                <div class="col-md-8">

                                                                    <textarea rows="15" placeholder="المحتوى" id="form-field-22" class="form-control" name="text_{{$k}}" required><?= old('text_' . $k) ?></textarea>

                                                                    <script>

                                                                    CKEDITOR.replace( 'text_{{$k}}' );

                                                                    </script>

                                                                    <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('text_' . $k) }}</div>

                                                                </div>

                                                            </div>

                                                            

                                                            

                                                            

                                                            <div class="form-group">

                                                                <label class="col-md-3 control-label">الكلمات الدلالية</label>

                                                                <div class="col-md-4">

                                                                    <input type="text" class="form-control input-circle" name="keyword_{{$k}}" placeholder="الكلمات الدلالية" value="{{ old('keyword_' . $k) }}">



                                                                    <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('keyword_' . $k) }}</div>

                                                                </div>

                                                            </div>

                                                            

                                                            <div class="form-group">

                                                                <label class="col-md-3 control-label">الوصف</label>

                                                                <div class="col-md-4">

                                                                    <input type="text" class="form-control input-circle" name="desc_{{$k}}" placeholder="الوصف" maxlength="160" value="{{ old('desc_' . $k) }}">



                                                                    <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('desc_' . $k) }}</div>

                                                                </div>

                                                            </div>



                                                            <div class="form-group">

                                                                <label class="col-md-3 control-label">URL</label>

                                                                <div class="col-md-4">

                                                                    <input type="text" class="form-control input-circle" name="url_{{$k}}" placeholder="URL" value="{{ old('url_' . $k) }}">



                                                                </div>

                                                            </div>

                                                        

                                                            <div class="form-group">

                                                                <label class="col-md-3 control-label">السعر</label>

                                                                <div class="col-md-4">

                                                                    <input type="text" class="form-control input-circle" name="price_{{$k}}" placeholder="السعر" value="{{ old('price_' . $k) }}">



                                                                </div>

                                                            </div>

                                                            

                                                            <div class="form-group">

                                                                <label class="col-md-3 control-label"> المدة الزمنية</label>

                                                                <div class="col-md-4">

                                                                    <input type="text" class="form-control input-circle" name="duration_{{$k}}" placeholder="المدة الزمنية" value="{{ old('duration_' . $k) }}">



                                                                </div>

                                                            </div>

                                                        

                                                            

                                                            <div class="form-group">

                                                                <label class="col-md-3 control-label">نوع الجولة</label>

                                                                <div class="col-md-4">

                                                                    <input type="text" class="form-control input-circle" name="type_{{$k}}" placeholder="نوع الجولة" value="{{ old('type_' . $k) }}">



                                                                </div>

                                                            </div>

                                                        

                                                            

                                                            <div class="form-group">

                                                                <label class="col-md-3 control-label">تكرار الرحلة</label>

                                                                <div class="col-md-4">

                                                                    <input type="text" class="form-control input-circle" name="run_{{$k}}" placeholder="تكرار الرحلة" value="{{ old('run_' . $k) }}">



                                                                </div>

                                                            </div>

                                                            

                                                            <hr>

                                                            <h3>  Highlights </h3>

                                                            <div class="form-group">

                                                                <label class="col-md-3 control-label">المحتوى <span style="color: red"> *</span></label>

                                                                <div class="col-md-8">

                                                                    <textarea rows="15" placeholder="المحتوى" id="form-field-22" class="form-control" name="Highlights_{{$k}}" required><?= old('Highlights_' . $k) ?></textarea>

                                                                    <script>

                                                                    CKEDITOR.replace( 'Highlights_{{$k}}' );

                                                                    </script>

                                                                    <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('Highlights_' . $k) }}</div>

                                                                </div>

                                                            </div>

                                                            <hr>

                                                            <h3>  Inclusions </h3>

                                                            <div class="form-group">

                                                                <label class="col-md-3 control-label">المحتوى <span style="color: red"> *</span></label>

                                                                <div class="col-md-8">

                                                                    <textarea rows="15" placeholder="المحتوى" id="form-field-22" class="form-control" name="Inclusions_{{$k}}" required><?= old('Inclusions_' . $k) ?></textarea>

                                                                    <script>

                                                                    CKEDITOR.replace( 'Inclusions_{{$k}}' );

                                                                    </script>

                                                                    <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('Inclusions_' . $k) }}</div>

                                                                </div>

                                                            </div>

                                                            <hr>

                                                            <h3>  Exclusions </h3>

                                                            <div class="form-group">

                                                                <label class="col-md-3 control-label">المحتوى <span style="color: red"> *</span></label>

                                                                <div class="col-md-8">

                                                                    <textarea rows="15" placeholder="المحتوى" id="form-field-22" class="form-control" name="Exclusions_{{$k}}" required><?= old('Exclusions_' . $k) ?></textarea>

                                                                    <script>

                                                                    CKEDITOR.replace( 'Exclusions_{{$k}}' );

                                                                    </script>

                                                                    <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('Exclusions_' . $k) }}</div>

                                                                </div>

                                                            </div>

                                                            <hr>

                                                            <h3>  Pricing </h3>

                                                            <div class="form-group">

                                                                <label class="col-md-3 control-label">المحتوى <span style="color: red"> *</span></label>

                                                                <div class="col-md-8">

                                                                    <textarea rows="15" placeholder="المحتوى" id="form-field-22" class="form-control" name="Pricing_{{$k}}" required><?= old('Pricing_' . $k) ?></textarea>

                                                                    <script>

                                                                    CKEDITOR.replace( 'Pricing_{{$k}}' );

                                                                    </script>

                                                                    <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('Pricing_' . $k) }}</div>

                                                                </div>

                                                            </div>

                                                            <hr>

                                                            <h3>  Description </h3>

                                                            <div class="form-group">

                                                                <label class="col-md-3 control-label">المحتوى <span style="color: red"> *</span></label>

                                                                <div class="col-md-8">

                                                                    <textarea rows="15" placeholder="المحتوى" id="form-field-22" class="form-control" name="Description_{{$k}}" required><?= old('Description_' . $k) ?></textarea>

                                                                    <script>

                                                                    CKEDITOR.replace( 'Description_{{$k}}' );

                                                                    </script>

                                                                    <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('Description_' . $k) }}</div>

                                                                </div>

                                                            </div>

                                                            





                                                            <hr>

                                                            <h3>ايام الرحلة</h3>

                                                            @for($i=1;$i<= $day;$i++)

                                                            

                                                            <div class="form-group">

                                                                <label class="col-md-3 control-label">اليوم {{$i}} </label>

                                                                <div class="col-md-4">

                                                                    <input type="text" class="form-control input-circle" placeholder="اليوم{{$i}}" name="daytitle{{$i}}_{{$k}}" value="{{ old('daytitle'.$i.'_'.$k) }}" >

                                                                    <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('daytitle'.$i.'_'.$k) }}</div>

                                                                </div>

                                                            </div>

                                                            

                                                             

                                                            

                                                            

                                                            

                                                            <div class="form-group">

                                                                <label class="col-md-3 control-label">المحتوى اليوم {{$i}} </label>

                                                                <div class="col-md-8">

                                                                    <textarea rows="15" placeholder="المحتوى اليوم{{$i}} " id="form-field-22" class="form-control" name="daycontent{{$i}}_{{$k}}" ><?= old('daycontent'.$i.'_'.$k) ?></textarea>

                                                                    <script>

                                                                    CKEDITOR.replace( 'daycontent{{$i}}_{{$k}}' );

                                                                    </script>

                                                                    <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('daycontent'.$i.'_'.$k) }}</div>

                                                                </div>

                                                            </div>

                                                            

                                                            @endfor

                                                            

















                                                            

                                                            @endforeach

                                                            

                                                            <hr>

                                                            <h3>عام</h3>

                                                            <div class="form-group">

                                                                <label class="col-md-3 control-label">القسم الرئيسي <span style="color: red"> *</span></label>

                                                                <div class="col-md-4">

                                                                    <select name="cid" id="cid" class="form-control input-medium" required>

                                                                        <option value="">اختر</option>

                                                                    @foreach($cats as $cat)

                                                                        <option value="{{$cat->id}}" <?=($cat->id == old('cid')) ? 'selected' : '' ?>>

                                                                            @foreach($lang_arr as $k => $v)

                                                                            {{' ( ' . $cat->{'title_' . $k} . ' ) ' }}

                                                                            @endforeach

                                                                        </option>

                                                                    @endforeach

                                                                    </select>

                                                                    <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('cid') }}</div>

                                                                </div>

                                                            </div>

د


                                                            <div class="form-group">

                                                                <label class="col-md-3 control-label"> الأماكن </label>

                                                                <div class="col-md-4">

                                                            @foreach($destinations as $destination)

                                                            <input type="checkbox" name="destinations[]" value="{{$destination->id}}"> 

                                                            {{ $destination->{'title_en'} }}

                                                            - {{ $destination->{'title_ar'} }}

                                                            <br>

                                                            @endforeach

                                                                </div>

                                                            </div>

                                                            



                                                            

                                                                
                                                                

                                                            </div>

                                                            <hr>

                                                            <h3>الصور</h3>



                                                            

                                                                

<div class="form-group last">

<label class="control-label col-md-3">الصورة</label>

<div class="col-md-9">

    

    <div class="fileinput fileinput-new" data-provides="fileinput" style="margin-bottom: 15px">

        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">

            @if(old('pic') == "")

            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" id="prev_img" /> 

            @else

            <img src="{{url(old('pic'))}}" alt="" id="prev_img" />

            @endif

        </div>

        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> 

        </div>

        <div>

            <span class="btn default btn-file">

            <span class="fileinput-new"> <a data-toggle="modal" data-target="#thepic" class="btn" type="button">Select image </a></span>

            <span class="fileinput-exists"> Change </span>

             </span>

            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>

        </div>

        <br>

        <input id="pic" type="hidden" value="{{old('pic')}}" name="pic">

    </div>



</div>

</div>

                                                            

      



<div class="modal fade" id="thepic">

<div class="modal-dialog" >

  <div class="modal-content" style="width: 900px;height: 500px">

    <div class="modal-header">

      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

      <h4 class="modal-title">اختر صورة</h4>

    </div>

    <div class="modal-body" style="width: 900px;height: 450px">

      <iframe width="100%" height="100%" src="{{url('/filemanager/dialog.php?type=2&field_id=pic&fldr=')}}" frameborder="0"></iframe>

    </div>

  </div><!-- /.modal-content -->

</div><!-- /.modal-dialog -->

</div>

                                                            @foreach($lang_arr as $k => $v)

                                                            <div class="form-group">

                                                                <label class="col-md-3 control-label">وصف الصورة ب{{$v}}</label>

                                                                <div class="col-md-4">

                                                                    <input type="text" class="form-control input-circle" placeholder="وصف الصورة" name="alt_{{$k}}" value="{{ old('alt_' . $k) }}">

                                                                    <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('alt_' . $k) }}</div> 

                                                                </div>

                                                            </div>

                                                            @endforeach

                                                            

                                                            @for($i=1;$i<= $picsnum;$i++)

                                                            <div class="form-group last">

                                                            <label class="control-label col-md-3">الصورة {{$i}}</label>

                                                            <div class="col-md-9">

                                                                

                                                                <div class="fileinput fileinput-new" data-provides="fileinput" style="margin-bottom: 15px">

                                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">

                                                                        @if(old('pic' . $i) == "")

                                                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" id="prev_img{{$i}}" /> 

                                                                        @else

                                                                        <img src="{{url(old('pic' . $i))}}" alt="" id="prev_img{{$i}}" />

                                                                        @endif 

                                                                    </div>

                                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> 

                                                                    </div>

                                                                    <div>

                                                                        <span class="btn default btn-file">

                                                                        <span class="fileinput-new"> <a data-toggle="modal" data-target="#thepic{{$i}}" class="btn" type="button">Select image </a></span>

                                                                        <span class="fileinput-exists"> Change </span>

                                                                         </span>

                                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>

                                                                    </div>

                                                                    <br>

                                                                    <input id="pic{{$i}}" type="hidden" value="{{old('pic' . $i)}}" name="pic{{$i}}">

                                                                </div>

                                                               

                                                            </div>

                                                            </div>

                                                                                                                        

                                                                  



                                                            <div class="modal fade" id="thepic{{$i}}">

                                                            <div class="modal-dialog" >

                                                              <div class="modal-content" style="width: 900px;height: 500px">

                                                                <div class="modal-header">

                                                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                                                                  <h4 class="modal-title">اختر صورة</h4>

                                                                </div>

                                                                <div class="modal-body" style="width: 900px;height: 450px">

                                                                  <iframe width="100%" height="100%" src="{{url('/filemanager/dialog.php?type=2&field_id=pic')}}{{$i}}&fldr=" frameborder="0"></iframe>

                                                                </div>

                                                              </div><!-- /.modal-content -->

                                                            </div><!-- /.modal-dialog -->

                                                            </div>



                                                            @foreach($lang_arr as $k => $v)

                                                            <div class="form-group">

                                                                <label class="col-md-3 control-label">وصف صورة {{$i}} ب{{$v}}</label>

                                                                <div class="col-md-4">

                                                                    <input type="text" class="form-control input-circle" name="pic{{$i}}alt_{{$k}}" placeholder="وصف الصورة {{$i}}" value="{{ old('pic' . $i . 'alt_' . $k) }}">

                                                                    

                                                                </div>

                                                            </div>

                                                            @endforeach

                                                            

                                                          @endfor



                                                            

                                                            <!--  -->

                                                          

                                                        <div class="form-actions">

                                                            <div class="row">

                                                                <div class="col-md-offset-3 col-md-9">

                                                                    <button type="submit" class="btn btn-circle green">أضف</button>

                                                                    

                                                                </div>

                                                            </div>

                                                        </div>

                                                    </form>

                                                    <!-- END FORM-->

                                                </div>

                                            </div>

                                            <script>

                                                function responsive_filemanager_callback(field_id) {

                                                    var url = {!! json_encode(url('')) !!};

                                                    var image = $('#' + field_id).val();

                                                    if(field_id == 'pic'){

                                                        $('#prev_img').attr('src',url+image);

                                                    }

                                                    

                                                    var picsnum = <?=$picsnum?>;

                                                    for (var i=1; i <= picsnum; i++) {



                                                        if(field_id == 'pic'+[i]){

                                                            $('#prev_img'+[i]).attr('src',url+image);

                                                        }



                                                    }

                                                }

                                            </script>

@endsection



