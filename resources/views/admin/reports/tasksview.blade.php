@extends('layouts.adminlayout')

@section('pagejs')

    <script src="{{url('/assetsAdmin/pages/scripts/form-samples.min.js')}}" type="text/javascript"></script>

    <script src="{{url('/assetsAdmin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>

    <script src="{{url('assetsAdmin/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/clockface/js/clockface.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>

    <script src="{{url('assetsAdmin/plugins/jquery.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/plugins/js.cookie.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>

    <script src="{{url('assetsAdmin/global/plugins/bootbox/bootbox.min.js')}}" type="text/javascript"></script>

    <script src="{{url('assetsAdmin/layouts/layout/scripts/layout.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/layouts/layout/scripts/demo.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/layouts/global/scripts/quick-sidebar.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/layouts/global/scripts/quick-nav.min.js')}}" type="text/javascript"></script>

    <script src="{{url('js/multiuploadscript.js')}}"></script>
    <script>
            $( "#paidValue" ).keyup(function() {
                var remainderValue = 0;
                remainderValue =   $('#price').val() - $(this).val();
                $('#remainderValue').val(remainderValue);
                });
    </script>
    <script>
            $(document).ready(function()
            {
                $('#clickmewow').click(function()
                {
                    $('#radio1003').attr('checked', 'checked');
                });
            })
    </script>
@endsection

@section('pagecss')


    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="{{url('css/multiuploadstyle.css')}}">

    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/clockface/css/clockface.css')}}" rel="stylesheet" type="text/css" />


@endsection



@section('content')

    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i> تفاصيلل المهمة
            </div>
        </div>
        <div class="portlet-body form">

                @if(session()->has('message'))
                <div class="alert alert-success">
                {{ session()->get('message') }}
                </div>
                @endif
                <!-- BEGIN FORM-->
            <form class="form-horizontal" >

                <div class="form-body">
                <h2> بيانات العميل</h2>
                <div class="form-group">
                    <label class="col-md-3 control-label">الاسم بالكامل </label>
                    <div class="col-md-4">
                        <p>{{ $clients[0]->name }}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">الاسم بالكامل بالانجليزية</label>
                    <div class="col-md-4">
                        <p>{{ $clients[0]->name_en }}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label"> E-mail </label>
                    <div class="col-md-4">
                        <p>{{ $clients[0]->email }}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label"> رقم التليفون </label>
                    <div class="col-md-4">
                        <p>{{ $clients[0]->number }}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">رقم التليفون الثاني</label>
                    <div class="col-md-4">
                        <p >{{ $clients[0]->number2 }}</p>
                    </div>
                </div>

                @if ($tasks[0]->type == 'clientvsservices')
                <hr>
                <h2> الخدمة </h2>
                <div class="form-group">
                    <div class="col-md-12">
                        <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                            <thead>
                                <tr>

                                    <th> اسم الخدمة </th>
                                    <th> معاد  </th>
                                    <th> فترة </th>
                                    <th> مكان </th>
                                    <th> ايام الكورس </th>
                                    <th> تكلفة الخدمة </th>
                                    <th> السعر </th>

                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td> {{ $clientvsservices[0]->name }} </td>
                                    <td> {{$clientvsservices[0]->time}} </td>
                                    <td> {{$clientvsservices[0]->servicesdate}}  </td>
                                    <td>  {{$clientvsservices[0]->place}} </td>
                                    <td>   {{  $clientvsservices[0]->day1  }}
                                        -- {{  $clientvsservices[0]->day2  }}
                                        -- {{  $clientvsservices[0]->day3  }}
                                        -- {{  $clientvsservices[0]->day4  }}
                                        -- {{  $clientvsservices[0]->day5  }}
                                        -- {{  $clientvsservices[0]->day6  }}
                                        -- {{  $clientvsservices[0]->day7  }}
                                    </td>
                                    <td>  {{ $clientvsservices[0]->price  }} -- {{  $clientvsservices[0]->currency   }} </td>
                                    <td>  {{ $clientvsservices[0]->price1 }} -- {{  $clientvsservices[0]->currency1  }} </td>

                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                @endif
            </form>
            <hr>
            <h2>  المهام    </h2>
            <!-- BEGIN FORM-->

            <form  class="form-horizontal" method="post" action="{{url('admin/ClientsVsServices/Clients_tasks_update')}}" enctype="multipart/form-data">
                <div class="form-group">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{$tasks[0]->id}}">
                    <label class="col-md-3 control-label">المهام</label>

                    <div class="col-md-12">
                        <div class="portlet-body">
                            <div class="mt-timeline-2">
                                <ul class="mt-container">
                                    <li class="mt-item">
                                        <div class="mt-timeline-content">
                                            <div class="mt-content-container ">
                                                <div class="mt-title">
                                                    <h2 class="mt-content-title ">{{ $tasks[0]->assigned_to}}</h3>
                                                    <div class="mt-content-title " style="font-size:15px;">{{$tasks[0]->assigned_at}}</div>
                                                </div>
                                                <div class="mt-author">
                                                    <div class="mt-avatar">
                                                        <img src="{{url($tasks[0]->logo)}}" />
                                                    </div>
                                                    <div class="mt-author-name">
                                                        <a href="javascript:;" class="font-blue-madison"></a>
                                                    </div>
                                                    <div class="mt-author-name">
                                                        من
                                                        <a href="javascript:;" class="font-blue-madison">{{$tasks[0]->username }}</a>
                                                    </div>
                                                    <div class="mt-author-notes font-grey-mint">{{$tasks[0]->created_at}}</div>

                                                </div>
                                                <div class="mt-content border-grey-salt" style="text-align:center;">
                                                    <a href="{{url('')}}/{{$tasks[0]->pic}}" target="_blank" style="padding-down:5px;">
                                                    <img class="timeline-body-img "  style="width:50%;Margin:1%;" src="{{url($tasks[0]->pic)}}" alt="{{$tasks[0]->title}}">
                                                    </a>
                                                </div>
                                                <div class="mt-content border-grey-salt" style="text-align:center;">

                                                    <h3 style="color:red;" style="text-align:center;">{{$tasks[0]->title}}</h3>
                                                    <p>
                                                        {{$tasks[0]->text}}
                                                    </p>

                                                </div>
                                                @if($tasks[0]->status == '3')
                                                <div class="mt-content border-grey-salt" style="text-align:center;">

                                                    <p>
                                                        <span aria-hidden="true" class="icon-like"></span>
                                                        <span class="glyphicon glyphicon-ok"> </span>
                                                        تم أغلاق المهمة
                                                    </p>
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    @if($tasks[0]->status == '1')
                    <div class="col-md-3">
                        <select name="case" id="case" class="form-control input-medium" required>
                            <option value="3" > تمت</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <button type="submit" class="btn btn-circle green">تعديل</button>
                    </div>
                    @endif
                </div>
            </form>
            <!-- END FORM-->
        </div>
        </div>
    </div>


@endsection



