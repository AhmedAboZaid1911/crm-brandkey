@extends('layouts.adminlayout')
@section('pagejs')
    <script src="{{url('/assetsAdmin/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{url('/assetsAdmin/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/assetsAdmin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>

    <script src="{{url('assetsAdmin/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/clockface/js/clockface.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>
@endsection
@section('pagecss')

    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/clockface/css/clockface.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{url('/assetsAdmin/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap-rtl.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light portlet-fit bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-settings font-red"></i>
                                            <span class="caption-subject font-red sbold uppercase"> الخدمات </span>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <form method="post" action="{{url('admin/reports/service/service/search')}}" class="form-horizontal" enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        <div class="col-md-12">
                                                                <div class="col-md-6">
                                                                    <div class="col-md-4">
                                                                        <label class="col-md-12 control-label" >
                                                                            من
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-md-8">
                                                                        <div class="input-group input-medium date date-picker" data-date="" data-date-format="yyyy-mm-dd" data-date-viewmode="years" >
                                                                            <input type="text" class="form-control" name="from" value="" required="required" readonly >
                                                                            <span class="input-group-btn">
                                                                                <button class="btn default" type="button" required="required">
                                                                                    <i class="fa fa-calendar"></i>
                                                                                </button>
                                                                            </span>
                                                                        </div>
                                                                        <!-- /input-group -->
                                                                        <span class="help-block">  </span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="col-md-4">
                                                                        <label class="col-md-12 control-label" >
                                                                            الي
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-md-8">
                                                                        <div class="input-group input-medium date date-picker" data-date="" data-date-format="yyyy-mm-dd" data-date-viewmode="years">
                                                                            <input type="text" class="form-control" name="to" value=""  readonly >
                                                                            <span class="input-group-btn">
                                                                                <button class="btn default" type="button">
                                                                                    <i class="fa fa-calendar"></i>
                                                                                </button>
                                                                            </span>
                                                                        </div>
                                                                        <!-- /input-group -->
                                                                        <span class="help-block">  </span>
                                                                    </div>
                                                                </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="col-md-2">
                                                                <label class="col-md-12 control-label" >
                                                                    اسم الخدمة
                                                                </label>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <select name="service_name" id="service_name" class="form-control  select2" required>

                                                                @foreach ($Services as $Service )
                                                                <option value="{{$Service->id}}" >
                                                                        {{$Service->name }}
                                                                </option>
                                                                @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <label class="control-label" >
                                                                    تابع ل
                                                                </label>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <select name="user" id="user" class="form-control select2 input-medium">
                                                                    <option value=" " > الكل</option>
                                                                    @foreach ($users as $user )
                                                                        <option value="{{$user->id}}" > {{$user->name}} </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="col-md-2">
                                                                <label class="col-md-12 control-label" >
                                                                    الجنسية
                                                                </label>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <select name="nationality" id="nationality" class="form-control select2 input-medium">
                                                                    <option value=" ">
                                                                        الكل
                                                                    </option>
                                                                    @foreach ($nationality as $nationalitys )
                                                                        <option value="{{ $nationalitys->id }}">
                                                                        {{ $nationalitys->{'title_en'} }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <label class="col-md-12 control-label" >
                                                                    النوع
                                                                </label>
                                                            </div>
                                                            <div class="col-md-4">

                                                                <select name="gender" id="gender" class="form-control  select2 input-medium" required>
                                                                    <option value=" ">
                                                                        الكل
                                                                    </option>
                                                                    @foreach ($gender as $gend )
                                                                        <option value="{{ $gend->id }}">
                                                                        {{ $gend->{'type'} }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="col-md-2">
                                                                <label class="col-md-2 control-label">  الحالة   </label>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <select name="servicesstatus" id="servicesstatus" class="form-control  select2 input-lager" required>
                                                                    <option value=" ">
                                                                        الكل
                                                                    </option>
                                                                    <option value="1" >
                                                                        حجز
                                                                    </option>
                                                                    <option value="2" >
                                                                        اتصال
                                                                    </option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-4 offset-md-2">
                                                                <button type="submit" class="btn btn-circle green">بحث</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        @if ($message != '')
                                        <div class="alert alert-success">
                                            {!!$message!!}
                                        </div>
                                        @endif
                                        @if($_POST)
                                        <form method="post" action="{{ route('reports_service_search.excel') }}" class="form-horizontal">
                                                {{ csrf_field() }}

                                            <input type="hidden" name="service_name" value="{{$service_name}}">
                                            <input type="hidden" name="nationalitys" value="{{$_POST['nationality']}}">

                                            <input type="hidden" name="genders" value="{{$_POST['gender']}}">
                                            <input type="hidden" name="user" value="{{$_POST['user'] }}">
                                            <input type="hidden" name="from" value="{{$from}}">
                                            <input type="hidden" name="to" value="{{$to}}">

                                            @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('81'))
                                            <div align="center">
                                                    <button type="submit"  class="btn btn-circle green-jungle">Export to Excel <img src="{{url('/images/excel.png')}}" width="24" height="24"/> </button>
                                                </div>
                                            @endif
                                        </form>
                                        @endif
                                        <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                                                <thead style="background-color: #364150;color:#fff;">
                                                        <tr>
                                                    <th>م</th>
                                                    <th> اسم العميل </th>
                                                    <th> اسم العميل بالانجليزية</th>
                                                    <th> رقم العميل </th>
                                                    <th> الايمال </th>
                                                    <th> تابع ل </th>
                                                    <th> تعديل الخدمات </th>
                                                </tr>
                                            </thead>
                                            @if($_POST && $finalclients != '')
                                            <tbody>
                                            @foreach($finalclients as $k=>$finalclient)
                                                <tr>
                                                    <td>{{$k+1}}</td>

                                                    <td> {{  $finalclient->name}}  </td>
                                                    <td> {{  $finalclient->name_en}}  </td>
                                                    <td> {{  $finalclient->number }} </td>
                                                    <td> {{  $finalclient->email }} </td>
                                                    <td> {{  $finalclient->assigned_to  }} </td>
                                                    <td>
                                                        <a class="edit btn btn-transparent green-jungle btn-outline btn-circle btn-sm active" href="{{url('admin/ClientsVsServices/service')}}/{{$finalclient->id}}"> تعديل <i class="fa fa-edit"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                            @endif
                                        </table>
                                    </div>

                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>

@endsection

