@extends('layouts.adminlayout')
@section('pagejs')
    <script src="{{url('/assetsAdmin/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{url('/assetsAdmin/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/assetsAdmin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>

    <script src="{{url('assetsAdmin/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/clockface/js/clockface.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>
@endsection
@section('pagecss')

    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/clockface/css/clockface.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{url('/assetsAdmin/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap-rtl.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light portlet-fit bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-settings font-red"></i>
                        <span class="caption-subject font-red sbold uppercase"> المستخدمين </span>
                    </div>

                </div>
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <form method="post" action="{{url('admin/reports/users/search')}}" class="form-horizontal" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="col-md-12">
                                        <div class="col-md-4">
                                                <div class="col-md-3">
                                                        <label class="col-md-12 control-label" >
                                                            من
                                                        </label>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <div class="input-group input-medium date date-picker" data-date="" data-date-format="yyyy-mm-dd" data-date-viewmode="years">
                                                            <input type="text" class="form-control" name="from" value="" required="required" readonly >
                                                            <span class="input-group-btn">
                                                                <button class="btn default" type="button" required="required">
                                                                    <i class="fa fa-calendar"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                        <!-- /input-group -->
                                                        <span class="help-block">  </span>
                                                    </div>
                                            </div>
                                            <div class="col-md-4">

                                    <div class="col-md-3">
                                        <label class="col-md-12 control-label" >
                                            الي
                                        </label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="input-group input-medium date date-picker" data-date="" data-date-format="yyyy-mm-dd" data-date-viewmode="years">
                                            <input type="text" class="form-control" name="to" value=""  readonly >
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <!-- /input-group -->
                                        <span class="help-block">  </span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="col-md-5">
                                        <label class=" control-label" >
                                            تابع ل
                                        </label>
                                    </div>
                                    <div class="col-md-7">
                                        <select name="user" id="user" class="form-control select2 input-larag" required>

                                            <option value="all" > الكل</option>

                                            @foreach ($users as $user )
                                                <option value="{{$user->id}}" > {{$user->name}} </option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>
                                </div>
                                <div class="col-md-3 col-md-offset-5">
                                <br><br>
                                    <div class="col-md-3">
                                        <button type="submit" class="btn btn-circle green">بحث</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    @if ($message != '')
                    <div class="alert alert-success">
                        {!!$message!!}
                    </div>
                    @endif
                    @if($_POST)
                    <form method="post" action="{{ route('reports_users_search.excel') }}" class="form-horizontal">
                            {{ csrf_field() }}
                        <input type="hidden" name="from" value="{{$from}}">
                        <input type="hidden" name="to" value="{{$to}}">
                        <input type="hidden" name="assigned_to" value="{{$assigned_to}}">
                        <input type="hidden" name="case" value="{{$case}}">

                        @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('77'))

                        @endif
                    </form>
                    @endif
                    <div class="col-md-12 ">
                        <div class="portlet light bordered">
                            <div class="portlet-body">
                                <ul class="nav nav-pills">
                                    <li class="active">
                                        <a href="#tab_2_1" data-toggle="tab"> العملاء </a>
                                    </li>
                                    <li>
                                        <a href="#tab_2_2" data-toggle="tab"> الخدمات </a>
                                    </li>
                                    <li>
                                        <a href="#tab_2_3" data-toggle="tab"> المهام </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="tab_2_1">
                                        <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                                                <thead style="background-color: #364150;color:#fff;">
                                                        <tr>
                                                    <th>م</th>
                                                    <th>اسم العميل</th>
                                                    <th> اسم العميل بالانجليزية</th>
                                                    <th> رقم العميل </th>
                                                    <th> الايمال </th>
                                                    <th> النوع </th>
                                                    <th> المصدر </th>
                                                    <th> الجنسية </th>
                                                    <th> الاهتمام </th>
                                                    <th> انشاء بواسطة </th>
                                                    <th> تابع ل </th>
                                                    <th>تاريخ الاضافة </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($clients as $k=>$client)
                                                <tr>
                                                    <td>{{$k+1}}</td>
                                                    <td> {{  $client->name}}  </td>
                                                    <td> {{  $client->name_en}}  </td>
                                                    <td> {{  $client->number }} </td>
                                                    <td> {{  $client->email }} </td>
                                                    <td> {{  $client->gender }} </td>
                                                    <td> {{  $client->lead }} </td>
                                                    <td> {{  $client->nationality }} </td>
                                                    <td> {{  $client->Interest  }} </td>
                                                    <td> {{  $client->created_by  }} </td>
                                                    <td> {{  $client->assigned_to  }} </td>
                                                    <td> {{  $client->created_at  }} </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="tab-pane fade" id="tab_2_2">
                                        <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                                                <thead style="background-color: #364150;color:#fff;">
                                                        <tr>
                                                                            <th>م</th>
                                                                            <th> اسم العميل </th>
                                                                            <th> رقم العميل </th>
                                                                            <th> الايمال </th>
                                                                            <th> انشاء بواسطة </th>
                                                                            <th> تعديل الخدمات </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    @foreach($clientvsservices as $k=>$clientvsservice)
                                                                        <tr>
                                                                            <td>{{$k+1}}</td>
                                                                            <td> {{  $clientvsservice->name}}  </td>
                                                                            <td> {{  $clientvsservice->number }} </td>
                                                                            <td> {{  $clientvsservice->email }} </td>
                                                                            <td> {{  $clientvsservice->created_by }} </td>
                                                                            <td>
                                                                                <a class="edit btn btn-transparent yellow btn-outline btn-circle btn-sm active" href="{{url('admin/ClientsVsServices/service')}}/{{$clientvsservice->id}}"> تعديل <i class="fa fa-edit"></i></a>
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                    </tbody>
                                        </table>
                                    </div>
                                    <div class="tab-pane fade" id="tab_2_3">
                                        <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                                                <thead style="background-color: #364150;color:#fff;">
                                                        <tr>
                                                                            <th>م</th>
                                                                            <th>
                                                                                اسم العميل / الخدمة
                                                                            </th>
                                                                            <th>الرقم</th>
                                                                            <th>المهمة </th>
                                                                            <th>التاريخ </th>
                                                                            <th> انشاء بواسطة </th>
                                                                            <th>تابع ل </th>
                                                                            <th> تفاصيل </th>
                                                                            <th> حذف </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    @foreach($tasks as $k=>$task)
                                                                        @if($task->status == '3')
                                                                        <tr class="bg-green-jungle ">
                                                                        @elseif($task->assigned_at < date('d-m-Y') )
                                                                        <tr class="bg-red-pink">
                                                                        @else
                                                                        <tr class="warning">
                                                                        @endif
                                                                            <td>{{$k+1}}</td>
                                                                            <td>
                                                                            @if($task->type =='clientvsservices')
                                                                                 {{$task->service}} / {{$task->client}}
                                                                            @endif
                                                                            @if($task->type =='client')
                                                                            {{$task->client}}
                                                                            @endif
                                                                            @if($task->type =='general')
                                                                                عام
                                                                            @endif
                                                                            </td>
                                                                            @if($task->type =='general')
                                                                            <td> عام  </td>
                                                                            @else
                                                                            <td> {{  $task->clientnum  }}  </td>
                                                                            @endif
                                                                            <td> {{  $task->title  }}  </td>
                                                                            <td> {{  $task->assigned_at  }}  </td>
                                                                            <td> {{  $task->created_by }}  </td>
                                                                            <td> {{  $task->assigned_to  }}  </td>
                                                                            <td>
                                                                                <a class="edit  btn btn-circle blue-soft active" href="{{url('admin/tasks/edit')}}/{{$task->id}}" > تفاصيل <i class="fa fa-edit"></i></a>
                                                                            </td>
                                                                            <td>
                                                                                <a class="delete btn btn-transparent red btn-outline btn-circle btn-sm active" href="{{url('admin/tasks/del')}}/{{$task->id}}" onclick="return confirm('Are you sure you want to delete this item?');"> حذف <i class="fa fa-trash-o"></i></a>
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                    </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

@endsection

