@extends('layouts.adminlayout')
@section('pagejs')
    <script src="{{url('/assetsAdmin/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{url('/assetsAdmin/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/assetsAdmin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>

    <script src="{{url('assetsAdmin/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/clockface/js/clockface.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>
@endsection
@section('pagecss')

    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/clockface/css/clockface.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{url('/assetsAdmin/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap-rtl.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light portlet-fit bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-settings font-red"></i>
                                            <span class="caption-subject font-red sbold uppercase"> العملاء </span>
                                        </div>

                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <form method="post" action="{{url('admin/reports/client/search')}}" class="form-horizontal" enctype="multipart/form-data">
                                                        {{ csrf_field() }}

                                                            <div class="col-md-6">
                                                                <div class="col-md-4">
                                                                        <label class="col-md-12 control-label" >
                                                                            من
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-md-8">
                                                                        <div class="input-group input-medium date date-picker" data-date="" data-date-format="yyyy-mm-dd" data-date-viewmode="years" >
                                                                            <input type="text" class="form-control" name="from" value="" required="required" readonly >
                                                                            <span class="input-group-btn">
                                                                                <button class="btn default" type="button" required="required">
                                                                                    <i class="fa fa-calendar"></i>
                                                                                </button>
                                                                            </span>
                                                                        </div>
                                                                        <!-- /input-group -->
                                                                        <span class="help-block">  </span>
                                                                    </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="col-md-4">
                                                                    <label class="col-md-12 control-label" >
                                                                        الي
                                                                    </label>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <div class="input-group input-medium date date-picker" data-date="" data-date-format="yyyy-mm-dd" data-date-viewmode="years">
                                                                        <input type="text" class="form-control" name="to" value=""  readonly >
                                                                        <span class="input-group-btn">
                                                                            <button class="btn default" type="button">
                                                                                <i class="fa fa-calendar"></i>
                                                                            </button>
                                                                        </span>
                                                                    </div>
                                                                    <!-- /input-group -->
                                                                    <span class="help-block">  </span>
                                                                </div>

                                                            </div>


                                                            <div class="col-md-6">
                                                                <div class="col-md-4">
                                                                    <label class="col-md-3 control-label" >
                                                                        النوع
                                                                    </label>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <select name="gender" id="gender" class="form-control  select2 input-medium" required>
                                                                        <option value="all">
                                                                            الكل
                                                                        </option>
                                                                        @foreach ($gender as $gend )
                                                                            <option value="{{ $gend->id }}">
                                                                            {{ $gend->{'type'} }}
                                                                            </option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="col-md-4">
                                                                    <label class="col-md-3 control-label" >
                                                                        الجنسية
                                                                    </label>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <select name="nationality" id="nationality" class="form-control select2 input-medium" required>
                                                                        <option value="all">
                                                                            الكل
                                                                        </option>
                                                                        @foreach ($nationality as $nationalitys )
                                                                            <option value="{{ $nationalitys->id }}">
                                                                            {{ $nationalitys->{'title_en'} }}
                                                                            </option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>


                                                            <div class="col-md-6">
                                                                <div class="col-md-4">
                                                                    <label class="control-label" >
                                                                        درجة الاهتمام
                                                                    </label>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <select name="Interest" id="Interest" class="form-control select2 input-medium" required>
                                                                        <option value="all">
                                                                            الكل
                                                                        </option>
                                                                        @foreach ($Interest as $Interests )
                                                                            <option value="{{ $Interests->id }}">
                                                                                {{ $Interests->{'type'} }}
                                                                            </option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="col-md-4">
                                                                    <label class="control-label" >
                                                                        المصدر
                                                                    </label>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <select name="lead" id="lead" class="form-control select2 input-medium" required>
                                                                        <option value="all">
                                                                            الكل
                                                                        </option>
                                                                        @foreach ($lead as $leads )
                                                                            <option value="{{ $leads->id }}">
                                                                                {{ $leads->{'lead'} }}
                                                                            </option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <div class="col-md-4">
                                                                    <label class="control-label" >
                                                                        تابع ل
                                                                    </label>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <select name="user" id="user" class="form-control select2 input-medium" required>
                                                                        <option value="all" > الكل</option>
                                                                        @foreach ($users as $user )
                                                                            <option value="{{$user->id}}" > {{$user->name}} </option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                    <div class="col-md-6 col-md-offset-6">
                                                                            <button type="submit" class="btn btn-circle green">بحث</button>
                                                                        </div>
                                                            </div>

                                                        </div>



                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        @if ($message != '')
                                        <div class="alert alert-success">
                                            {!!$message!!}
                                        </div>
                                        @endif
                                        @if($_POST)
                                        <form method="post" action="{{ route('reports_client_search.excel') }}" class="form-horizontal">
                                                {{ csrf_field() }}
                                            <input type="hidden" name="from" value="{{$from}}">
                                            <input type="hidden" name="to" value="{{$to}}">
                                            <input type="hidden" name="gender" value="{{$genders}}">
                                            <input type="hidden" name="nationality" value="{{$_POST['nationality']}}">
                                            <input type="hidden" name="Interest" value="{{$_POST['Interest']}}">
                                            <input type="hidden" name="lead" value="{{$_POST['lead']}}">

                                            <input type="hidden" name="assigned_to" value="{{$assigned_to}}">

                                            @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('75'))
                                            <div align="center">
                                                <button type="submit"  class="btn btn-circle green-jungle">Export to Excel <img src="{{url('/images/excel.png')}}" width="24" height="24"/> </button>

                                            </div>
                                            @endif
                                        </form>
                                        @endif
                                        <div class="table-responsive">
                                            <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                                                <thead style="background-color: #364150;color:#fff;">
                                                    <tr>
                                                        <th>م</th>
                                                        <th> اسم العميل </th>
                                                        <th> اسم العميل بالانجليزية</th>
                                                        <th> رقم التليفون  </th>
                                                        <th> الايميل </th>
                                                        <th> النوع </th>
                                                        <th> المصدر </th>
                                                        <th> الجنسية </th>
                                                        <th> الاهتمام </th>
                                                        <th> تابع ل </th>
                                                        <th>تاريخ الاضافة </th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($clients as $k=>$client)
                                                    <tr>
                                                        <td>{{$k+1}}</td>

                                                        <td> {{  $client->name}}  </td>
                                                        <td> {{  $client->name_en}}  </td>
                                                        <td> {{  $client->number }} </td>
                                                        <td> {{  $client->email }} </td>
                                                        <td> {{  $client->gender }} </td>
                                                        <td> {{  $client->lead }} </td>
                                                        <td> {{  $client->nationality }} </td>
                                                        <td> {{  $client->Interest  }} </td>
                                                        <td> {{  $client->assigned_to  }} </td>
                                                        <td> {{  $client->created_at  }} </td>

                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>

@endsection

