@extends('layouts.adminlayout')
@section('pagejs')
    <script src="{{url('/assetsAdmin/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{url('/assetsAdmin/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/assetsAdmin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>

    <script src="{{url('assetsAdmin/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/clockface/js/clockface.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>
@endsection
@section('pagecss')

    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/clockface/css/clockface.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{url('/assetsAdmin/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap-rtl.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light portlet-fit bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-settings font-red"></i>
                        <span class="caption-subject font-red sbold uppercase"> التقارير </span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">

                            @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('74'))
                            <div class="user-label col-md-3">
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i> العملاء </div>
                                        <div class="tools">
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <a  href="{{url('admin/reports/client')}}">
                                            <img src="{{url('/images/network.png')}}" width="200" height="150"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('74'))
                            <div class="user-label col-md-3">
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>  احتياج العميل  </div>
                                        <div class="tools">
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <a  href="{{url('admin/reports/clientneeds')}}">
                                            <img src="{{url('/images/customer-service.jpg')}}" width="200" height="150"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('76'))
                            <div class="user-label col-md-3">
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>المستخدمين </div>
                                        <div class="tools">
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <a  href="{{url('admin/reports/users')}}">
                                            <img src="{{url('/images/support.png')}}" width="200" height="150"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('78'))
                            <div class="user-label col-md-3">
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i> المهام </div>
                                        <div class="tools">
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <a  href="{{url('admin/reports/task')}}">
                                            <img src="{{url('/images/notes.png')}}" width="200" height="150"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('80'))
                            <div class="user-label col-md-3">
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i> الخدمات </div>
                                        <div class="tools">
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <a  href="{{url('admin/reports/service')}}">
                                            <img src="{{url('/images/technical-support.png')}}" width="200" height="150"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

