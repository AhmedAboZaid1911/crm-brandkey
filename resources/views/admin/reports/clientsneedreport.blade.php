@extends('layouts.adminlayout')
@section('pagejs')
    <script src="{{url('/assetsAdmin/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{url('/assetsAdmin/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/assetsAdmin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>

    <script src="{{url('assetsAdmin/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/clockface/js/clockface.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>
@endsection
@section('pagecss')

    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/clockface/css/clockface.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{url('/assetsAdmin/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap-rtl.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light portlet-fit bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-settings font-red"></i>
                                            <span class="caption-subject font-red sbold uppercase"> الخدمات </span>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <form method="post" action="{{url('admin/reports/clientneeds/search')}}" class="form-horizontal" enctype="multipart/form-data">
                                                        {{ csrf_field() }}

                                                        <div class="col-md-9">
                                                            <select name="need_id" id="need_id" class="form-control  select2" required>
                                                            @foreach ($needs as $need )
                                                            <option value="{{$need->id}}" >
                                                                    {{$need->Need }}
                                                            </option>
                                                            @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <button type="submit" class="btn btn-circle green">بحث</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        @if ($message != '')
                                        <div class="alert alert-success">
                                            {!!$message!!}
                                        </div>
                                        @endif
                                        @if($_POST)
                                        <form method="post" action="{{ route('reports_clientneeds_search.excel') }}" class="form-horizontal">
                                            {{ csrf_field() }}

                                            <input type="hidden" name="need_id" value="{{$need_id}}">

                                            @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('81'))
                                            <div align="center">
                                                    <button type="submit"  class="btn btn-circle green-jungle">Export to Excel <img src="{{url('/images/excel.png')}}" width="24" height="24"/> </button>
                                                </div>
                                            @endif
                                        </form>
                                        @endif
                                        <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                                            <thead style="background-color: #364150;color:#fff;">
                                                 <tr>
                                                    <th>م</th>
                                                    <th> اسم العميل </th>
                                                    <th> اسم العميل بالانجليزية</th>
                                                    <th>  رقم الموبيل  </th>
                                                    <th>  رقم التليفون	</th>
                                                    <th> الايميل </th>
                                                    <th> تابع ل </th>
                                                    <th>تاريخ الاضافة </th>
                                                    <th>الاحتياج </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($clientneeds as $k=>$clientneed)
                                                <tr>
                                                    <td>{{$k+1}}</td>

                                                    <td> {{  $clientneed->name}}  </td>
                                                    <td> {{  $clientneed->name_en}}  </td>
                                                    <td> {{  $clientneed->number }} </td>
                                                    <td> {{  $clientneed->number2 }} </td>
                                                    <td> {{  $clientneed->email }} </td>

                                                    <td> {{  $clientneed->assigned_to  }} </td>
                                                    <td> {{  $clientneed->created_at  }} </td>
                                                    <td> {{  $clientneed->needs  }} </td>

                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>

@endsection

