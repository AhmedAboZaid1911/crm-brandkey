@extends('layouts.adminlayout')
@section('pagejs')
    <script src="{{url('/assetsAdmin/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{url('/assetsAdmin/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/assetsAdmin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>

    <script src="{{url('assetsAdmin/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/clockface/js/clockface.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>
    <script>
            function myFunction() {
              window.print();
            }
            </script>
@endsection
@section('pagecss')

    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/clockface/css/clockface.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{url('/assetsAdmin/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap-rtl.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light portlet-fit bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-settings font-red"></i>
                                            <span class="caption-subject font-red sbold uppercase"> الخدمات </span>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <form method="post" action="{{url('admin/reports/service/clientvsservice/search')}}" class="form-horizontal" enctype="multipart/form-data">
                                                        {{ csrf_field() }}

                                                        <div class="col-md-9">
                                                            <select name="service_name" id="service_name" class="form-control  select2" required>
                                                            @foreach ($Services as $Service )
                                                            <option value="{{$Service->id}}" >
                                                                    {{$Service->name }} {{ $Service->time }} {{$Service->servicesdate }} {{$Service->place }}
                                                            </option>
                                                            @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <button type="submit" class="btn btn-circle green">بحث</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        @if ($message != '')
                                        <div class="alert alert-success">
                                            {!!$message!!}
                                        </div>
                                        @endif
                                        @if($_POST)
                                        <form method="post" action="{{ route('reports_clientvsservice_search.excel') }}" class="form-horizontal">
                                                {{ csrf_field() }}

                                            <input type="hidden" name="servicename" value="{{$service_name}}">

                                            @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('81'))
                                            <div align="center">
                                                <button type="submit"  class="btn btn-circle green-jungle">Export to Excel <img src="{{url('/images/excel.png')}}" width="24" height="24"/> </button>
                                                <button onclick="myFunction()">Print this page</button>


                                            </div>
                                            @endif
                                        </form>
                                        @endif
                                        <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                                                <thead style="background-color: #364150;color:#fff;">
                                                        <tr>
                                                    <th>م</th>
                                                    <th> اسم العميل </th>
                                                    <th> اسم العميل بالانجليزية</th>
                                                    <th> رقم العميل </th>
                                                    <th> الايمال </th>
                                                    <th> تابع ل </th>
                                                    <th> تعديل الخدمات </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($clientvsservices as $k=>$clientvsservice)
                                                <tr>
                                                    <td>{{$k+1}}</td>

                                                    <td> {{  $clientvsservice->name}}  </td>
                                                    <td> {{  $clientvsservice->name_en}}  </td>
                                                    <td> {{  $clientvsservice->number }} </td>
                                                    <td> {{  $clientvsservice->email }} </td>
                                                    <td> {{  $clientvsservice->assigned_to  }} </td>
                                                    <td>
                                                        <a class="edit btn btn-transparent green-jungle btn-outline btn-circle btn-sm active" href="{{url('admin/ClientsVsServices/service')}}/{{$clientvsservice->id}}"> تعديل <i class="fa fa-edit"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>

@endsection

