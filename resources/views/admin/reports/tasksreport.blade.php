@extends('layouts.adminlayout')
@section('pagejs')
    <script src="{{url('/assetsAdmin/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{url('/assetsAdmin/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/assetsAdmin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>

    <script src="{{url('assetsAdmin/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/clockface/js/clockface.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>
@endsection
@section('pagecss')

    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/clockface/css/clockface.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{url('/assetsAdmin/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap-rtl.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light portlet-fit bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-settings font-red"></i>
                                            <span class="caption-subject font-red sbold uppercase"> المهام </span>
                                        </div>

                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-12">
                                                        <form method="post" action="{{url('admin/reports/task/search')}}" class="form-horizontal" enctype="multipart/form-data">
                                                            {{ csrf_field() }}
                                                            <div class="col-md-6">
                                                                <div class="col-md-2">
                                                                    <label class="col-md-12 control-label" >
                                                                        من
                                                                    </label>
                                                                </div>
                                                                <div class="col-md-7">
                                                                    <div class="input-group input-larag date date-picker" data-date="" data-date-format="yyyy-mm-dd" data-date-viewmode="years">
                                                                        <input type="text" class="form-control" name="from" value="" required="required" readonly >
                                                                        <span class="input-group-btn">
                                                                            <button class="btn default" type="button" required="required">
                                                                                <i class="fa fa-calendar"></i>
                                                                            </button>
                                                                        </span>
                                                                    </div>
                                                                    <!-- /input-group -->
                                                                    <span class="help-block">  </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="col-md-2">
                                                                    <label class="col-md-12 control-label" >
                                                                        الي
                                                                    </label>
                                                                </div>
                                                                <div class="col-md-7">
                                                                    <div class="input-group input-larag date date-picker" data-date="" data-date-format="yyyy-mm-dd" data-date-viewmode="years">
                                                                        <input type="text" class="form-control" name="to" value=""  readonly >
                                                                        <span class="input-group-btn">
                                                                            <button class="btn default" type="button">
                                                                                <i class="fa fa-calendar"></i>
                                                                            </button>
                                                                        </span>
                                                                    </div>
                                                                    <!-- /input-group -->
                                                                    <span class="help-block">  </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="col-md-2">
                                                                    <label class="col-md-12 control-label" >
                                                                        الحالة
                                                                    </label>
                                                                </div>
                                                                <div class="col-md-7">
                                                                    <select name="case" id="case" class="form-control select2 input-larag" required>

                                                                        <option value="1" > الكل</option>
                                                                        <option value="2" > جاري</option>
                                                                        <option value="3" > متاخرة</option>
                                                                        <option value="4" > تمت</option>

                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="col-md-2">
                                                                    <label class="col-md-12 control-label" >
                                                                        تابع ل
                                                                    </label>
                                                                </div>
                                                                <div class="col-md-7">
                                                                    <select name="user" id="user" class="form-control  select2 input-larag" required>

                                                                        <option value="all" > الكل</option>

                                                                        @foreach ($users as $user )
                                                                            <option value="{{$user->id}}" > {{$user->name}} </option>
                                                                        @endforeach

                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 col-md-offset-6">
                                                            <br><br>
                                                                <div class="col-md-3">
                                                                    <button type="submit" class="btn btn-circle green">بحث</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                </div>

                                            </div>
                                        </div>
                                        @if ($message != '')
                                        <div class="alert alert-success">
                                            {!!$message!!}
                                        </div>
                                        @endif
                                        @if($_POST)
                                        <form method="post" action="{{ route('reports_tasks_search.excel') }}" class="form-horizontal">
                                                {{ csrf_field() }}
                                            <input type="hidden" name="from" value="{{$from}}">
                                            <input type="hidden" name="to" value="{{$to}}">
                                            <input type="hidden" name="case" value="{{$case}}">

                                            <input type="hidden" name="assigned_to" value="{{$assigned_to}}">

                                            @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('79'))
                                            <div align="center">
                                                    <button type="submit"  class="btn btn-circle green-jungle">Export to Excel <img src="{{url('/images/excel.png')}}" width="24" height="24"/> </button>
                                            </div>
                                            @endif
                                        </form>
                                        @endif
                                        <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                                            <thead style="background-color: #364150;color:#fff;">
                                                <tr>
                                                    <th>م</th>
                                                    <th>
                                                        اسم العميل / الخدمة
                                                    </th>
                                                    <th>الرقم</th>
                                                    <th>المهمة </th>
                                                    <th>التاريخ </th>
                                                    <th>تابع ل </th>
                                                    <th>تاريخ الانتهاء </th>
                                                    <th> الانتهاء من قبل </th>
                                                    <th> تفاصيل </th>
                                                    <th> حذف </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($tasks as $k=>$task)
                                                @if($task->status == '3')
                                                <tr class="bg-green-jungle ">
                                                @elseif($task->assigned_at < date('Y-m-d') )
                                                <tr class="bg-red-pink">
                                                @else
                                                <tr class="warning">
                                                @endif
                                                        <td>{{$k+1}}</td>
                                                    <td>
                                                    @if($task->type =='clientvsservices')
                                                         {{$task->service}} / {{$task->client}}
                                                    @endif
                                                    @if($task->type =='client')
                                                    {{$task->client}}
                                                    @endif
                                                    @if($task->type =='general')
                                                        عام
                                                    @endif
                                                    </td>
                                                    @if($task->type =='general')
                                                    <td> عام  </td>
                                                    @else
                                                    <td> {{  $task->clientnum  }}  </td>
                                                    @endif
                                                    <td> {{  $task->title  }}  </td>
                                                    <td> {{  $task->assigned_at  }} </td>
                                                    <td> {{  $task->assigned_to  }}  </td>
                                                    <td> {{  $task->updated_at  }}  </td>
                                                    <td> {{  $task->updated_by  }}  </td>

                                                    <td>
                                                        <a class="edit  btn btn-circle blue-soft active" href="{{url('admin/tasks/edit')}}/{{$task->id}}" > تفاصيل <i class="fa fa-edit"></i></a>
                                                    </td>
                                                    <td>
                                                        <a class="delete btn btn-transparent red btn-outline btn-circle btn-sm active" href="{{url('admin/tasks/del')}}/{{$task->id}}" onclick="return confirm('Are you sure you want to delete this item?');"> حذف <i class="fa fa-trash-o"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>

@endsection

