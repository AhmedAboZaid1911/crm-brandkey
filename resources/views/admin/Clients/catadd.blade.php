@extends('layouts.adminlayout')

@section('pagejs')

    <script src="{{url('/assetsAdmin/pages/scripts/form-samples.min.js')}}" type="text/javascript"></script>

    <script src="{{url('/assetsAdmin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>



@endsection

@section('pagecss')

<link href="{{url('/assetsAdmin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css" />

@endsection



@section('content')



<div class="portlet box green">
        @if(session()->has('message'))
        <div class="alert alert-danger">
        {{ session()->get('message') }}
        <br>
        اسم العميل :
        {{ session()->get('clientname') }}
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-6 col-md-6">
                        <a href="{{url('admin/Clients/Clients_view')}}/{{ session()->get('clientid') }}" id="sample_editable_1_new" class="btn btn-circle yellow-saffron " style="color:#fff;"> الذهاب للعميل
                            <i class="fa fa-backward"></i>
                        </a>
                    </div>
                    <div class="col-md-offset-6 col-md-6">
                    </div>
                </div>
            </div>



        </div>
        @endif



    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>اضافة {{$posttitlepage}}
        </div>
    </div>

                                        <div class="portlet-body form">
                                            <!-- BEGIN FORM-->
                                            <form method="post" action="{{url('admin/Clients/Clients_store')}}" class="form-horizontal" enctype="multipart/form-data">
                                                <div class="form-body">
                                                    {{ csrf_field() }}
                                                    @foreach($lang_arr as $k => $v)
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label-client-edit">الاسم بالكامل <span style="color: red"> *</span></label>
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control input-circle" placeholder="الاسم بالكامل" name="name" value="{{ old('name') }}" required>
                                                            <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('name') }}</div>
                                                        </div>
                                                        <label class="col-md-2 control-label-client-edit"> الاسم بالكامل بالانجليزية </label>
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control input-circle" placeholder="الاسم بالكامل بالانجليزية" name="name_en" value="{{ old('name_en	') }}" >
                                                            <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('name_en	') }}</div>
                                                        </div>
                                                        <label class="col-md-2 control-label-client-edit"> E-Mail <span style="color: red"> *</span></label>
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control input-circle" placeholder="E-Mail" name="email" value="{{ old('email') }}" required>
                                                            <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('email') }}</div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label-client-edit"> رقم التليفون </label>
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control input-circle" name="number" placeholder="رقم التليفون" value="{{ old('number') }}" required>
                                                            <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('number') }}</div>
                                                        </div>
                                                        <label class="col-md-2 control-label-client-edit">رقم التليفون الثاني</label>
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control input-circle" name="number2" placeholder="رقم التليفون الثاني" maxlength="100" value="{{ old('number2') }}">
                                                            <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('number2') }}</div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label-client-edit">العنوان</label>
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control input-circle" name="address" placeholder="العنوان" value="{{ old('address') }}">
                                                            <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('address') }}</div>
                                                        </div>
                                                        <label class="col-md-2 control-label-client-edit">تابع ل <span style="color: red"> *</span></label>
                                                        <div class="col-md-4">
                                                            <select name="assigned_to" id="assigned_to" class="form-control select2  input-larag" required>
                                                            @foreach($user as $users)
                                                                <option value="{{$users->id}}" <?=($users->id == Session::get('uid')) ? 'selected' : '' ?>>
                                                                    {{   $users->{'name'} }}
                                                                </option>
                                                            @endforeach
                                                            </select>
                                                            <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('cid') }}</div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label-client-edit"> النوع <span style="color: red"> *</span></label>
                                                        <div class="col-md-4">
                                                            <select name="gender" id="gender" class="form-control select2  input-larag" required>
                                                            @foreach($gender as $genders)
                                                                <option value="{{$genders->id}}" <?=($genders->id == old('gender')) ? 'selected' : '' ?>>
                                                                    {{ $genders->{'type'} }}
                                                                </option>
                                                            @endforeach
                                                            </select>
                                                            <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('cid') }}</div>
                                                        </div>
                                                        <label class="col-md-2 control-label-client-edit"> الاهتمام <span style="color: red"> *</span></label>
                                                        <div class="col-md-4">
                                                            <select name="Interest" id="Interest" class="form-control select2  input-larag" required>
                                                            @foreach($Interest as $Interests)
                                                                <option value="{{$Interests->id}}" <?=($Interests->id == old('Interest')) ? 'selected' : '' ?>>
                                                                    {{ $Interests->{'type'} }}
                                                                </option>
                                                            @endforeach
                                                            </select>
                                                            <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('cid') }}</div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label-client-edit"> Lead Source <span style="color: red"> *</span></label>
                                                        <div class="col-md-4">
                                                            <select name="lead" id="lead" class="form-control select2  input-larag" required>
                                                            @foreach($leads as $lead)
                                                                <option value="{{$lead->id}}" <?=($lead->id == old('lead')) ? 'selected' : '' ?>>
                                                                    {{ $lead->{'lead'} }}
                                                                </option>
                                                            @endforeach
                                                            </select>
                                                            <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('cid') }}</div>
                                                        </div>
                                                        <label class="col-md-2 control-label-client-edit"> الجنسية <span style="color: red"> *</span></label>
                                                        <div class="col-md-4">
                                                            <select name="nationality" id="nationality" class="form-control  select2 input-larag" required>
                                                            @foreach($nationalitys as $nationality)
                                                                <option value="{{$nationality->id}}" <?=($nationality->id == old('nationality')) ? 'selected' : '' ?>>
                                                                    {{ $nationality->{'title_en'} }}
                                                                </option>
                                                            @endforeach
                                                            </select>
                                                            <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('cid') }}</div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label-client-edit"> احتاج العميل <span style="color: red"> *</span></label>
                                                        <div class="col-md-4">
                                                            <select name="want" id="want" class="form-control select2  input-larag" required>
                                                            @foreach($wants as $want)
                                                                <option value="{{$want->id}}" <?=($want->id == old('type')) ? 'selected' : '' ?>>
                                                                    {{ $want->{'type'} }}
                                                                </option>
                                                            @endforeach
                                                            </select>
                                                            <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('cid') }}</div>
                                                        </div>
                                                        <label class="col-md-2 control-label-client-edit"> نوع الخدمة  <span style="color: red"> *</span></label>
                                                        <div class="col-md-4">
                                                            <select name="service_type" id="service_type" class="form-control  select2 input-larag" required>
                                                            @foreach($service_types as $service_type)
                                                                <option value="{{$service_type->id}}" <?=($service_type->id == old('type')) ? 'selected' : '' ?>>
                                                                    {{ $service_type->{'type'} }}
                                                                </option>
                                                            @endforeach
                                                            </select>
                                                            <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('cid') }}</div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label-client-edit"> حالة الإهتمام <span style="color: red"> *</span></label>
                                                        <div class="col-md-4">
                                                            <select name="interested_type" id="interested_type" class="form-control select2  input-larag" required>
                                                            @foreach($interested_types as $interested_type)
                                                                <option value="{{$interested_type->id}}" <?=($interested_type->id == old('type')) ? 'selected' : '' ?>>
                                                                    {{ $interested_type->{'type'} }}
                                                                </option>
                                                            @endforeach
                                                            </select>
                                                            <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('cid') }}</div>
                                                        </div>
                                                        <label class="col-md-2 control-label-client-edit"> حالة العميل وقت الإتصال <span style="color: red"> *</span></label>
                                                        <div class="col-md-4">
                                                            <select name="call_status" id="call_status" class="form-control  select2 input-larag" required>
                                                            @foreach($call_status as $call_statu)
                                                                <option value="{{$call_statu->id}}" <?=($call_statu->id == old('type')) ? 'selected' : '' ?>>
                                                                    {{ $call_statu->{'type'} }}
                                                                </option>
                                                            @endforeach
                                                            </select>
                                                            <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('cid') }}</div>
                                                        </div>
                                                        <label class="col-md-2 control-label-client-edit"> اسباب عدم الإهتمام <span style="color: red"> *</span></label>
                                                        <div class="col-md-4">
                                                            <select name="not_interested" id="not_interested" class="form-control  select2 input-larag" required>
                                                            @foreach($not_interested as $not_interest)
                                                                <option value="{{$not_interest->id}}" <?=($not_interest->id == old('nationality')) ? 'selected' : '' ?>>
                                                                    {{ $not_interest->{'type'} }}
                                                                </option>
                                                            @endforeach
                                                            </select>
                                                            <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('cid') }}</div>
                                                        </div>
                                                    </div>
                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label-client-edit">تعليقات</label>
                                                                <div class="col-md-10">
                                                                    <textarea rows="5" placeholder="المحتوى" id="form-field-22" class="form-control" name="comment" ><?= old('comment') ?></textarea>
                                                                </div>
                                                            </div>
                                                            @endforeach
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-6 col-md-6">
                                                                    <button type="submit" class="btn btn-circle green">أضف</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                <!-- END FORM-->
                                            </div>
                                        </div>


@endsection



