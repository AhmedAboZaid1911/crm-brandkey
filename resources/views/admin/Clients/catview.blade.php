@extends('layouts.adminlayout')



@section('pagejs')





    <script src="{{url('assetsAdmin/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/clockface/js/clockface.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/plugins/jquery.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/plugins/js.cookie.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>

    <script src="{{url('assetsAdmin/global/plugins/bootbox/bootbox.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/layouts/layout/scripts/demo.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/layouts/global/scripts/quick-sidebar.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/layouts/global/scripts/quick-nav.min.js')}}" type="text/javascript"></script>

    <script src="{{url('js/multiuploadscript.js')}}"></script>
    <script>

        $( "#paidValue" ).keyup(function() {
            var remainderValue = 0;
            remainderValue =   $('#price').val() - $(this).val();
            $('#remainderValue').val(remainderValue);
            });

    </script>

    <script>

            $(document).ready(function()

            {

                $('#clickmewow').click(function()

                {

                    $('#radio1003').attr('checked', 'checked');

                });

            })



    $('textarea').click(function (e) {

    var rows = $(this).val().split("\n");

    $(this).prop('rows', rows.length +1);

    });







    </script>

@endsection



@section('pagecss')



    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css" />



    <link rel="stylesheet" type="text/css" href="{{url('css/multiuploadstyle.css')}}">



@endsection







@section('content')



    <div class="portlet box green">

        <div class="portlet-title">

            <div class="caption">

                <i class="fa fa-gift"></i> {{$posttitlepage}}

            </div>

        </div>

        <div class="portlet-body form">

            <!-- BEGIN FORM-->

            <div class="row">

                <div class="col-md-12">

                    <div class="portlet light bordered" id="blockui_sample_1_portlet_body">

                        <div class="portlet-title">

                            <div class="caption">
                                @if(session()->has('message'))
                                    <div class="alert alert-success">
                                        {{ session()->get('message') }}
                                    </div>
                                @endif
                                <i class="icon-bubble font-green-sharp"></i>

                                <span class="caption-subject font-green-sharp sbold">بيانات العميل </span>

                            </div>

                        </div>

                        <div class="portlet-body">

                            <div class="row">

                                <div class="col-md-12">

                                    <!-- BEGIN SAMPLE TABLE PORTLET-->

                                        <div class="portlet-body">
                                            <div class="table-scrollable">
                                                <form method="post" action="{{url('admin/Clients/Clients_update')}}" class="form-horizontal" enctype="multipart/form-data">
                                                    <div class="form-body">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="id" value="{{$clients[0]->id}}">
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label-client-edit">الاسم بالكامل <span style="color: red"> *</span></label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control input-circle" placeholder="الاسم بالكامل" name="name" value="{{ $clients[0]->name }}" required>
                                                                <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('name') }}</div>
                                                            </div>
                                                            <label class="col-md-2 control-label-client-edit">الاسم بالكامل بالانجليزية</label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control input-circle" placeholder=" الاسم بالكامل بالانجليزية" name="name_en" value="{{ $clients[0]->name_en }}" required>
                                                                <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('name_en	') }}</div>
                                                            </div>
                                                            <label class="col-md-2 control-label-client-edit"> E-mail <span style="color: red"> *</span></label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control input-circle" placeholder="E-Mail" name="email" value="{{ $clients[0]->email }}" required>
                                                                <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('email') }}</div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label-client-edit"> رقم التليفون </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control input-circle" name="number" placeholder="رقم التليفون" value="{{ $clients[0]->number }}" required>
                                                                <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('number') }}</div>
                                                            </div>
                                                            <label class="col-md-2 control-label-client-edit">رقم التليفون الثاني</label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control input-circle" name="number2" placeholder="رقم التليفون الثاني" maxlength="100" value="{{ $clients[0]->number2 }}">
                                                                <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('number2') }}</div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label-client-edit">العنوان</label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control input-circle" name="address" placeholder="العنوان" value="{{ $clients[0]->address }}">
                                                                <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('address') }}</div>
                                                            </div>
                                                            <label class="col-md-2 control-label-client-edit">تابع ل <span style="color: red"> *</span></label>
                                                            <div class="col-md-4">
                                                                <select name="assigned_to" id="assigned_to" class="form-control  select2 input-larag" required>
                                                                    @foreach($user as $users)
                                                                        <option value="{{$users->id}}" <?=($users->id == $clients[0]->assigned_to ) ? 'selected' : '' ?>>
                                                                            {{ $users->{'name'} }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                                <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('cid') }}</div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label-client-edit"> النوع <span style="color: red"> *</span></label>
                                                            <div class="col-md-4">
                                                                <select name="gender" id="gender" class="form-control  select2 input-larag" required>
                                                                    @foreach($gender as $genders)
                                                                        <option value="{{$genders->id}}" <?=($genders->id == $clients[0]->gender ) ? 'selected' : '' ?>>
                                                                            {{ $genders->{'type'} }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                                <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('cid') }}</div>
                                                            </div>
                                                            <label class="col-md-2 control-label-client-edit"> الاهتمام <span style="color: red"> *</span></label>
                                                            <div class="col-md-4">
                                                                <select name="Interest" id="Interest" class="form-control  select2 input-larag" required>
                                                                    @foreach($Interest as $Interests)
                                                                        <option value="{{$Interests->id}}" <?=($Interests->id == $clients[0]->Interest ) ? 'selected' : '' ?>>
                                                                            {{ $Interests->{'type'} }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                                <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('cid') }}</div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label-client-edit"> Lead Source <span style="color: red"> *</span></label>
                                                            <div class="col-md-4">
                                                                <select name="lead" id="lead" class="form-control  select2  input-larag" required>
                                                                    @foreach($leads as $lead)
                                                                        <option value="{{$lead->id}}" <?=($lead->id == $clients[0]->lead ) ? 'selected' : '' ?>>
                                                                            {{ $lead->{'lead'} }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                                <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('cid') }}</div>
                                                            </div>
                                                            <label class="col-md-2 control-label-client-edit"> الجنسية <span style="color: red"> *</span></label>
                                                            <div class="col-md-4">
                                                                <select name="nationality" id="nationality" class="form-control  select2 input-larag" required>
                                                                    @foreach($nationalitys as $nationality)
                                                                        <option value="{{$nationality->id}}" <?=($nationality->id == $clients[0]->nationality ) ? 'selected' : '' ?>>
                                                                            {{ $nationality->{'title_en'} }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                                <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('cid') }}</div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label-client-edit"> احتاج العميل <span style="color: red"> *</span></label>
                                                            <div class="col-md-4">
                                                                <select name="want" id="want" class="form-control select2  input-larag" required>
                                                                @foreach($wants as $want)
                                                                    <option value="{{$want->id}}" <?=($want->id == $clients[0]->want) ? 'selected' : '' ?>>
                                                                        {{ $want->{'type'} }}
                                                                    </option>
                                                                @endforeach
                                                                </select>
                                                                <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('cid') }}</div>
                                                            </div>
                                                            <label class="col-md-2 control-label-client-edit"> نوع الخدمة  <span style="color: red"> *</span></label>
                                                            <div class="col-md-4">
                                                                <select name="service_type" id="service_type" class="form-control  select2 input-larag" required>
                                                                @foreach($service_types as $service_type)
                                                                    <option value="{{$service_type->id}}" <?=($service_type->id == $clients[0]->service_type) ? 'selected' : '' ?>>
                                                                        {{ $service_type->{'type'} }}
                                                                    </option>
                                                                @endforeach
                                                                </select>
                                                                <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('cid') }}</div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label-client-edit"> حالة الإهتمام <span style="color: red"> *</span></label>
                                                            <div class="col-md-4">
                                                                <select name="interested_type" id="interested_type" class="form-control select2  input-larag" required>
                                                                @foreach($interested_types as $interested_type)
                                                                    <option value="{{$interested_type->id}}" <?=($interested_type->id == $clients[0]->interested_type) ? 'selected' : '' ?>>
                                                                        {{ $interested_type->{'type'} }}
                                                                    </option>
                                                                @endforeach
                                                                </select>
                                                                <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('cid') }}</div>
                                                            </div>
                                                            <label class="col-md-2 control-label-client-edit"> حالة العميل وقت الإتصال <span style="color: red"> *</span></label>
                                                            <div class="col-md-4">
                                                                <select name="call_status" id="call_status" class="form-control  select2 input-larag" required>
                                                                @foreach($call_status as $call_statu)
                                                                    <option value="{{$call_statu->id}}" <?=($call_statu->id == $clients[0]->call_status) ? 'selected' : '' ?>>
                                                                        {{ $call_statu->{'type'} }}
                                                                    </option>
                                                                @endforeach
                                                                </select>
                                                                <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('cid') }}</div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label-client-edit"> اسباب عدم الإهتمام <span style="color: red"> *</span></label>
                                                            <div class="col-md-4">
                                                                <select name="not_interested" id="not_interested" class="form-control  select2 input-larag" required>
                                                                @foreach($not_interested as $not_interest)
                                                                    <option value="{{$not_interest->id}}" <?=($not_interest->id == $clients[0]->not_interested) ? 'selected' : '' ?>>
                                                                        {{ $not_interest->{'type'} }}
                                                                    </option>
                                                                @endforeach
                                                                </select>
                                                                <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('cid') }}</div>
                                                            </div>
                                                        </div>
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-6 col-md-6">
                                                                    @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('4'))
                                                                        <button type="submit" class="btn btn-circle green">تعديل</button>
                                                                    @endif
                                                                    <!--<a href="{{url('admin/Clients/Clients_view')}}/{{$clients[0]->id}}" id="sample_editable_1_new" class="btn btn-circle yellow-saffron " style="color:#fff;"> الرجوع للعميل-->
                                                                    <!--    <i class="fa fa-backward"></i>-->
                                                                    <!--</a>-->
                                                                </div>
                                                                <div class="col-md-offset-6 col-md-6">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <!--<div class="table-scrollable">-->

                                            <!--    <table class="table table-striped table-bordered table-advance table-hover">-->

                                            <!--        <thead>-->

                                            <!--            <tr>-->

                                            <!--                <th><i class="fa fa-user"></i>  </th>-->

                                            <!--                <th class=""> <i class="fa fa-edit"></i>  </th>-->

                                            <!--            </tr>-->

                                            <!--        </thead>-->

                                            <!--        <tbody>-->

                                            <!--            <tr>-->

                                            <!--                <td class="highlight">-->

                                            <!--                    <div class="success"></div>-->

                                            <!--                    الاسم بالكامل-->

                                            <!--                </td>-->

                                            <!--                <td class=""> {{ $clients[0]->name }} </td>-->

                                            <!--            </tr>-->
                                            <!--            <tr>-->

                                            <!--                <td class="highlight">-->

                                            <!--                    <div class="success"></div>-->

                                            <!--                    الاسم بالكامل بالانجليزية-->

                                            <!--                </td>-->

                                            <!--                <td class=""> {{ $clients[0]->name_en }} </td>-->

                                            <!--            </tr>-->

                                            <!--            <tr>-->

                                            <!--                <td class="highlight">-->

                                            <!--                    <div class="info"> </div>-->

                                            <!--                    الايميل-->

                                            <!--                </td>-->

                                            <!--                <td class=""> {{ $clients[0]->email }} </td>-->

                                            <!--            </tr>-->

                                            <!--            <tr>-->

                                            <!--                <td class="highlight">-->

                                            <!--                    <div class="info"> </div>-->

                                            <!--                    رقم التليفون-->

                                            <!--                </td>-->

                                            <!--                <td class=""> {{ $clients[0]->number }} </td>-->

                                            <!--            </tr>-->

                                            <!--            <tr>-->

                                            <!--                <td class="highlight">-->

                                            <!--                    <div class="info"> </div>-->

                                            <!--                    رقم التليفون الثاني-->

                                            <!--                </td>-->

                                            <!--                <td class=""> {{ $clients[0]->number2 }} </td>-->

                                            <!--            </tr>-->

                                            <!--            <tr>-->

                                            <!--                <td class="highlight">-->

                                            <!--                    <div class="info"> </div>-->

                                            <!--                    الوصف-->

                                            <!--                </td>-->

                                            <!--                <td class=""> {{ $clients[0]->Description }} </td>-->

                                            <!--            </tr>-->

                                            <!--            <tr>-->

                                            <!--                <td class="highlight" colspan="2">-->

                                            <!--                    <a class="edit btn btn-transparent blue btn-outline btn-circle btn-sm active" href="{{url('admin/Clients/Clients_edit')}}/{{$clients[0]->id}}"> تعديل <i class="fa fa-edit"></i></a>-->



                                            <!--                </td>-->



                                            <!--            </tr>-->



                                            <!--        </tbody>-->

                                            <!--    </table>-->

                                            <!--</div>-->

                                        </div>

                                    <!-- END SAMPLE TABLE PORTLET-->

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                     <div class="portlet light bordered">

                <div class="portlet-body">

                    <ul class="nav nav-pills">

                        <li class="active">

                            <a href="#tab_2_1" data-toggle="tab"> التعليقات </a>

                        </li>

                        <li>

                            <a href="#tab_2_2" data-toggle="tab"> المهام </a>

                        </li>

                        <li>

                            <a href="#tab_2_3" data-toggle="tab"> التعليقات القديمة </a>

                        </li>
                        <li>

                            <a href="#tab_2_4" data-toggle="tab"> رغبات العميل </a>

                        </li>
                        <li>

                            <a href="#tab_2_5" data-toggle="tab">Tags </a>

                        </li>



                        <li style="float:left;padding-left: 15px;">

                            <a class="edit btn btn-transparent green-jungle btn-outline btn-circle btn-sm active" href="{{url('admin/ClientsVsServices/index')}}/{{$clients[0]->id}}"> الخدمات <i class="fa fa-edit"></i></a>

                        </li>

                    </ul>

                    <div class="tab-content">

                        <div class="tab-pane fade active in" id="tab_2_1">

                            <!-- BEGIN FORM-->

                            <form method="post" action="{{url('admin/Clients/Clients_comment')}}" class="form-horizontal" enctype="multipart/form-data">



                                    {{ csrf_field() }}

                                    <input type="hidden" name="id" value="{{$clients[0]->id}}">

                                    <div class="form-group">

                                    <label class="col-md-3 control-label">تعليق</label>

                                    <div class="col-md-8">

                                        <textarea rows="5" placeholder="تعليق" id="form-field-22" class="form-control" name="comment" ></textarea>

                                    </div>

                                </div>

                                <div class="form-actions">

                                    <div class="row">

                                        <div class="col-md-offset-3 col-md-9">

                                            <button type="submit" class="btn btn-circle green">اضافة تعليق</button>

                                        </div>

                                    </div>

                                </div>

                            </form>

                            <!-- END FORM-->

                            <div class="form-group">

                                <div class="col-md-12">

                                    @foreach ($comment as $comments )

                                    <div class="portlet-body">

                                        <div class="mt-timeline-2">

                                            <ul class="mt-container">

                                                <li class="mt-item">

                                                    <div class="mt-timeline-content">

                                                        <div class="mt-content-container ">

                                                            <div class="mt-title">

                                                                <h3 class="mt-content-title font-blue-madison">{{ $comments->username }}</h3>

                                                            </div>

                                                            <div class="mt-author">

                                                                <div class="mt-author-name">

                                                                    <a href="javascript:;" class="font-blue-madison"></a>

                                                                </div>

                                                                <div class="mt-author-notes font-grey-mint">{{$comments->created_at}}</div>

                                                            </div>

                                                            <div class="mt-content border-grey-salt">

                                                                <p>

                                                                    {{$comments->text}}

                                                                </p>

                                                            </div>

                                                        </div>

                                                    </div>

                                                </li>

                                            </ul>

                                        </div>

                                    </div>

                                    @endforeach

                                </div>

                            </div>



                        </div>

                        <div class="tab-pane fade" id="tab_2_2">

                            <!-- BEGIN FORM-->
                            <form method="post" action="{{url('admin/Clients/Clients_tasks')}}" class="form-horizontal" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="id" value="{{$clients[0]->id}}">
                                <div class="form-group">
                                    <label class="control-label col-md-2">تاريخ</label>
                                    <div class="col-md-4">
                                        <div class="input-group input-larag date date-picker" data-date="" data-date-format="yyyy-mm-dd" data-date-viewmode="years">
                                            <input type="date" class="form-control" name="assigned_at" value="{{ old('assigned_at') }}" required>
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <!-- /input-group -->
                                        <span class="help-block">  </span>
                                    </div>
                                    <label class="col-md-2 control-label">  العنوان  <span style="color: red"> *</span> </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control input-circle" id="title" name="title" placeholder="العنوان " value="" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label"> المهمة <span style="color: red"> *</span></label>
                                    <div class="col-md-4">
                                        <textarea rows="10" placeholder="المحتوى" id="form-field-22" class="form-control" name="text" required></textarea>
                                    </div>
                                    <label class="control-label col-md-2">الصورة</label>
                                    <div class="col-md-4">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                            <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileinput-new"> Select image </span>
                                                    <span class="fileinput-exists"> Change </span>
                                                    <input type="file" name="pic"> </span>
                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                            </div>
                                        </div>
                                        <div class="clearfix margin-top-10">
                                            <span class="label label-danger"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">تابع ل <span style="color: red"> *</span></label>
                                    <div class="col-md-4">
                                        <select name="assigned_to" id="assigned_to" class="form-control select2  input-larag" required>
                                            @foreach($user as $users)
                                                <option value="{{$users->id}}" <?=($users->id == Session::get('uid')) ? 'selected' : '' ?>>
                                                    {{$users->{'name'} }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-5 col-md-7">
                                            <button type="submit" class="btn btn-circle green">اضافة مهمة</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                                <div class="form-group">

                                    <div class="col-md-12">
                                        @foreach ($tasks as $task )
                                        <br>
                                        <div class="portlet-body">

                                            <div class="mt-timeline-2">

                                                <ul class="mt-container">

                                                    <li class="mt-item">

                                                        <div class="mt-timeline-content">

                                                            <div class="mt-content-container ">

                                                                <div class="mt-title">

                                                                    <h2 class="mt-content-title ">{{ $task->assigned_to}}</h2>

                                                                    <div class="mt-author-notes font-grey-mint"> تاريخ انتهاء المهمة {{$task->assigned_at}}</div>

                                                                </div>

                                                                <div class="mt-author">

                                                                    <div class="mt-avatar">

                                                                        <img src="{{url($task->logo)}}" />

                                                                    </div>

                                                                    <div class="mt-author-name">

                                                                        <a href="javascript:;" class="font-blue-madison"></a>

                                                                    </div>

                                                                    <div class="mt-author-name">

                                                                        <a href="javascript:;" class="font-blue-madison">{{$task->username }}</a>

                                                                    </div>

                                                                    <div class="mt-author-notes font-grey-mint">{{$task->created_at}}</div>

                                                                </div>

                                                                <div class="mt-content border-grey-salt">

                                                                    @if ($task->pic != NULL )

                                                                        <img class="timeline-body-img pull-right" style="width:20%;" src="{{url($task->pic)}}" alt="{{$task->title}}">

                                                                    @endif

                                                                    <h3>{{$task->title}}</h3>

                                                                    <p>

                                                                        {{$task->text}}





                                                                    </p>

                                                                    <a href="{{url('admin/tasks/view/')}}/{{$task->id}}" class="btn btn-circle red">Read More</a>

                                                                    <div class="mt-content border-grey-salt">

                                                                        حالة المهمة :

                                                                        @if($task->status == '3')

                                                                        <button type="button" disabled class="btn btn-circle green-jungle">تمت</button>

                                                                        @elseif($task->assigned_at < date('Y-m-d') )

                                                                        <button type="button" disabled class="btn btn-circle red-pink">متاخرة</button>

                                                                        @else

                                                                        <button type="button" disabled class="btn btn-circle warning">جارية</button>

                                                                        @endif

                                                                    </div>

                                                                    {{-- <a  class="btn btn-circle bg-green-jungle" > تمت</a> --}}



                                                                </div>

                                                            </div>

                                                        </div>

                                                    </li>

                                                </ul>

                                            </div>

                                        </div>

                                        @endforeach

                                    </div>

                                </div>

                        </div>

                        <div class="tab-pane fade " id="tab_2_3">
                            <!-- BEGIN FORM-->
                                <div class="form-group">

                                   <div class="col-md-12">

                                        <div class="portlet-body">

                                            <div class="mt-timeline-2">

                                                <ul class="mt-container">

                                                    <li class="mt-item">

                                                        <div class="mt-timeline-content">

                                                            <div class="mt-content-container ">

                                                                <div class="old-comment mt-content border-grey-salt">

                                                                    <textarea class="common" readonly>

                                                                        {{$clients[0]->oldnotes}}

                                                                    </textarea>

                                                                </div>

                                                            </div>

                                                        </div>

                                                    </li>

                                                </ul>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            <!-- END FORM-->

                        </div>

                        <div class="tab-pane fade " id="tab_2_4">

                            <!-- BEGIN FORM-->

                            <form method="post" action="{{url('admin/Clients/Clients_Needs')}}" class="form-horizontal" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="client" value="{{$clients[0]->id}}">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">احتياج</label>
                                    <div class="col-md-4">
                                        <select name="need" id="need" class="form-control select2  input-larag" required>
                                        @foreach($Needs as $Need)
                                            <option value="{{$Need->id}}" >
                                                {{ $Need->{'Need'} }}
                                            </option>
                                        @endforeach
                                        </select>
                                    </div>

                                </div>

                                <div class="form-actions">

                                    <div class="row">

                                        <div class="col-md-offset-3 col-md-9">

                                            <button type="submit" class="btn btn-circle green">اضافة احتياج</button>

                                        </div>

                                    </div>

                                </div>

                            </form>

                            <!-- END FORM-->

                            <div class="form-group">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                    <div class="portlet-body">
                                        <div class="table-scrollable">
                                            <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                                                <thead>
                                                    <tr>
                                                        <th> م</th>
                                                        <th>  الاحتياج </th>
                                                        <th> حذف </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($ClientNeeds as $k=>$ClientNeed)
                                                        <tr>
                                                            <td>{{$k+=1}}</td>
                                                            <td> {{ $ClientNeed->clientneed }} </td>

                                                            <td>
                                                                    <a class="delete btn btn-transparent red btn-outline btn-circle btn-sm active" href="{{url('admin/Clients/Clients_Needs_delete/')}}/{{$ClientNeed->id}}/{{$clients[0]->id}}" onclick="return confirm('Are you sure you want to delete this item?');"> حذف <i class="fa fa-trash-o"></i></a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                <!-- END SAMPLE TABLE PORTLET-->
                            </div>
                        </div>
                        
                        <div class="tab-pane fade " id="tab_2_5">

                            <!-- BEGIN FORM-->

                            <form method="post" action="{{url('admin/Clients/Clients_Tags')}}" class="form-horizontal" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="client" value="{{$clients[0]->id}}">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Tag</label>
                                    <div class="col-md-4">
                                        <select name="tag" id="tag" class="form-control select2  input-larag" required>
                                        @foreach($tags as $tag)
                                            <option value="{{$tag->id}}" >
                                                {{ $tag->{'type'} }}
                                            </option>
                                        @endforeach
                                        </select>
                                    </div>

                                </div>

                                <div class="form-actions">

                                    <div class="row">

                                        <div class="col-md-offset-3 col-md-9">

                                            <button type="submit" class="btn btn-circle green">اضافة Tag</button>

                                        </div>

                                    </div>

                                </div>

                            </form>

                            <!-- END FORM-->

                            <div class="form-group">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                    <div class="portlet-body">
                                        <div class="table-scrollable">
                                            <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                                                <thead>
                                                    <tr>
                                                        <th> م</th>
                                                        <th>  tag </th>
                                                        <th> حذف </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($ClientTags as $k=>$ClientTag)
                                                        <tr>
                                                            <td>{{$k+=1}}</td>
                                                            <td> {{ $ClientTag->clienttag }} </td>

                                                            <td>
                                                                    <a class="delete btn btn-transparent red btn-outline btn-circle btn-sm active" href="{{url('admin/Clients/Clients_Tags_delete/')}}/{{$ClientTag->id}}/{{$clients[0]->id}}" onclick="return confirm('Are you sure you want to delete this item?');"> حذف <i class="fa fa-trash-o"></i></a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                <!-- END SAMPLE TABLE PORTLET-->
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>

    </div>





    </div>









@endsection
