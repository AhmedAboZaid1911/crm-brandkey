@extends('layouts.adminlayout')

@section('pagejs')

    <script src="{{url('/assetsAdmin/pages/scripts/form-samples.min.js')}}" type="text/javascript"></script>

    <script src="{{url('/assetsAdmin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>

    <script src="{{url('js/multiuploadscript.js')}}"></script>

    <script src="{{url('assetsAdmin/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/clockface/js/clockface.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>

    <script src="{{url('js/multiuploadscript.js')}}"></script>

@endsection

@section('pagecss')

    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="{{url('css/multiuploadstyle.css')}}">

    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/clockface/css/clockface.css')}}" rel="stylesheet" type="text/css" />


@endsection



@section('content')

    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i>تعديل {{$posttitlepage}}
            </div>
        </div>
        <div class="portlet-body form">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light bordered" id="blockui_sample_1_portlet_body">
                        <div class="portlet-title">
                            <div class="caption">
                                @if(session()->has('message'))
                                    <div class="alert alert-success">
                                        {{ session()->get('message') }}
                                    </div>
                                @endif
                                <i class="icon-bubble font-green-sharp"></i>
                                <span class="caption-subject font-green-sharp sbold">بيانات العميل </span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                                        <div class="portlet-body">
                                            <div class="table-scrollable">
                                                <form method="post" action="{{url('admin/Clients/Clients_update')}}" class="form-horizontal" enctype="multipart/form-data">
                                                    <div class="form-body">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="id" value="{{$clients[0]->id}}">
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label-client-edit">الاسم بالكامل <span style="color: red"> *</span></label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control input-circle" placeholder="الاسم بالكامل" name="name" value="{{ $clients[0]->name }}" required>
                                                                <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('name') }}</div>
                                                            </div>
                                                            <label class="col-md-2 control-label-client-edit">الاسم بالكامل بالانجليزية</label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control input-circle" placeholder=" الاسم بالكامل بالانجليزية" name="name_en" value="{{ $clients[0]->name_en }}" required>
                                                                <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('name_en	') }}</div>
                                                            </div>
                                                            <label class="col-md-2 control-label-client-edit"> E-mail <span style="color: red"> *</span></label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control input-circle" placeholder="E-Mail" name="email" value="{{ $clients[0]->email }}" required>
                                                                <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('email') }}</div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label-client-edit"> رقم التليفون </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control input-circle" name="number" placeholder="رقم التليفون" value="{{ $clients[0]->number }}" required>
                                                                <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('number') }}</div>
                                                            </div>
                                                            <label class="col-md-2 control-label-client-edit">رقم التليفون الثاني</label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control input-circle" name="number2" placeholder="رقم التليفون الثاني" maxlength="100" value="{{ $clients[0]->number2 }}">
                                                                <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('number2') }}</div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label-client-edit">العنوان</label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control input-circle" name="address" placeholder="العنوان" value="{{ $clients[0]->address }}">
                                                                <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('address') }}</div>
                                                            </div>
                                                            <label class="col-md-2 control-label-client-edit">تابع ل <span style="color: red"> *</span></label>
                                                            <div class="col-md-4">
                                                                <select name="assigned_to" id="assigned_to" class="form-control  select2 input-larag" required>
                                                                    @foreach($user as $users)
                                                                        <option value="{{$users->id}}" <?=($users->id == $clients[0]->assigned_to ) ? 'selected' : '' ?>>
                                                                            {{ $users->{'name'} }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                                <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('cid') }}</div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label-client-edit"> النوع <span style="color: red"> *</span></label>
                                                            <div class="col-md-4">
                                                                <select name="gender" id="gender" class="form-control  select2 input-larag" required>
                                                                    @foreach($gender as $genders)
                                                                        <option value="{{$genders->id}}" <?=($genders->id == $clients[0]->gender ) ? 'selected' : '' ?>>
                                                                            {{ $genders->{'type'} }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                                <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('cid') }}</div>
                                                            </div>
                                                            <label class="col-md-2 control-label-client-edit"> الاهتمام <span style="color: red"> *</span></label>
                                                            <div class="col-md-4">
                                                                <select name="Interest" id="Interest" class="form-control  select2 input-larag" required>
                                                                    @foreach($Interest as $Interests)
                                                                        <option value="{{$Interests->id}}" <?=($Interests->id == $clients[0]->Interest ) ? 'selected' : '' ?>>
                                                                            {{ $Interests->{'type'} }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                                <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('cid') }}</div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label-client-edit"> Lead Source <span style="color: red"> *</span></label>
                                                            <div class="col-md-4">
                                                                <select name="lead" id="lead" class="form-control  select2  input-larag" required>
                                                                    @foreach($leads as $lead)
                                                                        <option value="{{$lead->id}}" <?=($lead->id == $clients[0]->lead ) ? 'selected' : '' ?>>
                                                                            {{ $lead->{'lead'} }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                                <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('cid') }}</div>
                                                            </div>
                                                            <label class="col-md-2 control-label-client-edit"> الجنسية <span style="color: red"> *</span></label>
                                                            <div class="col-md-4">
                                                                <select name="nationality" id="nationality" class="form-control  select2 input-larag" required>
                                                                    @foreach($nationalitys as $nationality)
                                                                        <option value="{{$nationality->id}}" <?=($nationality->id == $clients[0]->nationality ) ? 'selected' : '' ?>>
                                                                            {{ $nationality->{'title_en'} }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                                <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('cid') }}</div>
                                                            </div>
                                                        </div>
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-6 col-md-6">
                                                                    <button type="submit" class="btn btn-circle green">تعديل</button>
                                                                    <a href="{{url('admin/Clients/Clients_view')}}/{{$clients[0]->id}}" id="sample_editable_1_new" class="btn btn-circle yellow-saffron " style="color:#fff;"> الرجوع للعميل
                                                                        <i class="fa fa-backward"></i>
                                                                    </a>
                                                                </div>
                                                                <div class="col-md-offset-6 col-md-6">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    <!-- END SAMPLE TABLE PORTLET-->
                                </div>
                            </div>
                        </div>
                    </div>

            </div>
            </div>
        </div>
    </div>
@endsection



