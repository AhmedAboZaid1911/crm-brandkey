@extends('layouts.adminlayout')
@section('pagejs')


    <script src="{{url('assetsAdmin/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/global/plugins/clockface/js/clockface.js')}}" type="text/javascript"></script>
    <script src="{{url('assetsAdmin/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>

    <script src="{{url('/assetsAdmin/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{url('/assetsAdmin/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/assetsAdmin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
    <script>
        $("#search").keyup(function()
        {
            /*alert($(this).val());*/
            $uid = '<?= $clientsbyuser->assigned_to_id?>';
            $uidn = '<?= $clientsbyuser->assigned_to?>';
            $searchkey = $(this).val();
            $.getJSON("{{url('admin/Clients/searchuser')}}",{searchkey: $searchkey,uid:$uid}, function(j)
            {
               var tbodyhtml = '';
               for (var i = 0; i < j.length; i++)
                {
                    var number = i+1;
                    tbodyhtml+='<tr><td>'+ number +'</td>'
                    +'<td><a  href="{{url('admin/Clients/Clients_view')}}/'+ j[i].id + '">' + j[i].id + '</a></td>'
                    +'<td><a  href="{{url('admin/Clients/Clients_view')}}/'+ j[i].id + '">' + j[i].name + '</a></td>'
                    +'<td><a  href="{{url('admin/Clients/Clients_view')}}/'+ j[i].id + '">' + j[i].name_en + '</a></td>'
                    +'<td><a  href="{{url('admin/Clients/Clients_view')}}/'+ j[i].id + '">' + j[i].number + '</a></td>'
                    +'<td><a  href="{{url('admin/Clients/Clients_view')}}/'+ j[i].id + '">' + j[i].number2 + '</a></td>'
                    +'<td><a  href="{{url('admin/Clients/Clients_view')}}/'+ j[i].id + '">' + j[i].email + '</a></td>'
                    +'<td><a  href="{{url('admin/Clients/Clients_view')}}/'+ j[i].id + '">' + j[i].created_at + '</a></td>'
                    +'<td>' + $uidn + '</td>'

                    +'<td><a class="edit btn btn-transparent red btn-outline btn-circle btn-sm active" href="{{url('admin/Clients/Clients_del')}}/'+ j[i].id +'"> حذف <i class="fa fa-trash"></i></a></td>'

                    '</tr>';
                }
               $("#olddata").html(tbodyhtml);
               $("#olddatap").hide();
               /* */


            });
        });
    </script>


    @endsection
@section('pagecss')

    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{url('/assetsAdmin/global/plugins/clockface/css/clockface.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('/assetsAdmin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap-rtl.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light portlet-fit bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-settings font-red"></i>
                                            <span class="caption-subject font-red sbold uppercase">{{$cattitlepage}}</span>
                                        </div>
                                    </div>
                                    <div class="portlet-body">


                                        @if(session()->has('message'))
                                        <div class="alert alert-success">
                                        {{ session()->get('message') }}
                                        </div>
                                        @endif

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-md-1 control-label">البحث</label>
                                                    <div class="col-md-11">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control input-circle-left" placeholder="البحث" id="search">
                                                            <span class="input-group-addon input-circle-right">
                                                                <i class="fa fa-search"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <form method="get" action="{{url('admin/Clients/searchbyuser')}}" class="form-horizontal" enctype="multipart/form-data">
                                                    <label class="col-md-1 control-label">من</label>
                                                    <div class="col-md-3">
                                                        <div class="input-group input-medium date date-picker" data-date="" data-date-format="yyyy-mm-dd" data-date-viewmode="years">
                                                            <input type="text" class="form-control" name="from" value="" required="required" readonly >
                                                            <span class="input-group-btn">
                                                                <button class="btn default" type="button" required="required">
                                                                    <i class="fa fa-calendar"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                        <!-- /input-group -->
                                                        <span class="help-block">  </span>
                                                    </div>
                                                    <label class="col-md-1 control-label">الي</label>
                                                    <div class="col-md-3">
                                                        <div class="input-group input-medium date date-picker" data-date="" data-date-format="yyyy-mm-dd" data-date-viewmode="years">
                                                            <input type="text" class="form-control" name="to" value=""  readonly >
                                                            <span class="input-group-btn">
                                                                <button class="btn default" type="button">
                                                                    <i class="fa fa-calendar"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                        <!-- /input-group -->
                                                        <span class="help-block">  </span>
                                                    </div>
                                                    <label class="col-md-1 control-label">تابع ل</label>
                                                    <div class="col-md-2">
                                                        <select name="user" id="user" class="form-control select2 input-medium" >
                                                            @foreach ($users as $user )
                                                                <option value="{{$user->id}}" > {{$user->name}} </option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                        <div class="col-md-1 ">
                                                            <button type="submit" class="btn btn-circle green">بحث</button>
                                                        </div>

                                                </form>
                                            </div>
                                        </div>



                                        <table class="tt table table-striped table-hover table-bordered" id="sample_editable_1">
                                            <thead>
                                                <tr>
                                                    <th>م</th>
                                                    <th> كود العميل </th>
                                                    <th> اسم العميل </th>
                                                    <th>  اسم العميل الانجليزية</th>
                                                    <th> رقم الموبيل </th>
                                                    <th> رقم التليفون </th>
                                                    <th> الايميل </th>
                                                    <th> تاريخ الاضافة </th>
                                                    <th> تابع ل </th>
                                                    <th> حذف </th>
                                                </tr>
                                            </thead>
                                            <tbody id="olddata">
                                                <?php $index=((($clientsbyuser->currentPage()-1)*$clientsbyuser->perPage())+1);?>
                                            @foreach($clientsbyuser as $k=>$clientsbyusers)
                                                <tr>
                                                    <td> <a href="{{url('admin/')}}/{{$module}}/Clients_view/{{$clientsbyusers->id}}"> {{$index}} </a></td>
                                                    <td> <a href="{{url('admin/')}}/{{$module}}/Clients_view/{{$clientsbyusers->id}}"> {{ $clientsbyusers->id}} </a></td>
                                                    <td> <a href="{{url('admin/')}}/{{$module}}/Clients_view/{{$clientsbyusers->id}}"> {{  $clientsbyusers->name}} </a> </td>
                                                    <td> <a href="{{url('admin/')}}/{{$module}}/Clients_view/{{$clientsbyusers->id}}"> {{  $clientsbyusers->name_en}} </a> </td>
                                                    <td> <a href="{{url('admin/')}}/{{$module}}/Clients_view/{{$clientsbyusers->id}}"> {{  $clientsbyusers->number}} </a> </td>
                                                    <td> <a href="{{url('admin/')}}/{{$module}}/Clients_view/{{$clientsbyusers->id}}"> {{  $clientsbyusers->number2}} </a> </td>
                                                    <td> <a href="{{url('admin/')}}/{{$module}}/Clients_view/{{$clientsbyusers->id}}"> {{  $clientsbyusers->email}} </a> </td>
                                                    <td> <a href="{{url('admin/')}}/{{$module}}/Clients_view/{{$clientsbyusers->id}}"> {{  $clientsbyusers->created_at}} </a> </td>
                                                    <td>  {{$clientsbyuser->assigned_to}} </td>
                                                    <td>
                                                        <a class="delete btn btn-transparent red btn-outline btn-circle btn-sm active" href="{{url('admin/')}}/{{$module}}/Clients_del/{{$clientsbyusers->id}}" onclick="return confirm('Are you sure you want to delete this item?');"> حذف <i class="fa fa-trash-o"></i></a>
                                                    </td>
                                                </tr>
                                                <?php $index++;?>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="col-md-12 result" id="olddatap">

                                        <div class="col-md-12">
                                            {{ $clientsbyuser->appends(['user' => '25'])->links() }}
                                        </div>
                                    </div>

                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>

@endsection

