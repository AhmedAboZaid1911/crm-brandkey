@extends('layouts.adminlayout')
@section('pagejs')
    <script src="{{url('/assetsAdmin/pages/scripts/form-samples.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/assetsAdmin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
@endsection
@section('pagecss')
<link href="{{url('/assetsAdmin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="portlet box green">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-gift"></i>تعديل بيانات المستخدم </div>
                                                    <div class="tools">
                                                        <a href="javascript:;" class="collapse"> </a>
                                                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                        <a href="javascript:;" class="reload"> </a>
                                                        <a href="javascript:;" class="remove"> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form method="post" action="{{url('/admin/updateprofile')}}" class="form-horizontal" enctype="multipart/form-data">

                                                        <div class="form-body">
                                                        {{ csrf_field() }}
                                                        
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">البريد الالكتروني</label>
                                                                <div class="col-md-4">
                                                                    <input type="email" class="form-control input-circle" name="email" value="{{$uemail}}">
                                                                    
                                                                </div>
                                                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">كلمة المرور</label>
                                                                <div class="col-md-4">
                                                                    <input type="password" class="form-control input-circle" name="password" value="{{$upass}}">
                                                                    
                                                                </div>
                                                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                                            </div>
                                                            
                                                            
                                                        </div>
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-3 col-md-9">
                                                                    <button type="submit" class="btn btn-circle green">تعديل</button>
                                                                    <button type="button" class="btn btn-circle grey-salsa btn-outline">الغاء</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>
                                            
@endsection

