@extends('layouts.app')

@section('content')
 
  <!-- Start Page Title Aria -->
  <div class="page-title">
    <div class="container">
      <div class="row">
        <div class="breadcrumb-block">
          <ol class="breadcrumb-pages">
            <li><a href="#"> الرئيسية </a><i class="fa fa-angle-left"></i></li>
            <li> اتصل بنا </li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <!-- End Page Title Aria -->

    <!-- Start Featured Search -->
    <section id="section-content">
        <div class="container inner-f">
      <div class="row">
        <div class="col-md-8 col-sm-12 col-xs-12 pull-right text-right">
          <div class="Contact-Form">
            <form class="leave-comment contact-form" method="post"  id="cform" autocomplete="on">
                
                {{ csrf_field() }}  
              <div class="Contact-us">
                <div class="form-input">
                  <input type="text" name="name" placeholder=" اسم المرسل " required="">
                  <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first(':') }}</div>
                </div>
                <div class="form-input">
                  <input type="email" name="email" placeholder=" البريد الإلكترونى  " required="">
                  <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('email') }}</div>
                </div>
                <div class="form-textarea">
                  <textarea class="txt-box textArea" name="msg" cols="40" rows="13" id="messageTxt" placeholder=" الرسالة " spellcheck="true" required=""></textarea>
                  <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('msg') }}</div>
                </div>
                <div class="form-submit">
                  <input type="submit" class="btn btn-large main-bg" value=" أرسل ">
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12 pull-right text-right">
          <div class="Contact-Info">
            <div class="Title-Contact">

              <h3> العنوان :  </h3>
            </div>
            <div class="Block-Contact">
              <ul>
                <li>
                  <i class="fa fa-map-marker"></i>
                  <p> ضع هنا العنوان </p>
                </li>
                <li>
                  <i class="fa fa-phone"></i>
                  <p> 01000757018 - 0572270072</p>
                </li>
                <li>
                  <i class="fa fa-fax"></i>
                  <p> 01000757018 - 0572270072</p>
                </li>
                <li>
                  <i class="fa fa-envelope"></i>
                  <p>codeofpage@gmail.com</p>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
        </div>
    </section>
    <!-- End Featured Search -->

@endsection