@extends('layouts.app')

@section('title',  $pagesinfo[3]->{'pagetitle_'.$lang} )

@section('description', $pagesinfo[3]->{'pagemetadesc_'.$lang} )

@section('keyword',  $pagesinfo[3]->{'pagekeywords_'.$lang} )

@section('content')
 

<!-- Bread Crumb -->
        <div class="bread">
            <div class="container">
                <div class="row">
                    <h1>{{ $pagesinfo[3]->{'pagetitle_'.$lang} }}</h1>
                    <p>{!! $pagesinfo[3]->{'text_'.$lang} !!}
                    </p>
                </div>
                <div class="sub-header container">
                    <div class="row">
                        <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a title="Home" itemprop="item" href="{{url($lang.'/index')}}"><span itemprop="name">Home Page</span></a>
                                <meta itemprop="position" content="1" />
                            </li>
                            >
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="active"><span itemprop="name"> {{ $pagesinfo[3]->{'pagetitle_'.$lang} }} </span>
                                <meta itemprop="position" content="2" />
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        
        <!-- Contact-us page -->
        <section class="contact">
            <div class="container">
                <div class="row">
                    <div class="map col-md-12 col-sm-12 col-xs-12">
                        <div id="gmap_canvas">
                           {!!$settings['maps']!!}
                        </div>
                    </div>
                    
                    <div class="contact-form col-md-7 col-sm-6 col-xs-12">
                        <h1 itemprop="name">Send us a message</h1>
                        <form method="POST" action="{{url('/contact-form')}}">
                                {{csrf_field()}}
                        <input type="hidden" name="tripname" value="{{ $pagesinfo[3]->{'pagetitle_'.$lang} }}" >
                        <input type="hidden" name="lang" value="{{ $lang }}" >
                        
                        <div class="form-group">
                            <label for="">Your Name</label>
                            <input type="text" name="name" class="form-control" placeholder="Your name" required="required">
                        </div>
                        <div class="form-group">
                            <label for="">E-mail</label>

                            <input type="email" name="email" class="form-control" placeholder="E-mail" required="required">
                        </div>
                        <div class="form-group">
                            <label for="">Phone</label>
                            <input type="phone" name="mobile" class="form-control" placeholder="Phone" required="required">
                        </div>
                        <div class="form-group">
                            <label for="">Your message</label>

                            <textarea class="form-control" name="msg" placeholder="Your message" required="required"></textarea>
                        </div>
                        <button class="btn" type="submit">Send Message</button>
                    </div>

                    <div class="contact-data col-md-5 col-sm-6 col-xs-12">
                        <h4 itemprop="name">Contact information</h4>
                         <div class="contact-1">
                            <i class="fa fa-map-marker"></i>
                            <span itemprop="name">  </span>
                            <p> {{$settings['address_'.$lang]}}</p>
                        </div>
                        <div class="contact-1">
                            <i class="fa fa-phone"></i>
                            <span itemprop="name">  </span>
                            <p>{{$settings['phone']}}</p>
                        </div>
                        <div class="contact-1">
                            <i class="fa fa-mobile"></i>
                            <span itemprop="name">  </span>
                            <p>{{$settings['mobile']}}</p>
                        </div>
                        <div class="contact-1">
                            <i class="fa fa-envelope-o"></i>
                            <span itemprop="name">  </span>
                            <p>{{$settings['email']}}</p>
                        </div>
                        <div class="contact-1">
                            <i class="fa fa-globe"></i>
                            <span itemprop="name">  </span>
                            <p> {{url('')}} </p>
                        </div>
                    </div>

                </div>
            </div>
        </section>


@endsection