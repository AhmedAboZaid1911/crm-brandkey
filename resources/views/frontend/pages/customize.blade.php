@extends('layouts.app')

@section('title',  $pagesinfo[2]->{'pagetitle_'.$lang} )

@section('description', $pagesinfo[2]->{'pagemetadesc_'.$lang} )

@section('keyword',  $pagesinfo[2]->{'pagekeywords_'.$lang} )

@section('content')
 

<!-- Bread Crumb -->
        <div class="bread">
            <div class="container">
                <div class="row">
                    <h1>{{ $pagesinfo[2]->{'pagetitle_'.$lang} }}</h1>
                    <p>{!! $pagesinfo[2]->{'text_'.$lang} !!}
                    </p>
                </div>
                <div class="sub-header container">
                    <div class="row">
                             <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a title="Home" itemprop="item" href="{{url($lang.'/index')}}"><span itemprop="name">Home Page</span></a>
                                <meta itemprop="position" content="1" />
                            </li>
                            >
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="active"><span itemprop="name"> {{ $pagesinfo[2]->{'pagetitle_'.$lang} }} </span>
                                <meta itemprop="position" content="2" />
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <!-- Products section -->
        <section class="shipping">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="course-details-right">
                            <h3> {{ $pagesinfo[2]->{'pagetitle_'.$lang} }}</h3>
                            <form method="POST" action="{{url('/customize-form')}}">
                                {{csrf_field()}}
                                <input type="hidden" name="tripname" value=" {{ $pagesinfo[2]->{'pagetitle_'.$lang} }} " >
                                <input type="hidden" name="lang" value="{{ $lang }}" >
                                
                            <div class="form-group">
                                <input type="text" name="name" class="form-control" placeholder="Your name" required="required">
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" class="form-control" placeholder="E-mail" required="required">
                            </div>



                                <select name="nationality" class="form-control" required="required">
                                    <option>Select your Nationality *                                                            
                                    </option >
                                    
                                    @foreach ($nationality as $nationalit )
                                    <option value="{{ $nationalit->{'title_'.$lang} }}">
                                    {{ $nationalit->{'title_'.$lang} }}
                                    </option>
                                    @endforeach
                                </select>
 
                            <div class="form-group" style="width: 48%;float:  left;">
                                <input type="Mobile" name="mobile" class="form-control" placeholder="Mobile" required="required">
                            </div>
                            <div class="form-group" style="width: 48%;float: right;">
                                <input type="Mobile" name="whatsapp" class="form-control" placeholder="What's up No." required="required">
                            </div>
                            <div class="form-group registration-date">
                                <label style="color:#333;">From</label>
                                <div class="input-group registration-date-time">
                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>
                                    <input class="form-control" name="from" id="registration-date" type="date" required="required">
                                </div>
                            </div>
                            <div class="form-group registration-date">
                                <label style="color:#333;">To</label>

                                <div class="input-group registration-date-time">
                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>
                                    <input class="form-control" name="to" id="registration-date" type="date" required="required">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-4">
                                    <div class="form-group label-floating is-empty">
                                        <input id="adults" class="form-control" name="adults" value="" placeholder="No of Adults *" >
                                        <span class="material-input"></span></div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="form-group label-floating is-empty">
                                        <input name="child" class="children form-control" value="" id="children" placeholder="Child" onchange="childAge()">
                                        <span class="material-input"></span></div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="form-group label-floating is-empty">
                                        <input name="childage" class="children_age form-control" value="" id="children_age" placeholder="Child Age">
                                        <span class="material-input"></span></div>
                                </div>
                            </div>

                            <textarea class="form-control" name="msg" rows="10" placeholder="Comments regarding your accommodation"></textarea>

                            <div class="row">
                                <div class="col-xs-12">
                                    <button type="submit" class="btn btn-primary" style="width: 100%;">Submit</button>
                                </div>

                            </div>


                        </div>
                    </div>






                </div>

            </div>
        </div>
    </section>

@endsection