@extends('layouts.app')

@section('title', ' Search Results'  )

@section('description', ' Search Results' )

@section('keyword',  ' Search Results' )

@section('content')
 
<!-- Bread Crumb -->
        <div class="bread">
            <div class="row">
                <h1> Search Results  </h1>
            </div>
            <div class="container">
            
                <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a title="Home Page" itemprop="item" href="{{url($lang.'/index')}}"><span itemprop="name">Home Page</span></a>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="active"><span itemprop="name">  Search Results </span>
                        <meta itemprop="position" content="2" />
                    </li>
                </ol>
            </div>
        </div>

        <!-- About page -->
        <section class="blog">
            <div class="container">
                
                <div class="row">
@if ($searchrsult != "" || $searchrsult != null )
                    @for($i=0;$i<count($searchrsult);$i++)
                    <div class="hvr-float-shadow  col-lg-3 col-md-3 col-sm-12 col-xs-12 service" style="visibility: visible; animation-name: fadeInRight;">

                        <div class="thumbnail">
                            <a href="{{url( $lang .'/'. $searchrsult[$i]->maincaturl .'/'.  $searchrsult[$i]->caturl .'/'. $searchrsult[$i]->{'url_en'} )}}"> 
                            <img src="{{url($searchrsult[$i]->pic)}}"  alt="{{$searchrsult[$i]->alt_en}}" class="img-responsive"></a>


                            <div class="caption">
                                <h3><a href="{{url( $lang .'/'. $searchrsult[$i]->maincaturl .'/'.  $searchrsult[$i]->caturl .'/'. $searchrsult[$i]->{'url_en'} )}}">{{ $searchrsult[$i]->{'title_'.$lang} }}</a></h3>
                                <p> {{str_limit(strip_tags($searchrsult[$i]->{'text_' . $lang}),'150','...')}}
                                <span><a href="{{url( $lang .'/'. $searchrsult[$i]->maincaturl .'/'.  $searchrsult[$i]->caturl .'/'. $searchrsult[$i]->{'url_en'} )}}">Read More</a></span>
                                </p>
                                <strong class="hvr-sweep-to-bottom button-more ">
                                    <a href="{{url( $lang .'/'. $searchrsult[$i]->maincaturl .'/'.  $searchrsult[$i]->caturl .'/'. $searchrsult[$i]->{'url_en'} )}}">Book Now </a>
                                </strong>

                            </div>
                            <div class="btn-add ">
                                <a href="{{url( $lang .'/'. $searchrsult[$i]->maincaturl .'/'.  $searchrsult[$i]->caturl .'/'. $searchrsult[$i]->{'url_en'} )}}"> From  {{ $searchrsult[$i]->{'price_'.$lang} }} $</a>
                            </div>
                        </div>
                    </div>
                    
                    @endfor
@else

        No Search Found 

@endif

                    
                </div>
                <script>
                    $(document).ready(function() {

                        $(".filter-button").click(function() {
                            var value = $(this).attr('data-filter');

                            if (value == "all") {
                                //$('.filter').removeClass('hidden');
                                $('.filter').show('1000');
                            } else {
                                //            $('.filter[filter-item="'+value+'"]').removeClass('hidden');
                                //            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');
                                $(".filter").not('.' + value).hide('3000');
                                $('.filter').filter('.' + value).show('3000');

                            }
                        });

                        if ($(".filter-button").removeClass("active")) {
                            $(this).removeClass("active");
                        }
                        $(this).addClass("active");

                    });

                </script>

            </div>
        </section>


@endsection