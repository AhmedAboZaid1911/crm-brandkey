@extends('layouts.app')

@section('title',  'Thank You' )

@section('description',  'Thank You')

@section('keyword',   'Thank You' )

@section('content')
 

<!-- Bread Crumb -->
        <div class="bread">
            <div class="container">
                <div class="sub-header container">
                    <div class="row">
                        <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a title="Home" itemprop="item" href="{{url($lang.'/index')}}"><span itemprop="name">Home Page</span></a>
                                <meta itemprop="position" content="1" />
                            </li>
                            >
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="active"><span itemprop="name"> Thank You </span>
                                <meta itemprop="position" content="2" />
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        
<div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6" style="text-align:center;margin:20px 0;">
                        <h2 class="title-4">Thank You</h2>
                        <p class="title-2">For contacting us, We received your request. For last minute reservation,Please contatct us on +201229029386</p>
                        <button class="btn btn-primary">
                            <a href="{{url($lang.'/index')}}"> Back to Home</a></button>
                    </div>
                    <div class="thank col-md-6">
                        <img img itemprop="image" class="img-responsive" src="{{url($settings['logo'])}}" alt="thankyou" title="thankyou">
                    </div>

                </div>

            </div>
        </div>


    </div>


@endsection