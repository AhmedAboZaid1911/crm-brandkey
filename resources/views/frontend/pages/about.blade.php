@extends('layouts.app')

@section('title', $about[0]->{'title_'.$lang})

@section('description', $about[0]->{'desc_'.$lang})
@section('keyword', $about[0]->{'keyword_'.$lang })

@section('content')
 
<!-- Bread Crumb -->
        <div class="bread">
            <div class="container">
                <div class="row">
                    <h1>{{$about[0]->{'title_'.$lang} }}</h1>
                    
                </div>
                <div class="sub-header container">
                    <div class="row">
                        <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a title="Home" itemprop="item" href="{{url($lang.'/index/')}}"><span itemprop="name">Home</span></a>
                                <meta itemprop="position" content="1" />
                            </li>
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="active"><span itemprop="name"> {{ $about[0]->{'title_'.$lang} }} </span>
                                <meta itemprop="position" content="2" />
                            </li>
                        </ol>
                    </div>
                </div>


            </div>

        </div>

        <!-- About page -->
        
        <section class="about-us">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-5">
                            <img img itemprop="image" class="img-responsive" src="{{url($about[0]->{'pic'}) }}" alt="{{ $about[0]->{'alt_'.$lang} }}" title="{{ $about[0]->{'alt_'.$lang} }}">
                        </div>
                        <p itemprop="description">
                        {!! $about[0]->{'text_'.$lang} !!}
                        </p>

                    </div>

                </div>
            </div>

        </section>
    </div>


@endsection