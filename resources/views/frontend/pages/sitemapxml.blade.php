<?php
//header("Content-Type: application/xml; charset=utf-8");
print '<?xml version="1.0" encoding="UTF-8"?>';
header('Content-type: application/xml');
header('Pragma: public');
header('Cache-control: private');
header('Expires: -1');
?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<url>
  <loc>{{url('/en')}}</loc>
  <changefreq>daily</changefreq>
  <lastmod><?= date('Y-m-d', time())?></lastmod>
  <priority>1.0</priority>
</url>
<url>
  <loc>{{url('/en/index')}}</loc>
  <changefreq>daily</changefreq>
  <lastmod><?= date('Y-m-d', time())?></lastmod>
  <priority>1.0</priority>
</url>
<url>
  <loc>{{url('en/contact-us')}}</loc>
  <changefreq>daily</changefreq>
  <lastmod><?= date('Y-m-d', time())?></lastmod>
  <priority>1.0</priority>
</url>
<url>
  <loc>{{url('en/About-Us')}}</loc>
  <changefreq>daily</changefreq>
  <lastmod><?= date('Y-m-d', time())?></lastmod>
  <priority>1.0</priority>
</url>

<url>
  <loc>{{url('en/thankyou')}}</loc>
  <changefreq>daily</changefreq>
  <lastmod><?= date('Y-m-d', time())?></lastmod>
  <priority>1.0</priority>
</url>
<url>
  <loc>{{url('en/search')}}</loc>
  <changefreq>daily</changefreq>
  <lastmod><?= date('Y-m-d', time())?></lastmod>
  <priority>1.0</priority>
</url>
<url>
  <loc>{{url('en/customize-your-tour')}}</loc>
  <changefreq>daily</changefreq>
  <lastmod><?= date('Y-m-d', time())?></lastmod>
  <priority>1.0</priority>
</url>
<url>
  <loc>{{url('en/albums')}}</loc>
  <changefreq>daily</changefreq>
  <lastmod><?= date('Y-m-d', time())?></lastmod>
  <priority>1.0</priority>
</url>
<url>
  <loc>{{url('en/videos')}}</loc>
  <changefreq>daily</changefreq>
  <lastmod><?= date('Y-m-d', time())?></lastmod>
  <priority>1.0</priority>
</url>

@for($i=0;$i<count($pagesinfo);$i++)
<url>
  <loc>{{url('en/' . $pagesinfo[$i]->pagename_en)}}</loc>
  <changefreq>daily</changefreq>
  <lastmod><?= date('Y-m-d', time())?></lastmod>
  <priority>1.0</priority>
</url>
@endfor

<?php for($i=0;$i<count($trips);$i++) { 
?>

<url>

  <loc>{{url('en/' . $trips[$i]->url_en)}}</loc>

  <changefreq>daily</changefreq>

  <lastmod><?= date('Y-m-d', time())?></lastmod>

  <priority>1.0</priority>

</url>

<?php foreach($trips[$i]->tours as $tours) { 
?>

<url>

  <loc>{{url('en/' . $trips[$i]->url_en . '/' . $tours->url_en)}}</loc>

  <changefreq>daily</changefreq>

  <lastmod><?= date('Y-m-d', time())?></lastmod>

  <priority>1.0</priority>

</url>

<?php


}


?>

<?php


}


?>

</urlset>