@extends('layouts.app')

@section('title', $maincatss[0]->{'title_'.$lang}  )

@section('description', $maincatss[0]->{'desc_'.$lang} )

@section('keyword',  $maincatss[0]->{'keyword_'.$lang} )

@section('content')
 
<!-- Bread Crumb -->
<!-- Bread Crumb -->
                <div class="bread">
            <div class="container">
                <div class="row">
                    <h1> {{ $maincatss[0]->{'title_'.$lang} }} </h1>
                    <p> {!! $maincatss[0]->{'text_'.$lang} !!}
                    </p>
                </div>
                <div class="sub-header container">
                    <div class="row">

                        <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a title="Home Page" itemprop="item" href="{{url($lang.'/index')}}"><span itemprop="name">Home Page</span></a>
                                <meta itemprop="position" content="1" />
                            </li>
                            
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="active"><span itemprop="name">{{ $maincatss[0]->{'title_'.$lang} }}</span>
                                <meta itemprop="position" content="2" />
                            </li>
                        </ol>
                        
                    </div>
                </div>
            </div>
        </div>
      <!-- About page -->
        <section class="ship-home">
            <div class="container">
                <div class="row">
                @for($i=0;$i<count($cat);$i++)
                    
                
                    <div class="wow fadeInLeft hvr-float-shadow  col-lg-4 col-md-4 col-sm-12 col-xs-12 service">

                        <div class="thumbnail">
                            <a href="{{url($lang.'/'. $maincatss[0]->{'url_'.$lang}.'/'.$cat[$i]->{'url_'.$lang})}}"> 
                            <img data-src="" title="{{ $cat[$i]->{'alt_'.$lang} }}" alt="{{ $cat[$i]->{'alt_'.$lang} }}" class="img-responsive" src="{{url($cat[$i]->pic)}}" data-holder-rendered="true"></a>
                            <div class="caption">
                                <h3><a href="{{url($lang.'/'. $maincatss[0]->{'url_'.$lang}.'/'.$cat[$i]->{'url_'.$lang})}}"> {{ $cat[$i]->{'title_'.$lang} }} </a></h3>
                                <p>{{str_limit(strip_tags($cat[$i]->{'text_' . $lang}),'150','...')}}
                                </p>
                                <a class="text-theme-colored font-13 font-weight-600" href="{{url($lang.'/'. $maincatss[0]->{'url_'.$lang}.'/'.$cat[$i]->{'url_'.$lang})}}">View Details →</a>
                            </div>

                        </div>
                    </div>
                @endfor    

                </div>
            </div>
        </section>
    </div>



@endsection