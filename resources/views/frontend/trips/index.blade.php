@extends('layouts.app')

@section('title',  $pagesinfo[0]->{'pagetitle_en'} )

@section('description', $pagesinfo[0]->{'pagemetadesc_en'} )

@section('keyword',  $pagesinfo[0]->{'pagekeywords_en'} )

@section('content')
 
<!-- Bread Crumb -->
        <div class="bread">
            <div class="container">
                <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a title="الرئيسية" itemprop="item" href="{{url('/index')}}"><span itemprop="name">الرئيسية</span></a>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="active"><span itemprop="name"> {{ $pagesinfo[0]->{'pagetitle_en'} }} </span>
                        <meta itemprop="position" content="2" />
                    </li>
                </ol>
            </div>
        </div>

        <!-- About page -->
        <section class="blog">
            <div class="container">
                <h2> {{ $pagesinfo[0]->{'pagetitle_en'} }} </h2>
                <div class="row">

                    <div align="center">            
                    <p>
                    {!! $pagesinfo[0]->{'text_en'} !!}
                    </p>
                    </div>
                    <br/>


                    @for($i=0;$i<count($cats);$i++)
                    <div class=" gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-12 filter hdpe">
                        <a href="{{url('/menu' .'/'.  $cats[$i]->{'url_en'} )}}"> <img src="{{url($cats[$i]->pic)}}"  alt="{{$cats[$i]->alt_en}}" class="img-responsive"></a>


                        <h2><a href="{{url('/menu' .'/'.  $cats[$i]->{'url_en'} )}}">{{$cats[$i]->title_en}}</a></h2>
                    </div>
                    @endfor



                    
                </div>
                <script>
                    $(document).ready(function() {

                        $(".filter-button").click(function() {
                            var value = $(this).attr('data-filter');

                            if (value == "all") {
                                //$('.filter').removeClass('hidden');
                                $('.filter').show('1000');
                            } else {
                                //            $('.filter[filter-item="'+value+'"]').removeClass('hidden');
                                //            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');
                                $(".filter").not('.' + value).hide('3000');
                                $('.filter').filter('.' + value).show('3000');

                            }
                        });

                        if ($(".filter-button").removeClass("active")) {
                            $(this).removeClass("active");
                        }
                        $(this).addClass("active");

                    });

                </script>

            </div>
        </section>


@endsection