@extends('layouts.app') @section('title', $item[0]->{'title_'.$lang} )
@section('description', $item[0]->{'desc_'.$lang} ) 
@section('keyword', $item[0]->{'keyword_'.$lang} ) 
@section('pagejs')
<script src="{{url('/assets/js/star-rating.min.js')}}" type="text/javascript"></script>
<script>         
 $(function(){                   
  $('.rating').on('rating.change', function(event, value, caption) {    
                        comId = $(this).attr('id');   
                        //alert(comId);    
                        $.ajax({url: "{{url('/rating')}}",dataType: "json", data: {vote:value, comId:comId, type:'trips'}, success: function( data ) {   
                           },  error: function(e) { 
                                   console.log(e); 
                                  },  timeout: 30000  
                        });              
    });                   
  });    
</script>
@endsection
@section('content')

<!-- Bread Crumb -->
<div class="bread2">
    <div class="container">
        <div class="row">
            <h1> {{ $item[0]->{'title_'.$lang} }} </h1>
            <p> {!! $item[0]->{'text_'.$lang} !!}
            </p>
        </div>
        <div class="sub-header container">
            <div class="row">

                <ol class="breadcrumb2" itemscope itemtype="http://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a title="Home Page" itemprop="item" href="{{url($lang.'/index')}}"><span itemprop="name">Home Page</span></a>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a title="{{ $maincatss[0]->{'title_'.$lang} }}" itemprop="item" href="{{url($lang .'/'.  $maincatss[0]->{'url_'.$lang})}}"><span itemprop="name"> {{ $maincatss[0]->{'title_'.$lang} }} </span></a>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a title="{{ $catss[0]->{'title_'.$lang} }}" itemprop="item" href="{{url($lang .'/'.  $maincatss[0]->{'url_'.$lang} .'/'.  $catss[0]->{'url_'.$lang})}}"><span itemprop="name"> {{ $catss[0]->{'title_'.$lang} }} </span></a>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="active"><span itemprop="name"> {{ $item[0]->{'title_'.$lang} }} </span>
                        <meta itemprop="position" content="2" />
                    </li>
                </ol>

            </div>
        </div>
    </div>
</div>

<!-- Products section -->
<section class="shipping">
    <div class="container">
        <div class="row">
            <div class="col-md-12">


                <div class="booking-block col-lg-4 col-md-4 hidden-sm hidden-xs">
                    <div class="course-details-right" data-spy="affix" data-offset-top="500" data-offset-bottom="1480">
                        <h3>Booking Now</h3>
                        <form method="POST" action="{{url('/trip-form')}}">
                            {{csrf_field()}}
                            <input type="hidden" name="tripname" value="{{ $item[0]->{'title_'.$lang} }}">
                            <input type="hidden" name="lang" value="{{ $lang }}">

                            <div class="form-group">
                                <input type="text" name="name" class="form-control" placeholder="Your name" required="required">
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" class="form-control" placeholder="E-mail" required="required">
                            </div>



                            <select name="nationality" class="form-control" required="required">
                                    <option>Select your Nationality *                                                            
                                    </option >
                                    
                                    @foreach ($nationality as $nationalit )
                                    <option value="{{ $nationalit->{'title_'.$lang} }}">
                                    {{ $nationalit->{'title_'.$lang} }}
                                    </option>
                                    @endforeach
                                </select>

                            <div class="form-group" style="width: 48%;float:  left;">
                                <input type="Mobile" name="mobile" class="form-control" placeholder="Mobile" required="required">
                            </div>
                            <div class="form-group" style="width: 48%;float: right;">
                                <input type="Mobile" name="whatsapp" class="form-control" placeholder="What's up No." required="required">
                            </div>
                            <div class="form-group">
                                <div class="date">

                                    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
                                    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
                                    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
                                    <label style="color:#333;">From</label>


                                    <div class='input-group date' id='datetimepicker1'>
                                        <input type='text' name="from" class="form-control" required="required">
                                        <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>




                                    <label style="color:#333;">To</label>


                                    <div class='input-group date' id='datetimepicker2'>
                                        <input type='text' name="to" class="form-control" required="required">
                                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>




                                </div>
                            </div>

                            <script>
                                $(function() {
                                    $('#datetimepicker1,#datetimepicker2').datepicker({
                                        format: "dd/mm/yyyy",
                                        language: "es",
                                        autoclose: true,
                                        todayHighlight: true
                                    });
                                });

                            </script>


                            <div class="form-group" style="width: 32%;float: right;margin-right: 2px;">
                                <input type="text" name="childage" class="form-control" placeholder="Child Age *">
                            </div>
                            <div class="form-group" style="width: 30%;float: right;width: 32%;float: right;margin-right: 2px;">
                                <input type="text" name="child" class="form-control" placeholder="No. Child *">
                            </div>
                            <div class="form-group" style="width: 30%;float: right;width: 33%;float: right;margin-right: 2px;">
                                <input type="text" name="adults" class="form-control" placeholder="No. Adults *">
                            </div>




                            <textarea class="form-control" name="msg" rows="4" placeholder="Please advise your tour requirements"></textarea>

                            <div class="row">
                                <div class="col-xs-12">
                                    <button type="submit" class="btn btn-primary" style="width: 100%;">BOOK NOW</button>
                                </div>

                            </div>


                        </form>

                    </div>
                </div>



                <div class="booking-block hidden-lg hidden-md col-sm-12 col-xs-12">


                    <div class="course-details-right" id="booking">
                        <h3>Booking Now</h3>
                        <form method="POST" action="{{url('/trip-form')}}">
                            {{csrf_field()}}
                            <input type="hidden" name="tripname" value="{{ $item[0]->{'title_'.$lang} }}">

                            <div class="form-group">
                                <input type="text" name="name" class="form-control" placeholder="Your name" required="required">
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" class="form-control" placeholder="E-mail" required="required" >
                            </div>



                            <select name="nationality" class="form-control" required="required" >
                                    <option>Select your Nationality *                                                            
                                    </option >
                                    
                                    @foreach ($nationality as $nationalit )
                                    <option value="{{ $nationalit->{'title_'.$lang} }}">
                                    {{ $nationalit->{'title_'.$lang} }}
                                    </option>
                                    @endforeach
                                </select>

                            <div class="form-group" style="width: 48%;float:  left;">
                                <input type="Mobile" name="mobile" class="form-control" placeholder="Mobile" required="required">
                            </div>
                            <div class="form-group" style="width: 48%;float: right;">
                                <input type="Mobile" name="whatsapp" class="form-control" placeholder="What's up No." required="required">
                            </div>
                            <div class="form-group">
                                <div class="date">

                                    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
                                    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
                                    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
                                    <label style="color:#333;">From</label>


                                    <div class='input-group date' id='datetimepicker1'>
                                        <input type='text' name="from" class="form-control" required="required">
                                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>




                                    <label style="color:#333;">To</label>


                                    <div class='input-group date' id='datetimepicker2'>
                                        <input type='text' name="to" class="form-control" required="required">
                                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>




                                </div>
                            </div>

                            <script>
                                $(function() {
                                    $('#datetimepicker1,#datetimepicker2').datepicker({
                                        format: "dd/mm/yyyy",
                                        language: "es",
                                        autoclose: true,
                                        todayHighlight: true
                                    });
                                });

                            </script>

                            <div class="form-group" style="width: 32%;float: right;margin-right: 2px;">
                                <input type="text" name="childage" class="form-control" placeholder="Child Age *">
                            </div>
                            <div class="form-group" style="width: 30%;float: right;width: 32%;float: right;margin-right: 2px;">
                                <input type="text" name="child" class="form-control" placeholder="No of Child *">
                            </div>
                            <div class="form-group" style="width: 30%;float: right;width: 33%;float: right;margin-right: 2px;">
                                <input type="text" name="adults" class="form-control" placeholder="No of Adults *">
                            </div>

                            <textarea class="form-control" name="msg" rows="4" placeholder="Please advise your tour requirements"></textarea>

                            <div class="row">
                                <div class="col-xs-12">
                                    <button type="submit" class="btn btn-primary" style="width: 100%;">BOOK NOW</button>
                                </div>

                            </div>
                        </form>

                    </div>
                </div>










                <div class="col-md-8 col-sm-6 col-xs-12">

                    <div class="program col-md-12 wow fadeInLeft" style="visibility: visible;">
                        <div class="bs-example" data-example-id="carousel-with-captions">
                            <div class="carousel slide" id="carousel-example-captions" data-ride="carousel">
                                <ol class="carousel-indicators">
                                @for($i=0;$i<count($images);$i++) 
                                    @if ($images[$i]->pic != null || $images[$i]->pic != '')
                                    <li data-target="#carousel-example-captions" data-slide-to="{{$i}}" class=" @if( $i == 0) active @endif"></li>                                   
                                @endif
                                @endfor
                                </ol>

                                <div class="carousel-inner" role="listbox">

                                    @for($i=0;$i<count($images);$i++) 
                                    @if ($images[$i]->pic != null || $images[$i]->pic != '')
                                    <div class="item @if( $i == 0) active @endif">
                                        <img style="height: 400px !important;" alt="{{$images[$i]->{'picalt_' . $lang} }}" title="{{$images[$i]->{'picalt_' . $lang} }}" data-src="holder.js/900x500/auto/#777:#777" src="{{url($images[$i]->pic)}}" data-holder-rendered="true">
                                </div>
                                @endif
                                @endfor
                            </div>
                            <a href="#carousel-example-captions" class="left carousel-control" role="button" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a>
                            <a href="#carousel-example-captions" class="right carousel-control" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <span class="sr-only">Next</span> </a>
                        </div>
                    </div>

                    <h1 class="col-md-6 col-sm-6 col-xs-12" itemprop="name"> From {!! $item[0]->{'price_'.$lang} !!} $ </h1>
                    <div class="col-md-6 col-sm-6 col-xs-12 rating">
                        @if($vcount > 0)
                            <input value="{{$vote/$vcount}}" id="{{$item[0]->id}}" type="number" class="rating" min=0 max=5 step=1 data-size="md" data-stars="5" disabled> 
                        @else                     
                            <input value="0" id="{{$item[0]->id}}" type="number" class="rating" min="0" max="5" step="1" data-size="md" data-stars="5" disabled>         
                        @endif
                    </div>
                    <div class="col-xs-4 col-sm-4 features-item">

                        <i class="far fa-clock"></i>


                        <h3>DURATION </h3>

                        <p>
                            {!! $item[0]->{'duration_'.$lang} !!}
                        </p>
                    </div>
                    <div class="col-xs-4 col-sm-4 features-item">

                        <i class="fas fa-plane"></i>

                        <h3>Type </h3>

                        <p>
                            {!! $item[0]->{'type_'.$lang} !!}
                        </p>

                    </div>
                    <div class="col-xs-4 col-sm-4 features-item">

                        <i class="far fa-calendar-alt"></i>

                        <h3>Run</h3>

                        <p>
                            {!! $item[0]->{'run_'.$lang} !!}
                        </p>

                    </div>


                    <div class="bs-example bs-example-tabs" data-example-id="togglable-tabs">
                        <ul class="nav nav-tabs" id="myTabs" role="tablist">
                            <li role="presentation" class="active"><a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">Full Info</a></li>
                            <li role="presentation" class="hidden-xs"><a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile" aria-expanded="false">Highlights</a></li>
                            <li role="presentation" class="hidden-xs"><a href="#Inclusions" role="tab" id="Inclusions-tab" data-toggle="tab" aria-controls="Inclusions" aria-expanded="false">Inclusions</a></li>
                            <li role="presentation" class="hidden-xs"><a href="#Exclusions" role="tab" id="Exclusions-tab" data-toggle="tab" aria-controls="Exclusions" aria-expanded="false">Exclusions</a></li>
                            <li role="presentation" class="hidden-xs"><a href="#Pricing" role="tab" id="Pricing-tab" data-toggle="tab" aria-controls="Pricing" aria-expanded="false">Pricing</a></li>
                            <li role="presentation" class="hidden-xs"><a href="#Policies" role="tab" id="Policies-tab" data-toggle="tab" aria-controls="Policies" aria-expanded="false"> Policies</a></li>
                            <li role="presentation" class="hidden-xs"><a href="#Reviews" role="tab" id="Reviews-tab" data-toggle="tab" aria-controls="Reviews" aria-expanded="false"> Reviews</a></li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade active in" role="tabpanel" id="home" aria-labelledby="home-tab">
                                <div class="descrption">
                                    <h2><i class="fa fa-info"></i> Important Info</h2>
                                    <div class="data-day">
                                        <ul>
                                            <li><strong><i class="fa fa-clock-o"></i>Tour duration:</strong> {!! $item[0]->{'duration_'.$lang} !!} </li>
                                            <li><strong><i class="fa fa-map-marker"></i>Location:</strong>  
                                            @foreach ($loct as $k => $loc )
                                            {{$loc->title_en }} 
                                            @if ($k != count($loct)-1)
                                                -
                                            @endif  
                                            @endforeach
                                            </li>
                                            <li><strong><i class="fa fa-plane"></i>Tour Type:</strong> {!! $item[0]->{'type_'.$lang} !!} </li>
                                            <li><strong><i class="fa fa-calendar"></i> Scheduling:</strong>{!! $item[0]->{'run_'.$lang} !!}</li>

                                            {!! $item[0]->{'text_'.$lang} !!}
                                        </ul>
                                    </div>
                                </div>

                                <div class="descrption">
                                    <h2><i class="fa fa-star"></i> Highlights</h2>
                                    <div class="data-day">
                                        <ul>
                                            {!! $item[0]->{'Highlights_'.$lang} !!}

                                        </ul>
                                    </div>
                                </div>

                                <div class="descrption">
                                    <h2><i class="fas fa-plane"></i> Tour Description</h2>
                                    
                                    <div class="data-day">
                                        <ul>
                                            {!! $item[0]->{'Description_'.$lang} !!}
                                        </ul>
                                    </div>


                                    <div class="bs-example" data-example-id="collapse-accordion">
                                        <div class="panel-group" role="tablist" id="accordion" aria-multiselectable="true">
                                            @for($i=1;$i<=15;$i++) 
                                            @if ( $item[0]->{'daytitle'.$i.'_'.$lang} != '' || $item[0]->{'daytitle'.$i.'_'.$lang} != null )
                                                
                                            
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading{{$i}}">
                                                    <h4 class="panel-title">
                                                        <a href="#collapse{{$i}}" role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="true" aria-controls="collapseOne" class=""> 
                                                            {!! $item[0]->{'daytitle'.$i.'_'.$lang} !!} 
                                                            </a> </h4>
                                                </div>
                                                <div class="panel-collapse collapse in" role="tabpanel" id="collapse{{$i}}" aria-labelledby="headingOne" aria-expanded="true" style="">
                                                    <div class="panel-body">
                                                        {!! $item[0]->{'daycontent'.$i.'_'.$lang} !!}
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                            @endfor

                                        </div>
                                    </div>
                                </div>




                            <div class="descrption">
                                <h2><i class="fa fa-check"></i> Inclusions</h2>
                                <div class="data-day">
                                    <ul>
                                        {!! $item[0]->{'Inclusions_'.$lang} !!}
                                    </ul>
                                </div>
                            </div>

                            <div class="descrption">
                                <h2><i class="fa fa-star"></i> Exclusions</h2>
                                <div class="data-day">
                                    <ul>
                                        {!! $item[0]->{'Exclusions_'.$lang} !!}
                                    </ul>
                                </div>
                            </div>
                            <div class="descrption">
                                <h2><i class="fa fa-pencil"></i> Policies</h2>
                                <div class="data-day">
                                    <ul>
                                        
                                    </ul>
                                </div>
                            </div>

                            <div class="descrption">
                                <h2><i class="fa fa-money"></i> Pricing</h2>

                                <div class="data-day prices_box">
                                    <ul>
                                        {!! $item[0]->{'Pricing_'.$lang} !!}
                                    </ul>
                                </div>



                            </div>



                        </div>
                        <div class="tab-pane fade" role="tabpanel" id="profile" aria-labelledby="profile-tab">
                            <div class="descrption">
                                <h2><i class="fa fa-star"></i> Highlights</h2>
                                <div class="data-day">
                                    <ul>
                                        {!! $item[0]->{'Highlights_'.$lang} !!}
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" role="tabpanel" id="Inclusions" aria-labelledby="Inclusions-tab">
                            <div class="descrption">
                                <h2><i class="fa fa-check"></i> Inclusions</h2>
                                <div class="data-day">
                                    <ul>
                                        {!! $item[0]->{'Inclusions_'.$lang} !!}
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" role="tabpanel" id="Exclusions" aria-labelledby="Exclusions-tab">
                            <div class="descrption">
                                <h2><i class="fa fa-star"></i> Exclusions</h2>
                                <div class="data-day">
                                    <ul>
                                        {!! $item[0]->{'Exclusions_'.$lang} !!}
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" role="tabpanel" id="Pricing" aria-labelledby="Pricing-tab">

                            <div class="descrption">
                                <h2><i class="fa fa-money"></i> Pricing</h2>

                                <div class="data-day prices_box">
                                    <ul>
                                        {!! $item[0]->{'Pricing_'.$lang} !!}
                                    </ul>
                                </div>



                            </div>
                        </div>
                        <div class="tab-pane fade" role="tabpanel" id="Policies" aria-labelledby="Policies-tab">
                            <p>
                                {!! $Policies[0]->{'text_'.$lang} !!}
                            </p>
                        </div>
                        <div class="tab-pane fade" role="tabpanel" id="Reviews" aria-labelledby="Reviews-tab">
                            <div class="descrption">
                                <!--   @foreach ( $rate as $rates )
                                            <div class="media">
                                                <div class="media-left">                                                    
                                                </div>
                                                <div class="media-body">
                                                     <h5 class="media-heading"><strong>{{$rates->name}}</strong></h5>
                                                    <p> {{$rates->comments}} </p>
                                                </div>
                                            </div>
                                            <hr>
                                            @endforeach -->









                                <div class="col-md-12 share">
                                    <script src="{{url('assets/js/review.js')}}"></script>
                                    <h2>Reviews</h2>
                                    <div class="well well-sm">
                                        <div class="text-left">
                                            <a class="btn btn-success btn-danger" href="#reviews-anchor" id="open-review-box">Add Your Review </a>


                                        </div>

                                        <div class="row" id="post-review-box" style="display:none;">
                                            <div class="col-md-12">
                                                <form action="{{url('/trip-reiveiw')}}" method="post" style=" width: 100%;">
                                                    {{csrf_field()}}
                                                    <input type="hidden" name="lang" value="{{ $lang }}">
                                                    <input type="hidden" name="itemid" value="{{$item[0]->id}}">
                                                    <input type="hidden" name="tripname" value="{{ $item[0]->{'title_'.$lang} }}">
                                                    <div class="form-group" style="width: 48%;float:  right;">
                                                        <input type="E-Mail" name="email" class="form-control" placeholder="email" required>
                                                    </div>
                                                    <div class="form-group" style="width: 48%;float:  left;">
                                                        <input type="Full Name" name="name" class="form-control" placeholder="Name" required>
                                                    </div>

                                                    <textarea class="form-control animated" cols="50" id="new-review" name="comments" placeholder="Enter your review here..." rows="5" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 54px;" required></textarea>
                                                    <div class="text-right">
                                                        <div class="rating">
                                                                                
                                                            <input value="0" id="{{$item[0]->id}}" type="number" class="rating" min="0" max="5" step="1" data-size="md" data-stars="5">         
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="text-left">
                                                        <a class="btn btn-danger btn-sm" href="#" id="close-review-box" style="display:none; margin-right: 10px;">
                                                                        <span class="glyphicon glyphicon-remove"></span>Cancel
                                                                    </a>


                                                        <input class="btn btn-success" type="submit" value="Add Review">

                                                    </div>


                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <h5>Relate Review</h5>
                                    @foreach ( $rate as $rates )
                                    <div class="media">
                                        <div class="media-left">
                                        </div>



                                        <div class="media-body">
                                            <h5 class="media-heading"><strong>{{$rates->name}}</strong></h5>


                                            <p>{{$rates->comments}}</p>
                                        </div>

                                    </div>

                                    <hr> @endforeach
                                    <div id="TA_selfserveprop831" class="TA_selfserveprop">
                                        <ul id="dENIMvI" class="TA_links xOzU7rQImSif">
                                            <li id="8LK7vnibg" class="4VFW9Z0SO">
                                                <a target="_blank" href="https://www.tripadvisor.com/"><img src="https://www.tripadvisor.com/img/cdsi/img2/branding/150_logo-11900-2.png" alt="TripAdvisor"/></a>
                                            </li>
                                        </ul>
                                    </div>
                <script async src="https://www.jscache.com/wejs?wtype=selfserveprop&amp;uniq=831&amp;locationId=8747035&amp;lang=en_US&amp;rating=true&amp;nreviews=4&amp;writereviewlink=true&amp;popIdx=true&amp;iswide=true&amp;border=false&amp;display_version=2"></script>

                                </div>



                            </div>
                        </div>
                    </div>


                </div>










            </div>


        </div>

    </div>
    </div>
</section>


<section class="course-home">
    <div class="container">
        <h2><a href="#">Related Tours</a></h2>

    </div>

    <div class="ship-home container">
        <div class="row">
            @for($i=0;$i
            <count($related);$i++) 
                
                <div class="wow fadeInRight hvr-float-shadow  col-lg-3 col-md-3 col-sm-12 col-xs-12 service" style="visibility: visible; animation-name: fadeInRight;">

                    <div class="thumbnail">
                        <a href="{{url($lang .'/' . $maincatss[0]->{'url_'.$lang}.'/'. $catss[0]->url_en .'/'. $related[$i]->url_en)}}"> 
                                    <img data-src="{{url($related[$i]->pic)}}" alt="{{ $related[$i]->{'title_'.$lang} }}" class="img-responsive" src="{{url($related[$i]->pic)}}" data-holder-rendered="true"></a>

                        <div class="caption">
                            <h3><a href="{{url($lang .'/' . $maincatss[0]->{'url_'.$lang}.'/'. $catss[0]->url_en .'/'. $related[$i]->url_en)}}">{{ $related[$i]->{'title_'.$lang} }}</a></h3>
                            <p> {{str_limit(strip_tags($related[$i]->{'text_' . $lang}),'150','...')}}
                                <span><a href="{{url($lang .'/'. $maincatss[0]->{'url_'.$lang}.'/'. $catss[0]->url_en .'/'. $related[$i]->url_en)}}">Read More</a></span>
                            </p>
                            <strong class="hvr-sweep-to-bottom button-more ">
                                        <a href="{{url($lang .'/'. $maincatss[0]->{'url_'.$lang} .'/'. $catss[0]->url_en .'/'. $related[$i]->url_en)}}">Book Now </a>
                                        </strong>

                        </div>
                        <div class="btn-add ">
                            <a href="{{url($lang .'/' . $maincatss[0]->{'url_'.$lang}.'/'. $catss[0]->url_en .'/'. $related[$i]->url_en)}}"> FROM  {{ $related[$i]->{'price_'.$lang} }} $ </a>
                        </div>
                    </div>
                </div>
                @endfor


        </div>
    </div>
</section>

    <div class="booking hidden-lg hidden-md hidden-sm">
            <a class="page-scroll" href="#booking">
                <li> Booking </li>
            </a>

    </div>
    
    
        <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js "></script>

    <script src="{{url('/assets/js/agency.js')}}" type="text/javascript"></script>

    
    
@endsection
