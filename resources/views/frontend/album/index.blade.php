@extends('layouts.app')

@section('title',  $pagesinfo[0]->{'pagetitle_'.$lang} )

@section('description', $pagesinfo[0]->{'pagemetadesc_'.$lang} )

@section('keyword',  $pagesinfo[0]->{'pagekeywords_'.$lang} )

@section('content')
 

                  

        <div class="bread">
            
            <div class="container">
                <div class="row">
                    <h1> {{ $pagesinfo[0]->{'pagetitle_'.$lang} }} </h1>
                    <p>  {!! $pagesinfo[0]->{'text_'.$lang} !!} </p>
                </div>
                <div class="sub-header container">
                    <div class="row">            
                        <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a title="Home" itemprop="item" href="{{url($lang.'/index')}}"><span itemprop="name">Home</span></a>
                                <meta itemprop="position" content="1" />
                            </li>
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="active"><span itemprop="name"> {{ $pagesinfo[0]->{'pagetitle_'.$lang} }} </span>
                                <meta itemprop="position" content="2" />
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            
            <div class="row">
                @for($i=0;$i<count($album);$i++)
                <div class="col-md-4">
                    <div class="basic-widgets">
                        <div class="project-hover">
                            <img src="{{url($album[$i]->img)}}" class="img-responsive transition">
                            <div class="text-view transition text-center">
                                <h3> {{$album[$i]->{'title_'.$lang} }} </h3>
                            </div>
                            <div class="btn-view transition text-center">
                                <a href="{{url($lang.'/albums/'.$album[$i]->{'url_'.$lang})}}" class="btn btn-default"> View Album</a>
                            </div>
                        </div>
                    </div>
                </div>
                @endfor
            </div>
        </div>

    </div>


@endsection