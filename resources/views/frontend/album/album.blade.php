@extends('layouts.app')

@section('title', $album[0]->{'title_'.$lang})

@section('description', $album[0]->{'desc_'.$lang})

@section('keyword', $album[0]->{'keyword_'.$lang})



@section('content')

 
<!-- Bread Crumb -->
        <div class="bread">
            <div class="container">
                <div class="row">
                    <h1>{{ $album[0]->{'title_'.$lang} }}</h1>
                    <p>{!! $album[0]->{'text_'.$lang} !!}
                    </p>
                </div>
                <div class="sub-header container">
                    <div class="row">

                        <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a title="الرئيسية" itemprop="item" href="{{url($lang.'/index')}}"><span itemprop="name">Home Page</span></a>
                                <meta itemprop="position" content="1" />
                            </li>
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a title="Home" itemprop="item" href="{{url($lang.'/albums')}}"><span itemprop="name"> {{ $pagesinfo[0]->{'pagetitle_'.$lang} }} </span></a>
                                <meta itemprop="position" content="1" />
                            </li>
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="active"><span itemprop="name"> {{ $album[0]->{'title_'.$lang} }} </span>
                                <meta itemprop="position" content="2" />
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>



    
        <section class="ship-home">
            <div class="container">
                <div class="row">


                    <link rel="stylesheet" href="{{url('assets/vlb_files1/visuallightbox.css')}}" type="text/css" />
                    <link rel="stylesheet" href="{{url('assets/vlb_files1/vlightbox1.css')}}" type="text/css" media="screen" />
                    <script src="{{url('assets/vlb_engine/visuallightbox.js')}}" type="text/javascript"></script>
                    <script src="{{url('assets/vlb_engine/vlbdata1.js')}}" type="text/javascript"></script>

                    <div id="vlightbox1">
                        @for($i=0;$i<count($images);$i++)
                        <div class="col-md-4 col-sm-6">
                            <a class="vlightbox1" href="{{url($images[$i]->pic)}}" title="{{$images[$i]->{'picalt_'.$lang} }}">
                                <img src="{{url($images[$i]->pic)}}" alt="{{$images[$i]->{'picalt_'.$lang} }}" />
                            </a>
                        </div>
                        @endfor
                    </div>


                </div>
            </div>
        </section>
        

@endsection