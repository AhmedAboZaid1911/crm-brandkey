@extends('layouts.app')

@section('title',  $pagesinfo[1]->{'pagetitle_'.$lang} )

@section('description', $pagesinfo[1]->{'pagemetadesc_'.$lang} )

@section('keyword',  $pagesinfo[1]->{'pagekeywords_'.$lang} )

@section('content')
 

                  

        <div class="bread">
            
            <div class="container">
                <div class="row">
                    <h1> {{ $pagesinfo[1]->{'pagetitle_'.$lang} }} </h1>
                    <p>  {!! $pagesinfo[1]->{'text_'.$lang} !!} </p>
                </div>
                <div class="sub-header container">
                    <div class="row">            
                        <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a title="Home" itemprop="item" href="{{url($lang.'/index')}}"><span itemprop="name">Home</span></a>
                                <meta itemprop="position" content="1" />
                            </li>
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="active"><span itemprop="name"> {{ $pagesinfo[1]->{'pagetitle_'.$lang} }} </span>
                                <meta itemprop="position" content="2" />
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            
            <div class="row">
                @for($i=0;$i<count($video);$i++)
                
                <div class="wow zoomInUp hvr-float-shadow  col-lg-3 col-md-3 col-sm-12 col-xs-12 service">
                    <div class="thumbnail">
                        <div class="embed-responsive embed-responsive-16by9">
                         <a href="{{url($lang.'/'. $pagesinfo[1]->{'pagename_'.$lang}.'/'.$video[$i]->{'url_'.$lang})}}"><img src="{{url($video[$i]->pic)}}" class="img-responsive transition" alt="{{ $video[$i]->{'title_'.$lang} }}" title="{{ $video[$i]->{'title_'.$lang} }}"></a>
                        </div>
                        <div class="caption">
                            <h2><a href="{{url($lang.'/'. $pagesinfo[1]->{'pagename_'.$lang}.'/'.$video[$i]->{'url_'.$lang})}}">{{ $video[$i]->{'title_'.$lang} }}</a></h2>
                        </div>
                    </div>
                </div>
                @endfor
            </div>
        </div>

    </div>


@endsection