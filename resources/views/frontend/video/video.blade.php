@extends('layouts.app')

@section('title', $video[0]->{'title_'.$lang})

@section('description', $video[0]->{'desc_'.$lang})

@section('keyword', $video[0]->{'keyword_'.$lang})



@section('content')

 
<!-- Bread Crumb -->
        <div class="bread">
            <div class="container">
                <div class="row">
                    <h1>{{ $video[0]->{'title_'.$lang} }}</h1>
                    <p>{!! $video[0]->{'text_'.$lang} !!}
                    </p>
                </div>
                <div class="sub-header container">
                    <div class="row">

                        <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a title="الرئيسية" itemprop="item" href="{{url($lang.'/index')}}"><span itemprop="name">Home Page</span></a>
                                <meta itemprop="position" content="1" />
                            </li>
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a title="Home" itemprop="item" href="{{url($lang.'/'. $pagesinfo[1]->{'pagename_'.$lang}  )}}"><span itemprop="name"> {{ $pagesinfo[1]->{'pagetitle_'.$lang} }} </span></a>
                                <meta itemprop="position" content="1" />
                            </li>
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="active"><span itemprop="name"> {{ $video[0]->{'title_'.$lang} }} </span>
                                <meta itemprop="position" content="2" />
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>



    
        <section class="ship-home">
            <div class="container">
                <div class="row">


                    <div class="wow zoomInUp hvr-float-shadow  col-lg-6 col-md-6 col-sm-12 col-xs-12 service">

                        <div class="thumbnail">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe width="100%" height="100%" src="https://www.youtube.com/embed/{{$video[0]->video}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </section>
        

@endsection