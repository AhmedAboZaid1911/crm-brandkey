@extends('layouts.app')

@section('content')
<!-- Start Page Title Aria -->
  <div class="page-title">
    <div class="container">
      <div class="row">
        <div class="breadcrumb-block">
          <ol class="breadcrumb-pages">
            <li><a href="{{url('')}}"> الرئيسية </a><i class="fa fa-angle-left"></i></li>
            <li>{{$cat[0]->title_ar}}</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <!-- End Page Title Aria -->

    <!-- Start Featured Search -->
    <section id="section-content">
        <div class="container inner-f">
      <div class="row">
        
        <div class="col-md-12 col-sm-12 col-xs-12 pull-right text-right">
          <div class="page-container">
            <div class="title-container">
                            <h2>{{$cat[0]->title_ar}}</h2>
                        </div>
            <div class="block-results">
              <div class="row">
                  
          @foreach($posts as $post)
                <div class="col-md-4 col-sm-6 col-xs-12">
                  <div class="block-product">
                     <div class="img-product text-right">
                      <img src="{{url($post->pic)}}" alt="{{$post->title_ar}}">
                      
                    </div>
                    <div class="info-block">
                      <h2 class="title-block"><a href="{{url('')}}/{{$cat[0]->url_ar}}/{{$post->url_ar}}">{{$post->title_ar}}</a></h2>
                      <ul>
                        <li><i class="fa fa-info"></i> السعر : {{$post->price}}ريال </li>
                        <li><i class="fa fa-eye"></i> {{$post->views}} </li>
                      </ul>
                    </div>
                  </div>
                </div><!-- col-md-3 col-sm-6 col-xs-12 -->
                @endforeach
                {{ $posts->links() }} 
              </div>
            </div>
          </div>
        </div>
      </div>
        </div>
    </section>
    <!-- End Featured Search -->
    
@endsection