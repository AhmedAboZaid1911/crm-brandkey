@extends('layouts.app')

@section('title', $settings['title_'.$lang])

@section('description', $settings['desc_'.$lang])

@section('keyword', $settings['keyword_'.$lang])

@section('content')



        <header role="banner" itemscope itemtype="http://schema.org/WPHeader">
            <ul class="bxslider">
            @for($i=0;$i<count($slider);$i++)
                <li style="background-image: linear-gradient(rgba(40, 40, 40, 0), rgba(61, 61, 62, 0)), url({{url($slider[$i]->pic)}})">
                </li>
            @endfor    
            </ul>
        </header>


        <div class="container ">
            <div class="row ">
                <div class="search col-md-8 col-md-offset-1 col-xs-12">



                    <div class="search-text">Where do you want to go?</div>
                    <div class="panel-body">
                        <div class="row">
                            <form action="{{url($lang.'/search')}}"  method="get" class="form-group">
                                <div class="col-md-4 col-sm-3">
                                    <div class="single-query form-group">
                                        <div class="intro">
                                            <label>Where would you like to go?</label>

                                            <select class="form-control" name="place">                                            
                                                <option value="all">
                                                All Destinations
                                                </option>
                                                
                                                @foreach ($destinations as $destination )
                                                <option value="{{ $destination->id }}">
                                                {{ $destination->{'title_'.$lang} }}
                                                </option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <div class="single-query form-group">
                                        <div class="intro">
                                            <label>Duration By Day</label>
                                            <input type="number" placeholder="10 , 15 ,,,, Ect," class="form-control" name="day" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <div class="single-query form-group">
                                        <div class="intro">
                                            <label>Price Range By USD </label>
                                            <input type="number" placeholder="100  , 250 ,,,, Ect," class="form-control" name="price" >
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-2 col-sm-3">

                                    <button style="margin:27px 0; " type="submit " class="btn btn-embossed btn-danger">Search</button>
                                </div>
                                
                            </form>

                        </div>
                    </div>

                </div>
            </div>



        </div>

        <section class="block-about">
            <div class="container ">
                <div class="row ">

                    <div class="col-md-5 col-xs-12 right-side">

                        <div class="thumbnail">
                            <div class="embed-responsive embed-responsive-16by9">

                                <iframe width="560" height="315" src="https://www.youtube.com/embed/{{$settings['video']}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                            </div>

                        </div>

                    </div>
                       <div class="col-md-3 col-xs-12 right-side">
                        <a href="{{url($lang.'/'.$pagesinfo[2]->{'pagename_'.$lang})}}" target="blank"> <img class="img-responsive" src="{{url('images/customize-your-tour.jpeg')}}"></a>



                    </div>
                    
                    <div class="col-md-4 col-xs-12 right-side">
                      
                      <a href="{{$tripadvisor[0]->socialurl}}" target="blank">  <img class="img-responsive" src="{{url('images/visitor.png')}}">
                      </a>

                    </div>

                 



                </div>
            </div>

        </section>




        <section class="course-home">
            <div class="container">
                <h2><a href="#">Popular Destinations</a></h2>

            </div>
            <div class="ship-home container">
                <div class="row">
                    @for($i=0;$i<count($related);$i++) 
                        
                        <div class="hvr-float-shadow  col-lg-3 col-md-3 col-sm-12 col-xs-12 service" style="visibility: visible; animation-name: fadeInRight;">

                            <div class="thumbnail">
                                <a href="{{url($lang .'/' . $related[$i]->maincattours .'/' . $related[$i]->tours .'/'. $related[$i]->url_en)}}"> 
                                <img data-src="{{url($related[$i]->pic)}}" alt="{{ $related[$i]->{'title_'.$lang} }}" class="img-responsive" src="{{url($related[$i]->pic)}}" data-holder-rendered="true"></a>

                                <div class="caption">
                                    <h3><a href="{{url($lang .'/' . $related[$i]->maincattours .'/' . $related[$i]->tours .'/'. $related[$i]->url_en)}}">{{ $related[$i]->{'title_'.$lang} }}</a></h3>
                                    <p> {{str_limit(strip_tags($related[$i]->{'text_' . $lang}),'150','...')}}
                                    <span><a href="{{url($lang .'/' . $related[$i]->maincattours .'/' . $related[$i]->tours .'/'. $related[$i]->url_en)}}">Read More</a></span>
                                    </p>
                                    <strong class="hvr-sweep-to-bottom button-more ">
                                    <a href="{{url($lang .'/' . $related[$i]->maincattours .'/' . $related[$i]->tours .'/'. $related[$i]->url_en)}}">Book Now </a>
                                    </strong>

                                </div>
                                <div class="btn-add ">
                                    <a href="{{url($lang .'/' . $related[$i]->maincattours .'/' . $related[$i]->tours .'/'. $related[$i]->url_en)}}"> From  {{ $related[$i]->{'price_'.$lang} }} $</a>
                                </div>
                            </div>
                        </div>
            
                    @endfor

                </div>
            </div>
        </section>




        <section class="home-about ">
            <div class="container ">
                <div class="row ">


                    <div class=" col-md-12">

                        <div class="col-md-6">
                            <div class="row">
                                <img class="img-responsive" src="{{url($about[0]->{'pic' }) }}">
                            </div>

                        </div>
                        <p itemprop="description ">
                        {{str_limit(strip_tags($about[0]->{'text_'.$lang}),'500','...')}}
                        
                        </p>

                        <strong class="hvr-sweep-to-top button-more "><a href="{{url($lang.'/About-Us')}}">Read More</a></strong>

                    </div>




                </div>
            </div>

        </section>

      <!--  <section class="block-about hidden-xs">
            <div class="container ">
                <div class="row ">

                    <div class="wow bounceInRight col-md-5 col-xs-12 right-side">

                        <div class="thumbnail">
                            <div class="embed-responsive embed-responsive-16by9">

                                <iframe width="560" height="315" src="https://www.youtube.com/embed/Njs7K7yGdVs" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                            </div>

                        </div>

                    </div>
                    <div class="wow bounceInRight col-md-4 col-xs-12 right-side">
                        <img class="img-responsive" src="images/visitor.png">

                    </div>

                    <div class="wow bounceInRight col-md-3 col-xs-12 right-side">
                        <a href="customize-your-tour.html"> <img class="img-responsive" src="images/customize-your-tour.png"></a>



                    </div>



                </div>
            </div>

        </section>-->





        <section id="counter" class="counter">
            <div class="main_counter_area">
                <div class="overlay p-y-3">
                    <div class="container">
                        <div class="row">
                            <div class="main_counter_content text-center white-text wow fadeInUp">
                                <div class="col-md-3">
                                    <div class="single_counter p-y-2 m-t-1">
                                        <i class="fas fa-users"></i>
                                        <h2 class="statistic-counter">{{$settings['customers']}}</h2>
                                        <p>CUSTOMERS</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="single_counter p-y-2 m-t-1">
                                        <i class="fas fa-tree"></i>
                                        <h2 class="statistic-counter">{{$settings['destinations']}}</h2>
                                        <p>DESTINATIONS</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="single_counter p-y-2 m-t-1">
                                        <i class="fas fa-plane"></i>
                                        <h2 class="statistic-counter">{{$settings['tours']}}</h2>
                                        <p>TOURS</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="single_counter p-y-2 m-t-1">
                                        <i class="fas fa-globe"></i>
                                        <h2 class="statistic-counter">{{$settings['tour_types']}}</h2>
                                        <p>TOUR TYPES
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End of counter Section -->


        <!-- Add this script before </body> -->


        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.min.js"></script>


        <script>
            jQuery('.statistic-counter').counterUp({
                delay: 10,
                time: 2000
            });

        </script>






        <!-- shipping section -->
        <section class="client-home ">
            <div class="container ">
                <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
                <div class="row ">
                    <div class="col-md-12 col-xs-12">
                        <div class="container">
                            <h2>{{ $clientname[0]->{'title_'.$lang} }}</h2>
                            <section class="customer-logos slider">
                            @for($i=0;$i< count($client);$i++)
                                <div class="slide"><img src="{{url($client[$i]->pic)}}" alt="{{$client[$i]->{'picalt_' . $lang} }}"></div>
                            @endfor
                            </section>
                        </div>
                    </div>
                </div>
                <script>
                    $(document).ready(function() {
                        $('.customer-logos').slick({
                            slidesToShow: 6,
                            slidesToScroll: 1,
                            autoplay: true,
                            autoplaySpeed: 1500,
                            arrows: false,
                            dots: false,
                            pauseOnHover: false,
                            responsive: [{
                                breakpoint: 768,
                                settings: {
                                    slidesToShow: 4
                                }
                            }, {
                                breakpoint: 520,
                                settings: {
                                    slidesToShow: 1
                                }
                            }]
                        });
                    });

                </script>
            </div>
        </section>
@endsection
