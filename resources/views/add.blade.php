@extends('layouts.app')

@section('content')

  <!-- Start Page Title Aria -->
  <div class="page-title">
    <div class="container">
      <div class="row">
        <div class="breadcrumb-block">
          <ol class="breadcrumb-pages">
            <li><a href="{{url('')}}"> الرئيسية </a><i class="fa fa-angle-left"></i></li>
            <li> أضافة إعلان </li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <!-- End Page Title Aria -->

    <!-- Start Featured Search -->
    <section id="section-content">
        <div class="container inner-f">
            <div class="row">
                <div class="page-content">
          <div class="reg-content">
            <form action="">
              <div class="add-adv">
                            <div class="col-md-6 col-sm-12 col-xs-12 pull-right">
                            
                                <div class="form-input">
                                    <label>عنوان الإعلان</label>
                                    <div class="form-block">
                                        <input type="text" name="title_ar" placeholder="عنوان الإعلان" required="">
                                        <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('title_ar') }}</div>
                                    </div>
                                </div>
                                
                                <div class="form-input">
                                    <label> القسم </label>
                                    <div class="form-block">
                      <label>
                                            <select name="cid" class="form-control input-medium">
                                            <option value="">اختر</option>
                                            @foreach($cats as $cat)
                                                <option value="{{$cat->id}}">{{$cat->title_ar}}</option>
                                            @endforeach
                                            </select>
                            <div dir="rtl" style="color:red;font-size: 15px">{{ $errors->first('cid') }}</div>
                      </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12 pull-right">
                  <div class="form-input">
                                    <label> قسم الاعلان </label>
                                    <div class="form-block">
                      <label>
                                        <select class="form-control">
                                          <option value=" نوع الاعلان " selected=""> نوع الاعلان </option>
                                          <option value="volvo">Volvo</option>
                                          <option value="saab">Saab</option>
                                          <option value="mercedes">Mercedes</option>
                                          <option value="audi">Audi</option>
                                        </select>
                      </label>
                                    </div>
                                </div>
                                <div class="form-input">
                                    <label> السعر </label>
                                    <div class="form-block">
                                        <input type="text" name="price" placeholder="السعر" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 pull-right">
                <div class="form-group">
<label class="control-label col-md-3">الصورة</label>
<div class="col-md-9">
    
    <div class="fileinput fileinput-new" data-provides="fileinput" style="margin-bottom: 15px">
        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" id="prev_img" /> 
        </div>
        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> 
        </div>
        <div>
            <span class="btn default btn-file">
            <span class="fileinput-new"> <a data-toggle="modal" data-target="#thepic" class="btn" type="button">Select image </a></span>
            <spanclass="fileinput-exists"> Change </span>
             </span>
            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
        </div>
        <br>
        <input id="pic" type="hidden" value="" name="pic">
    </div>

</div>
</div>
                                                            
      

<div class="modal fade" id="thepic">
<div class="modal-dialog" >
  <div class="modal-content" style="width: 900px;height: 500px">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4 class="modal-title">اختر صورة</h4>
    </div>
    <div class="modal-body" style="width: 900px;height: 450px">
      <iframe width="100%" height="100%" src="{{url('/filemanager/dialog.php?type=2&field_id=pic&fldr=')}}" frameborder="0"></iframe>
    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 pull-right">
                                <div class="form-input">
                                    <label>وصف الاعلان</label>
                                    <textarea class="txt-box textArea" name="message" cols="40" rows="7" id="messageTxt" placeholder="وصف الاعلان" spellcheck="true" required=""></textarea>
                                </div>
                  <button type="submit" class="btn btn-bg main-bg" value=""> أضف  </button>
                            </div>
                        </div>

            </form>
          </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Featured Search -->

@endsection

