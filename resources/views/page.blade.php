@extends('layouts.app')

@section('content')
 <!-- Start Page Title Aria -->
  <div class="page-title">
    <div class="container">
      <div class="row">
        <div class="breadcrumb-block">
          <ol class="breadcrumb-pages">
            <li><a href="{{url('')}}"> الرئيسية </a><i class="fa fa-angle-left"></i></li>
            <li>{{$page[0]->title_ar}}</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <!-- End Page Title Aria -->

    <!-- Start Featured Search -->
    <section id="section-content">
        <div class="container inner-f">
            <div class="row">
                <div class="page-content">
                    <div class="page-content-st text-right">
{{$page[0]->text_ar}}
                    </div>
                  
                </div>
            </div>
        </div>
    </section>
    <!-- End Featured Search -->
    
@endsection