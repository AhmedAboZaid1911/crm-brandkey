
<div class="page-sidebar-wrapper">
        <!-- BEGIN SIDEBAR -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <div class="page-sidebar navbar-collapse collapse">
            <!-- BEGIN SIDEBAR MENU -->
            <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
            <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
            <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
            <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
            <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
            <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
            <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <li class="sidebar-toggler-wrapper hide">
                    <div class="sidebar-toggler">
                        <span></span>
                    </div>
                </li>
                <!-- END SIDEBAR TOGGLER BUTTON -->
                <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->


                <li class="nav-item start @if($active == 'home') ? active @endif">
                    <a href="{{ url('/admin/index') }}" class="nav-link nav-toggle">
                        <i class="icon-home"></i>
                        <span class="title">الرئيسية</span>
                    </a>
                </li>
                 <li class="nav-item @if($active == 'settings') ? active @endif">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="icon-diamond"></i>
                        <span class="title">الاعدادات</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('46'))
                        <li class="nav-item start @if($active == 'settings') ? active @endif">
                            <a href="{{ url('/admin/editsettings') }}" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title">اعدادات </span>
                            </a>
                        </li>
                        @endif
                        @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('48'))
                        <li class="nav-item start @if($active == 'settings') ? active @endif">
                            <a href="{{ url('/admin/nationality/index') }}" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title"> الجنسيات </span>
                            </a>
                        </li>
                        @endif
                        @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('52'))
                        <li class="nav-item start @if($active == 'settings') ? active @endif">
                            <a href="{{ url('/admin/leads/index') }}" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title"> مصادر العميل </span>
                            </a>
                        </li>
                        @endif
                        @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('52'))
                        <li class="nav-item start @if($active == 'settings') ? active @endif">
                            <a href="{{ url('/admin/Needs/index') }}" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title"> رغبات  العميل </span>
                            </a>
                        </li>
                        @endif
                        @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('57'))
                        <li class="nav-item start @if($active == 'settings') ? active @endif">
                            <a href="{{ url('/admin/interest/index') }}" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title"> الاهتمام  </span>
                            </a>
                        </li>
                        @endif
                        @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('62'))
                        <li class="nav-item start @if($active == 'settings') ? active @endif">
                            <a href="{{ url('/admin/gender/index') }}" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title">  النوع </span>
                            </a>
                        </li>
                        @endif
                        @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('67'))
                        <li class="nav-item start @if($active == 'settings') ? active @endif">
                            <a href="{{ url('/admin/members/index') }}" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title"> المستخدمين </span>
                            </a>
                        </li>
                        @endif
                        @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('67'))
                        <li class="nav-item start @if($active == 'settings') ? active @endif">
                            <a href="{{ url('/admin/wants/index') }}" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title">احتياجات  العميل </span>
                            </a>
                        </li>
                        @endif
                        @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('67'))
                        <li class="nav-item start @if($active == 'settings') ? active @endif">
                            <a href="{{ url('/admin/tags/index') }}" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title"> Tags </span>
                            </a>
                        </li>
                        @endif
                        @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('67'))
                        <li class="nav-item start @if($active == 'settings') ? active @endif">
                            <a href="{{ url('/admin/not_interested/index') }}" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title"> اسباب عدم الاهتمام </span>
                            </a>
                        </li>
                        @endif
                        @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('67'))
                        <li class="nav-item start @if($active == 'settings') ? active @endif">
                            <a href="{{ url('/admin/call_status/index') }}" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title"> حالة العميل وقت الإتصال </span>
                            </a>
                        </li>
                        @endif
                        @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('67'))
                        <li class="nav-item start @if($active == 'settings') ? active @endif">
                            <a href="{{ url('/admin/interested_type/index') }}" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title"> نوع العميل المهتم    </span>
                            </a>
                        </li>
                        @endif
                        @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('67'))
                        <li class="nav-item start @if($active == 'settings') ? active @endif">
                            <a href="{{ url('/admin/service_type/index') }}" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title"> نوع الخدمة  </span>
                            </a>
                        </li>
                        @endif
                    </ul>
                </li>
                <li class="nav-item @if($active == 'Services') ? active @endif">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="icon-diamond"></i>
                        <span class="title">الخدمات</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">

                        @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('11'))
                        <li class="nav-item @if($active == 'Services') ? active @endif">
                            <a href="{{ url('/admin/Services/index') }}" class="nav-link ">
                                <span class="title"> الخدمات </span>
                            </a>
                        </li>
                        @endif
                        @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('16'))
                        <li class="nav-item @if($active == 'Services') ? active @endif">
                            <a href="{{ url('/admin/Services_name/index') }}" class="nav-link ">
                                <span class="title"> اسماء الخدمات </span>
                            </a>
                        </li>
                        @endif
                        @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('21'))
                        <li class="nav-item @if($active == 'Services') ? active @endif">
                            <a href="{{ url('/admin/Services_time/index') }}" class="nav-link ">
                                <span class="title"> مواعيد الخدمات </span>
                            </a>
                        </li>
                        @endif
                        {{-- @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('26'))
                        <li class="nav-item @if($active == 'Services') ? active @endif">
                            <a href="{{ url('/admin/Services_period/index') }}" class="nav-link ">
                                <span class="title"> فترات الخدمات </span>
                            </a>
                        </li>
                        @endif --}}
                        @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('31'))
                        <li class="nav-item @if($active == 'Services') ? active @endif">
                            <a href="{{ url('/admin/Services_place/index') }}" class="nav-link ">
                                <span class="title">  الاماكن </span>
                            </a>
                        </li>
                        @endif
                        @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('36'))
                        <li class="nav-item @if($active == 'Services') ? active @endif">
                            <a href="{{ url('/admin/Services_days/index') }}" class="nav-link ">
                                <span class="title">  الايام </span>
                            </a>
                        </li>
                        @endif
                        @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('41'))
                        <li class="nav-item @if($active == 'Services') ? active @endif">
                            <a href="{{ url('/admin/Services_currency/index') }}" class="nav-link ">
                                <span class="title">  العملات </span>
                            </a>
                        </li>
                        @endif
                    </ul>
                </li>
                <li class="nav-item @if($active == 'Clients') ? active @endif">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="icon-users"></i>
                        <span class="title">العملاء</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('1'))
                        <li class="nav-item @if($active == 'Clients') ? active @endif">
                            <a href="{{ url('/admin/Clients/index') }}" class="nav-link ">
                                <span class="title"> العملاء </span>
                            </a>
                        </li>
                        @endif
                        @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('2'))
                        <li class="nav-item @if($active == 'Clients') ? active @endif">
                            <a href="{{url('/admin/Clients/Clients_add')}}" class="nav-link ">
                                <span class="title">اضافة عميل</span>
                            </a>
                        </li>
                        @endif
                    </ul>
                </li>
                @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('6'))
                <li class="nav-item start @if($active == 'Tasks') ? active @endif">
                    <a href="{{ url('/admin/tasks/today') }}" class="nav-link nav-toggle">
                        <i class="icon-home"></i>
                        <span class="title"> المهام </span>
                    </a>
                </li>
                @endif

               <li class="nav-item @if($active == 'Reports') ? active @endif">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="icon-home"></i>
                        <span class="title">التقارير</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('72'))
                        <li class="nav-item @if($active == 'Reports') ? active @endif">
                            <a href="{{ url('/admin/reports') }}" class="nav-link ">
                                <i class="icon-home"></i>
                                <span class="title"> التقارير </span>
                            </a>
                        </li>
                        @endif
                        @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('74'))
                        <li class="nav-item @if($active == 'Reports') ? active @endif">
                            <a href="{{url('admin/reports/client')}}" class="nav-link ">
                                <i class="icon-users"></i>
                                <span class="title"> العملاء </span>
                            </a>
                        </li>
                        @endif
                        @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('74'))
                        <li class="nav-item @if($active == 'Reports') ? active @endif">
                            <a href="{{url('admin/reports/clientneeds')}}" class="nav-link ">
                                <i class="icon-users"></i>
                                <span class="title"> احتياج العميل  </span>
                            </a>
                        </li>
                        @endif
                        @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('76'))
                        <li class="nav-item @if($active == 'Reports') ? active @endif">
                            <a href="{{url('admin/reports/users')}}" class="nav-link ">
                                <i class="icon-users"></i>
                                <span class="title"> المستخدمين </span>
                            </a>
                        </li>
                        @endif
                        @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('78'))
                        <li class="nav-item @if($active == 'Reports') ? active @endif">
                            <a href="{{url('admin/reports/task')}}" class="nav-link ">
                                <i class="icon-users"></i>
                                <span class="title"> المهام </span>
                            </a>
                        </li>
                        @endif
                        @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('80'))
                        <li class="nav-item @if($active == 'Reports') ? active @endif">
                            <a href="{{url('admin/reports/service')}}" class="nav-link ">
                                <i class="icon-users"></i>
                                <span class="title"> الخدمات </span>
                            </a>
                        </li>
                        @endif  
                    </ul>
                </li>

                {{--  </li>
                @if(\App\Http\Controllers\admin\RolesController::checkroles_menu('72'))
                <li class="nav-item start @if($active == 'Reports') ? active @endif">
                    <a href="{{ url('/admin/reports') }}" class="nav-link nav-toggle">
                        <i class="icon-home"></i>
                        <span class="title"> التقارير </span>
                    </a>
                </li>
                @endif  --}}

                </ul>

            <!-- END SIDEBAR MENU -->
            <!-- END SIDEBAR MENU -->
        </div>
        <!-- END SIDEBAR -->
    </div>
    <!-- END SIDEBAR -->
