<html lang="{{$lang}}">
<!DOCTYPE html>
<html>

<head>
    <title>@yield('title')</title>
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keyword')">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">


    <link rel="stylesheet" href="{{url('assets/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/animate.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/jquery.bxslider.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/bootsnav.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/trak.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/hover.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/star-rating.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/responsive.css')}}">
    <link rel="shortcut icon" href="{{url('assets/images/favicon.ico')}}">
    <script src="{{url('assets/js/jquery-3.1.0.min.js')}}"></script>
    <script src="{{url('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{url('assets/js/bootsnav.js')}}"></script>
    <script src="{{url('assets/js/scripts.js')}}"></script>
    <script src="{{url('assets/js/wow.js')}}"></script>
    <script src="{{url('assets/js/jquery.bxslider.min.js')}}"></script>
        

    <script>
        wow = new WOW({
            animateClass: 'animated',
            offset: 100,
            callback: function(box) {
                console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
            }
        });
        wow.init();
        document.getElementById('moar').onclick = function() {
            var section = document.createElement('section');
            section.className = 'section--purple wow fadeInDown';
            this.parentNode.insertBefore(section, this);
        };

    </script>
</head>


<body itemscope itemtype="http://schema.org/WebPage " itemprop="name ">

    <div class="wrapper ">
        <!-- Navigation -->
        <div class="navigation ">
            <!-- Top Header -->
            <div class="top-header ">
                <div class="container ">
                    <div class="row ">
                        <div class="col-md-4  logo">
                            <div class="container ">
                                <div class="row ">
                                    <a class="navbar-brand" href="{{url($lang.'/index/')}}"><img src="{{url($settings['logo'])}}" alt=""></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 ">
                            <ul>

                                <li><a href="tel:{{$settings['phone']}}"> <i class="fas fa-phone-volume" style="color: #e91e63;"></i>{{$settings['phone']}}</a></li>
                                         

                                
                                
                                <li><a href="https://wa.me/{{$settings['phone']}}"><i class="fa fa-whatsapp" target="_blank" style="color: #4AC659;"></i>{{$settings['phone']}}</a></li>
                            </ul>

                        </div>


                    </div>
                </div>
            </div>



            <div class="menu">
                <div class="container ">
                    <div class="row ">
                        <nav class="navbar navbar-default brand-center bootsnav" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">

                            <div class="container ">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>
                                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                    <ul class="nav navbar-nav " data-in="fadeInDown " data-out="fadeOutUp ">
                                        <li><a class="visited " href="{{url($lang.'/index')}}" title="Home Page " itemprop="url "><span itemprop="name ">Home Page</span><span class="sr-only ">(current)</span></a></li>




                                        @for ( $i=0 ; $i<count($trips) ; $i++)
                                        <li class="dropdown ">
                                            <a href="{{url($lang .'/' . $trips[$i]->{'url_'.$lang})}} " class="dropdown-toggle " data-toggle="dropdown "> {{ $trips[$i]->{'title_'.$lang} }} </a>
                                            <ul class="dropdown-menu ">
                                                @foreach($trips[$i]->tours as $tours)
                                                    <li><a href="{{url($lang .'/' . $trips[$i]->url_en .'/'. $tours->url_en)}}" title="{{ $tours->{'title_en'} }} " itemprop="url ">
                                                    <span itemprop="name ">{{ $tours->{'title_'.$lang} }}</span></a></li>
                                                @endforeach

                                            </ul>
                                        </li>
                                       @endfor
                                            

                                        <li><a href="{{url($lang.'/'.$pagesinfo[3]->{'pagename_'.$lang})}}" title="{{ $pagesinfo[3]->{'pagetitle_'.$lang} }}" itemprop="url "><span itemprop="name ">{{ $pagesinfo[3]->{'pagetitle_'.$lang} }}</span></a></li>

                                    </ul>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>


        </div>



        @yield('content')



<div id="wh-call-to-action" class=" wh-animation-in" wh-click="clickOnCallToAction">
            <a wh-href="href" wh-target="target" href="javascript:void(0)" target="">
                <div class="wh-call-to-action-content" wh-html-unsafe="text">Whatsapp / Viber Chat Now</div>
            </a>
        </div>






        <div class="plus-button" id="round">
            <i class="fas fa-comments"></i>
        </div>
        <div class="social-button viber-button">
            <a href="viber://pa?chatURI=<{{$settings['phone']}}>"> <i class="fab fa-viber"></i> </a>
        </div>
        <div class="social-button twitter-button">
            
            <a href="https://wa.me/{{$settings['phone']}}" target="blank"> <i class="fab fa-whatsapp"></i></a>
        </div>
           
    
        <div class="social-button facebook-button">
            <a href="https://www.facebook.com/messages/t/traveljoyegypttours" target="blank" > <i class="fab fa-facebook-messenger" target="blank"></i> </a>
        </div>
        <div class="social-button pinterest-button">
         <a href="tel:{{$settings['phone']}}" target="blank">  <i class="fas fa-phone-volume"></i></a>

        </div>
        
        

<script>
$( "#round" ).click(function() {
  $( "#wh-call-to-action" ).toggle( "slow" );
});
</script>


        
        
        <script>
            $(document).ready(function() {

                // Social plus button function
                $('.plus-button').click(function() {
                    $(this).toggleClass('open');
                    $('.social-button').toggleClass('active');
                });

            });
            //# sourceURL=pen.js

        </script>

@yield('pagejs')





        <div class="line"></div>



        <footer role="contentinfo " itemscope itemtype="http://schema.org/WPFooter ">
            <meta itemprop="name " content="Webpage footer for Dr. Health " />
            <meta itemprop="description " content="Information about Dr. Health " />
            <meta itemprop="copyrightYear " content="2018 " />
            <meta itemprop="copyrightHolder " content="Dr. Health " />
            <div class="container ">
                <div class="row ">

                    <div class="col-md-12">
                
                        <div class="social ">
                         @foreach ($social as $socials )
                            <a id="{{$socials->socialname}}" itemprop="sameAs " target="_blank" href="{{$socials->socialurl}}"><i class="fa fa-{{$socials->socialname}}"></i></a>
                        @endforeach   
                        
                    </div>
                        
                        <ul class="list-unstyled ">
                            <li><a href="{{url($lang.'/index')}}" title="Home " itemprop="url "><span itemprop="name ">
                            Home Page
                            </span></a></li>
                            
                            <li><a href="{{url($lang.'/About-Us')}}" title=" {{ $about[0]->{'title_'.$lang} }}  " itemprop="url "><span itemprop=" {{ $about[0]->{'title_'.$lang} }}  "> 
                            {{ $about[0]->{'title_'.$lang} }}  
                            </span></a></li>

                            <li><a href="{{url($lang.'/'.$pagesinfo[2]->{'pagename_'.$lang})}}" title="{{ $pagesinfo[2]->{'pagetitle_'.$lang} }} " itemprop="url "><span itemprop="courses ">
                            {{ $pagesinfo[2]->{'pagetitle_'.$lang} }}
                            </span></a></li>
                            
                            <li><a href="{{url($lang.'/'.$pagesinfo[0]->{'pagename_'.$lang})}}" title="{{ $pagesinfo[0]->{'pagetitle_'.$lang} }}" itemprop="url "><span itemprop="Comapny Department"> 
                            {{ $pagesinfo[0]->{'pagetitle_'.$lang} }}
                            </span></a></li>
                            
                            <li><a href="{{url($lang.'/'.$pagesinfo[1]->{'pagename_'.$lang})}}" title="{{ $pagesinfo[1]->{'pagetitle_'.$lang} }} " itemprop="url "><span itemprop="Events"> 
                            {{ $pagesinfo[1]->{'pagetitle_'.$lang} }}
                            </span></a></li>
                            
                            <li><a href="{{url($lang.'/'.$pagesinfo[3]->{'pagename_'.$lang})}}" title="{{ $pagesinfo[3]->{'pagetitle_'.$lang} }} " itemprop="url "><span itemprop="new brands ">     
                            {{ $pagesinfo[3]->{'pagetitle_'.$lang} }}
                            </span></a></li>


                        </ul>

                    </div>

                    <div class="col-md-12">
                        <ul class="list-unstyled ">
                            @for ( $i=0 ; $i<count($pages) ; $i++)
                                <li><a href=" {{url($lang .'/page/' . $pages[$i]->{'url_'.$lang})}} " title="{{ $pages[$i]->{'title_'.$lang} }}" itemprop="url "><span itemprop="about ">
                                {{ $pages[$i]->{'title_'.$lang} }}
                                </span></a></li>
                            @endfor
                            
                        </ul>

                    </div>
                    <div class="col-md-12">
                        <ul class="list-unstyled ">
                            @for ( $i=0 ; $i<count($trips) ; $i++)
                                <li><a href=" {{url($lang .'/' . $trips[$i]->{'url_'.$lang})}} " title="{{ $trips[$i]->{'title_'.$lang} }}" itemprop="url "><span itemprop="about ">
                                {{ $trips[$i]->{'title_'.$lang} }}
                                </span></a></li>
                            @endfor
                            
                        </ul>

                    </div>


                </div>
            </div>
            <div class="footer-btm ">
                <p> <a href="http://brandkey.org" target="_blank ">Brandkey.org <span style="color: #fb6060;font-size: 20px;margin:2px;">♥</span></a> &copy; 2018 All Rights Reserved</p>
            </div>
        </footer>
        <div class="scroll-to-top">
            <span><i class="fa fa-hand-o-up"></i></span>
        </div>
    </div>
</body>

</html>
