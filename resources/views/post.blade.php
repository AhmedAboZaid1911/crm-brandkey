@extends('layouts.app')

@section('content')

  <!-- Start Page Title Aria -->
  <div class="page-title">
    <div class="container">
      <div class="row">
        <div class="breadcrumb-block">
          <ol class="breadcrumb-pages">
            <li><a href="{{url('')}}"> الرئيسية </a><i class="fa fa-angle-left"></i></li>
            <li><a href="{{url($cat[0]->url_ar)}}"> {{$cat[0]->title_ar}} </a><i class="fa fa-angle-left"></i></li>
            <li>{{$post[0]->title_ar}}</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <!-- End Page Title Aria -->

    <!-- Start Featured Search -->
    <section id="section-content">
        <div class="container inner-f">
      <div class="row">
        
        <div class="col-md-12 col-sm-12 col-xs-12 pull-right text-right">
          <div class="page-container">
            <div class="title-container">
                            <h2>{{$post[0]->title_ar}}</h2>
                        </div>
            <div class="block-results">
              <div id="featured-block" class="featured-slider text-center">
                <div class="slider-results">
                  <div class="slider-item">
                    <img src="{{url($post[0]->pic)}}" alt="{{$post[0]->title_ar}}">
                  </div><!-- Slider Item -->
                  <div class="slider-item">
                    <img src="{{url($post[0]->pic1)}}" alt="{{$post[0]->title_ar}}">
                  </div><!-- Slider Item -->
                  <div class="slider-item">
                    <img src="{{url($post[0]->pic2)}}" alt="{{$post[0]->title_ar}}">
                  </div><!-- Slider Item -->
                  <div class="slider-item">
                    <img src="{{url($post[0]->pic3)}}" alt="{{$post[0]->title_ar}}">
                  </div><!-- Slider Item -->
                  <div class="slider-item">
                    <img src="{{url($post[0]->pic4)}}" alt="{{$post[0]->title_ar}}">
                  </div><!-- Slider Item -->
                  <div class="slider-item">
                    <img src="{{url($post[0]->pic5)}}" alt="{{$post[0]->title_ar}}">
                  </div><!-- Slider Item -->
                
                </div>
              </div>
              <div class="info-contents-list">
                                <ul>
                                    <li><i class="fa fa-shopping-basket"></i> {{$post[0]->price}} ريال</li>
                                    <li><i class="fa fa-calendar-check-o"></i> {{date('d-m-Y',$post[0]->time)}}</li>
                                    <li><i class="fa fa-eye"></i> {{$post[0]->views}} </li>
                                </ul>
                            </div>
              <div class="exert-content">
                            <h2> تفاصيل الاعلان: </h2>
                            <p>{{$post[0]->text_ar}}</p>
                        </div>
              <div class="comment-content">
                            <h2> التعليقات </h2>
               
              </div>
              <div class="comment-content">
                <h2> أضف تعليق </h2>
               <form method="post" action="" class="form-horizontal">

                {{ csrf_field() }}  
                <div class="form-textarea">
                    <textarea class="txt-box textArea" name="text" cols="40" rows="13" id="messageTxt" placeholder="  أضف تعليق " spellcheck="true" required=""></textarea>
                  </div>
                  
                <div class="form-textarea">
                    <input type="submit" value="أضف" class="btn btn-circle green" />
                  </div>
                </form>
                        </div>


            </div>
          </div>
          <div class="page-container">
            <div class="title-container">
                            <h2> إعلانات مشابهه </h2>
                        </div>
            <div class="block-results">
              <div class="row">
                  
          @foreach($other_posts as $other_post)
                <div class="col-md-4 col-sm-6 col-xs-12">
                  <div class="block-product">
                     <div class="img-product text-right">
                      <img src="{{url($other_post->pic)}}" alt="{{$other_post->title_ar}}">
                      
                    </div>
                    <div class="info-block">
                      <h2 class="title-block"><a href="{{url('')}}/{{$cat[0]->url_ar}}/{{$other_post->url_ar}}">{{$other_post->title_ar}}</a></h2>
                      <ul>
                        <li><i class="fa fa-info"></i> السعر : {{$other_post->price}}ريال </li>
                        <li><i class="fa fa-eye"></i> {{$other_post->views}} </li>
                      </ul>
                    </div>
                  </div>
                </div><!-- col-md-3 col-sm-6 col-xs-12 -->
                @endforeach
                
              </div>
            </div>
          </div>
        </div>
      </div>
        </div>
    </section>
    <!-- End Featured Search -->
@endsection