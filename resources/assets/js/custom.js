/* ------------------------------------------ */
/*             TABLE OF CONTENTS
/* ------------------------------------------ */

"use strict";

/*-----------------------------------------------------------------------------------*/
/*	Start MENU
/*-----------------------------------------------------------------------------------*/

	$(".flexnav").flexNav({
	  'animationSpeed':     250,            // default for drop down animation speed
	});

	$(".login-btn a").on('click', function(){
		$(".login-form").fadeIn(150);
	  $(".login-form").addClass('active-login');
	});
	$(".login-form .close-search").on('click', function(){
		$(".login-form").fadeOut(300);
	  $(".login-form").removeClass('active-login');
	})

	$(".reg-btn a").on('click', function(){
		$(".reg-form").fadeIn(150);
	  $(".reg-form").addClass('active-reg');
	});
	$(".reg-form .close-search").on('click', function(){
		$(".reg-form").fadeOut(300);
	  $(".reg-form").removeClass('active-reg');
	})

	$(document).ready(function() {
	  $('.image-link').magnificPopup({type:'image'});
	});

/*-----------------------------------------------------------------------------------*/
/*	End MENU
/*-----------------------------------------------------------------------------------*/

$(".lang-btn").on('click', function(){
	$(".lang-block").toggleClass('active-lang');
});

/*-----------------------------------------------------------------------------------*/
/*	Start owl Slider
/*-----------------------------------------------------------------------------------*/

$(document).ready(function() {
	$('#slider-home').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		autoplay: true,
		autoplaySpeed: 4000,
		prevArrow: '<a class="prev-post"><i class="fa fa-angle-left"></i></a>',
		nextArrow: '<a class="next-post"><i class="fa fa-angle-right"></i></a>',
	});

	$('.slider-results').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: false,
		autoplay: true,
		autoplaySpeed: 4000,
		prevArrow: '<a class="prev-post"><i class="fa fa-angle-left"></i></a>',
		nextArrow: '<a class="next-post"><i class="fa fa-angle-right"></i></a>',
	});

});

/*-----------------------------------------------------------------------------------*/
/*	End owl Slider
/*-----------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------*/
/*	Strat LOADING OVERLAY
/*-----------------------------------------------------------------------------------*/

$(window).load(function()
{
	$("#loading").fadeOut(300);
});

$(window).load(function() {
  $('body').css({'overflow':'auto', 'height':'auto', 'position':'relative'});
});


/*-----------------------------------------------------------------------------------*/
/*	End LOADING OVERLAY
/*-----------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------*/
/*	Start Back to top button
/*-----------------------------------------------------------------------------------*/

var winScroll = $(window).scrollTop();
	$('#to-top').click(function(){
		$('html, body').animate({scrollTop: '0px'}, 800);
		return false;
});

$('.fa-hover').wrapInner('<span />');

/*-----------------------------------------------------------------------------------*/
/*	End Back to top button
/*-----------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------*/
/*	Start Parallax Mobile
/*-----------------------------------------------------------------------------------*/

$(document).ready(function () {
    if (navigator.userAgent.match(/Android/i) ||
        navigator.userAgent.match(/webOS/i) ||
        navigator.userAgent.match(/iPhone/i) ||
        navigator.userAgent.match(/iPad/i) ||
        navigator.userAgent.match(/iPod/i) ||
        navigator.userAgent.match(/BlackBerry/i)) {
        $('.parallax').addClass('mobile');
    }
});

/*-----------------------------------------------------------------------------------*/
/*	End Parallax Mobile
/*-----------------------------------------------------------------------------------*/

/* ---------------------------------------------------------------------- */
/*	Start WOW Animation
/* ---------------------------------------------------------------------- */

$(document).ready(function () {
 new WOW().init();
});

/*-----------------------------------------------------------------------------------*/
/*	End WOW Animation
/*-----------------------------------------------------------------------------------*/
