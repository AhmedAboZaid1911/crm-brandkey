public function ratinglikeajax(Request $request)
    {
        $pid = $request->postId;
        
        $type = $request->type;
		if(!isset($_COOKIE['post_' . $pid])){
			setcookie('post_' . $pid, 'like', time() + (86400 * 90), "/"); // 86400 = 1 day
			
			$likesnum = DB::table('posts')->select('likes')->where([['id',$pid],['type',$type]])->get();
			DB::table('posts')->where([['id',$pid],['type',$type]])->update(['likes' => ($likesnum[0]->likes)+1]);
			$total = ($likesnum[0]->likes)+1;
			return $total;
		}
        else{
			return "0";
		}

    }
	
	public function ratingdislikeajax(Request $request)
    {
        $pid = $request->postId;
        
        $type = $request->type;
		if(!isset($_COOKIE['post_' . $pid])){
			setcookie('post_' . $pid, 'unlike', time() + (86400 * 90), "/"); // 86400 = 1 day
			$unlikesnum = DB::table('posts')->select('unlikes')->where([['id',$pid],['type',$type]])->get();
			DB::table('posts')->where([['id',$pid],['type',$type]])->update(['unlikes' => ($unlikesnum[0]->unlikes)+1]);
			$total = ($unlikesnum[0]->unlikes)+1;
			return $total;
		}
        else{
			return "0";
		}

    }
------------------------------get high rated-------------------------------

$allpostsbysubcat = DB::table('posts')->where([['catid',$subcatinfo[0]->id],['type','newspost']])->get();
		foreach($allpostsbysubcat as $k=>$v)
		{
			$p[]= $v;
			$p[$k]->dif = $p[$k]->likes - $p[$k]->unlikes;
		}
		
		
		usort($p, function($first,$second){
			return gmp_cmp($second->dif,$first->dif);
	
		});
		$p = array_slice($p,0,2);